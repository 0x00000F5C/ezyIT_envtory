<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AuditTrails extends Model {
	use Traits\Uuids;

    protected $table = "m_audit_trails";
	protected $fillable = [
		"uid",
		"audit_actor", 
		"audit_trigger", 
		"audit_event", 
		"audit_item", 
		"audit_ip", 
		"audit_timestamp",
		"audit_comment",
		"data_status",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	
	public $incrementing = false;
	
	public function user() {
		return $this->hasOne("App\User", "id", "audit_actor");
	}
	
	public function auditItem() {
		// add more later as we go
		// "route name"			=> "its app"
		$dic = [
			"user"					=> "App\User", 
			"role"					=> "App\Role", 
			"shop"					=> "App\Shop", 
			"po"					=> "App\Po", 
			"supplier"				=> "App\Supplier", 
			"products"				=> "App\Product", 
			"productCategories"		=> "App\ProductCategories", 
			"carrier"				=> "App\Carrier", 
			"companyProfiles"		=> "App\CompanyProfile", 
			"order"					=> "App\Order", 
			"profiles"				=> "App\User", /* Alias */
			"paymentMethod"			=> "App\PaymentMethod",
			"skuProducts"			=> "App\SKUProduct",
			"skuProductPrices"		=> "App\SKUProductPrice",
			"warehouse"				=> "App\Warehouse",
			"warehouseMapping"		=> "App\WarehouseZones",
			"warehouseMappingRacks"	=> "App\WarehouseRacks",
			"warehouseTrays"		=> "App\WarehouseTrays",
			"zones"					=> "App\Zones",
			'processPo' => 'App\Invoice',
			'makePayment' => 'App\MakeOrderPayment',
			'productReceived' => 'App\Stock',
			'readyToStockIn' => 'App\StockIn',
			'stockIn' => 'App\StockIn',
			'brands' => 'App\Brands',
			'sku' => 'App\Sku',
			'shippingBin' => 'App\ShippingBin',
			'picking' => 'App\Picking',
			'pickingTypes' => 'App\PickingType',
			'pickingList' => 'App\PickingList',
			'orderType' => 'App\OrderType',
			'shippingList' => 'App\ShippingList',
			'shippingStatus' => 'App\ShippingStatus',
			'logisticInventory' => 'App\PackingLogistic',
			'stockInLogistic' => 'App\StockInLogistic',
			'packingList' => 'App\PackingList'
		];
		return $this->hasOne($dic[$this->audit_trigger], "id", "audit_item");
	}
}
