<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CarrierDeliveryOption extends Model {
	use Traits\Uuids;

    protected $table = "m_carrier_delivery_options";
	protected $fillable = [
		"carrier_id",
		"DeliveryOption"
	];
	protected $hidden = [];
	public $incrementing = false;
}
