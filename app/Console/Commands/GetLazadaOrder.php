<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Shop;
use App\Order;

class GetLazadaOrder extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:get-order';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $shops = Shop::where('account', '<>', '')->where('api_key', '<>', '')->where('platform_id',1)->get();
      if($shops){
          foreach ($shops as $shop){
            if($shop->platform_id==1){
              $result = Order::syncWithLazada($shop,null);
            }
          }
      }

      dd($result);
    }
}
