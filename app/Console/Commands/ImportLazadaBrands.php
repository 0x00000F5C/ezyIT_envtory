<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use \App\Library;
use \App\Brands;
use App\SyncLog;

class ImportLazadaBrands extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:import-brands {user_id} {offset}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Brands from Lazada and store in local db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sync = SyncLog::create([
            'name' => 'brand',
            'platform' => 'lazada',
            'status' => 'running',
            'user' => $this->argument('user_id')
        ]);

        $offset = $this->argument('offset');
        $limit = 1000;
        $api = Library::lazadaApi('GetBrands',$limit,$offset);
        if ($api) {
            $client = new \GuzzleHttp\Client();
            $res = json_decode($client->get($api)->getBody(), true);
            $resBrands = $res["SuccessResponse"]["Body"]["Brands"];
            if (!empty($resBrands)) {
                $this->saveBrands($resBrands);
            }
        }

        $sync->update(['status' => 'success']);
    }

    protected function saveBrands($resBrands)
    {
        $brands = [];
        $userId = $this->argument('user_id');
        foreach ($resBrands as $brand) {
            if (isset($brand['BrandId']) && isset($brand['Name'])) {
                $data = [
                    'id' => $brand['BrandId'],
                    'name' => $brand['Name'],
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                    'insert_by' => $userId,
                    'update_by' => $userId
                ];
                $data['global_identifier'] = isset($brand['GlobalIdentifier']) ? $brand['GlobalIdentifier'] : NULL ;
                $check = Brands::find($brand['BrandId']);
                if (!$check) {
                  array_push($brands, $data);
                }
            }
        }
        Brands::insert($brands);
        $this->info('Brand Imported');
    }
}
