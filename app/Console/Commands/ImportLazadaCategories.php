<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\ProductCategories;
use App\SyncLog;
use App\Library;

class ImportLazadaCategories extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:import-categories {user_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get Product Categories from Lazada and store in local db';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sync = SyncLog::create([
            'name' => 'categories',
            'platform' => 'lazada',
            'status' => 'running',
            'user' => $this->argument('user_id')
        ]);

        global $categories;
        $categories = [];
        $api = Library::lazadaApi('GetCategoryTree');
        if ($api) {
            $client = new \GuzzleHttp\Client();
            $res = json_decode($client->get($api)->getBody(), true);
            if (isset($res["ErrorResponse"])) {
                $this->comment($res["ErrorResponse"]["Head"]["ErrorMessage"]);
                return 0;
            }
            $resCategories = $res["SuccessResponse"]["Body"];
            $userId = $this->argument('user_id');
            $this->extractCategories($resCategories,0,$userId);
            $success = ProductCategories::insert($categories);
            if ($success) {
                $this->info('Imported');
                $sync->update(['status' => 'success']);
                return 1;
            }else{
                $this->comment('Import Failed');
                $sync->update(['status' => 'failed']);
                return 0;
            }
        }

    }

    protected function extractCategories($resCategories,$parent=0,$userId)
    {
        global $categories;
        foreach ($resCategories as $key => $category) {
            $data = [
                'id' => $category['categoryId'],
                'cat_name' => $category['name'],
                'cat_parent' => $parent,
                'data_status' => 2,
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                'insert_by' => $userId,
                'update_by' => $userId,
            ];
            if ($data) {
                array_push($categories, $data);
            }
            if (!empty($category['children'])) {
                $data = $this->extractCategories($category['children'],$category['categoryId'],$userId);
                if ($data) {
                    array_push($categories, $data);
                }
            }
        }
    }
}
