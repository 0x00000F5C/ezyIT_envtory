<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Library;
use App\Role;
use App\Permission;

class ReloadPermissions extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'permission:reload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reload Permission and Assign it to Superadmin';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $menus = Library::modules();
        $actions = Library::permission();
        $superadmin = Role::where("name", "superadmin")->first();
        $added = 0;
        foreach ($menus as $menu) {
            foreach ($actions as $action) {
               $cekPermission = Permission::where('name',$action.'-'.$menu)->count();
               if (empty($cekPermission)) {
                    Permission::create(['name' => $action.'-'.$menu]);
                    $superadmin->givePermissionTo($action.'-'.$menu);
                    $this->info($action.'-'.$menu.' Added and Assigned to Superadmin Role');
                    $added += 1;
               }
            }
        }
        if ($added == 0) {
            $this->comment('There is no new Permission');
        }
    }
}
