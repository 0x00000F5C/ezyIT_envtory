<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use Spatie\ArrayToXml\ArrayToXml;

use App\Shop;
use App\Sku;
use App\Library;
use Storage;

class UpdateLazadaProduct extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:update-product {sku}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Updated Product with Lazada';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
      $sku = Sku::find($this->argument('sku'));
      $shops = $sku->shops()
              ->whereNotNull('account')
              ->whereNotNull('api_key')
              ->where('platform_id',1)
              ->get();
      foreach ($shops as $key => $shop) {
          $this->updateProduct($sku,$shop);
      }
    }

    protected function updateProduct($sku,$shop)
    {
            $account = $shop->account;
            $api_key = $shop->api_key;

            $api = Library::lazadaShopApi($account, $api_key,'UpdateProduct');

            $attributes = [];
            foreach ($sku->product->attributes()->get() as $key => $value) {
                $attributes[$value->name] = $value->value;
            }

            $skuAttributes = [];
            foreach ($sku->attributes()->get() as $key => $value) {
                if ($value->name !== 'Image') {
                    $skuAttributes[$value->name] = $value->value;
                }
            }

            $images = [];
            $lazadaImages = $sku->images('lazada')->get();
            for ($i=0; $i < 8; $i++) {
              $img = isset($lazadaImages[$i]) ? $lazadaImages[$i]->filename : '';
              array_push($images,$img);
            }

            $skuAttributes['Images'] = [
              'image' => $images
            ];

            $array = [
                'Product' => [
                    'PrimaryCategory' => $sku->product->m_product_categories_id,
                    'Attributes' => $attributes,
                    'Skus' => [
                        'Sku' => $skuAttributes
                    ]
                ]
            ];

            $xml = ArrayToXml::convert($array, 'Request', false);

            if ($api && $xml) {

                $client = new \GuzzleHttp\Client();

                try {
                    $request = $client->post($api, ['body' => $xml]);
                    $response = json_decode($request->getBody(), true);
                    if (isset($response['ErrorResponse'])) {
                      $this->error($response['ErrorResponse']['Head']['ErrorMessage']);
                    }
                } catch (HttpException $ex) {
                  $this->comment($ex);
                }

            }

    }
}
