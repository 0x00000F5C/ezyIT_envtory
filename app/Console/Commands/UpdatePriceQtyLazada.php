<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Product;
use App\Sku;
use App\SkuPrice;

class UpdatePriceQtyLazada extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'lazada:update-price-qty {product_id}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Lazada Product Price and Qty';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $product = Product::find($this->argument('product_id'));

        $shops = $product->sku->shops()->get();

        $data = [
          'SellerSku' => $product->sku->sku,
          'Quantity' => $product->stock_active
        ];

        $response = [];
        foreach ($shops as $key => $shop) {
          $price = SkuPrice::where('sku_id', $product->sku->id)->where('shop_id', $shop->id)->first();

          if($price){
            $data['Price'] = $price->price;

            $data['SalePrice'] = !empty($price->special_price) ? $price->special_price : null;
            $data['SaleStartDate'] = !empty($price->special_from_date) ? $price->special_from_date : null;
            $data['SaleEndDate'] = !empty($price->special_to_date) ? $price->special_to_date : null;
          }

          $response[] = Product::updateStockPriceLazada($shop, $data);
        }


        $this->comment('Updated');
    }
}
