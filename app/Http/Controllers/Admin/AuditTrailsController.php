<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\AuditTrails;
use App\User;

use Yajra\Datatables\Datatables;

class AuditTrailsController extends Controller {
	protected $table = "m_audit_trails";

	public function index() {
		if (request()->ajax()) {
			$trails = AuditTrails::all();
			return Datatables::of($trails)->addColumn("audit_info", function($trail) {
				$item = $trail->auditItem()->first();
				if (count($item) > 0) {
					$item = $item->toArray();
					foreach($item as $key => $value) {
						if (preg_match("/name$/i", $key)) {
							$it = $value;
							break;
						}
					}
					if(empty($it)) {
						$it = "null (ID: " . $trail->audit_item . ")";
					}
				} else {
					$it = "null (ID: " . $trail->audit_item . ")";
				}
				return $it . " from " . $trail->audit_trigger . " was " .  $trail->audit_event . (substr($trail->audit_event, -1) != "e" ? "ed" : "d") . " by <em>" . $trail->user()->first()["name"] . "</em>";
			})->editColumn("audit_timestamp", function($trail) {
				return date_format(date_create_from_format("Y-m-d H:i:s", $trail->audit_timestamp), "d F Y H:i:s");
			})->make(true);
		}
		return view("admin.auditTrails.index", [
			"pageTitle"	=> "Audit Trails",
			"active"	=> ["setting", "auditTrails"]
		]);
	}
	
	public function export($mode) {
		$audits = AuditTrails::all();

		$export[0] = ["No.", "Timestamp", "Information", "IP Address", "Comment"];

		for($i = 0; $i < count($audits); $i++) {
			$export[$i + 1] = [
				$i + 1,
				date_format(date_create_from_format("Y-m-d H:i:s", $audits[$i]->audit_timestamp), "d F Y H:i:s"),
				"",
				$audits[$i]->audit_ip,
				$audits[$i]->audit_comment
			];
			
			$item = $audits[$i]->auditItem()->first();
			if (count($item) > 0) {
				$item = $item->toArray();
				foreach($item as $key => $value) {
					if (preg_match("/name$/i", $key)) {
						$it = $value;
						break;
					}
				}
				if(empty($it)) {
					$it = "null (ID: " . $audits[$i]->audit_item . ")";
				}
			} else {
				$it = "null (ID: " . $audits[$i]->audit_item . ")";
			}
			$export[$i + 1][2] = "Item " . $it . " from " . $audits[$i]->audit_trigger . " was " .  $audits[$i]->audit_event . (substr($audits[$i]->audit_event, -1) != "e" ? "ed" : "d") . " by " . $audits[$i]->user()->first()["name"];
		}

		switch(strtolower($mode)) {
			default:
			case "html": {
				return View("admin.auditTrails.report", [
					"datas"	=> array_splice($export, 1),
					"title"	=> "Audit Trails " . date("d F Y")
				]);
				break;
			}
			case "xls": {
				Excel::create("auditTrails_" . date("Y-m-d"), function($excel) use($export) {
					$excel->sheet("Sheet 1", function($sheet) use($export) {
						$sheet->fromArray($export, null, "A1", false, false);
					});
				})->export("xls");
				break;
			}
			case "pdf": {
				$pdf = PDF::loadView("admin.auditTrails.report", [
					"datas"	=> $export,
					"title"	=> "Audit Trails " . date("d F Y")
				]);
				return $pdf->stream();
				break;
			}
		}
	}
}
