<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\Brands;
use App\Library;
use App\Http\Requests\BrandsRequest;

use Yajra\Datatables\Facades\Datatables;
use Artisan;

class BrandsController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$brands = Brands::all();
			return Datatables::of($brands)->make(true);
		}
		$lastSync = Brands::select('created_at')->orderBy('created_at','desc')->first();
		return view("admin.brands.index", [
			"pageTitle"	=> "Brands",
			"lastSync" => $lastSync
		]);
	}
	public function show() { }
	public function create() { }
	public function edit() { }
	public function store(BrandsRequest $request) {
		$insert["name"]					= $request->input("brand-name");
		$insert["global_identifier"]	= $request->input("global-identifier");
		$insert["insert_by"]			= Auth::user()->id;
		$insert["update_by"]			= Auth::user()->id;
		if(Library::saveTrail("brands", "create", Brands::create($insert)->id)){
			return ["st" => true];
		} else {
			return ["st" => false];
		}
	}
	public function update(BrandsRequest $request, $id) {
		$insert["name"]					= $request->input("brand-name");
		$insert["global_identifier"]	= $request->input("global-identifier");
		$insert["update_by"]			= Auth::user()->id;
		$brand = Brands::find($id);
		if($brand->update($insert)) {
			Library::saveTrail("brands", "update", $brand->id);
			return ["st" => true];
		} else {
			return ["st" => false];
		}
	}
	public function destroy($id) {
		$brand = Brands::find($id);
		Library::saveTrail("zones", "destroy", $brand->id, "Name: " . $brand->name);
		$brand->delete();
        return redirect("app/brands")->with("status", "success")->with("message", "Data deleted successfully");
	}

    public function storeLazadaBrands(Request $request)
    {
    	if (request()->ajax()) {
    		for ($offset=0; $offset < 60000; $offset = $offset+1000) {
				Artisan::queue('lazada:import-brands', [
			        'user_id' => Auth::user()->id,
			        'offset' => $offset
			    ]);
    		}
			return response()->json([
				'status' => 'success'
				]);
    	}
    }

}
