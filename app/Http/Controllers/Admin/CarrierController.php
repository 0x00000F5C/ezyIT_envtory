<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Http\Requests\CarrierRequest;
use Illuminate\Support\Facades\Auth;

use App\Carrier;
use App\CarrierDeliveryOption;
use App\AuditTrails;
use App\Library;
use Datatables;

class CarrierController extends Controller {
	public function index() {
		if (request()->ajax()) {
			return Datatables::of(Carrier::all())->editColumn("Default_actual", function ($carrier) {
				return (bool) $carrier->Default ? '<span class="label label-success">Default</span>' : '<span class="label label-info">Not default</span>';
			})->editColumn("ApiIntegration_actual", function ($carrier) {
				return (bool) $carrier->ApiIntegration ? '<span class="label label-success">True</span>' : '<span class="label label-info">False</span>';
			})->addColumn("action", function ($carrier) {
				return Library::gridAction($carrier, "carrier");
			})->make(true);
		}
		return view("admin.carrier.index", [
			"pageTitle"	=> "Carriers",
			"active"	=> ["setting", "carrier"]
		]);
	}

	public function create() {
		$model = new Carrier;
		$model->Default = false;
		$model->ApiIntegration = false;
		return view("admin.carrier.form",[
		  "pageTitle"		=> "Create New Carrier",
		  "model"			=> $model,
		  "deliveryOptions"	=> [],
		  "active"			=> ['setting', 'carrier']
		]);
	}

	public function store(CarrierRequest $request) {
        $insert["Name"]							= $request->input("name");
        $insert["Default"]						= !empty($request->input("default")) ? 1 : 0;
        $insert["ApiIntegration"]				= !empty($request->input("api-integration")) ? 1 : 0;
        $insert["TrackingCodeValidationRegex"]	= $request->input("tracking-code-validation-regex");
        $insert["TrackingCodeExample"]			= $request->input("tracking-code-example");
        $insert["TrackingUrl"]					= $request->input("tracking-url");
		if((bool) $insert["Default"]) { Carrier::where("Default", 1)->update(["Default" => 0]); }
		$a = Carrier::create($insert)->id;
		$deliveryOptions = [];
		foreach($request->input("delivery-option") as $e){
			if(!empty($e)){
				$deliveryOptions[] = ["carrier_id" => $a, "DeliveryOption" => $e];
			}
		}
		if(Library::saveTrail("carrier", "create", $a)) {
			CarrierDeliveryOption::insert($deliveryOptions);
			return redirect("app/carrier")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function show($id = 0) {
		echo $id;
	}

	public function edit($id) {
		$model = Carrier::find($id);
		if($model === null) { abort(404); }
		$model->Default = (bool) $model->Default;
		$model->ApiIntegration = (bool) $model->ApiIntegration;
		$deliveryOptions = $model->deliveryOptions()->get();
		return view("admin.carrier.form",[
		  "pageTitle"		=> "Create New Carrier",
		  "model"			=> $model,
		  "deliveryOptions"	=> count($deliveryOptions) > 0 ? $deliveryOptions : [],
		  "active"			=> ['setting', 'carrier']
		]);
	}

	public function update(CarrierRequest $request, $id) {
        $insert["Name"]							= $request->input("name");
        $insert["Default"]						= !empty($request->input("default")) ? 1 : 0;
        $insert["ApiIntegration"]				= !empty($request->input("api-integration")) ? 1 : 0;
        $insert["TrackingCodeValidationRegex"]	= $request->input("tracking-code-validation-regex");
        $insert["TrackingCodeExample"]			= $request->input("tracking-code-example");
        $insert["TrackingUrl"]					= $request->input("tracking-url");
		if((bool) $insert["Default"]) { Carrier::where("Default", 1)->update(["Default" => 0]); }
		$carrier = Carrier::find($id);
		if($carrier->update($insert)) {
			Library::saveTrail("carrier", "update", $carrier->id);
			$deliveryOptions = [];
			foreach($request->input("delivery-option") as $e){
				if(!empty($e)){
					$deliveryOptions[] = ["carrier_id" => $carrier->id, "DeliveryOption" => $e];
				}
			}
			CarrierDeliveryOption::where("carrier_id", $carrier->id)->delete();
			CarrierDeliveryOption::insert($deliveryOptions);
			return redirect("app/carrier")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($id) {
		Library::saveTrail("carrier", "destroy", $id, "Name: " . Carrier::find($id)->carrier_name);
		Carrier::find($id)->delete();
        return redirect("app/carrier")->with("status", "success")->with("message", "Data deleted successfully");
	}
}
