<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacRequest;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\CompanyProfilesRequest;

use App\CompanyProfile;
use App\ReportTemplate;
use App\AuditTrails;
use App\Library;
use Datatables;

class CompanyProfileController extends Controller {
	public function index() {
		/* if (request()->ajax()) {
			$profiles = CompanyProfile::all();
			return Datatables::of($profiles)->addColumn("action", function ($profile) {
				return Library::gridAction($profile, "companyProfiles");
			})->make(true);
		}
		return view("admin.CompanyProfile.index", [
			"pageTitle"	=> "Company profiles",
			"active"	=> ["setting", "companyProfiles"]
		]); */
		$model = CompanyProfile::first();
		if($model === null) { $model = new CompanyProfile; }
		return view("admin.CompanyProfile.form",[
		  "pageTitle"		=> "Edit Profile",
		  "model"			=> $model,
		  "reportTemplate"	=> ReportTemplate::where("name", "General template")->first(),
		  "mailTemplate"	=> ReportTemplate::where("name", "Email template")->first(),
		  "active"			=> ['setting', 'company_profiles'],
		  "currency"		=> Library::currency()
		]);
	}

	public function create() {
		$model = new CompanyProfile;
		return view("admin.CompanyProfile.form",[
		  "pageTitle"		=> "Create New Profile",
		  "model"			=> $model,
		  "reportTemplate"	=> ReportTemplate::where("name", "General template")->first(),
		  "mailTemplate"	=> ReportTemplate::where("name", "Email template")->first(),
		  "active"			=> ['setting', 'companyProfiles'],
		  "currency"		=> Library::currency()
		]);
	}

	public function store(CompanyProfilesRequest $request) {
		$insert["profile_type"]				= $request->input("profile-type");
		$insert["profile_name"]				= $request->input("profile-name");
		$insert["profile_billing_address"]	= $request->input("profile-billingaddress");
		$insert["profile_city"]				= $request->input("profile-city");
		$insert["profile_state"]			= $request->input("profile-state");
		$insert["profile_zip"]				= $request->input("profile-zipcode");
		$insert["profile_country"]			= $request->input("profile-country");
		$insert["profile_phone_number"]		= $request->input("profile-phonenumber");
		$insert["profile_fax_number"]		= $request->input("profile-faxnumber");
		$insert["profile_email_address"]	= $request->input("profile-emailaddress");
		$insert["profile_website"]			= $request->input("profile-website");
		$insert["profile_gst"]				= $request->input("profile-gst");
		$insert["profile_business_number"]	= $request->input("profile-businessnumber");
		$insert["profile_currency"]			= $request->input("profile-currency");
		$insert["profile_timezone"]			= $request->input("profile-timezone");
		$insertB["name"]					= empty($request->input("profile-reporttemplatetitle")) ? "General template" : $request->input("profile-reporttemplatetitle");
		$insertB["header"]					= $request->input("profile-reporttemplateheader");
		$insertB["footer"]					= $request->input("profile-reporttemplatefooter");
		$insert["data_status"]				= 2;
		if(empty(ReportTemplate::where("name", "General template")->first())) {
			ReportTemplate::create($insertB);
		} else {
			ReportTemplate::where("name", "General template")->update($insertB);
		}
		if(empty(ReportTemplate::where("name", "Email template")->first())) {
			ReportTemplate::create(["name" => "Email template", "header" => $request->input("profile-emailtemplateheader"), "footer" => $request->input("profile-emailtemplatefooter")]);
		} else {
			ReportTemplate::where("name", "Email template")->update(["header" => $request->input("profile-emailtemplateheader"), "footer" => $request->input("profile-emailtemplatefooter")]);
		}
		if ($request->hasFile("profile-logo") && $request->file("profile-logo")->isValid()) {
			$request->file("profile-logo")->move('img/cProfiles/', $request->input("profile-type") . '.jpg');
		}
		if(Library::saveTrail("companyProfiles", "create", CompanyProfile::create($insert)->id)) {
			return redirect("app/companyProfiles")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function show($id = 0) {
		echo $id;
	}

	public function edit($id) {
		$model = CompanyProfile::find($id);
		if($model === null) { abort(404); }
		return view("admin.CompanyProfile.form",[
		  "pageTitle"		=> "Edit Profile",
		  "model"			=> $model,
		  "reportTemplate"	=> ReportTemplate::where("name", "General template")->first(),
		  "mailTemplate"	=> ReportTemplate::where("name", "Email template")->first(),
		  "active"			=> ['setting', 'company_profiles'],
		  "currency"		=> Library::currency()
		]);
	}

	public function update(CompanyProfilesRequest $request, $id) {
		$insert["profile_type"]				= $request->input("profile-type");
		$insert["profile_name"]				= $request->input("profile-name");
		$insert["profile_billing_address"]	= $request->input("profile-billingaddress");
		$insert["profile_city"]				= $request->input("profile-city");
		$insert["profile_state"]			= $request->input("profile-state");
		$insert["profile_zip"]				= $request->input("profile-zipcode");
		$insert["profile_country"]			= $request->input("profile-country");
		$insert["profile_phone_number"]		= $request->input("profile-phonenumber");
		$insert["profile_fax_number"]		= $request->input("profile-faxnumber");
		$insert["profile_email_address"]	= $request->input("profile-emailaddress");
		$insert["profile_website"]			= $request->input("profile-website");
		$insert["profile_business_number"]	= $request->input("profile-businessnumber");
		$insert["profile_gst"]				= $request->input("profile-gst");
		$insert["profile_currency"]			= $request->input("profile-currency");
		$insert["profile_timezone"]			= $request->input("profile-timezone");
		$insertB["name"]					= empty($request->input("profile-reporttemplatetitle")) ? "General template" : $request->input("profile-reporttemplatetitle");
		$insertB["header"]					= $request->input("profile-reporttemplateheader");
		$insertB["footer"]					= $request->input("profile-reporttemplatefooter");
		$insert["data_status"]				= 2;
		if(empty(ReportTemplate::where("name", "General template")->first())) {
			ReportTemplate::create($insertB);
		} else {
			ReportTemplate::where("name", "General template")->update($insertB);
		}
		if(empty(ReportTemplate::where("name", "Email template")->first())) {
			ReportTemplate::create(["name" => "Email template", "header" => $request->input("profile-emailtemplateheader"), "footer" => $request->input("profile-emailtemplatefooter")]);
		} else {
			ReportTemplate::where("name", "Email template")->update(["header" => $request->input("profile-emailtemplateheader"), "footer" => $request->input("profile-emailtemplatefooter")]);
		}
		if ($request->hasFile("profile-logo") && $request->file("profile-logo")->isValid()) {
			$request->file("profile-logo")->move('img/cProfiles/', $request->input("profile-type") . '.jpg');
		}
		$profile = CompanyProfile::find($id);
		if($profile->update($insert)) {
			Library::saveTrail("companyProfiles", "update", $profile->id);
			return redirect("app/companyProfiles")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($id) {
		Library::saveTrail("companyProfiles", "destroy", $id, "Name: " . CompanyProfile::find($id)->profile_name);
		CompanyProfile::find($id)->delete();
        return redirect("app/supplier")->with("status", "success")->with("message", "Data deleted successfully");
	}
}