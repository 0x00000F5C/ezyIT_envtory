<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardProduct extends Controller
{
    public function index()
    {
    	return view('admin.dashboard.product',[
            "pageTitle"	=> "Products",
    		'controlSidebar' => true
		]);
    }
}
