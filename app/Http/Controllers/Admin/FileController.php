<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File as Thefile;
use Illuminate\Http\Response;
use App\File;

class FileController extends Controller
{
    public function getFile($path,$filename)
    {
    	$entry = File::where('filename', '=', $filename)->firstOrFail();
  		$file = Storage::disk('local')->get($path.'/'.$entry->filename);
  		return (new Response($file, 200))
              ->header('Content-Type', $entry->mime_type);
    }
}
