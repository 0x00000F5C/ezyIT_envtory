<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\PackingLogistic;
use App\StockInLogistic;
use App\User;

use App\Library;
use Datatables;

class logisticInventoryController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$logistics = PackingLogistic::all();
			return Datatables::of($logistics)->addColumn("dimension", function ($logistic) {
				return $logistic->width . " x " . $logistic->height . " x " . $logistic->length;
			})->addColumn("operator_name", function ($logistic) {
				return $logistic->operator()->first()->name;
			})->addColumn("action", function ($logistic) {
				return '<form action="' . url('logisticInventory/'.$logistic->id) . '" method="POST" id="delete-' . $logistic->id . '">' . csrf_field() . method_field("DELETE") . '<a href="' . url('app/stockInLogistic/create?packingCode=' . $logistic->packing_code) . '" class="btn-stockInLogistic btn btn-default btn-xs btn-flat"><i class="fa fa-download"></i> Stock in</a> <a href="' . url('app/logisticInventory/' . $logistic->id) . '/edit" class="btn-editLogistic btn btn-warning btn-xs btn-flat" data-value="' . $logistic->id . '"><i class="fa fa-pencil"></i> Update</a> ' . '<button type="button" class="btn btn-danger btn-xs btn-flat" onclick="confirmDelete(' . $logistic->id . ',\'' . ucwords("orderType") . '\')"><i class="fa fa-trash"></i> Delete</button>' . '</form>';
			})->make(true);
		}
		return view("admin.inventoryLogistic.index", [
			"pageTitle"	=> "Logistic inventory",
			"staffs"	=> User::all(),
			"active"	=> ["setting", "inventoryLogistic"]
		]);
	}
	public function show() {}
	public function create() {}
	public function edit() {}

	public function store(Request $request) {
		$insert["packing_code"]	=	$request->input("logistic-code");
		$insert["packing_name"]	=	$request->input("logistic-name");
		$insert["operator"]		=	$request->input("logistic-operator");
		$insert["qty"]			=	$request->input("logistic-qty");
		$insert["width"]		=	$request->input("logistic-width");
		$insert["height"]		=	$request->input("logistic-height");
		$insert["length"]		=	$request->input("logistic-length");
		if(empty($request->input("id"))) {
			$inserted = PackingLogistic::create($insert)->id;
			$editmode = false;
		} else {
			$old = PackingLogistic::findOrFail($request->input("id"));
			$inserted = $old->id;
			$old->update($insert);
			$editmode = true;
		}
		if(Library::saveTrail("logisticInventory", $editmode ? "update" : "create", $inserted)) {
			return redirect("app/logisticInventory")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update() {}
	public function destroy() {}
}
