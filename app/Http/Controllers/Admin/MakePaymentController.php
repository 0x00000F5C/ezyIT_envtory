<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;

use App\Po;
use App\PoDetail;
use App\MakeOrderPayment;
use App\MakeInvoicePayment;
use App\Supplier;
use App\PaymentMethod;
use App\AuditTrails;
use App\Library;

class MakePaymentController extends Controller {
	public function index(Request $request) {
		$model = Po::where("prefix", $request->input("poid"))->firstOrFail();

		if(!empty($model)) {
			$invoices = $model->invoice()->select(DB::raw("`t_invoice_master`.*, `m_invoice_payments`.`amount`"))->join("m_invoice_payments", "t_invoice_master.id", "=", "m_invoice_payments.invoice_id", "left")->get();
			foreach($invoices as $i => $o) {
				$invoices[$i]->total_amount_actual = Library::formatCurrency($o->total_amount);
				$invoices[$i]->amount_due = $o->total_amount - $o->amount;
				$invoices[$i]->amount_due_actual = Library::formatCurrency($o->total_amount - $o->amount);
			}
			$details = $model->detail()->get();
			$supplier = $model->supplier()->first();
			$supplierContact = $supplier->contacts()->first();
			$payment = $model->payment()->first();
			$paymentID = "PAY" . strtoupper(substr(preg_replace("/[^a-zA-Z0-9]+/i", "", $supplier->supplier_name), 0, 3)) . strtoupper(date("dM")) . str_pad(MakeOrderPayment::where("payment_id", "like", "___" . strtoupper(substr(preg_replace("/[^a-zA-Z0-9]+/i", "", $supplier->supplier_name), 0, 3)) . strtoupper(date("dM") . "%"))->count() + 1, 3, "0", STR_PAD_LEFT);
		}

		if (request()->ajax()) {
			return !empty($model) ? ["status" => true, "order" => $model, "invoices" => $invoices, "details" => $details, "supplier" => $supplier, "supplierContact" => $supplierContact, "payment" => $payment, "paymentID" => !empty($payment) ? $payment->payment_id : $paymentID] : ["status" => false, "order" => null, "invoices" => null, "details" => null, "supplier" => null, "supplierContact" => null, "payment" => null, "paymentID" => null];
		}

		return view("admin.makePayment.form",[
			"pageTitle"	=> "Make a payment",
			"model"		=> !empty($model) ? ["status" => true, "order" => $model, "invoices" => $invoices, "details" => $details, "supplier" => $supplier, "supplierContact" => $supplierContact, "payment" => $payment, "paymentID" => !empty($payment) ? $payment->payment_id : $paymentID] : ["status" => false, "order" => null, "invoices" => null, "details" => null, "supplier" => null, "supplierContact" => null, "payment" => null, "paymentID" => null],
			"suppliers" => Supplier::select(DB::raw("m_suppliers.*, m_suppliers_contact.email"))->join("m_suppliers_contact", "m_suppliers_contact.supplier_id", "=", "m_suppliers.id")->groupBy("m_suppliers.supplier_name")->get(),
			"pMethods"	=> PaymentMethod::all(),
			"active"	=> ['setting', 'makePayment']
		]);
	}

	public function show($id) {
		$model = Po::where("prefix", $id)->firstOrFail();

		if(!empty($model)) {
			$invoices = $model->invoice()->select(DB::raw("`t_invoice_master`.*, `m_invoice_payments`.`amount`"))->join("m_invoice_payments", "t_invoice_master.id", "=", "m_invoice_payments.invoice_id", "left")->get();
			foreach($invoices as $i => $o) {
				$invoices[$i]->total_amount_actual = Library::formatCurrency($o->total_amount);
				$invoices[$i]->amount_due = $o->total_amount - $o->amount;
				$invoices[$i]->amount_due_actual = Library::formatCurrency($o->total_amount - $o->amount);
			}
			$details = $model->detail()->get();
			$supplier = $model->supplier()->first();
			$supplierContact = $supplier->contacts()->first();
			$payment = $model->payment()->first();
			$paymentID = "PAY" . strtoupper(substr(preg_replace("/[^a-zA-Z0-9]+/i", "", $supplier->supplier_name), 0, 3)) . strtoupper(date("dM")) . str_pad(MakeOrderPayment::where("payment_id", "like", "___" . strtoupper(substr(preg_replace("/[^a-zA-Z0-9]+/i", "", $supplier->supplier_name), 0, 3)) . strtoupper(date("dM") . "%"))->count() + 1, 3, "0", STR_PAD_LEFT);
		}

		if (request()->ajax()) {
			return !empty($model) ? ["status" => true, "order" => $model, "invoices" => $invoices, "details" => $details, "supplier" => $supplier, "supplierContact" => $supplierContact, "payment" => $payment, "paymentID" => !empty($payment) ? $payment->payment_id : $paymentID] : ["status" => false, "order" => null, "invoices" => null, "details" => null, "supplier" => null, "supplierContact" => null, "payment" => null, "paymentID" => null];
		}

		return view("admin.makePayment.form",[
			"pageTitle"	=> "Make a payment",
			"model"		=> !empty($model) ? ["status" => true, "order" => $model, "invoices" => $invoices, "details" => $details, "supplier" => $supplier, "supplierContact" => $supplierContact, "payment" => $payment, "paymentID" => !empty($payment) ? $payment->payment_id : $paymentID] : ["status" => false, "order" => null, "invoices" => null, "details" => null, "supplier" => null, "supplierContact" => null, "payment" => null, "paymentID" => null],
			"suppliers" => Supplier::select(DB::raw("m_suppliers.*, m_suppliers_contact.email"))->join("m_suppliers_contact", "m_suppliers_contact.supplier_id", "=", "m_suppliers.id")->groupBy("m_suppliers.supplier_name")->get(),
			"pMethods"	=> PaymentMethod::all(),
			"active"	=> ['setting', 'makePayment']
		]);
	}

	public function create() {}

	public function edit($id) {}

	public function store(Request $request) {
		$insert["payment_id"] = $request->input("payment-id");
		$insert["order_id"] = $request->input("order-id-actual");
		$insert["payment_date"] = $request->input("payment-date");
		$insert["payment_method"] = $request->input("payment-method");
		$a = MakeOrderPayment::create($insert)->id;
		foreach($request->input("payment-invoices") as $i => $o){
			$invoicePayments[] = [
				"payment_id"	=> $a,
				"invoice_id"	=> $o,
				"amount"		=> $request->input("payments")[$i]
			];
		}
		if(Library::saveTrail("makePayment", "create", $a)) {
			MakeInvoicePayment::insert($invoicePayments);
			return redirect("app/po/process?po=" . $request->input("order-id"))->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(Request $request, $id) {
		$id = MakeOrderPayment::where("order_id", $id)->first()->id;
		foreach($request->input("payment-invoices") as $i => $o){
			$invoicePayments[] = [
				"payment_id"	=> $id,
				"invoice_id"	=> $o,
				"amount"		=> $request->input("payments")[$i]
			];
		}
		MakeInvoicePayment::where("payment_id", $id)->delete();
		if(Library::saveTrail("makePayment", "update", $id)) {
			MakeInvoicePayment::insert($invoicePayments);
			return redirect("app/po/process?po=" . $request->input("order-id"))->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($id) {}
}
