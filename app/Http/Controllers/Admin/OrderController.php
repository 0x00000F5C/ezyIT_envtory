<?php

namespace App\Http\Controllers\Admin;

use App\OrderDetailStock;
use App\OrderStatus;
use App\OrderType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Controller;
use App\Http\Requests\OrderRequest;

use App\Order;
use App\OrderDetail;
use App\Shop;
use App\Product;
use Datatables;
use App\Library;
use App\Sku;
use App\Carrier;
use App\CompanyProfile;
use PDF;
use Uuid;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if (request()->ajax()) {
        	if($request->status == 0){
		        $order = Order::limit($request->length);
	        }else{
		        $order = Order::where('status', $request->status);
	        }

            return Datatables::of($order)
                ->addColumn('platform', function ($order) {
                    return Library::shopPlatform($order->shop->platform_id);
                })
                ->addColumn('shop', function ($order) {
                  return $order->shop->shop_name;
                })
	            ->addColumn('status_order', function ($order) {
                  return $order->getStatus->status_name;
                })
	            ->addColumn('type_order', function ($order) {
		            return $order->orderType->name;
	            })
                ->addColumn('actions', function ($order) {
	                $action = '';
	                $view = '<a href="' . url('app/order/' . $order->id) .'" class="btn-editOrder btn btn-warning btn-xs btn-flat" data-value="' . $order->id . '"><i class="fa fa-eye"></i> Show</a> ';
	                $delete = '<button type="button" onclick="delOrder(this)" data-id="'.$order->id.'" class="btn btn-danger btn-xs btn-flat"><i class="fa fa-times"></i> Cancel Order</button>';
	                if (Auth::user()->can('view-order')) {
		                $action .= $view;
	                }
	                if (Auth::user()->can('delete-order') && $order->status==2) {
		                $action .= $delete;
	                }
					$action .= '<div class="btn-group">
						<button type="button" class="btn btn-flat btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file-text-o"></i> Print ...</button>
						<button type="button" class="btn btn-default btn-flat btn-xs dropdown-toggle" data-toggle="dropdown">
							<span class="caret"></span>
							<span class="sr-only">Toggle Dropdown</span>
						</button>
						<ul class="dropdown-menu pull-right" role="menu">' .
							'<li><a href="' . url('app/order/' . $order->order_no . '/printOrder') . '"><i class="fa fa-print"></i> Print order</a></li>' .
							'<li><a href="' . url('app/order/' . $order->order_no . '/printInvoice') . '"><i class="fa fa-print"></i> Print invoice</a></li>' .
							'<li><a href="' . url('app/order/' . $order->order_no . '/printShippingLabel') . '"><i class="fa fa-print"></i> Print shipping label</a></li>' .
							'<li><a href="' . url('app/order/' . $order->order_no . '/printStockList') . '"><i class="fa fa-print"></i> Print stock list</a></li>' .
							'<li><a href="' . url('app/order/' . $order->order_no . '/printCarrierManifest') . '"><i class="fa fa-print"></i> Print carrier manifest</a></li>' .
						'</ul>
					</div>';
	                return $action;
                })
            ->make(true);
        }
        return view('admin.order.index',[
        	'statuses' => OrderStatus::all(),
            'pageTitle' => 'Order'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    	$model = new Order;
        $sku_lists = Sku::all();
	    $sku_autocomplete=array(["id"=>'',"text"=>'select SKU Product']);
	    foreach($sku_lists as $sku){
	    	if($sku->product){
		        if($sku->product->stock_active > 0)
			        array_push($sku_autocomplete,array("id"=>$sku->id,"text"=>$sku->sku.' - '.$sku->product->attribute('name'),"product"=>$sku->product->attribute('name'),"stock"=>$sku->product->stock_active));
		    }
	    }
        return view('admin.order.form',[
            'model' => $model,
            'shops' => Shop::all(),
	        'sku_autocomplete' => json_encode($sku_autocomplete),
			'shippingMethods' => Carrier::all(),
            'pageTitle' => 'Create Order'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OrderRequest $request)
    {
	    $comp = CompanyProfile::first();
        $data = $request->except(['products']);
        $data['shop_id'] = $data['shop'];
        $data['order_no'] = Order::generatePoNumber($data['shop'],$data['date']);
        $data['status'] = 2;
        $dataDetail = $request->only(['products']);
        if ($order = Order::create($data)) {
			Library::saveTrail("order", "create", $order->id);
			$items_count = 0;
            foreach ($dataDetail['products'] as $key => $value) {
            	if($value['product_sku_id']!='select SKU Product'){
            		$items_count+=$value['qty'];
		            $value['id'] = Uuid::generate()->string;
	                $value['t_order_master_id'] = $order->id;
	                $value['paid_price'] = $value['price'];
	                $value['currency'] = $comp->profile_currency;
	                $value['created_at'] =  \Carbon\Carbon::now();
	                $value['updated_at'] =  \Carbon\Carbon::now();
		            $value['insert_by'] =  Auth::user()->id;
		            $value['update_by'] =  Auth::user()->id;

		            if($detail = OrderDetail::create($value)){
		                $product_sku = Sku::find($value['product_sku_id']);
		                $data_stocks = Library::findStockBatch($product_sku->product->id,$value['qty']);
		                foreach ($data_stocks as $stock){
							if(OrderDetailStock::create([
								't_order_detail_id' => $detail->id,
								't_stocks_id' => $stock['stocks_id'],
								'batch_id' => $stock['batch_id'],
								'cell_id' => $stock['cell_id'],
								'qty' => $stock['qty_to_pick']
							])){
								Library::updateStock($stock['stocks_id'],$stock['qty_to_pick'],'out',$order->order_no);
								//update stock to lazada
								$shop = Shop::find($data['shop']);
								Product::updateStockPriceLazada($shop, ['SellerSku'=>$value['product_sku_id'], 'Quantity'=>$value['qty']]);
							};
		                }
		            }
	            }
            }
	        $type = OrderType::getByQty($items_count);
	        $order->update(['items_count'=>$items_count,'order_types_id'=>$type->id]);
            return redirect('app/order')
              ->with('status', 'success')
              ->with('message', 'Order Successfully Created');

        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $model = Order::find($id);
        return view('admin.order.view',[
            'model' => $model,
	        'details' => $model->detail,
            'shops' => Shop::all(),
            'pageTitle' => 'Show Order'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
	    $order->update(['status'=>11]); //canceled orders
	    $details = $order->detail;
	    foreach ($details as $detail){
	    	$detail_stock = OrderDetailStock::where('t_order_detail_id',$detail->id)->get();
	    	foreach ($detail_stock as $d){
			    $qtyStock = Library::updateStock($d->t_stocks_id,$d->qty,'in',$order->order_no);
			    //update stock to lazada
			    Product::updateStockPriceLazada($order->shop, ['SellerSku' => $detail->sku->sku, 'Quantity' => $qtyStock]);
		    }
	    }
        Library::saveTrail('order', 'delete', $order->id, 'PREFIX : '. $order->order_no);
	    return response()->json(['status'=>'success','msg'=>'Order Canceled.','text'=>'Canceled Order!']);
    }

    public function getSON(Request $request)
    {
        $son = Order::generateShopOrder($request->shop);
        return response()->json($son);
    }

    public function getDetail(Request $request)
    {
        $details = OrderDetail::where('t_order_master_id',$request->order)->get();
        $data = [];
        foreach ($details as $detail) {
            $data[] = [
            	'order_no' => $detail->order->order_no,
                'shop_order_no' => $detail->order->shop_order_no,
                'product_sku_id' => $detail->sku->sku,
	            'product_name' => $detail->sku->product->attribute('name'),
                'price' => $detail->price,
                'qty' => $detail->qty,
	            'detail_order_id' => $detail->id,
            ];
        }
        return response()->json($data);
    }

    public function syncOrders()
    {
	    $user = Auth::user()->id;
	    $shops = Shop::where('account', '<>', '')->where('api_key', '<>', '')->where('platform_id',1)->get();
    	if($shops){
	        foreach ($shops as $shop){
	        	if($shop->platform_id==1){
			        $result = Order::syncWithLazada($shop,$user);
		        }
	        }
    	}

    	return response()->json($result);
    }

	public function printOrder($id) {
        $model = Order::where("order_no", $id)->firstOrFail();
		return view("admin.order.print",[
            "order" => $model,
	        "details" => $model->detail,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function printInvoice($id) {
        $model = Order::where("order_no", $id)->firstOrFail();
		return view("admin.order.printInvoice",[
            "order" => $model,
	        "details" => $model->detail,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function printShippingLabel($id) {
        $model = Order::where("order_no", $id)->firstOrFail();
		return view("admin.order.printShippingLabel",[
            "order" => $model,
	        "details" => $model->detail,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function printStockList($id) {
        $model = Order::where("order_no", $id)->firstOrFail();
		return view("admin.order.printStockList",[
            "order" => $model,
	        "details" => $model->detail,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function printCarrierManifest($id) {
        $model = Order::where("order_no", $id)->firstOrFail();
		return view("admin.order.printCarrierManifest",[
            "order" => $model,
	        "details" => $model->detail,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => false
        ]);
	}

	public function multiPrintOrders(Request $request) {
        $model = Order::whereIn("order_no", $request->input("selection"))->get();
		return view("admin.order.multiPrints",[
            "orders" => $model,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function multiPrintInvoices(Request $request) {
        $model = Order::whereIn("order_no", $request->input("selection"))->get();
		return view("admin.order.multiPrintInvoices",[
            "orders" => $model,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function multiPrintShippingLabels(Request $request) {
        $model = Order::whereIn("order_no", $request->input("selection"))->get();
		return view("admin.order.multiPrintShippingLabels",[
            "orders" => $model,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function multiPrintStockLists(Request $request) {
        $model = Order::whereIn("order_no", $request->input("selection"))->get();
		return view("admin.order.multiPrintStockLists",[
            "orders" => $model,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function multiPrintCarrierManifests(Request $request) {
        $model = Order::whereIn("order_no", $request->input("selection"))->get();
		return view("admin.order.multiPrintCarrierManifests",[
            "orders" => $model,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function getPDF($id) {
		$model = Order::where("order_no", $id)->firstOrFail();
		return PDF::loadView("admin.order.pdf", [
            "order" => $model,
	        "details" => $model->detail,
            "shops" => Shop::all(),
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => false
		])->download("Order - " . $id . ".pdf");
	}
}
