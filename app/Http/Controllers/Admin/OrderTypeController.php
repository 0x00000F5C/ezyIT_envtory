<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\OrderType;
use Validator;
use Response;
use App\Library;
use Datatables;

class OrderTypeController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$ordtype = OrderType::all();
			//return Datatables::of($ordtype)->make(true);
//            onclick="confirmDelete(' . $type->id . ',\'' . ucwords("orderType") . '\')"
			return Datatables::of($ordtype)->addColumn("action", function ($type) {
//				return Library::gridAction($type,'orderType');
				return '<form action="' . url('orderType/'.$type->id) . '" method="POST" id="delete' . $type->id . '">' . csrf_field() . method_field("DELETE") .
				'<button type="button" class="btn btn-warning btn-xs btn-flat btn-editOrderType" data-value="' . $type->id . '"><i class="fa fa-pencil"></i> Update</button> ' .
				'<button type="button" class="btn btn-danger btn-xs btn-flat deleteOrderType" data-id="' . $type->id . '"><i class="fa fa-trash"></i> Delete</button>' .
				'</form>';
			})->make(true);
		}
		return view("front.orderType.index", [
			"pageTitle"	=> "Order type management",
			"active"	=> ["setting", "orderType"]
		]);
		}

	public function show() {}

	public function add(Request $request)
	{
			$data = new OrderType;
			$data->name = $request->name;
			$data->min_qty = $request->min_qty;
			$data->max_qty = $request->max_qty;
			$data->save();
			return back()
				->with('success', 'Record Added successfully.');
	}

	public function edit(Request $request)
	{
		$id = $request->input('id');
        $insert["name"] = $request->input('name');
        $insert["min_qty"] = $request->input('min_qty');
        $insert["max_qty"] = $request->input('max_qty');
        $insert["troy_needed_per"] = $request->input("pickingtype-troy-needed-per");
        $data = OrderType::find($id);
        $data->update($insert);
        return response(['msg' => 'Record Updated successfully.', 'status' => 'success']);
	}

	public function destroy( Request $request, $id ) {
        try {
            Library::saveTrail("orderTypes", "destroy", $id, "Name: " . OrderType::find($id)->name);
            OrderType::find($id)->delete();
            return response(['msg' => 'Order Type deleted', 'status' => 'success']);
        }
        catch (\Exception $e) {
            return response(['msg' => $e->getMessage(), 'status' => 'failed']);
        }
	}

	public function store() {}
}
