<?php

namespace App\Http\Controllers\Admin;

use App\CompanyProfile;
use App\Order;
use App\OrderDetail;
use App\Picking;
use App\ShippingBin;
use App\Shop;
use App\Traits\ValetTrait;
use App\WarehouseTrays;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\PackingList;
use App\Carrier;

use App\Library;
use Datatables;

class PackingListController extends Controller
{
    use ValetTrait;
    public function index(Request $request)
    {
        if (request()->ajax()) {
            $lists = PackingList::limit($request->length);
            return Datatables::of($lists)
                ->addColumn('staff', function ($lists) {
                    return $lists->staff->firstname;
                })
                ->addColumn('packing_status', function ($lists) {
                    $status = ['Ready To Pack', 'Ready To Ship'];
                    return $status[$lists->status];
                })->addColumn("action", function ($lists) {
                    return $lists->status == 0 ? '<a href="' . url('app/packingList/' . $lists->id) . '/edit" class="btn-showShippingList btn btn-flat btn-xs btn-primary" data-value="' . $lists->id . '"><i class="fa fa-edit"></i> Update</a>' : '';
                })->make(true);
        }
        return view("admin.packingList.index", [
            "pageTitle" => "Packing List",
        ]);
    }

    public function create()
    {
        return view('admin.packingList.create', [
            "pageTitle" => "Create Packing",
            "trays" => WarehouseTrays::all(),
            "bins" => ShippingBin::all()
        ]);
    }

    public function edit($id){
        return view('admin.packingList.edit', [
            "pageTitle" => "Create Packing",
            "trays" => WarehouseTrays::all(),
            "bins" => ShippingBin::all(),
            'packing' =>PackingList::find($id)
        ]);
    }

    public function orderList(Request $request)
    {
        $warehouseTray = WarehouseTrays::where('prefix', $request->input('tray_id'))->first();
        if($warehouseTray){
            $pickings = $warehouseTray->pickings;
        }else{
            return response()->json(['status' => false, 'msg' => 'Tray Not Found'], 200);
        }


        if ($pickings)
            $pickings[0]->picking_list['date'] = date('d F Y', strtotime($pickings[0]->picking_list['date']));
        $orders = [];
        $html = ['orders' => '', 'details' => ''];
        foreach ($pickings as $picking) {
            if ($picking->orderDetailStock->orderDetail->order->status == 5) {
                $order_id = $picking->orderDetailStock->orderDetail->order->id;
                $orders[$order_id] = $picking->orderDetailStock->orderDetail->order;

                $html['details'] .= '<tr class="' . $picking->stockInBatch->orderDetail->order->id . '" style="display:none">
	                <td>
	                   ' . $picking->stockInBatch->batch_id .$picking->stockInBatch->orderDetail->sku->sku . '
	                </td>
	                <td>
	                   ' . $picking->stockInBatch->batch_id . '
	                </td>
	                <td>
	                   ' . $picking->stockInBatch->orderDetail->sku->product->attribute('name') . '
	                </td>
	            </tr>';

            }
        }

        foreach ($orders as $order) {
            $html['orders'] .= '<tr class="click" data-order="' . $order->id . '">' .
                '<td>' . $order->order_no . '</td>' .
                '<td><a onclick="loadPack(\''.$order->id.'\');" class="btn btn-success btn-sm btn-flat">PACK</a></td>' .
                '</tr>';
        }

        $data['pickings'] = $pickings[0]->picking_list;
        $data['orders'] = $orders;

        return response()->json(['data' => $data, 'html' => $html, 'status' => true], 200);
    }


    public function orderListEdit(Request $request)
    {
        $order = Order::find($request->input('order_id'));
//        dd( $order->detail[0]->detailStock->picking);
        $pickings = $order->detail[0]->detailStock->picking;
        if ($pickings)
            $pickings[0]->picking_list['date'] = date('d F Y', strtotime($pickings[0]->picking_list['date']));
        $orders = [];
        $html = ['orders' => '', 'details' => ''];
            if ($order->status == 6) {
                $orders[$order->id] = $order;

                foreach ($order->detail as $detail){
                    $html['details'] .= '<tr class="' . $order->id . '" style="display:nonse">
	                <td>
	                   ' . $detail->sku->sku . '
	                </td>
	                <td>
	                   ' . $detail->detailStock->batch_id . '
	                </td>
	                <td>
	                   ' . $detail->sku->product->attribute('name') . '
	                </td>
	            </tr>';
                }

            }

        foreach ($orders as $order) {
            $html['orders'] .= '<tr class="click" data-order="' . $order->id . '">' .
                '<td>' . $order->order_no . '</td>' .
                '<td><a onclick="loadPack(\''.$order->id.'\');" class="btn btn-success btn-sm btn-flat">PACK</a></td>' .
                '</tr>';
        }

        $data['pickings'] = $pickings[0]->picking_list;
        $data['orders'] = $orders;
        $data['tray_prefix'] = $pickings[0]->tray->prefix;

        return response()->json(['data' => $data, 'html' => $html, 'status' => true], 200);
    }

    public function store(Request $request)
    {
        $data = $request->input('packing');
        $format = 'PACK' . date('dmy');
        $packing = PackingList::select('packing_id')->orderBy('packing_id', 'desc')->first();
        $data['packing_id'] = Library::generateNumber($format, count($packing) > 0 ? $packing->packing_id : 0, 3);
        $data['packing_date'] = date('Y-m-d');

        if ($packList = PackingList::create($data)) {
            Library::saveTrail("packing_list", "create", $packList->id);
            $order = Order::find($packList->order_id);
            $order->update(['status' => 6]);

            //set status in Lazada
            if ($order->shop->platform_id == 1) {
                $this->setStatusInLazada($order->detail, $order->shop);
            }
        }

        return redirect('app/packingList')
            ->with('status', 'success')
            ->with('message', 'Packing in process!');
    }

    public function setStatusInLazada($details, $shop)
    {
        $arrItemIds = [];
        foreach ($details as $d) {
            $arrItemIds[] = $d->order_item_id;
        }
        $itemsIds = implode(',', $arrItemIds);
        Order::setStatusReadyToShip($shop, $itemsIds);
    }

    public function loadPacking(Request $request){
        $input = $request->all();
        $order = Order::find($input['order_id']);

        return view('admin.packingList._packing_form', compact('order'));
    }

    public function savePACK(Request $request){
        $input = $request->all();
        $order = Order::find($input['order_id']);
        if($input['type_pack']=='update'){
            $packing = PackingList::where('order_id', $order->id)->first();
            $packing ->status = 1;
            $packing->save();
        }else{
            $data = array();
            $data['order_id'] = $order->id;
            $format = 'PACK' . date('dmy');
            $packing = PackingList::select('packing_id')->orderBy('packing_id', 'desc')->first();
            $data['packing_id'] = Library::generateNumber($format, count($packing) > 0 ? $packing->packing_id : 0, 3);
            $data['packing_date'] = date('Y-m-d');
            $data['status'] = 1;
            $packing = PackingList::create($data);
        }

        Library::saveTrail("packing_list", "update", $packing->id);
        $order->update(['status' => 7]);
        \Session::flash('status', 'success');
        \Session::flash('message', 'Packing is done !');

        //set status in Lazada
        if ($order->shop->platform_id == 1) {
            $this->setStatusInLazada($order->detail, $order->shop);
        }
        return array(
            'status'=>true,
            'order_id'=>$order->id
        );
    }

    public function printInvoice($id) {
        $model = Order::where("id", $id)->firstOrFail();
        return view("admin.packingList.printInvoice",[
            "order" => $model,
            "details" => $model->detail,
            "shops" => Shop::all(),
            "company" => CompanyProfile::first(),
            "currency" => Library::currency(Library::defaultCurrency()),
            "print" => true,
            "nowDate" => $this->getHumanDateTime()
        ]);
    }

}