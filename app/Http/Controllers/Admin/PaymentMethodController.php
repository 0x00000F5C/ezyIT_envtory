<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests\PaymentMethodRequest;
	
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\PaymentMethod;
use App\Library;
use App\AuditTrails;
use Datatables;

class PaymentMethodController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$methods = PaymentMethod::all();
			return Datatables::of(collect($methods))->editColumn("is_bank", function ($method) {
				return ((bool) $method->is_bank ? '<input type="checkbox" disabled="disabled" checked="checked">' : '<input type="checkbox" disabled="disabled">');
			})->addColumn("action", function ($method) {
				return Library::gridAction($method, "paymentMethod");
			})->make(true);
		}
		return view("admin.paymentMethod.index", [
			"pageTitle"	=> "Payment methods",
			"active"	=> ["setting", "paymentMethod"]
		]);
	}

	public function create() {
		return view("admin.paymentMethod.form",[
		  "pageTitle"	=> "Create New Payment Method",
		  "model"		=> new PaymentMethod,
		  "active"		=> ['setting', 'paymentMethod'],
		]);
	}

	public function edit($id) {
		$model = PaymentMethod::find($id);
		if($model === null) { abort(404); }
		return view("admin.paymentMethod.form",[
		  "pageTitle"	=> "Create New Payment Method",
		  "model"		=> $model,
		  "active"		=> ['setting', 'paymentMethod'],
		]);
	}
	public function store(PaymentMethodRequest $request) {
		$insert["name"]		=	$request->input("method-name");
		$insert["is_bank"]	=	$request->input("method-isbank") ? (integer) $request->input("method-isbank") : 0;
		if(Library::saveTrail("paymentMethod", "create", PaymentMethod::create($insert)->id)) {
			if (request()->ajax()) { return ["status" => true, "pMethods" => PaymentMethod::all()]; }
			return redirect("app/paymentMethod")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			if (request()->ajax()) { return ["status" => false]; }
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(PaymentMethodRequest $request, $id) {
		$insert["name"]		=	$request->input("method-name");
		$insert["is_bank"]	=	$request->input("method-isbank") ? (integer) $request->input("method-isbank") : 0;
		PaymentMethod::find($id)->update($insert);
		if(Library::saveTrail("paymentMethod", "update", PaymentMethod::find($id)->id)) {
			return redirect("app/paymentMethod")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($id) {
		Library::saveTrail("paymentMethod", "destroy", $id, "Name: " . PaymentMethod::find($id)->name);
		PaymentMethod::find($id)->delete();
        return redirect("app/paymentMethod")->with("status", "success")->with("message", "Data deleted successfully");
	}
}
