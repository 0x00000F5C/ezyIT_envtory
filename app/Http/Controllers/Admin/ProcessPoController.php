<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\InvoiceRequest;

use App\Po;
use App\PoDetail;
use App\Supplier;
use App\Product;
use App\Library;
use App\AuditTrails;
use App\Invoice;
use App\InvoiceDetail;
use App\File;
use App\Stock;
use App\StockHistories;
use App\CompanyProfile;
use Datatables;
use Auth;
use Uuid;

class ProcessPoController extends Controller
{
    public function index()
    {
      $po = isset($_GET['po']) ? $_GET['po'] : null;
      $po = Po::where('prefix',$po)->first();
      $invoice = new Invoice;
      $invoice->date = date('Y-m-d');
      if (empty($po)) {
        $po = new Po;
      }
      return view('admin.processPo.index', [
        'po' => $po,
        'invoice' => $invoice,
        'pageTitle' => 'Process Purchase Order',
      ]);
    }

    public function store(InvoiceRequest $request)
    {
      $data = $request->except(['invoice','detail']);
      $detail = $request->detail;
      $invoiceDetail = [];
      if (!empty($detail)) {
        if ($invoice = Invoice::create($data)) {
          foreach ($detail as $key => $value) {
            $value['id'] = Uuid::generate()->string;
            $value['t_invoice_master_id'] = $invoice->id;
            $value['created_at'] =  \Carbon\Carbon::now();
            $value['updated_at'] =  \Carbon\Carbon::now();
            $value['insert_by'] =  Auth::user()->id;
            $value['update_by'] =  Auth::user()->id;
            
            array_push($invoiceDetail,$value);
            if ($value['qty'] > 0) {
              $stock[$key] = [
                't_invoice_master_id' => $invoice->id,
                'm_products_id' => $value['m_products_id'],
                'received_date' => $request->date,
                'batch_id' => $this->getBatchId($request->t_po_master_id,$value['m_products_id']),
                'stock_type' => 'CD',
                'status' => $this->getBatchStatus($value['m_products_id']),
                'zone_type' => null
              ];
              $stocHystories[$key] = new StockHistories([
                'qty' => $value['qty'],
              ]);
            }
          }
          if (invoiceDetail::insert($invoiceDetail)) {
            if (isset($stock)) {
              foreach ($stock as $key => $value) {
                $saveStock = Stock::create($stock[$key]);
                $saveStock->histories()->save($stocHystories[$key]);
              }
            }
          }
          Po::where('id',$request->t_po_master_id)->update(['status' => 2]);
          return redirect('/app/po/process?po='.$request->po_prefix)
            ->with('status', 'success')
            ->with('message', 'Invoice Successfully Saved');
        }
      }
      return back()->with('status', 'danger')
            ->with('message', 'Save Invoice Failed');
    }

    public function update(InvoiceRequest $request, $id)
    {
      $data = $request->except(['invoice','detail']);
      $detail = $request->detail;
      $invoice = Invoice::find($id);
      if (!empty($detail)) {
        if ($invoice->update($data)) {
          foreach ($detail as $key => $value) {
            $value['updated_at'] =  \Carbon\Carbon::now();
            InvoiceDetail::where('t_invoice_master_id',$invoice->id)
                            ->where('m_products_id',$value['m_products_id'])
                            ->update($value);
            StockHistories::whereHas('stock',function($stock) use ($invoice,$value){
                              $stock->where('t_invoice_master_id',$invoice->id)
                                    ->where('m_products_id',$value['m_products_id']);
                            })->update(['qty' => $value['qty']]);
          }
          return redirect('/app/po/process?po='.$request->po_prefix)
            ->with('status', 'success')
            ->with('message', 'Invoice Successfully Saved');
        }
      }
      return back()->with('status', 'danger')
            ->with('message', 'Save Invoice Failed');
    }

    public function destroy(Invoice $invoice)
    {
        Stock::where('t_invoice_master_id',$invoice->id)->delete();
        $invoice->delete();
        Library::saveTrail('invoice', 'delete', $invoice->id, 'INV : '.$invoice->prefix);
        return redirect()->back()
                ->with('status', 'success')
                ->with('message', 'Invoce Deleted');
    }

    public function searchPo(Request $request)
    {
        $q = $request->po;
        $po = Po::select('prefix')->where('status','!=',3)->limit(10);
        if ($q) {
          $po->where('prefix','like','%'.$q.'%');
        }
        return $po->orderBy('prefix')->orderBy('created_at')->get();
    }

    public function loadPoData(Request $request)
    {
      $po = Po::where('prefix', $request->po)->first();
      $data = [];
      if ($po) {

        foreach ($po->detail as $no => $detail) {
          ++$no;
          $received = $this->getOrderReceived($detail->t_po_master_id,$detail->m_products_id);
          $invoice_qty = $detail->qty - $received;
          $data['products'][] = [
            'no' => $no,
            'product_po_id' => $detail->product_po_id,
            'product_code' => $detail->product->product_code,
            'product_name' => $detail->product->attribute('name'),
            'qty' => $detail->qty,
            'received' => $received,
            'invoice_qty' => '<input type="hidden" name="detail['.$no.'][m_products_id]" value="'.$detail->product->id.'"><input type="number" name="detail['.$no.'][qty]" id="invoice_qty_'.$no.'" class="form-control qty" onchange="calculateAmount('.$no.')" value="'.$invoice_qty.'" max="'.$invoice_qty.'" min="0" required>',
            'price' => $detail->price,
            'invoice_price' => '<input type="number" name="detail['.$no.'][price]" id="invoice_price_'.$no.'" class="form-control price" onchange="calculateAmount('.$no.')" value="'.$detail->price.'" min="0" required>',
            'amount' => $detail->qty * $detail->price,
            'invoice_amount' => '<input type="hidden" id="invoice_total_'.$no.'" value="'.$invoice_qty * $detail->price.'" class="invoice_amount"><span id="text_invoice_total_'.$no.'">'.$invoice_qty * $detail->price.'</span>',
          ];
        }

        $data['invoice'] = [];
        foreach ($po->invoice as $key => $invoice) {
          $batch = Stock::where('t_invoice_master_id',$invoice->id)->where('stock_type','CD')->count();
          $action = '';
          if ($batch) {
            $updateDelete = '';
            $update = '<button type="button" class="btn btn-primary btn-xs btn-flat" data-action="update" data-id="'.$invoice->id.'" data-toggle="modal" data-target="#invoiceModal"><i class="fa fa-edit"></i> Update</button> ';
            $delete = '<button type="button" class="btn btn-danger btn-xs btn-flat" onclick="confirmDelete('.$invoice->id.',\'Invoice\')"><i class="fa fa-trash"></i> Delete</button>';
            if (Auth::user()->can('update-processPo')) {
              $updateDelete .= $update;
            }
            if (Auth::user()->can('delete-processPo')) {
              $updateDelete .= $delete;
            }
              $action = '<form action="'.url('app/po/process/'.$invoice->id).'" method="POST" id="delete-'.$invoice->id.'">
                          '.csrf_field().method_field("DELETE").$updateDelete.'
                          </form>';
          }else{
            $action = '<small class="label label-success">Received</small>';
          }
          $data['invoice'][] = [
            'no' => ++$key,
            'prefix' => $invoice->prefix,
            'date' => $invoice->date,
            'qty_received' => $invoice->detail->sum('qty'),
            'total_amount' => $invoice->total_amount,
            'action' => $action
          ];
        }

        $data['file'] = File::get('po',$po->id);
        $po = Po::with('supplier')->where('prefix', $request->po)->first();
        $data['po'] = $po;
        if ($po->status == 2) {
          $data['po']['close'] = '<button type="button" class="btn btn-warning btn-flat" id="closePoBtn" onclick="closePo()"><i class="fa fa-ban"></i> Close Purchase Order</button>';
        }
      }
      return response()->json($data);
    }

    protected function getOrderReceived($po_id,$product_id)
    {
      $received = InvoiceDetail::whereHas('invoice',function($inv) use ($po_id){
                    $inv->where('t_po_master_id',$po_id);
                  })
                  ->where('m_products_id',$product_id)
                  ->sum('qty');
      return $received;
    }

    public function upload(Request $request)
    {
      $upload = File::add('po',$request->t_po_master_id,$request->file,$request->filename);
      if ($upload) {
        $path = '/app/po/';
        if (!empty($request->po_prefix)) {
          $path = '/app/po/process?po='.$request->po_prefix;
        }
        return redirect($path)
            ->with('status', 'success')
            ->with('message', 'File Successfully Uploaded');
      }
      return back()->with('status', 'danger')
            ->with('message', 'Upload File Failed');
    }

    protected function getBatchId($po_id,$product_id)
    {
      $po = Po::find($po_id);
      $supplier = Supplier::find($po->m_supplier_id);
      $product = Product::find($product_id);
      $productPrefix = strtoupper(substr($product->product_code, 0, 3));
      $supplierPrefix = strtoupper(substr($supplier->supplier_prefix, 0, 3));
      $firstPrefix = $supplierPrefix.$productPrefix.date('dmy');
      $lastNumber = Stock::selectRaw('RIGHT(max(batch_id),4) as max_number')
                      ->where('batch_id','like',$firstPrefix.'%')
                      ->first();
      $newNumber = sprintf("%04d", $lastNumber->max_number+1);
      $prefix = $firstPrefix.$newNumber;
      return $prefix;
    }

    protected function getBatchStatus($product_id)
    {
      $status = 2;
      $check = Stock::where('m_products_id',$product_id)
                ->where('status',2)->count();
      if ($check) {
        $status = 0;
      }
      return $status;
    }

    public function getInvoice(Request $request)
    {
      $id = $request->invoice;
      $data = [];
      $po = Po::where('prefix', $request->po)->first();
      if ($request->action == 'add') {
        $data['title'] = 'Add Invoice';
        $data['action_btn'] = 'Save Invoice';
        $data['action_form'] = url('/app/po/process');
        $data['additional_form'] = '';
        $invoice = new Invoice;
        $invoice->date = date('Y-m-d');
        $invoice->ship_cost = $po->ship_cost;
        $invoice->misc_cost = $po->misc_cost;
        $invoice->total_amount = $po->ship_cost + $po->misc_cost;
        foreach ($po->detail as $no => $detail) {
          ++$no;
          $received = $this->getOrderReceived($detail->t_po_master_id,$detail->m_products_id);
          $invoice_qty = $detail->qty - $received;
          $amount = $detail->qty * $detail->price;
          $data['products'][] = [
            'no' => $no,
            'product_code' => $detail->product->product_code,
            'product_name' => $detail->product->attribute('name'),
            'qty' => $detail->qty,
            'received' => $received,
            'invoice_qty' => '<input type="hidden" name="detail['.$no.'][m_products_id]" value="'.$detail->product->id.'"><input type="number" name="detail['.$no.'][qty]" id="invoice_qty_'.$no.'" class="form-control qty" onchange="calculateAmount('.$no.')" value="'.$invoice_qty.'" max="'.$invoice_qty.'" min="0" required>',
            'price' => $detail->price,
            'invoice_price' => '<input type="number" name="detail['.$no.'][price]" id="invoice_price_'.$no.'" class="form-control price" onchange="calculateAmount('.$no.')" value="'.$detail->price.'" min="0" required>',
            'amount' => $amount,
            'invoice_amount' => '<input type="hidden" id="invoice_total_'.$no.'" value="'.$amount.'" class="invoice_amount"><span id="text_invoice_total_'.$no.'">'.$amount.'</span>',
          ];
          $invoice->total_amount = $invoice->total_amount + $amount;
        }
      }else{
        $invoice = Invoice::find($id);
        $data['title'] = 'Update Invoice '.$invoice->prefix;
        $data['action_btn'] = 'Update Invoice';
        $data['action_form'] = url('/app/po/process/'.$invoice->id.'/update');
        $data['additional_form'] = '<input type="hidden" name="_method" value="PUT">';
        foreach ($invoice->detail as $no => $detail) {
            ++$no;
            $ordered = PoDetail::where('t_po_master_id',$po->id)->where('m_products_id',$detail->m_products_id)->first();
            $received = $detail->where('m_products_id',$detail->m_products_id)->sum('qty') - $detail->qty;
            $invoice_qty = $detail->qty;
            $rest = $ordered->qty - $received;
            $data['products'][] = [
              'no' => $no,
              'product_code' => $detail->product->product_code,
              'product_name' => $detail->product->product_name,
              'qty' => $ordered->qty,
              'received' => $received,
              'invoice_qty' => '<input type="hidden" name="detail['.$no.'][m_products_id]" value="'.$detail->product->id.'"><input type="number" name="detail['.$no.'][qty]" id="invoice_qty_'.$no.'" class="form-control qty" onchange="calculateAmount('.$no.')" value="'.$invoice_qty.'" max="'.$rest.'" min="0" required>',
              'price' => $detail->price,
              'invoice_price' => '<input type="number" name="detail['.$no.'][price]" id="invoice_price_'.$no.'" class="form-control price" onchange="calculateAmount('.$no.')" value="'.$detail->price.'" min="0" required>',
              'amount' => $ordered->qty * $detail->price,
              'invoice_amount' => '<input type="hidden" id="invoice_total_'.$no.'" value="'.$invoice_qty * $detail->price.'" class="invoice_amount"><span id="text_invoice_total_'.$no.'">'.$invoice_qty * $detail->price.'</span>',
            ];
          }
      }

      $data['invoice'] = $invoice;

      return response()->json($data);
    }

    public function printInvoice(Request $request) {
		$po = Po::where("prefix", $request->input("po"))->first();
		$invoice = Invoice::where("prefix", $request->input("invoice"))->first();
		$data = [];
		foreach ($invoice->detail as $no => $detail) {
			++$no;
			$ordered = PoDetail::where("t_po_master_id", $po->id)->where("m_products_id", $detail->m_products_id)->first();
			$received = $detail->where("m_products_id", $detail->m_products_id)->sum("qty") - $detail->qty;
			$invoice_qty = $detail->qty;
			$rest = $ordered->qty - $received;
			$data["products"][] = (object) [
				"no" => $no,
				"product_code" => $detail->product->product_code,
				"product_name" => $detail->product->product_name,
				"qty" => $ordered->qty,
				"received" => $received,
				"invoice_qty" => $invoice_qty,
				"rest" => $rest,
				"price" => $detail->price,
				"invoice_price" => $detail->price,
				"amount" => $ordered->qty * $detail->price,
				"invoice_amount" => $invoice_qty * $detail->price,
			];
		}

		return view('admin.processPo._printInvoice', [
			"po"		=> $po,
			"invoice"	=> $invoice,
			"products"	=> $data["products"],
			"company"	=> CompanyProfile::first(),
			"pageTitle"	=> 'Process Purchase Order',
		]);
    }

}
