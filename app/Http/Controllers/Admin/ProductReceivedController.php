<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ProductReceivedRequest;

use App\Po;
use App\Invoice;
use App\InvoiceHistories;
use App\Stock;
use App\StockHistories;
use App\Zones;
use App\Library;
use App\ProductCategories;
use App\Warehouse;
use App\WarehouseZones;
use App\WarehouseRacks;
use App\WarehouseCells;
use Datatables;

class ProductReceivedController extends Controller
{
    public function index()
    {
    	$po = isset($_GET['po']) ? $_GET['po'] : null;
		$po = Po::where('prefix',$po)->first();
		if (empty($po)) {
		$po = new Po;
		}
		$pos = Po::select('prefix')->where('status','!=',3)->limit(5)->orderBy('prefix')->orderBy('created_at')->get();
    	return view('admin.productReceived.index',[
    		'pageTitle' => 'Product Received',
    		'po' => $po,
	        'pos' => $pos
		]);
    }

    public function store(ProductReceivedRequest $request)
    {
    	$data = $request->except(['purchase_prefix']);
    	foreach ($request->batch as $key => $batch) {
    		if (isset($batch['receive']) && isset($batch['cell_id'])) {
				$stock = Stock::find($batch['id']);
				$stock->stock_type = 'AC';
				$stock->status = 3;
				$stock->zone_type = $batch['zone_type'];
				$stock->cell_id = $batch['cell_id'];
				$stock->save();

				$newBatch = Stock::whereHas('invoice', function($inv) use ($stock) {
	    	    		$inv->where('t_po_master_id',$stock->invoice->t_po_master_id);
	    	    	})->where('status',0)->where('m_products_id',$stock->m_products_id)
					->first();
				if ($newBatch) {
					$newBatch->update(['status' => 2]);
				}

				if ($batch['qty'] > 0) {
					$rejected = new StockHistories([
						'qty' => $batch['qty'],
						'is_in' => 0,
						'note' => 'rejected',
						]);
					$stock->histories()->save($rejected);
				}
    		}
    	}
    	return redirect('/app/po/receive?po='.$request->purchase_prefix)
            ->with('status', 'success')
            ->with('message', 'Selected Product Added to <a href="'.url('/app/po/readyToStockIn').'">Ready to Stock In</a><br />
                    Download QR Code <a target="_blank" href="'.url('/app/po/readyToStockIn/qrcode').'">Here</a>');
    }

    public function loadData(Request $request)
    {

    }

    public function loadProductReceived(Request $request)
    {
    	$po = Po::where('prefix', $request->po)->first();
    	$data = [];

    	if ($po) {
	    	$stock = Stock::whereHas('invoice', function($inv) use ($po) {
	    	    		$inv->where('t_po_master_id',$po->id);
    	    	})->where('status',2)->get();
	    	// $data['all'] = $po;
	    	$data['po'] = [
		    	'prefix' => $po->prefix,
		    	'supplier_name' => $po->supplier->supplier_name,
		    	'po_date' => $po->po_date,
		    	'email' => $po->email,
	    	];
	    	// $data['received'] = $stock;
	    	$no = 1;
	    	$data['readyToReceive'] = [];
	    	foreach ($stock as $batch) {
	    		$cold_qty = $batch->histories->first()->qty;
	    		$data['readyToReceive'][] = [
		    		'action' => '<input type="checkbox" name="batch['.$no.'][receive]" class="receive" /><input type="hidden" name="batch['.$no.'][id]" value="'.$batch->id.'" />',
		    		'inv_no' => $batch->invoice->prefix,
		    		'date' => $batch->received_date,
		    		'invoice' => '<b>No : </b> '.$batch->invoice->prefix.'<br><b>Date : </b> '.$batch->received_date,
		    		'cold_batch_id' => $batch->batch_id,
		    		'product' => '<b>Code : </b> '.$batch->product->product_code.'<br><b>Name : </b> '.$batch->product->attribute('name'),
		    		'qty' => '<b>Qold : </b> <span id="cold-'.$no.'">'.$cold_qty.'</span><br><b>Receive : </b><span id="receive-'.$no.'">'.$cold_qty.'</span>',
		    		'product_code' => $batch->product->product_code,
		    		'product_name' => $batch->product->attribute('name'),
		    		'stock_type' => Library::batchStatus($batch->stock_type),
		    		'cold_qty' => '<span id="cold-'.$no.'">'.$cold_qty.'</span>',
		    		'receive_qty' => '<span id="receive-'.$no.'">'.$cold_qty.'</span>',
		    		'reject_qty' => '<input type="number" class="form-control qty reject" name="batch['.$no.'][qty]" value="0" min="0" max="'.$cold_qty.'" id="reject-'.$no.'" onchange="setQty('.$no.')" disabled />',
		    		'zone_type' => $this->zoneType($no,$batch->product->m_product_categories_id),
		    		'warehouse' => $this->warehouse($no,$batch->product->m_product_categories_id),
		    		'cell_id' => '<select disabled class="form-control select2 cell" style="width:200px" name="batch['.$no.'][cell_id]" id="cells-'.$no.'"></select>'
	    		];
	    		$no++;
	    	}
    	}
    	return response()->json($data);
    }

    protected function zoneType($no,$category)
    {
    	$zones = Zones::all();
    	$zoneType = '<select disabled class="form-control price zone" style="width: 120px" name="batch['.$no.'][zone_type]" id="zone_type-'.$no.'" onchange="getCells('.$no.','.$category.')">';
    	$zoneType .= '<option value="">Please select</option>';
    	foreach ($zones as $zone) {
    		$zoneType .= '<option value="'.$zone->id.'">'.$zone->name.'</option>';
    	}
    	$zoneType .= '</select>';
    	return $zoneType;
    }

    protected function warehouse($no,$category)
    {
    	$warehouse = Warehouse::all();
    	$list = '<select disabled style="width:150px" class="form-control price warehouse" id="warehouse-'.$no.'" onchange="getCells('.$no.','.$category.')">';
		$list .= '<option value="">Please select</option>';
    	foreach ($warehouse as $lists) {
    		$list .= '<option value="'.$lists->id.'">'.$lists->name.'</option>';
    	}
    	$list .= '</select>';
    	return $list;
    }

    public function getCells($zone,$warehouse,$category)
    {
    	global $categories;
    	$categories = [];
    	$category = ProductCategories::find($category);
		array_unshift($categories, $category->id);
		if ($category->cat_parent) {
			$this->getCategoriesTree($category->cat_parent);
		}

    	$cells = WarehouseCells::select('id','prefix as text')->whereHas('rack',function($rack) use ($zone,$warehouse,$categories) {
			$rack->whereHas('zone', function($wz) use ($zone,$warehouse) {
				$wz->where('type', $zone)->where('m_warehouse_id',$warehouse);
			})->whereHas('categories', function($cat) use ($categories) {
				$cat->whereIn('id', $categories);
			});
		})->where('status',0)->get();

    	return $cells;
    }

    protected function getCategoriesTree($id)
	{
		global $categories;
		$category = ProductCategories::find($id);
		array_unshift($categories, $category->id);
		if ($category->cat_parent) {
			$this->getProductCategoriesChild($category->cat_parent);
		}
	}

	protected function getProductCategoriesChild($category)
	{
		global $categories;
		$category = ProductCategories::where('id',$category)->first();
		array_unshift($categories, $category->cat_parent);
		if ($category->cat_parent) {
			$this->getProductCategoriesChild($category->cat_parent);
		}
	}

	public function searchCells($zone,$warehouse,$category)
	{
		$prefix = isset($_GET['prefix']) ? $_GET['prefix'] : null;

		if ($prefix) {
			$cells = WarehouseCells::select('id','prefix as text')->whereHas('rack',function($rack) use ($zone,$warehouse) {
				$rack->whereHas('zone', function($wz) use ($zone,$warehouse) {
					$wz->where('m_warehouse_id',$warehouse);
				});
			})->where('prefix','like','%'.$prefix.'%')->where('status',0)->get();
		}else{
			$cells = $this->getCells($zone,$warehouse,$category);
		}

    	return $cells;

	}

}
