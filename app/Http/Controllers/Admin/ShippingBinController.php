<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

use App\ShippingBin;

use App\Library;
use Yajra\Datatables\Facades\Datatables;

class ShippingBinController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$bins = ShippingBin::all();
			return Datatables::of(collect($bins))->addColumn("status", function ($bin) {
				return $bin->in_use == 0 ? '<span class="label label-success">Available</span>' : '<span class="label label-info">In use</span>';
			})->addColumn("action", function ($bin) {
				return '<form action="' . url('app/shippingBin/' . $bin->id) . '" method="POST" id="delete-' . $bin->id . '">' . csrf_field() . method_field("DELETE") . (Auth::user()->can('update-shippingBin') ? '<a href="' . url('app/shippingBin/' . $bin->id . '/edit') . '" class="btn-editShippingBin btn btn-warning btn-xs btn-flat"><i class="fa fa-pencil"></i> Update</a> ' : '') . (Auth::user()->can('update-shippingBin') ? '<button type="button" class="btn btn-danger btn-xs btn-flat" onclick="confirmDelete(\''.$bin->id.'\',\''.ucwords("shippingBin").'\')"><i class="fa fa-trash"></i> Delete</button>' : '') . '</form>';
			})->make(true);
		}
		return view("admin.shippingBin.index", [
			"pageTitle"	=> "Shipping Bin",
			"active"	=> ["setting", "shippingBin"]
		]);
	}

	public function show($id) {}

	public function create() {}

	public function edit($id) {}

	public function store(Request $request) {
		$insert["bin_id"]	=	$request->input("bin-id");
		if(empty($request->input("id"))) {
			$inserted = ShippingBin::create($insert)->id;
			$editmode = false;
		} else {
			$old = ShippingBin::findOrFail($request->input("id"));
			$inserted = $old->id;
			$old->update($insert);
			$editmode = true;
		}
		if(Library::saveTrail("shippingBin", $editmode ? "update" : "create", $inserted)) {
			return redirect("app/shippingBin")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(Request $request, $id) {}

	public function destroy($id) {
		Library::saveTrail("shippingBin", "destroy", $id, "Name: " . ShippingBin::find($id)->bin_id);
		ShippingBin::find($id)->delete();
        return redirect("app/shippingBin")->with("status", "success")->with("message", "Data deleted successfully");
	}
}
