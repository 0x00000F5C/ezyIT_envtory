<?php

namespace App\Http\Controllers\Admin;

use App\Carrier;
use App\PackingLogistic;
use App\ShippingBin;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\ShippingList;
use App\Order;
use App\Shop;

use App\Library;
use Datatables;

class ShippingListController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$shippings = Order::getReadyToShip();
			return Datatables::of($shippings)->addColumn("date_actual", function ($shipping) {
				return date_format(date_create_from_format("Y-m-d", $shipping->date), "d F Y");
			})->addColumn("customer_name_actual", function ($shipping) {
				return $shipping->customer_name . " " . $shipping->customer_last_name;
			})->addColumn("shop_platform", function ($shipping) {
				return $shipping->shop()->first()->shop_name;
			})->addColumn("items_count", function ($shipping) {
				return count($shipping->detail()->get());
			})->addColumn("items_actual", function ($shipping) {
				return $shipping->detail()->get();
			})->addColumn("action", function ($shipping) {
			    $result = '';
			    if($shipping->status == 7){
                    $result .= '<a href="' . url('app/shippingList/' . $shipping->id) . '" class="btn-showShippingList btn btn-flat btn-xs btn-success" data-value="' . $shipping->id . '"><i class="fa fa-share"></i> Prepare</a>';
                }else{
                    $result .= '<a href="' . url("app/shippingStatus/" . $shipping->id . "/print") . '"><button type="button" class="btn-printShippingList btn btn-xs btn-flat btn-warning"><i class="fa fa-print"></i> Print shipping label</button></a>';
                }

//                $result .= $shipping->status != 7 ? '<a href="' . url('app/order/' . $shipping->id) . '" class="btn-showShippingList btn btn-flat btn-xs btn-default" data-value="' . $shipping->id . '"><i class="fa fa-share"></i> Show</a> <a href="' . url("app/shippingStatus/" . $shipping->id . "/print") . '"><button type="button" class="btn-printShippingList btn btn-xs btn-flat btn-warning"><i class="fa fa-print"></i> Print shipping label</button></a>' : '<a href="' . url('app/shippingList/' . $shipping->id . "/edit") . '" class="btn-completeShippingList btn btn-flat btn-xs btn-primary" data-value="' . $shipping->id . '"><i class="fa fa-check"></i> Complete shipping</a>';
//                $result .= $shipping->status == 7 ? '<a href="' . url('app/order/' . $shipping->id) . '" class="btn-showShippingList btn btn-flat btn-xs btn-default" data-value="' . $shipping->id . '"><i class="fa fa-share"></i> Show</a> <a href="' . url("app/shippingStatus/" . $shipping->id . "/print") . '"><button type="button" class="btn-printShippingList btn btn-xs btn-flat btn-warning"><i class="fa fa-print"></i> Print shipping label</button></a>' : '<a href="' . url('app/shippingList/' . $shipping->id . "/edit") . '" class="btn-completeShippingList btn btn-flat btn-xs btn-primary" data-value="' . $shipping->id . '"><i class="fa fa-check"></i> Complete shipping</a>';

                return $result;
			})->make(true);
		}
		return view("front.shippingList.index", [
			"pageTitle"	=> "Shipping List Management",
			"active"	=> ["setting", "shippingList"]
		]);
	}

    public function show($id)
    {
        $model = Order::find($id);//
        $packingLogistic =PackingLogistic::pluck('packing_name', 'id')->all();
        $couriers =Carrier::pluck('Name', 'id')->all();
        $shippingBins =ShippingBin::pluck('bin_id', 'id')->all();
        return view('front.shippingList.prepare',[
            'model' => $model,
            'details' => $model->detail,
            'shops' => Shop::all(),
            'couriers' => $couriers,
            'shippingBins' => $shippingBins,
            'packingLogistic' => $packingLogistic,
            'pageTitle' => 'Prepare Shipping'
        ]);
    }

//	public function create() {}
    public function store(Request $request) {
	    $input = $request->all();
	    $input['prepare_date'] = Carbon::now();
	    $order = Order::find($input['order_id']);
        $order->update(['status' => 8]);
        ShippingList::create($input);
        return redirect('app/shippingList')
            ->with('status', 'success')
            ->with('message', 'Prepare Shiping Done!');
    }

    public function checkout(){
        $shippingBins =ShippingBin::pluck('bin_id', 'id')->all();
        return view('front.shippingList.checkout',[
            'shops' => Shop::all(),
            'shippingBins' => $shippingBins,
            'pageTitle' => 'Shipping Checkout'
        ]);
    }

    public function saveCheckout(Request $request){
        $input = $request->all();
        $result = array('status' => true);

        if(count($input['det'])>0){
            foreach ($input['det'] as $key => $item){
                if((int)$item['status']==1){
                    $shippingLists = ShippingList::where('shipping_bin_id', $input['bin_id'])
                        ->where('id', (int)$item['id'])
                        ->where('status', 0)
                        ->where('order_id', $item['order_id'])->first();
                    if($shippingLists){
                        $shippingLists->status=1;
                        $shippingLists->courier_name=$item['courier_name'];
                        $order = Order::find($item['order_id']);
                        $order->status =9;
                        $order->save();
                        $shippingLists->save();
                    }
                }
            }
            \Session::flash('status', 'success');
            \Session::flash('message', 'Shipping Checkout is done !');

        }else{
            $result['msg']="Shipping Checkout Failed !";
            $result['status']=false;
        }

        return $result;
    }

    public function detailByBinId(Request $request) {
        $shippingLists = ShippingList::where('shipping_bin_id', $request['bin_id'])->where('status', 0)->get();
        $result = array('status' => true);
        if(count($shippingLists)>0){
            foreach ($shippingLists as $key => $item){
                if($item->status==0){
                    $item->status_lbl='Ready to Ship';
                }else{
                    $item->status_lbl='Shiped';
                }

                $shippingLists[$key]=$item;
            }
            $result['data'] = $shippingLists;
        }else{
            $result['msg']="Shipping Not Found";
            $result['status']=false;
        }
        return $result;
    }
    public function checkBarcode(Request $request) {
        $shippingList = ShippingList::where('shipping_bin_id', $request['bin_id'])
//            ->where('status', 0)
            ->where('tracking_number', $request['barcoder_tracking_number'])->first();
        $result = array('status' => true);
        if($shippingList){
            $dt=Carbon::now();
            $shippingList->Date =$dt->format('M d, Y');
            $shippingList->Time=$dt->format('h:i A');
            $shippingList->courier_lbl=$shippingList->courier->Name;
            $result['data'] = $shippingList;
        }else{
            $result['msg']="Barcode Not Found";
            $result['status']=false;
        }
        return $result;
    }

    public function getTrackerNumber($id){
        $order =Order::where("id", $id)->firstOrFail();
//        if ($order->shop->platform_id == 1) {
//            $this->setStatusInLazada($order->detail, $order->shop);
//        }

        $arrItemIds = [];
        foreach ($order->detail as $d) {
            $arrItemIds[] = $d->order_item_id;
        }
        $itemsIds = implode(',', $arrItemIds);
//        return $itemsIds;
        return Order::SetStatusToPackedByMarketplace($order->shop, $itemsIds, $order->shop_order_id);
    }
    public function destroy() {}
}
