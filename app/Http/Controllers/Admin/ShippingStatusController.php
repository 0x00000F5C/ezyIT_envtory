<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\ShippingStatus;
use App\CompanyProfile;

use App\Library;
use Datatables;

class ShippingStatusController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$shippings = collect([
				(object) [
					"id" => 1, 
					"shipping_order_id" => "LZDSHP1410160000001", 
					"shipping_order_date" => "14 October 2016", 
					"shipping_customer" => "Budhi", 
					"shipping_platform" => "Lazada", 
					"shipping_courier" => "Lazada express", 
					"shipping_tracking" => "BDOAA123456789", 
					"shipping_items" => "10",
					"shipping_ready" => true
				],
				(object) [
					"id" => 2, 
					"shipping_order_id" => "LZDSHP1410160000002", 
					"shipping_order_date" => "14 October 2016", 
					"shipping_customer" => "Budhi", 
					"shipping_platform" => "Lazada", 
					"shipping_courier" => "DHL", 
					"shipping_tracking" => "BDOAA123456788", 
					"shipping_items" => "5",
					"shipping_ready" => false
				],
				(object) [
					"id" => 3, 
					"shipping_order_id" => "LZDSHP1410160000003", 
					"shipping_order_date" => "14 October 2016", 
					"shipping_customer" => "Budhi", 
					"shipping_platform" => "Lazada", 
					"shipping_courier" => "Lazada express", 
					"shipping_tracking" => "BDOAA123456787", 
					"shipping_items" => "8",
					"shipping_ready" => false
				],
				(object) [
					"id" => 4, 
					"shipping_order_id" => "LZDSHP1410160000004", 
					"shipping_order_date" => "14 October 2016", 
					"shipping_customer" => "Budhi", 
					"shipping_platform" => "Lazada", 
					"shipping_courier" => "FedEx", 
					"shipping_tracking" => "BDOAA987654321", 
					"shipping_items" => "14",
					"shipping_ready" => true
				],
				(object) [
					"id" => 5, 
					"shipping_order_id" => "LZDSHP1410160000005", 
					"shipping_order_date" => "14 October 2016", 
					"shipping_customer" => "Budhi", 
					"shipping_platform" => "Lazada", 
					"shipping_courier" => "Lazada express", 
					"shipping_tracking" => "BDOAA887654321", 
					"shipping_items" => "21",
					"shipping_ready" => false
				],
				(object) [
					"id" => 6, 
					"shipping_order_id" => "LZDSHP1410160000006", 
					"shipping_order_date" => "14 October 2016", 
					"shipping_customer" => "Budhi", 
					"shipping_platform" => "Lazada", 
					"shipping_courier" => "Lazada express", 
					"shipping_tracking" => "BDOAA787654321", 
					"shipping_items" => "32",
					"shipping_ready" => true
				]
			]);
			return Datatables::of($shippings)->addColumn("action", function ($shipping) {
				return '<a href="' . url('shippingStatus/' . $shipping->id . "/show") . '" class="btn-showShipping btn btn-flat btn-xs btn-primary" data-value="' . $shipping->id . '"><i class="fa fa-clipboard"></i> Show shipping status</a> <a href="' . url('app/shippingStatus/' . $shipping->shipping_order_id . "/print") . '"><button type="submit" class="btn btn-flat btn-xs btn-default"><i class="fa fa-print"></i> Print</button></a>';
			})->make(true);
		}
		return view("front.shippingStatus.index", [
			"pageTitle"	=> "Shipping Status",
			"active"	=> ["setting", "shippingStatus"]
		]);
	}
	public function show() {}
	public function create() {}
	public function edit() {}
	public function store() {}
	public function update() {}
	public function destroy() {}

	public function printReceipt($id){
        // $model = ShippingStatus::where("order_no", $id)->firstOrFail();
        return view("front.shippingStatus.print",[
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => true
        ]);
	}

	public function getPDF($id) {
		// $model = ShippingStatus::where("order_no", $id)->firstOrFail();
		return PDF::loadView("front.shippingStatus.pdf", [
			"company" => CompanyProfile::first(),
			"currency" => Library::currency(Library::defaultCurrency()),
			"print" => false
		])->download("Shipment - " . $id . ".pdf");
	}
}