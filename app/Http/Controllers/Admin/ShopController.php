<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\ShopRequest;
use App\Http\Requests\ShopCategoryRequest;

use App\Shop;
use App\ShopCategories;
use App\Library;
use App\AuditTrails;
use Datatables;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // If request is ajax, return data to datatable
        if (request()->ajax()) {
            $shop = Shop::all();
            return Datatables::of($shop)
              ->addColumn('platform', function ($shop) {
                  return Library::shopPlatform($shop->platform_id);
              })
              ->addColumn('actions', function ($shop) {
                  return Library::gridAction($shop,'shop');
              })
              ->make(true);
        }
        return view('admin.shop.index',[
            'pageTitle' => 'Shop',
            'active' => ['setting','shop']
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new Shop;
        return view('admin.shop.form',[
            'model' => $model,
            'platforms' => Library::shopPlatform(),
            'categories' => ShopCategories::orderBy('name')->get(),
            'modes' => Library::shopMode(),
            'pageTitle' => 'Add New Shop'
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(ShopRequest $request)
    {
        $data = $request->all();
        $data['status'] = 1;

        if (Library::saveTrail("shop", "create", Shop::create($data)->id)) {
            return redirect('app/shop')
              ->with('status', 'success')
              ->with('message', 'New Shop Successfully Added');
        }

        return redirect()->back()
            ->with('status', 'danger')
            ->with('message', 'Failed to Add Shop');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $model = Shop::find($id);
		if($model === null) { abort(404); }
        return view('admin.shop.form',[
            'model' => $model,
            'platforms' => Library::shopPlatform(),
            'categories' => ShopCategories::orderBy('name')->get(),
            'modes' => Library::shopMode(),
            'pageTitle' => 'Update Shop'
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(ShopRequest $request, $id)
    {
        $data = $request->all();

        $shop = Shop::find($id);

        if ($shop->update($data)) {
			Library::saveTrail("shop", "update", $shop->id);
            return redirect('app/shop')
              ->with('status', 'success')
              ->with('message', 'Shop Successfully Updated');
        }

        return redirect()->back()
            ->with('status', 'danger')
            ->with('message', 'Failed to Update Shop');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        Library::saveTrail("shop", "destrooy", $shop->id, "Name: " . $shop->shop_name);
		$shop->delete();
        return redirect()->back()
                ->with('status', 'success')
                ->with('message', 'Shop Deleted');
    }

    public function storeCategory(ShopCategoryRequest $request)
    {
        ShopCategories::create([
            'name' => $request->name
        ]);
        return [
            'success' => true, 
            'message' =>  'Category '.$request->name.' Saved',
            'categories' => ShopCategories::select('id','name')->orderBy('name')->get()
        ];
    }

    public function export($type)
    {
      $data = Shop::with('category')
              // ->select('shop_name')
              // ->select('category.name')
              // ->with(['platform', function($q){

              // }])
              ->first();
      return $data->category->name;
      $title = 'Users Data';

      switch ($type) {
          case 'excel':
            Excel::create($title, function($excel) use($data) {
              $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->setAutoSize(false);
                $sheet->fromArray($data);
              });
            })->export('xls');
            break;

          case 'pdf':
            $pdf = PDF::loadView('admin.user.report', [
              'users'=>$data,
              'title'=>$title
              ]);
            return $pdf->stream();
            break;

          case 'html':
            return view('admin.user.report', [
              'users'=>$data,
              'title'=>$title
              ]);
          default:
            abort(404);
            break;
      }
    }
	
	public function getShopsByName(Request $request) {
		return Shop::where("shop_name", "like", "%" . $request->input("q") . "%")->get();
	}

  public function testApi($account,$api_key)
  {
    $api = Library::lazadaShopApi($account,$api_key,'GetBrands',5,0);
    if ($api) {
        $client = new \GuzzleHttp\Client();
        $res = json_decode($client->get($api)->getBody(), true);
        if (!empty($res["SuccessResponse"])) {
            return ['message'=>'ok'];
        }
    }
    return ['message'=>'error'];
  }
}
