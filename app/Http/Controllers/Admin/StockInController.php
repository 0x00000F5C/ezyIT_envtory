<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\StockIn;
use App\Stock;
use App\SkuAttributes;

use Artisan;

class StockInController extends Controller {
	public function index() {
		$stockIn = Stock::where('status',3)->paginate(10);
//		return $stockIn['data'];
		return view("admin.stockIn.index", [
			"pageTitle"	=> "Stock In",
			'stockIn' => $stockIn
		]);
	}
	public function show() {}
	public function create() {}
	public function edit() {}

	public function store() {
		$id = request()->id;
		$stock = Stock::find($id);
		$stock->update(['status'=>1]);

		$skuAttr = SkuAttributes::where('m_product_sku_id',$stock->product->sku->id)
								->where('name','quantity');
		$dataQty = [
			'name' => 'quantity',
			'value' => $stock->product->stock_active,
		];

		if ($skuAttr->count()) {
			$skuAttr->update($dataQty);
		}else{
			$qty = new SkuAttributes($dataQty);
			$stock->product->sku->attributes()->save($dataQty);
 		}

		Artisan::queue('lazada:update-price-qty', [
        'product_id' => $stock->product->id
    ]);
		return ['message' => 'Batch Added to Stock'];
	}

	public function update() {}
	public function destroy() {}
}
