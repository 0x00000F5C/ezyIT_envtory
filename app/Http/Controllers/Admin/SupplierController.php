<?php
/*
 * Supplier controller
 * Table: Supplier
 * 
 * — Eeno
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Requests;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\SupplierRequest;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Supplier;
use App\SupplierContacts;
use App\ProductCategories;
use App\Library;
use App\AuditTrails;
use Datatables;

class SupplierController extends Controller {
	public function index() {
		if (request()->ajax()) {
		$suppliers = Supplier::all();
			return Datatables::of($suppliers)->addColumn("supplier_mode_proc", function ($supplier) {
				switch($supplier->supplier_mode){
					case "1": {
						return "Consigment";
					}
					case "2": {
						return "Direct";
					}
					default: {
						return "Undefined";
					}
				}
			})->addColumn("supplier_pic1", function ($supplier) {
				return $supplier->contacts()->first()->contact_name;
			})->addColumn("supplier_phone1", function ($supplier) {
				return $supplier->contacts()->first()->phone;
			})->addColumn("supplier_email1", function ($supplier) {
				return $supplier->contacts()->first()->email;
			})->addColumn("action", function ($supplier) {
				return Library::gridAction($supplier, "supplier");
			})->make(true);
		}
		return view("admin.supplier.index", [
			"pageTitle"	=> "Suppliers",
			"active"	=> ["setting", "supplier"]
		]);
	}

	public function create() {
		$model = new Supplier;
		$model->supplier_categories = json_decode($model->supplier_categories);
		return view("admin.supplier.form",[
		  "pageTitle"	=> "Create New Supplier",
		  "model"		=> $model,
		  "categories"	=> ProductCategories::all()
		]);
	}

	public function store(SupplierRequest $request) {
		// return $request->input("supplier-categories");
		$insert["supplier_name"]		= $request->input("company-name");
		$insert["supplier_prefix"]		= $request->input("company-prefix");
		$insert["supplier_address"]		= $request->input("company-address");
		$insert["supplier_country"]		= $request->input("company-country");
		$insert["supplier_state"]		= $request->input("company-state");
		$insert["supplier_city"]		= $request->input("company-city");
		$insert["supplier_zipcode"]		= $request->input("company-zipcode");
		$insert["supplier_mode"]		= 2;
		$insert["supplier_min_po"]		= $request->input("company-minpo") ? $request->input("company-minpo") : 0;
		$insert["supplier_max_po"]		= $request->input("company-maxpo") ? $request->input("company-maxpo") : 0;
		$insert["supplier_categories"]	= count($request->input("supplier-categories")) > 0 ? json_encode($request->input("supplier-categories")) : "[]";
		$insert["data_status"]		= 2;
		$supplier = Supplier::create($insert);
        if(Library::saveTrail("supplier", "create", $supplier->id)) {
        	$supplier->categories()->attach($request->input("supplier-categories"));
			for($i = 0; $i < count($request->input("contact-name")); $i++){
				SupplierContacts::create(["supplier_id" => $supplier->id, "contact_name" => $request->input("contact-name")[$i], "email" => $request->input("contact-email")[$i], "phone" => $request->input("contact-phone")[$i]]);
			}
			return redirect("app/supplier")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function show($id = 0) {
		//
	}

	public function edit($id) {
		$model = Supplier::find($id);
		if($model === null) { abort(404); }
		$model->supplier_pic		=	json_decode($model->supplier_pic);
		$model->supplier_phone		=	json_decode($model->supplier_phone);
		$model->supplier_email		=	json_decode($model->supplier_email);
		$model->supplier_categories	=	json_decode($model->supplier_categories);
		return view("admin.supplier.form",[
		  "pageTitle"	=> "Create New Supplier",
		  "model"		=> $model,
		  "categories"	=> ProductCategories::all(),
		  "contacts"	=> $model->contacts()->get()
		]);
	}

	public function update(SupplierRequest $request, $id) {
		$insert["supplier_name"]		= $request->input("company-name");
		$insert["supplier_prefix"]		= $request->input("company-prefix");
		$insert["supplier_address"]		= $request->input("company-address");
		$insert["supplier_country"]		= $request->input("company-country");
		$insert["supplier_state"]		= $request->input("company-state");
		$insert["supplier_city"]		= $request->input("company-city");
		$insert["supplier_zipcode"]		= $request->input("company-zipcode");
		$insert["supplier_mode"]		= 2;
		$insert["supplier_min_po"]		= $request->input("company-minpo") ? $request->input("company-minpo") : 0;
		$insert["supplier_max_po"]		= $request->input("company-maxpo") ? $request->input("company-maxpo") : 0;
		$insert["supplier_categories"]	= count($request->input("supplier-categories")) > 0 ? json_encode($request->input("supplier-categories")) : "[]";
		$insert["data_status"]		= 2;
		$supplier = Supplier::find($id);
		if($supplier->update($insert)) {
			$supplier->categories()->sync($request->input("supplier-categories"));
			$supplier->contacts()->delete();
			for($i = 0; $i < count($request->input("contact-name")); $i++){
				SupplierContacts::create(["supplier_id" => $id, "contact_name" => $request->input("contact-name")[$i], "email" => $request->input("contact-email")[$i], "phone" => $request->input("contact-phone")[$i]]);
			}
			Library::saveTrail("supplier", "update", $supplier->id);
			return redirect("app/supplier")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($id) {
		Library::saveTrail("supplier", "destroy", $id, "Name: " . Supplier::find($id)->supplier_name);
		Supplier::find($id)->contacts()->delete();
		Supplier::find($id)->delete();
        return redirect("app/supplier")->with("status", "success")->with("message", "Data deleted successfully");
	}
	
	public function getSuppliers(Request $request) {
		$model = Supplier::where("supplier_categories", "REGEXP", "[[:<:]]" . $request->val . "[[:>:]]")->get();
		if (count($model) > 0) {
			for($i = 0; $i < count($model); $i++) {
				/* $model[$i]->supplier_pic		=	json_decode($model[$i]->supplier_pic);
				$model[$i]->supplier_phone		=	json_decode($model[$i]->supplier_phone);
				$model[$i]->supplier_email		=	json_decode($model[$i]->supplier_email); */
				$model[$i]->supplier_categories	=	json_decode($model[$i]->supplier_categories);
			}
		}
		return [
			"status" => true,
			"message" => "Successfully returned data",
			"data" => $model->toArray()
		];
	}
	
	public function genPrefix(Request $request) {
		return ["st" => true, "data" => strtoupper(substr($request->input("q"), 0, 1)) . str_pad(count(Supplier::where("supplier_prefix", "like", strtoupper(substr($request->input("q"), 0, 1)) . "%")->get()) + 1, 2, "0", STR_PAD_LEFT)];
	}
}