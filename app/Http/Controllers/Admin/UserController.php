<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as FacRequest;

use App\Http\Requests;
use App\Http\Requests\UserRequest;
use App\Http\Controllers\Controller;

use App\User;
use App\Role;
use App\Permission;
use App\Library;
use App\AuditTrails;

use Auth;
use Route;
use Datatables;
use Excel;
use PDF;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // If request is ajax, return data to datatable
      if (request()->ajax()) {
        $users = User::all();
        return Datatables::of($users)
          ->addColumn('role', function($user) {
            return $user->roles()->pluck('name')->pop();
          })
          ->addColumn('action', function ($user) {
              // This for Edit and Delete. Just Sent data parameter and route
              return Library::gridAction($user,'user');
          })
          ->make(true);
      }
      // If not ajax, return view
      return view('admin.user.index',[
        'pageTitle' => 'User',
        'active' => ['setting','user']
      ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $model = new User;
        $model->active = 1;
        return view('admin.user.form',[
          'pageTitle' => 'Create New User',
          'model' => $model,
          'active' => ['setting', 'user'],
          'roles' => Role::all()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $request)
    {
      $data = $request->except('password','role');
      $data["password"] = bcrypt($request->password);
      if (Library::saveTrail("user", "create", $insertid = User::create($data)->id, "")) {
        User::find($insertid)->assignRole($request->role);
        return redirect('app/user')
          ->with('status', 'success')
          ->with('message', 'New User Successfully Created');
      }
      return redirect()->back()
        ->with('status', 'danger')
        ->with('message', 'Failed to Create User');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $model = User::find($id);
	  if($model === null) { abort(404); }
      // Check User Authoriz to create superadmin user
      $this->cekUserAuthoriz($model);
      return view('admin.user.form',[
        'pageTitle' => 'Update User',
        'model' => $model,
        'active' => ['setting', 'user'],
        'roles' => Role::all()
      ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
      $data = $request->except('name', 'password','role');
      $data['active'] = $request->active == 1 ? 1 : 0;
      if (!empty($request->password)) {
        $data["password"] = bcrypt($request->password);
      }
      $user = User::find($id);
      if ($user->update($data)) {
		Library::saveTrail("user", "update", $user->id);
        $user->syncRoles($request->role);
        return redirect('app/user')
          ->with('status', 'success')
          ->with('message', 'User Updated');
      }
      return redirect()->back()
        ->with('status', 'danger')
        ->with('message', 'User Update Failed');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        $this->cekUserAuthoriz($user);
		Library::saveTrail("user", "destroy", $user->id, "Name: " . $user->name);
        $user->delete();
        return redirect()->back()
                ->with('status', 'success')
                ->with('message', 'User Deleted');
    }

    /*
    * Check User permission for creating user that has role
    */
    protected function cekUserAuthoriz($user)
    {
      if ($user->can('create-role') && !Auth::user()->can('create-role')) {
        abort(403);
      }
    }

    public function export($type)
    {
      $data = User::selectRaw('name as username, firstname, lastname, email')
              ->get();
      $title = 'Users Data';

      switch ($type) {
          case 'excel':
            Excel::create($title, function($excel) use($data) {
              $excel->sheet('Sheet 1', function($sheet) use($data) {
                $sheet->setAutoSize(false);
                $sheet->fromArray($data);
              });
            })->export('xls');
            break;

          case 'pdf':
            $pdf = PDF::loadView('admin.user.report', [
              'users'=>$data,
              'title'=>$title
              ]);
            return $pdf->stream();
            break;

          case 'html':
            return view('admin.user.report', [
              'users'=>$data,
              'title'=>$title
              ]);
          default:
            abort(404);
            break;
      }
    }
}
