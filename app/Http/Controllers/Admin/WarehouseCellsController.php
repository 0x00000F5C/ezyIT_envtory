<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Warehouse;
use App\WarehouseZones;
use App\WarehouseRacks;
use App\WarehouseCells;
use App\Http\Requests\WarehouseRacksRequest;
use App\Library;
use PDF;
use Barcode;

class WarehouseCellsController extends Controller {
	public function index() {}
	public function create() {}
	public function edit() {}
	public function store() {}
	public function update() {}
	public function destroy() {}
	
	public function genLocations(Request $request, $wid, $zid, $rid) {
		$ret = [];
		for ($i = 0; $i < $request->input("row"); $i++) {
			for ($ii = 0; $ii < $request->input("col"); $ii++) {
				WarehouseCells::create([
					"prefix"				=> $request->input("prefix") . str_pad($i + 1, 2, "0", STR_PAD_LEFT) . str_pad($ii + 1, 2, "0", STR_PAD_LEFT),
					"colomn"				=> $i + 1,
					"row"					=> $ii + 1,
					"barcode"				=> 0,
					"m_warehouse_racks_id"	=> $rid
				]);
			}
		}
		return ["st" => true, "data" => $ret];
	}

    public function getBarcodes(Request $request, $wid, $zid) {
		if (empty($request->input("cells"))) {
			abort(404);
		}

		$a = "";
		foreach($request->input("cells") as $cell) {
			$a .= '<div style="text-align: center; margin-bottom: 16px;"><img src="data:image/png;base64,' . Barcode::getBarcodePNG($cell, "C128", 3, 100) . '" alt="' . $cell . '" width="200px" /><br/>' . $cell . '</div><script type="text/javascript">window.print();</script>';
		}

		return $a;
    }
}
