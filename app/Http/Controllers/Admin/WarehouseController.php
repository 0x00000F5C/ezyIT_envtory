<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;

use App\Warehouse;
use App\WarehouseContact;
use App\Http\Requests\WarehouseRequest;
use App\User;

use App\Library;

class WarehouseController extends AdminController {

	public function index(){
		$warehouses = Warehouse::orderBy('is_default', 'desc')->get();
		return view("admin.warehouse.index",[
			"pageTitle"	=> "Warehouse Management",
			"warehouses"	=> $warehouses,
		]);
	}

	public function create(){
		return view("admin.warehouse.form",[
			"pageTitle"	=> "Create New Warehouse",
			"warehouse" => new Warehouse
		]);
	}

	public function store(WarehouseRequest $request){
		$data = $request->all();
		$contacts = array();
		foreach ($data['contact'] as $key => $value){
			if (isset($value['pic']) || isset($value['mobile']) || isset($value['email']) || isset($value['phone'])){
				$contacts[] = new WarehouseContact($value);
			}
		}

		if ($warehouse = Warehouse::create($data)) {
			$warehouse->contacts()->saveMany($contacts);
			Library::saveTrail("warehouse", "create", $warehouse->id);
			return redirect()->route('warehouse.index')
				->with('status', 'success')
				->with('message', 'New Warehouse Successfully Added');
		}

		return redirect()->back()
			->with('status', 'danger')
			->with('message', 'Failed to Add Warehouse');
	}

	public function edit($id){
		$warehouse = Warehouse::find($id);
		return view("admin.warehouse.form",[
			"pageTitle"	=> "Warehouse Management",
			"warehouse"	=> $warehouse,
			"contacts"	=> $warehouse->contacts,
		]);
	}

	public function update(WarehouseRequest $request, $id)
	{
		$data = $request->all();
//        unset($data['name']);
//        unset($data['prefix']);
		$contacts = array();
		foreach ($data['contact'] as $key =>$value){
			if (isset($value['pic']) || isset($value['mobile']) || isset($value['email']) || isset($value['phone'])){
				$contacts[] = new WarehouseContact($value);
			}
		}
		WarehouseContact::where("m_warehouse_id", $id)->delete();
		$warehouse = Warehouse::find($id);
		if ($warehouse->contacts()->saveMany($contacts) && $warehouse->update($data)) {
			Library::saveTrail("warehouse", "update", $id);
			return redirect()->route('warehouse.index')
				->with('status', 'success')
				->with('message', 'New Warehouse Successfully Updated');
		}

		return redirect()->back()
			->with('status', 'danger')
			->with('message', 'Failed to Update Warehouse');

	}

	public function destroy($id){
		Library::saveTrail("warehouse", "destroy", $id);
	}
	
	public function genPrefix(Request $request) {
		return ["prefix" => strtoupper(substr($request->input("q"), 0, 3)) . (count(Warehouse::where("name", "like", substr($request->input("q"), 0, 3) . "%")->get()) + 1)];
	}
	
	public function getContact(Request $request) {
		return ["users" => User::whereRaw("concat(ifnull(firstname, ''), ' ', ifnull(lastname, '')) like '" . $request->input("n") . "%'")->get()];
	}
}