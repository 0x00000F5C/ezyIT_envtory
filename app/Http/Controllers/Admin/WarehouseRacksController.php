<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Warehouse;
use App\WarehouseZones;
use App\WarehouseRacks;
use App\WarehouseCells;
use App\Http\Requests\WarehouseRacksRequest;
use App\Library;

class WarehouseRacksController extends Controller {
	public function index() {}
	public function show($wid, $zid, $rid) {
		dd($wid, $zid, $rid);
	}

	public function create($wid, $zid) {
		$warehouse = Warehouse::find($wid);
		if($warehouse === null) { abort(404); }
		$zone = $warehouse->zones()->where("id", $zid)->first();
		return view("admin.warehouse.racks.form",[
			"pageTitle"	=> "Create New Warehouse Rack",
			"warehouse"	=> $warehouse,
			"zone"		=> $zone,
			"model"		=> new WarehouseRacks,
			"rackid"	=> $warehouse->prefix . $zone->prefix . "RCK" . str_pad(count(WarehouseRacks::where("prefix", "like", $warehouse->prefix . $zone->prefix . "RCK%")->get()) + 1, 3, "0", STR_PAD_LEFT)
		]);
	}

	public function edit($rid) {
		$model = WarehouseRacks::find($rid);
		$zone = $model->zone()->first();
		return view("admin.warehouse.racks.form",[
			"pageTitle"	=> "Edit Warehouse Rack",
			"zone"		=> $zone,
			"warehouse"	=> $zone->warehouse()->first(),
			"model"		=> $model,
			"cells"		=> $model->cells()->get()
		]);
	}

	public function store(WarehouseRacksRequest $request, $wid, $zid) {
		$insert["m_warehouse_zones_id"]	=	$zid;
		$insert["prefix"]				=	$request->input("rack-id");
		$insert["name"]					=	$request->input("rack-name");
		$insert["lenght"]				=	$request->input("rack-length");
		$insert["widht"]				=	$request->input("rack-width");
		$insert["height"]				=	$request->input("rack-height");
		$insert["cell_prefix"]			=	0;
		$insert["cell_columns"]			=	$request->input("rack-cellcolumns");
		$insert["cell_rows"]			=	$request->input("rack-cellrows");
		$rack = WarehouseRacks::create($insert);
		if(Library::saveTrail("warehouseMappingRacks", "create", $rack->id)) {
			$cells = [];
			$cellIt = $request->input("cells");
			$cellStat = $request->input("cellStatus");
			for($i = 0; $i < count($cellIt); $i++) {
				array_push($cells, WarehouseCells::create(["prefix" => $cellIt[$i], "colomn" => 0, "row" => 0, "barcode" => 0, "m_warehouse_racks_id" => $rack->id, "status" => $cellStat[$i]])->id);
			}
			$rack->cells()->attach($cells);
			$rack->categories()->attach($request->categories);
			return redirect("app/warehouse/" . $wid . "/mapping")->with("status", "success")->with("message", "Data saved successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to save data");
		}
	}

	public function update(WarehouseRacksRequest $request, $wid, $zid, $rid) {
		$insert["name"]					=	$request->input("rack-name");
		$insert["lenght"]				=	$request->input("rack-length");
		$insert["widht"]				=	$request->input("rack-width");
		$insert["height"]				=	$request->input("rack-height");
		$rack = WarehouseRacks::find($rid);
		$rack->update($insert);
		$rack->categories()->sync($request->categories);
		if(Library::saveTrail("warehouseMappingRacks", "update", $rack->id)) {
			return redirect("app/warehouse/" . $wid . "/mapping")->with("status", "success")->with("message", "Data updated successfully");
		} else {
			redirect()->back()->with("status", "danger")->with("message", "Failed to update data");
		}
	}

	public function destroy($wid, $zid, $rid) {
		$rack = WarehouseRacks::find($rid);
		if(Library::saveTrail("warehouseMappingRacks", "destroy", $rack->id, "Name:" . $rack->name . " (" . $rack->prefix . ")")) {
			$rack->delete();
			return ["st" => true, "msg" => "Data successfully deleted."];
		} else {
			return ["st" => false, "msg" => "Failed to delete data."];
		}
	}

	public function getRacks(Request $request, $wid) {
		$dt = Warehouse::find($wid)->zones()->where("id", $request->input("q"))->first()->racks()->where("m_warehouse_zones_id", $request->input("q"))->get()->toArray();
		for($i = 0; $i < count($dt); $i++) {
			$racks[$i] = $dt[$i];
			// $racks[$i]["editAddress"] = route("warehouse.mapping.racks.edit", [$wid, $request->input("q"), $dt[$i]["id"]]); // edit address generated server-side so no domain migration required
			$racks[$i]["editAddress"] = route("warehouseMappingRacks.edit", $dt[$i]["id"]);
		}
		return ["st" => count($dt) > 0 ? true : false, "data" => count($dt) > 0 ? collect($racks) : null, "addAddress" => route("warehouse.mapping.racks.create", [$wid, $request->input("q")])];
	}

	public function getCategories($rid) {
		$rackCategories = [];
		if ($rid) {
			$wz = WarehouseRacks::find($rid);
			$categories = $wz->categories()->get();
			if ($categories) {
				foreach ($categories as $key => $value) {
					array_push($rackCategories,$value->id);
				}
			}
		}
		return $rackCategories;
	}
}
