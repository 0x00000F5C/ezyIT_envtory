<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use App\Zones;
use App\Library;
use App\Http\Requests\ZonesRequest;

use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Facades\Datatables;

class ZonesController extends Controller {
	public function index() {
		if (request()->ajax()) {
			$zones = Zones::all();
			return Datatables::of($zones)->addColumn("action", function ($zone) {
				return '<form action="' . url('app/zones/'.$zone->id) . '" method="POST" id="delete-' . $zone->id . '">' . csrf_field() . method_field("DELETE") . (Auth::user()->can('update-zones') ? '<a href="zones/' . $zone->id . '/edit" class="btn-editZone btn btn-warning btn-xs btn-flat" data-value="' . $zone->id . '"><i class="fa fa-pencil"></i> Update</a> ' : '') . (Auth::user()->can('delete-zones') ? '<button type="button" class="btn btn-danger btn-xs btn-flat" onclick="confirmDelete(' . $zone->id . ',\'' . ucwords("zone") . '\')"><i class="fa fa-trash"></i> Delete</button>' : '') . '</form>';
			})->make(true);
		}
		return view("admin.zones.index", [
			"pageTitle"	=> "Zones",
			"active"	=> ["setting", "zones"]
		]);
	}

	public function show() {}

	public function create() {
		$model = new Zones;
		return view("admin.zones.form", [
			"pageTitle"	=> "Create New Zone",
			"model"		=> $model,
			"active"	=> ['setting', 'zones']
		]);
	}
	
	public function edit($id) {
		$model = Zones::find($id);
		if($model === null) { abort(404); }
		return view("admin.zones.form", [
		  "pageTitle"	=> "Update Zone",
		  "model"		=> $model,
		  "active"		=> ['setting', 'zones']
		]);
	}

	public function store(ZonesRequest $request) {
		$insert["name"]			= $request->input("zone-name");
		$insert["prefix"]		= $request->input("zone-prefix");
		$insert["cycle_count"]	= $request->input("cycle-count");
		if(Library::saveTrail("zones", "create", Zones::create($insert)->id)){
			return ["st" => true];
		} else {
			return ["st" => false];
		}
	}

	public function update(ZonesRequest $request, $id) {
		$insert["name"]			= $request->input("zone-name");
		$insert["prefix"]		= $request->input("zone-prefix");
		$insert["cycle_count"]	= $request->input("cycle-count");
		$zone = Zones::find($id);
		if($zone->update($insert)) {
			Library::saveTrail("zones", "update", $zone->id);
			return ["st" => true];
		} else {
			return ["st" => false];
		}
	}

	public function destroy(Zones $zones, $id) {
		$zones = Zones::find($id);
		Library::saveTrail("zones", "destroy", $zones->id, "Name: " . $zones->name);
		$zones->delete();
        return redirect("app/zones")->with("status", "success")->with("message", "Data deleted successfully");
	}
	
	public function checkPrefix(Request $request) {
		if(count(Zones::where("prefix", $request->input("q"))->get()) !== 0) {
			return ["st" => false];
		} else {
			return ["st" => true];
		}
	}
	
	public function genPrefix(Request $request) {
		$pr = Zones::where("prefix", "like", strtoupper(substr($request->input("q"), 0, 3)) . "%")->get();
		if(count($pr) > 0) {
			if(strlen(count($pr)) < 2) {
				$prefix = strtoupper(substr($request->input("q"), 0, 2)) . (count($pr) + 1);
			} else {
				$prefix = strtoupper(substr($request->input("q"), 0, 1)) . str_pad((count($pr) + 1), 2, "0", STR_PAD_LEFT);
			}
		} else {
			$prefix = strtoupper(substr($request->input("q"), 0, 3));
		}
		return ["st" => true, "data" => $prefix];
	}

	public function storeZone(Request $request) {
		$insert["name"]		= $request->input("name");
		$insert["prefix"]	= $request->input("prefix");
		if(Library::saveTrail("zones", "create", Zones::create($insert)->id)){
			return ["st" => true];
		} else {
			return ["st" => false];
		}
	}
	
	public function getZones(Request $request) {
		return Zones::where("name", "like", "%" . $request->input("q") . "%")->get();
	}
}
