<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Stock;
use App\StockHistories;
use Datatables;
use PDF;

class readyToStockInController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (request()->ajax()) {
            $stockIn = Stock::with(['product.attributes' => function($att) {
                $att->where('name', 'name');
            }])->where('status',3)->get();
            return Datatables::of($stockIn)
            ->addColumn('invoice', function($stockIn){
                $invoice = !empty($stockIn->invoice) ? $stockIn->invoice->prefix : '-';
                return $invoice;
            })
            ->addColumn('received', function($stockIn){
                return StockHistories::where('t_stocks_id',$stockIn->id)->where('is_in',1)->sum('qty');
            })
            ->addColumn('rejected', function($stockIn){
                return StockHistories::where('t_stocks_id',$stockIn->id)->where('is_in',0)->sum('qty');
            })
            ->addColumn('supplier', function($stockIn){
                $supplier = isset($stockIn->invoice->po->supplier->supplier_name) ? $stockIn->invoice->po->supplier->supplier_name : '';
                return $supplier;
            })
            ->make(true);
        }

        return view('admin.productReceived.toStockIn',[
            'pageTitle' => 'Ready to Stock In'
            ]);
    }

    public function qrcode()
    {
      $received = Stock::where('status',3)->get();
      if (!$received) {
        abort(404);
      }
      $pdf = PDF::loadView('admin.productReceived.qrcode', [
                'received' => $received,
                'title' => 'Product Received',
                'pageTitle' => 'Product Received',
                'print' => false
              ]);
      return $pdf->stream();
      // return $pdf->download('Product Received QR Code.pdf');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
