<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Route;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route = Route::current()->getName();
        if ($route) {
            list($url,$action) = explode('.', $route);
            $permission = [
                "index" => "view-" . $url,
                "create" => "create-" . $url,
                "store" => "create-" . $url,
                "edit" => "update-" . $url,
                "update" => "update-" . $url,
                "destroy" => "delete-" . $url,
                "show" => "view-" . $url,
                "storeCategory" => "create-" . $url,
                "export" => "view-" . $url,
                "mapping" => "update-" . $url, //for warehouse by @vinra
                "trays" => "update-" . $url, //for warehouse trays. --Eeno
            ];

            if (! $request->user()->can($permission[$action])) {
               abort(403);
            }
        }

        return $next($request);

    }
}
