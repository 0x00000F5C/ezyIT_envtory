<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CompanyProfilesRequest extends FormRequest {
	public function authorize() {
		return true;
	}

	public function rules() {
		return [
			"profile-name"				=>	"required",
			"profile-type"				=>	"required",
			"profile-billingaddress"	=>	"required",
			"profile-emailaddress"		=>	"required|email"
			/* "profile-website"			=>	"required|url",
			"profile-gst"				=>	"required",
			"profile-businessnumber"	=>	"required",
			"profile-currency"			=>	"required",
			"profile-timezone"			=>	"required" */
		];
	}

	public function messages() {
        return [
			"profile-name.required"				=> "This field is required",
			"profile-type.required"				=> "This field is required",
			"profile-billingaddress.required"	=> "This field is required",
			"profile-emailaddress.required"		=> "This field is required",
			"profile-emailaddress.email"		=> "Provided data is not a valid email address",
			/* "profile-website.required"			=> "This field is required",
			"profile-website.url"				=> "Provided data is not a valid web address",
			"profile-gst.required"				=> "This field is required",
			"profile-businessnumber.required"	=> "This field is required",
			"profile-currency.required"			=> "This field is required",
			"profile-timezone.required"			=> "This field is required" */
        ];
    }
}