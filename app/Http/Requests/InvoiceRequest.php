<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class InvoiceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't_po_master_id' => 'required',
            'prefix' => 'required',
            'date' => 'required|date',
            'ship_cost' => 'required|numeric|min:0',
            'misc_cost' => 'required|numeric|min:0',
            'total_amount' => 'required|numeric',
            'invoice' => 'nullable',
        ];
    }
}
