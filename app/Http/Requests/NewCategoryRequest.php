<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NewCategoryRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            "parent"	=>	"required",
			"category"		=>	"required|unique:m_product_categories,cat_name"
        ];
    }

	public function messages() {
        return [
            'category.unique' => "Another category by this name has already exists",
        ];
    }
}
