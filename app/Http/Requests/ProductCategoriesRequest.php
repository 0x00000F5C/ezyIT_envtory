<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductCategoriesRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        return [
            "category-parent"	=>	"required",
			"category-name"		=>	"required"
        ];
    }

	public function messages() {
        return [
            'category-name.unique' => "Another category by this name has already exists",
        ];
    }
}
