<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest {
	public function authorize() {
		return true;
	}

    public function rules(){
        $id = $this->route('products') ? $this->route('products') : null;
        return [
            // 'product_prefix' => 'required|unique:m_products,product_prefix,'.$id,
            // 'product_code' => 'required',
            // 'm_product_categories_id' => 'required',
        ];
    }

	public function messages() {
        return [
        ];
    }
}
