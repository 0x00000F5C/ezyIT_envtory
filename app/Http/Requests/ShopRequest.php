<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ShopRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('shop') ? $this->route('shop') : "null";
        return [
            'platform_id' => 'sometimes|required',
            'shop_name' => 'sometimes|required',
            'prefix' => 'sometimes|required|size:6|unique:m_shop,prefix,'.$id,
            'pic_name' => 'required',
            'account' => 'email',
        ];
    }
}
