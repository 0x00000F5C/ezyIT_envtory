<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SupplierRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
		$id = $this->route("supplier") ? $this->route("supplier") : null;
		$validation = [
			"company-name"			=>	"required|unique:m_suppliers,supplier_name," . $id . "|max:255", 
			"company-prefix"		=>	"required|unique:m_suppliers,supplier_prefix," . $id . "|size:3", 
			"company-address"		=>	"required|max:1024", 
			"company-country"		=>	"max:255", 
			"company-state"			=>	"max:255", 
			"company-city"			=>	"max:255", 
			"company-zipcode"		=>	"required|digits_between:5,6", 
			"contact-name.0"		=>	"required",
			"contact-email.0"		=>	"required",
			"contact-phone.0"		=>	"required",
			"contact-phone.*"		=>	"numeric|digits_between:9,16"
		];
		return $validation;
    }

	public function messages() {
        return [
            'company-name.required'				=>	"Please input a name",
            'company-name.unique'				=>	"Another supplier by this name has already exists",
            'company-prefix.required'			=>	"Please input a prefix",
			'company-prefix.unique'				=>	"This company prefix has already exists",
			"contact-address.required"			=>	"This field is required", 
			"contact-name.required"				=>	"Please input at least one name", 
			"company-zipcode.digits_between"	=>	"Inputted data is not a valid zip code",
			"company-zipcode.required"			=>	"Please provide a proper zipcode",
			"contact-email.required"			=>	"Please input at least one email", 
			"contact-phone.required"			=>	"Please input at least one phone number", 
			"contact-phone.numeric"				=>	"Inputted data is not a valid number", 
			"contact-phone.digits_between"		=>	"Inputted data is invalid", 
			"supplier-categories.required"		=>	"Please select at least one category",
			"supplier-categories.min"			=>	"Please select at least one category"
        ];
    }
}
