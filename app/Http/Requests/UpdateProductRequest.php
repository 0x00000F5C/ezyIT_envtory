<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateProductRequest extends FormRequest {
	public function authorize() {
		return true;
	}

    public function rules(){
		$id = $this->input("id");
        return [
            "product-code"			=>	"required|unique:m_products,product_code," . $id,
            "product-name"			=>	"required|max:255",
            "product-category"		=>	"required|max:255",
            "product-subcategory"	=>	"required|max:255",
            "product-brand"			=>	"required|max:255",
            "product-model"			=>	"required|max:255",
            "product-submodel"		=>	"required|max:255",
            "product-color"			=>	"required|max:255",
            "product-suppliers.*"	=>	"required"
        ];
    }

	public function messages() {
        return [
            'product-code.unique'			=> "Another product by this code has already exists",
            'product-suppliers.required'	=> "Please select at least 1 (One) supplier",
        ];
    }
}
