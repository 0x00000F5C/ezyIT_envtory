<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->route('user') ? $this->route('user') : null;
        $validation = [
            'name' => 'required|unique:users,name,'.$id,
            'email' => 'required|unique:users,email,'.$id,
            'password' => 'required|min:6|confirmed',
            'role' => 'required',
        ];
        if ($this->isMethod('PUT')) {
            $validation['password'] = 'confirmed';
        }
        return $validation;
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            // 'required' => "Can't be Empty",
            // 'role.required' => 'Select Role',
            // 'confirmed' => ''
        ];
    }
}
