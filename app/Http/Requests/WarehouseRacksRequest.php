<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseRacksRequest extends FormRequest {
    public function authorize() {
        return true;
    }

	public function rules() {
		return [
            "rack-id"			=> "required|unique:m_warehouse_racks,prefix," . (!empty($this->input("id")) ? $this->input("id") : "null")
        ];
    }
	
	public function messages() {
		return [
			"rack-id.required"			=> "This field is required",
			"rack-id.unique"			=> "Another Rack ID with this name has already exists",
		];
	}
}
