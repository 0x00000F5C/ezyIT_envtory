<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class WarehouseRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
        switch($this->method()) {
			case 'POST': {
                return [
                    'name' => 'required',
                    'prefix' => 'required|unique:m_warehouses,prefix',
                    'address' => 'required'
                ];
            }
			case 'PUT': {
                return [
                    'name' => 'required',
                    'prefix' => 'required|unique:m_warehouses,prefix,'.$this->route()->parameter('warehouse'),
                    'address' => 'required'
                ];
            }
            default: break;
        }

    }
}
