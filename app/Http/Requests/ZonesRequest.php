<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ZonesRequest extends FormRequest {
    public function authorize() {
        return true;
    }

    public function rules() {
		$id = !empty($this->input("id")) ? $this->input("id") : "null";
        return [
            "zone-name"		=> "required",
			"zone-prefix"	=> "required|unique:m_zone_type,prefix," . $id
        ];
    }
	
	public function messages() {
		return [
			"zone-name.required"	=> "This field is required",
			"zone-prefix.required"	=> "This field is required",
			"zone-prefix.unique"	=> "A zone with this prefix is already exists"
		];
	}
}
