<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MakeInvoicePayment extends Model {
	use Traits\Uuids;

	protected $table = "m_invoice_payments";
	protected $fillable = [
		"payment_id",
		"invoice_id",
		"amount"
	];
	protected $hidden = [];
	public $incrementing = false;
	
	public function orderPayment() {
		return $this->belongsTo("App\MakeOrderPayment", "payment_id");
	}
}
