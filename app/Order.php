<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use DateTime;
use DB;

class Order extends Model
{
	use Traits\Uuids;

    protected $table = 't_order_master';

    protected $fillable = [
		'shop_order_no',
	    'order_no',
	    'shop_id',
	    'shop_order_id',
		'date',
		'due_date',
		'customer_name',
        'customer_last_name',
		'person_in_charge',
	    'bill_first_name',
	    'bill_last_name',
		'bill_address',
	    'bill_address2',
	    'bill_postal_code',
		'main_phone_cust',
		'sec_phone_cust',
		'email',
		'city',
		'state',
		'country',
		'remark',
		'status', //related with m_order_statuses table
		"insert_by",
		"update_by",
        "shipping_method",
        'ship_first_name',
        'ship_last_name',
        'ship_company_name',
        'ship_address_1',
        'ship_address_2',
        'ship_country',
        'ship_city',
        'ship_state',
        'ship_postal_code',
        'ship_email',
        'ship_phone',
        'subtotal',
        'shipping_cost',
        'discount',
        'tax',
        'total',
	    'payment_method',
	    'delivery_info',
	    'gift_option',
	    'gift_message',
	    'nat_registration_no',
	    'items_count',
	    'extra_attributes',
	    'order_types_id',
	    'created_at',
	    'updated_at',
	    'bin_id'
    ];
	
	public $incrementing = false;

    public function detail()
    {
    	return $this->hasMany('App\OrderDetail','t_order_master_id','id');
    }

    public function shop(){
        return $this->belongsTo('\App\Shop','shop_id','id');
    }

    public function staff()
    {
    	return $this->belongsTo(User::class,'insert_by','id');
    }

    public function getStatus()
    {
    	return $this->belongsTo(OrderStatus::class,'status','id');
    }

    public function orderType()
    {
    	return $this->belongsTo(OrderType::class, 'order_types_id', 'id');
    }

    public function shippingProvider()
    {
    	return $this->belongsTo(Carrier::class, 'shipping_method', 'id');
    }

	public static function getOrderList($orderType=null)
	{
		if($orderType!=null or $orderType!=''){
			$orders = Order::where(['status'=>2,'order_types_id'=>$orderType])->get();
		}else{
			$orders = Order::where('status',2)->get();
		}

		return $orders;
	}

	public static function generatePoNumber($shop_id,$date)
	{
		$numdate = date('dmy',strtotime($date));
		$shop = Shop::find($shop_id);
		$format = $shop->prefix.$numdate;
		$order = Order::select('order_no')->where(['shop_id'=>$shop_id])->orderBy('order_no','desc')->first();
		return Library::generateNumber($format,count($order)>0?$order->order_no:0,5);
	}

	public static function generateShopOrder($shop_id)
	{
		$numdate = date('my');
		$shop = Shop::find($shop_id);
		if($shop){
			$order = Order::select('shop_order_no')->where(['shop_id'=>$shop_id])->orderBy('order_no','desc')->first();
			$format = substr($shop->prefix,0,3).$numdate;
			return Library::generateNumber($format,count($order)>0?$order->shop_order_no:0,5);
		}else{
			return '';
		}
	}

    private static function checkLastSyncOrders($user)
    {
	    $created = [];
	    $lastSync = SyncLog::where('name', 'last_orders_sync')->where('platform','lazada')->first();

	    if ($lastSync) {
		    $dateTime = new DateTime($lastSync->created_at);
		    $iso8601 = $dateTime->format(DateTime::ATOM);
		    $created = ['CreatedAfter' => $iso8601];
	    }else{
		    SyncLog::create([
			    'name' => 'last_orders_sync','platform' => 'lazada','status' => 'running',
			    'user' => $user
		    ]);
	    }

	    return $created;
    }

    public static function syncWithLazada($shop, $user)
    {
    	$orders = [];
	    $lastSync = Self::checkLastSyncOrders($user);
		$account = $shop->account;
		$api_key = $shop->api_key;

        $api = Library::lazadaShopApi($account, $api_key, 'GetOrders', null, null, $lastSync);
	    if ($api) {
		    $client = new \GuzzleHttp\Client();
		    $res = json_decode($client->get($api)->getBody(), true);
		    if (isset($res["ErrorResponse"])) {
			    return $res["ErrorResponse"];
		    }
		    $response = $res["SuccessResponse"]["Body"]["Orders"];
		    if($response){
			    $orderList = [];
		        foreach ($response as $resp){
				    $orderList[] = $resp['OrderId'];
				    $order = Self::getOrderApi($account, $api_key, $resp['OrderId']);
				    $orders[$resp['OrderId']] = $order;
			        $api = Library::lazadaShopApi($account, $api_key, 'GetOrderItems', null, null, ['OrderId'=>$resp['OrderId']]);
				    if($api){
					    $client = new \GuzzleHttp\Client();
					    $res = json_decode($client->get($api)->getBody(), true);
					    if (isset($res["ErrorResponse"])) {
						    return $res["ErrorResponse"];
					    }
					    $respDetail = $res["SuccessResponse"]["Body"]["OrderItems"];
					    if($respDetail){
					        foreach($respDetail as $respDet){
					            $orders[$resp['OrderId']]['orderItems'][] = $respDet;
						    }
					    }
				    }
			    }
		    }
	    }
	    if($orders){
		    $result = Self::saveFromLazada($orders,$shop,$user);
	    }else{
		    $result = ['status'=>'success','msg'=>'Please try again later...', 'data' => $orders];
	    }
	    return $result;
    }

    private static function getOrderApi($account, $api_key, $orderId)
    {
	    $api = Library::lazadaShopApi($account, $api_key, 'GetOrder', null, null, ['OrderId'=>$orderId]);
	    if($api){
		    $client = new \GuzzleHttp\Client();
		    $res = json_decode($client->get($api)->getBody(), true);
		    if (isset($res["ErrorResponse"])) {
			    return $res["ErrorResponse"];
		    }
		    $response = $res["SuccessResponse"]["Body"]["Orders"];
		    return $response[0];
	    }
    }

    public static function saveFromLazada($orders,$shop,$user)
    {
    	$CreatedDates = [];
    	DB::beginTransaction();
    	try {
		    foreach ($orders as $order) {
			    $order_no = Order::generatePoNumber($shop->id, date('Y-m-d'));
			    $type = OrderType::getByQty($order['ItemsCount']);
			    $shopPrefix = substr($shop->prefix, 0, 3);
			    $data = [
				    'shop_order_no' => $shopPrefix . $order['OrderNumber'],
				    'order_no' => $order_no,
				    'shop_id' => $shop->id,
				    'shop_order_id' => $order['OrderId'],
				    'date' => date('Y-m-d', strtotime($order['CreatedAt'])),
				    'due_date' => date('Y-m-d', strtotime($order['PromisedShippingTimes'])),
				    'customer_name' => $order['CustomerFirstName'],
				    'customer_last_name' => $order['CustomerLastName'],
				    'bill_first_name' => $order['AddressBilling']['FirstName'],
				    'bill_last_name' => $order['AddressBilling']['LastName'],
				    'bill_address' => $order['AddressBilling']['Address1'],
				    'bill_address2' => $order['AddressBilling']['Address2'],
				    'bill_postal_code' => $order['AddressBilling']['PostCode'],
				    'main_phone_cust' => $order['AddressBilling']['Phone'],
				    'sec_phone_cust' => $order['AddressBilling']['Phone2'],
				    'email' => $order['AddressBilling']['CustomerEmail'],
				    'city' => $order['AddressBilling']['City'],
				    'state' => isset($order['AddressBilling']['Address3']) ? $order['AddressBilling']['Address3'] : '',
				    'country' => $order['AddressBilling']['Country'],
				    'remark' => $order['Remarks'],
				    'status' => 2, //new or pending
				    'ship_first_name' => $order['AddressShipping']['FirstName'],
				    'ship_last_name' => $order['AddressShipping']['LastName'],
				    'ship_address_1' => $order['AddressShipping']['Address1'],
				    'ship_address_2' => $order['AddressShipping']['Address2'],
				    'ship_country' => $order['AddressShipping']['Country'],
				    'ship_city' => $order['AddressShipping']['City'],
				    'ship_postal_code' => $order['AddressShipping']['PostCode'],
				    'ship_email' => $order['AddressShipping']['CustomerEmail'],
				    'ship_phone' => $order['AddressShipping']['Phone'],
				    'shipping_cost' => $order['ShippingFee'],
				    'subtotal' => $order['Price'],
				    'total' => $order['Price']+$order['ShippingFee'],
				    'payment_method' => $order['PaymentMethod'],
				    'delivery_info' => $order['DeliveryInfo'],
				    'gift_option' => $order['GiftOption'],
				    'gift_message' => $order['GiftMessage'],
				    'nat_registration_no' => $order['NationalRegistrationNumber'],
				    'items_count' => $order['ItemsCount'],
				    'order_types_id' => $type->id,
				    'extra_attributes' => $order['ExtraAttributes'],
				    'insert_by' => $user,
				    'update_by' => $user,
				    'created_at' => $order['CreatedAt'],
				    'updated_at' => $order['UpdatedAt']
			    ];
			    if ($new_order = Order::create($data)) {
				    $CreatedDates[] = $order['CreatedAt'];
				    Library::saveTrail("order", "create", $new_order->id, 'By Synchronization with lazada');
				    foreach ($order['orderItems'] as $det) {
					    $product_sku = Sku::where('sku', $det['Sku'])->first();
					    $params = [
						    't_order_master_id' => $new_order->id,
						    'order_item_id' => $det['OrderItemId'],
						    'product_sku_id' => ($product_sku ? $product_sku->id : 'xxxxxxxxx'),
						    'price' => $det['ItemPrice'],
						    'paid_price' => $det['PaidPrice'],
						    'currency' => $det['Currency'],
						    'tax_amount' => $det['TaxAmount'],
						    'qty' => 1,
						    'insert_by' => $user,
						    'update_by' => $user
					    ];
					    if ($detail = OrderDetail::create($params)) {
						    $data_stocks = [];
						    if ($product_sku) {
							    $data_stocks = Library::findStockBatch($product_sku->product->id, 1);
						    }
						    foreach ($data_stocks as $stock) {
							    if (OrderDetailStock::create([
								    't_order_detail_id' => $detail->id,
								    't_stocks_id' => $stock['stocks_id'],
								    'batch_id' => $stock['batch_id'],
								    'cell_id' => $stock['cell_id'],
								    'qty' => $stock['qty_to_pick']
							    ])
							    ) {
								    $qtyStock = Library::updateStock($stock['stocks_id'], $stock['qty_to_pick'], 'out', $new_order->order_no);

								    //update stock to lazada
								    Product::updateStockPriceLazada($shop, ['SellerSku' => $det['Sku'], 'Quantity' => $qtyStock]);
							    };
						    }
					    }
				    }
			    }
		    }
		    rsort($CreatedDates);
		    $lastSync = SyncLog::where('name', 'last_orders_sync')->where('platform', 'lazada')->first();
		    $lastSync->update(['status' => 'success', 'created_at' => $CreatedDates[0]]);

		    DB::commit();
		    return ['status'=>'success','msg'=>'Successfully save orders data!', 'data'=>$orders];
	    } catch (\Exception $e){
    		DB::rollback();
		    return ['status'=>'error','msg'=>'Failed synchronization orders!', 'data'=>$orders];
	    }

    }
	
	public static function getReadyToShip(){
		return Order::whereIn("status", [7, 8])->get();
	}

	public static function setStatusToPackByMarketPlace($shop, $items)
	{
		$account = $shop->account;
		$api_key = $shop->api_key;

		$api = Library::lazadaShopApi($account, $api_key, 'SetStatusToPackedByMarketplace', null, null, ['OrderItemIds'=>$items,'DeliveryType'=>'dropship']);
		if($api){
			$client = new \GuzzleHttp\Client();
			$res = json_decode($client->get($api)->getBody(), true);
			if (isset($res["ErrorResponse"])) {
				return $res["ErrorResponse"];
			}
			$response = $res["SuccessResponse"]["Body"]["OrderItems"];
			return $response;
		}
	}

	public static function setStatusReadyToShip($shop, $items)
	{
		$account = $shop->account;
		$api_key = $shop->api_key;

		$api = Library::lazadaShopApi($account, $api_key, 'SetStatusToReadyToShip', null, null, ['OrderItemIds'=>$items,'DeliveryType'=>'dropship']);
		if($api){
			$client = new \GuzzleHttp\Client();
			$res = json_decode($client->get($api)->getBody(), true);
			if (isset($res["ErrorResponse"])) {
				return $res["ErrorResponse"];
			}
			$response = $res["SuccessResponse"]["Body"]["OrderItems"];
			return $response;
		}
	}

    public static function SetStatusToPackedByMarketplace($shop, $items, $id)
    {
        $account = $shop->account;
        $api_key = $shop->api_key;
//GetOrderItems SetStatusToPackedByMarketplace
//        $api = Library::lazadaShopApi($account, $api_key, 'SetStatusToPackedByMarketplace', null, null, ['OrderItemIds'=>$items,'DeliveryType'=>'dropship', 'ShippingProvider' => "Ta-Q-Bin"]);
        $api = Library::lazadaShopApi($account, $api_key, 'GetOrder', null, null, ['OrderId'=>$id,'DeliveryType'=>'pickup']);
//        return $api;
        if($api){
            $client = new \GuzzleHttp\Client();
            $res = json_decode($client->get($api)->getBody(), true);
            return $res;
            if (isset($res["ErrorResponse"])) {
                return $res["ErrorResponse"];
            }
//            $response = $res["SuccessResponse"]["Body"]["OrderItems"];
            $response = $res["SuccessResponse"]["Body"];
            return $response;
        }
    }

//	public function payment_method(){
//	    return $this->belongsTo(\App\PaymentMethod::class, 'payment_method');
//    }

//    public function shop(){
//	    return $this->belongsTo(Shop::class, 'shop_order_id');
//    }

}
