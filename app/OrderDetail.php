<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetail extends Model
{
	use Traits\Uuids;

    protected $table = 't_order_detail';

    protected $fillable = [
	    't_order_master_id',
		'order_item_id',
		'product_sku_id',
		'price',
		'paid_price',
		'currency',
		'tax_amount',
		'qty',
		"insert_by",
		"update_by"
    ];
	
	public $incrementing = false;

    public function order()
    {
        return $this->belongsTo('App\Order','t_order_master_id','id');
    }

    public function sku()
    {
    	return $this->belongsTo('App\Sku','product_sku_id','id');
    }

    public function detailStock()
    {
    	return $this->hasOne(OrderDetailStock::class, 't_order_detail_id', 'id');
    }

}
