<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderDetailStock extends Model
{
	protected $table = 't_order_detail_t_stocks';

	public $timestamps = false;

	protected $fillable = ['t_order_detail_id', 't_stocks_id', 'batch_id', 'cell_id', 'qty'];

	public function cell()
	{
		return $this->belongsTo(WarehouseCells::class, 'cell_id', 'id');
	}

	public function orderDetail()
	{
		return $this->belongsTo(OrderDetail::class, 't_order_detail_id', 'id');
	}

    public function picking()
    {
        return $this->hasMany(Picking::class, 'det_stock_id', 'id');
    }
}
