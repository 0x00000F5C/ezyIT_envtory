<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderType extends Model
{
    use Traits\Uuids;

    protected $table = 'm_order_types';
    protected $hidden = ['created_at','updated_at','insert_by','update_by'];

    protected $fillable = [
        'id',
        'name',
        'min_qty',
        'max_qty',
        'insert_by',
        'update_by',
        'created_at',
        'updated_at'
    ];

    public $incrementing = false;

    public function orders()
    {
    	return $this->hasMany(Order::class, 'order_types_id', 'id');
    }

    public static function getByQty($qty)
    {
    	$type = OrderType::where('min_qty','<=',$qty)->where('max_qty','>=',$qty)->first();
    	return $type;
    }

}
