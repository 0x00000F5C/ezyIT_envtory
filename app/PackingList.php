<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackingList extends Model {
	use Traits\Uuids;
    protected $table	= "t_packings";

	protected $fillable	= [
		"id",
		"packing_id",
		"order_id",
		"packing_date",
		"bin_id",
		"status",
		"insert_by",
		"update_by"
	];

	protected $hidden	= ['created_at','updated_at'];

	public function order()
	{
		return $this->belongsTo(Order::class, 'order_id', 'id');
	}

	public function staff()
	{
		return $this->belongsTo(User::class, 'insert_by', 'id');
	}
	
}
