<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PackingLogistic extends Model {
	use Traits\Uuids;

	protected $table = "m_packing_logistics";
	protected $fillable = [
		"packing_code",
		"packing_name",
		"operator",
		"qty",
		"width",
		"height",
		"length",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	public $incrementing = false;
	
	public function operator() {
		return $this->hasOne("App\User", "id", "operator");
	}
}
