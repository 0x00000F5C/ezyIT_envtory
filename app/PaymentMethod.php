<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentMethod extends Model {
	use Traits\Uuids;

    protected $table	= "m_payment_method";
	protected $fillable	= [
		"uid",
		"name",
		"is_bank",
		"insert_by",
		"update_by"
	];
	protected $hidden	= [];
	
	public $incrementing = false;
	
	public function payment() {
		//
	}
}
