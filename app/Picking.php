<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Picking extends Model {
	use Traits\Uuids;

	protected $table = 't_picking_items';

	protected $fillable = [
		'picking_list_id',
		'det_stock_id',
		'tray_id',
		'item_id',
		'status', //0=new;1=have tray
	];
	public $incrementing = false;

	public function picking_list()
	{
		return $this->belongsTo(PickingList::class, 'picking_list_id', 'id');
	}

	public function stockInBatch()
	{
		return $this->belongsTo(OrderDetailStock::class, 'det_stock_id', 'id');
	}

	public function tray()
	{
		return $this->belongsTo(WarehouseTrays::class, 'tray_id', 'id');
	}

	public function orderDetailStock()
	{
		return $this->belongsTo(OrderDetailStock::class, 'det_stock_id', 'id');
	}

}
