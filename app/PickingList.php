<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickingList extends Model
{
	use Traits\Uuids;

	protected $table = 't_picking_lists';

	protected $fillable = [
		'user_id',
		'picking_code',
		'date',
		'status', //[0:pending;1:in_progress,2:done]
		'insert_by',
		'update_by',
		'qty_of_order',
		'total_item',
		'picking_type_id',
		'qty_of_tray',
		'start',
		'end'
	];

	public $incrementing = false;

	public function detail()
	{
		return $this->hasMany(Picking::class);
	}

	public function staff()
	{
		return $this->belongsTo(User::class, 'user_id', 'id');
	}

	public function type()
	{
		return $this->belongsTo(PickingType::class, 'picking_type_id', 'id');
	}

	public static function generatePickingCode()
	{
		$pickLists = PickingList::select('picking_code')->where('date',date('Y-m-d'))->orderBy('created_at','desc')->first();
		if(sizeof($pickLists)==0){
			return 'PL'.date('d').'001';
		}else{
			$NOL = '0';
			$format = 'PL'.date('d');
			$R = $pickLists->picking_code;
		    $K = sprintf("%d", substr($R, - 3));
		    $K = $K + 1;
		    $L = $K;
		    while (strlen($L) != 3) {
			    $L = $NOL . $L;
		    }
		    return $format . $L;
		}
	}

	public static function getPickingList()
	{
		return PickingList::where('status', 0)->get();
	}

	public static function getReadyToPack()
	{
		return PickingList::where('status', 2)->get();
	}
}
