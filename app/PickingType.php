<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PickingType extends Model {
	use Traits\Uuids;

	protected $table = "m_picking_types";

	protected $fillable = [
		"picking_type_name",
		"qty",
		"order_type",
        "troy_needed",
        "troy_needed_per"
	];

	protected $hidden = [];

	public $incrementing = false;
	
	public function orderType() {
		return $this->hasOne("App\OrderType", "id", "order_type");
	}
}
