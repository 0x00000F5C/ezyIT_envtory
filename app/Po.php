<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Po extends Model
{
	use Traits\Uuids;

     protected $table = 't_po_master';

     protected $fillable = [
          'id',
		'prefix',
          'po_date',
		'exp_date',
		'm_supplier_id',
		'ship_address',
          'email',
		'misc_cost',
		'ship_cost',
		'total_det_mount',
		'total_amount',
          'status',
          'rate',
          'reason_closed',
		  'remarks',
		"insert_by",
		"update_by"
     ];
	 
	 public $incrementing = false;

     protected $hidden = [];

     public function supplier()
     {
        return $this->belongsTo('App\Supplier','m_supplier_id','id');
     }

     public function detail()
     {
          return $this->hasMany('App\PoDetail','t_po_master_id','id');
     }

     public function invoice()
     {
          return $this->hasMany('App\Invoice','t_po_master_id','id');
     }
	 
	 public function payment()
	 {
		 return $this->belongsTo('App\MakeOrderPayment','id','order_id');
	 }
}
