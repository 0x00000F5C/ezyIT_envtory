<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductCategories extends Model {
	protected $table = "m_product_categories";
	protected $fillable = [
		"id",
		"cat_parent",
		"cat_name",
		"data_status",
		"insert_by",
		"update_by"
	];
	protected $hidden = ['cat_parent','data_status','insert_by','update_by','created_at','updated_at'];

	public function parentCategory() {
		return $this->hasOne("App\ProductCategories", "id", "cat_parent");
	}

	public function childCategories() {
		return $this->hasMany("App\ProductCategories", "cat_parent", "id");
	}

	public function parent()
    {
        return $this->belongsTo('App\ProductCategories', 'cat_parent');
    }

    public function children()
    {
        return $this->hasMany('App\ProductCategories', 'cat_parent');
    }

    public function supplier() {
		return $this->belongsToMany('App\Supplier', 'p_category_supplier', 'category_id', 'supplier_id');
	}
	
	public function racks() {
		return $this->belongsToMany("App\WarehouseRacks", "m_warehouse_racks_categories", "category_id", "rack_id");
	}
}
