<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportTemplate extends Model {
	protected $table = "m_report_templates";
	protected $fillable = [
		"name",
		"paper_size",
		"header",
		"footer"
	];
	protected $hidden = [];
}
