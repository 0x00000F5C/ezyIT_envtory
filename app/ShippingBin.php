<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShippingBin extends Model {
	use Traits\Uuids;

    protected $table = "m_shipping_bin";
	protected $fillable = [
		"bin_id",
		"is_used",
		"insert_by",
		"update_by"
	];
	protected $hidden = ['updated_at', 'created_at'];
	
	public $incrementing = false;
	
	public function warehouse() {
		return $this->belongsTo("App\Warehouse", "m_warehouse_id");
	}
}
