<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
	use Traits\Uuids;

    protected $table = 'm_shop';

    protected $fillable = [
		"uid",
		'platform_id', 
		'm_shop_categories_id', 
		'shop_name', 
		'pic_name', 
		'mode', 
		'account', 
		'api_key', 
		'status', 
		'prefix',
		"insert_by",
		"update_by"
    ];
	
	public $incrementing = false;

    protected $hidden = [
		'platform_id', 
		'm_shop_categories_id', 
		'pic_name', 
		'mode', 
		'account', 
		'api_key', 
		'status', 
		"insert_by",
		"update_by",
		'created_at',
		'updated_at',
    ];

    public function category()
    {
        return $this->belongsTo('App\ShopCategories','m_shop_categories_id','id');
    }

    public function sku() {
		return $this->belongsToMany('App\Sku', 'p_sku_shop', 'shop_id', 'sku_product_id');
	}
}
