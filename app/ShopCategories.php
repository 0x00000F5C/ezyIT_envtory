<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopCategories extends Model
{
	use Traits\Uuids;

    protected $table = 'm_shop_categories';

    protected $fillable = ["uid",'name',"insert_by","update_by"];
	
	public $incrementing = false;

    public function shops()
    {
        return $this->hasMany('App\Shop');
    }
}
