<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Sku extends Model
{
	use Traits\Uuids;
    use SoftDeletes;

    public $incrementing = false;

    protected $dates = ['deleted_at'];

    protected $table = 'm_product_sku';

    protected $fillable = ['m_products_id','sku','bundle','insert_by','updated_by','retail_price','min_price','max_price','cost_price','save_qty','max_qty','min_qty','max_shelved_life','bundle_qty'];

    protected $hidden = ['insert_by','update_by','deleted_at','bundle','created_at','updated_at'];

    public function product() {
        return $this->belongsTo("App\Product", "m_products_id", "id");
    }

    public function attributes() {
			return $this->hasMany('App\SkuAttributes','m_product_sku_id','id');
		}

    public function attribute($name) {
        return Self::attributes()->where('name',$name)->pluck('value')->first();
    }

    public function shops() {
        return $this->belongsToMany('App\Shop', 'p_sku_shop', 'sku_id', 'shop_id');
    }

    public function prices() {
        return $this->hasMany('App\SkuPrice', 'sku_id', 'id');
    }

    public function price($shop_id) {
        return $this->hasOne('App\SkuPrice', 'sku_id', 'id')->where('shop_id', $shop_id);
    }

    public function images($storage = 'local') {
        return $this->hasMany('App\File', 'module_id', 'id')
							->where('module','sku')
							->where('storage',$storage);
    }

    public static function xml($id) {
        $sku = Self::find($id);
        if ($sku) {
            return response()->view('admin.sku.xml', [
                'sku' => $sku
            ])->header('Content-Type', 'text/xml');
        }
        return false;
    }

}
