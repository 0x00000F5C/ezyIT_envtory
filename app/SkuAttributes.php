<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuAttributes extends Model
{
    protected $table = 'm_product_sku_attributes';

    protected $fillable = [
	    'id', 'm_product_sku_id', 'name', 'value'
    ];

    protected $hidden = ['created_at','updated_at','m_product_sku_id'];

    public $incrementing = false;
}
