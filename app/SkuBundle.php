<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuBundle extends Model
{
    use Traits\Uuids;

    protected $table = 'm_product_sku_bundle';

    protected $fillable = ['id','bundle_id','sku_id','qty'];
}
