<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SkuPrice extends Model
{
    use Traits\Uuids;

    public $incrementing = false;

    protected $table = 'm_product_sku_price';

    protected $fillable = ['id','sku_id','shop_id','price','special_price','special_from_date','special_to_date','insert_by','update_by'];

    protected $hidden = ['id','insert_by','update_by','created_at','updated_at'];
}
