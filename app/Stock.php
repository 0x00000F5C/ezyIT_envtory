<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stock extends Model
{
	use Traits\Uuids;

	
    protected $table = 't_stocks';

    protected $fillable = ['t_invoice_master_id','m_products_id','received_date','batch_id',
	    'stock_type', // 'AC = Active, CD = Cold', OS = Out Of Stock
	    'status', //0 = pending, 2 = ready to received, 3 = ready to stock in, 1 = ready to pick, 4 = Out Of Stock
	    'zone_type','cell_id',"insert_by","update_by"
    ];
	
	public $incrementing = false;

	public function histories()
	{
	  return $this->hasMany('App\StockHistories','t_stocks_id','id');
	}

	public function invoice()
    {
        return $this->belongsTo('App\Invoice','t_invoice_master_id','id');
    }

	public function product()
    {
        return $this->belongsTo('App\Product','m_products_id','id');
    }

    public function cell()
    {
    	return $this->hasOne('App\WarehouseCells','id','cell_id');
    }

    public function in()
    {
    	return $this->histories()->where('is_in',1);
    }

    public function out()
    {
    	return $this->histories()->where('is_in',0);
    }

    public static function active($product)
    {
        $histories_in = StockHistories::whereHas('stock', function($stock) use ($product) {
            $stock->where('m_products_id',$product->id)->where(['stock_type'=>'AC', 'status'=>'1']);
        })->where('is_in',1)->sum('qty');
        $histories_out = StockHistories::whereHas('stock', function($stock) use ($product) {
            $stock->where('m_products_id',$product->id)->where(['stock_type'=>'AC', 'status'=>'1']);
        })->where('is_in',0)->sum('qty');

        return $histories_in - $histories_out;
    }

    public static function activePending($product)
    {
        $histories_in = StockHistories::whereHas('stock', function($stock) use ($product) {
            $stock->where('m_products_id',$product->id)->where(['stock_type'=>'AC', 'status'=>'3']);
        })->where('is_in',1)->sum('qty');
	    $histories_out = StockHistories::whereHas('stock', function($stock) use ($product) {
		    $stock->where('m_products_id',$product->id)->where(['stock_type'=>'AC', 'status'=>'3']);
	    })->where('is_in',0)->sum('qty');

        return $histories_in - $histories_out;
    }

    public static function cold($product)
    {
	    $histories_in = StockHistories::whereHas('stock', function($stock) use ($product) {
		    $stock->where('m_products_id',$product->id)->where(['stock_type'=>'CD']);
	    })->where('is_in',1)->sum('qty');
	    $histories_out = StockHistories::whereHas('stock', function($stock) use ($product) {
		    $stock->where('m_products_id',$product->id)->where(['stock_type'=>'CD']);
	    })->where('is_in',0)->sum('qty');

	    return $histories_in - $histories_out;
    }

    protected $with = ['histories'];

}
