<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockHistories extends Model
{
	use Traits\Uuids;

    
    protected $table = 't_stock_histories';

    protected $fillable = ['t_stocks_id','qty','is_in','note',"insert_by","update_by"];
	
	public $incrementing = false;

    public function stock()
    {
        return $this->belongsTo('App\Stock','t_stocks_id','id');
    }

}
