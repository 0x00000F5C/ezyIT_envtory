<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StockInLogistic extends Model {
	use Traits\Uuids;

	protected $table = "m_stock_in_logistics";
	protected $fillable = [
		"packing_code",
		"packing_name",
		"operator",
		"width",
		"height",
		"length",
		"qty",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	public $incrementing = false;
}
