<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SyncLog extends Model
{
    protected $table = 'sync_logs';

    protected $fillable = ['name','platform','user','status','created_at', 'updated_at'];

    
}
