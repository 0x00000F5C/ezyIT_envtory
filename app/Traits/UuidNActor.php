<?php
namespace App\Traits;

use Webpatser\Uuid\Uuid;

trait UuidNActor {
	protected static function boot() {
		parent::boot();

		static::creating(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
		});

		static::inserting(function ($model) {
            $model->{$model->getKeyName()} = Uuid::generate()->string;
		});
	}
}