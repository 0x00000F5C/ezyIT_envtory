<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Contracts\Auth\CanResetPassword as AuthCanResetPassword;
use Illuminate\Auth\Passwords\CanResetPassword as PasswordsCanResetPassword;

class User extends Authenticatable
{
	use Traits\Uuids;

    use Notifiable;
    use HasRoles;
	use Traits\Uuids;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
		"uid",'name', 'firstname', 'lastname', 'email', 'password', 'active','insert_by','update_by'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
	public $incrementing = false;
}
