<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseTrays extends Model {
	use Traits\Uuids;

    protected $table = "m_warehouse_trays";
	protected $fillable = [
		"uid",
		"prefix",
		"m_warehouse_id",
		"size",
		"max_qty",
		"status",
		"insert_by",
		"update_by"
	];
	protected $hidden = ['updated_at', 'created_at'];
	
	public $incrementing = false;
	
	public function warehouse() {
		return $this->belongsTo("App\Warehouse", "m_warehouse_id");
	}

	public function pickings(){
        return $this->hasMany(Picking::class, 'tray_id');
    }
}