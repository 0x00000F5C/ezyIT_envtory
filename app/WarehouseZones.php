<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseZones extends Model {
	use Traits\Uuids;

    protected $table = "m_warehouse_zones";
	protected $fillable = [
		"id",
		"name",
		"prefix",
		"cycle_count",
		"type",
		"m_warehouse_id",
		"m_warehouse_types_id",
		"insert_by",
		"update_by"
	];
	protected $hidden = [];
	
	public $incrementing = false;
	
	public function warehouse() {
		return $this->belongsTo("App\Warehouse", "m_warehouse_id");
	}
	
	public function racks() {
		return $this->hasMany("App\WarehouseRacks", "m_warehouse_zones_id");
	}
	
	public function categories() {
		return $this->belongsToMany("App\ProductCategories", "m_warehouse_zones_categories", "warehouse_zones_id", "category_id");
	}
}
