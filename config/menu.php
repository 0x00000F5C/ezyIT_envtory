<?php
return [
	'User Management' => [
		[
			'name' => 'Supplier',
			'route' => 'supplier.index',
			'icon' => 'fa fa-vcard',
			'access' => 'view-shop',
		],
		[
			'name' => 'Shop',
			'route' => 'shop.index',
			'icon' => 'fa fa-shopping-bag',
			'access' => 'view-shop',
		],
		[
			'name' => 'Product Categories',
			'route' => 'productCategories.index',
			'icon' => 'fa fa-shopping-basket',
			'access' => 'view-shop',
		],
		[
			'name' => 'Products',
			'route' => 'products.index',
			'icon' => 'fa fa-cart-arrow-down',
			'access' => 'view-shop',
		],
		[
			'name' => 'User Company',
			'route' => 'companyProfiles.index',
			'icon' => 'fa fa-sitemap',
			'access' => 'view-shop',
		],
		[
			'name' => 'User',
			'route' => 'user.index',
			'icon' => 'fa fa-user',
			'access' => 'view-user',
			// Child Menu Example
			// 'child' => [
			// 	[
			// 		'name' => 'Childe 1',
			// 		'route' => 'user.index',
			// 		'icon' => 'fa fa-user',
			// 		'access' => 'view-user'
			// 	],
			// 	[
			// 		'name' => 'Childe 2',
			// 		'route' => 'user.index',
			// 		'icon' => 'fa fa-user',
			// 		'access' => 'view-user'
			// 	]
			// ]
		],
		[
			'name' => 'Role',
			'route' => 'role.index',
			'icon' => 'fa fa-lock',
			'access' => 'view-role'
		],
        [
            'name' => 'Warehouse Management',
            'route' => 'warehouse.index',
            'icon' => 'fa fa-cog',
            'access' => 'view-warehouse'
        ]
	],

];