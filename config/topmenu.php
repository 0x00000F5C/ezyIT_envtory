<?php
return [
	[
		'name' => 'Products',
		'route' => "dashboard.product",
		'icon' => 'fa fa-home',
		'access' => ['view-warehouses', 'view-zones', 'view-skuProducts','view-dashboard'],
		'child' => [
			[
				'name' => 'Supplier',
				'route' => 'supplier.index',
				'icon' => 'fa fa-vcard',
				'access' => ['view-supplier'],
			],
			[
				'name' => 'Purchase Order',
				'route' => 'po.index',
				'icon' => 'fa fa-shopping-bag',
				'access' => ['view-po'],
			],
			[
				'name' => 'Process PO',
				'route' => 'processPo.index',
				'icon' => 'fa fa-check-square-o',
				'access' => ['view-processPo'],
			],
			[
				'name' => 'Product Received',
				'route' => 'productReceived.index',
				'icon' => 'fa fa-arrow-down',
				'access' => ['view-productReceived'],
			],
			[
				'name' => 'Ready to Stock In',
				'route' => 'readyToStockIn.index',
				'icon' => 'fa fa-cart-plus',
				'access' => ['view-readyToStockIn'],
			],
			[
				'name' => 'Stock In',
				'route' => 'stockIn.index',
				'icon' => 'fa fa-download',
				'access' => ['view-stockIn'],
			],
			[
				'name' => 'Logistic Inventory',
				'route' => 'logisticInventory.index',
				'icon' => 'fa fa-cubes',
				'access' => ['view-logisticInventory'],
			],
			[
				'name' => 'divider'
			],
			[
				'name' => 'Manage SKU',
				'route' => 'sku.index',
				'icon' => 'fa fa-barcode',
				'access' => ['view-sku'],
			],
			[
				'name' => 'Manage Image',
				'route' => 'skuImage.index',
				'icon' => 'fa fa-picture-o',
				'access' => ['view-sku'],
			],
			[
				'name' => 'Cycle count',
				'route' => 'cycleCount.index',
				'icon' => 'fa fa-hourglass-start',
				'access' => ['view-cycleCount'],
			],
			[
				'name' => 'divider'
			],
			[
				'name' => 'Brands',
				'route' => 'brands.index',
				'icon' => 'fa fa-bookmark',
				'access' => ['view-brands'],
			],
			[
				'name' => 'Products',
				'route' => 'products.index',
				'icon' => 'fa fa-cart-arrow-down',
				'access' => ['view-products'],
			],
			[
				'name' => 'Product Categories',
				'route' => 'productCategories.index',
				'icon' => 'fa fa-shopping-basket',
				'access' => ['view-productCategories'],
			],
		]
	],
	[
		'name' => 'Orders',
		'route' => 'dashboard.index',
		'icon' => 'fa fa-barcode',
		'access' => ['view-order'],
		'child' => [
			[
				'name' => 'Order',
				'route' => 'order.index',
				'icon' => 'fa fa-shopping-cart',
				'access' => ['view-order'],
			],
			[
				'name' => 'Picking list',
				'route' => 'pickingList.index',
				'icon' => 'fa fa-th-list',
				'access' => ['view-pickingList'],
			],
			[
				'name' => 'Picking process',
				'route' => 'picking.index',
				'icon' => 'fa fa-download',
				'access' => ['view-picking'],
			],
			[
				'name' => 'Packing list',
				'route' => 'packingList.index',
				'icon' => 'fa fa-gift',
				'access' => ['view-picking'],
			],
			[
				'name' => 'Shipping list',
				'route' => 'shippingList.index',
				'icon' => 'fa fa-clipboard',
				'access' => ['view-shippingList'],
			],
			[
				'name' => 'Delivery status',
				'route' => 'shippingStatus.index',
				'icon' => 'fa fa-globe',
				'access' => ['view-shippingStatus'],
			]

		]
	],
	[
		'name' => 'Warehouse',
		'route' => "dashboard.warehouse",
		'icon' => 'fa fa-list-ol',
		'access' => ['view-supplier','view-po','view-processPo'],
		'child' => [
			[
				'name' => 'Warehouses',
				'route' => "warehouse.index",
				'icon' => 'fa fa-home',
				'access' => ['view-warehouse'],
			],
			[
				'name' => 'Zone types',
				'route' => "zones.index",
				'icon' => 'fa fa-window-restore',
				'access' => ['view-zones'],
			],
			[
				'name' => 'Shipping bin',
				'route' => "shippingBin.index",
				'icon' => 'fa fa-cube',
				'access' => ['view-shippingBin'],
			]
		]
	],
	[
		'name' => 'Reports',
		'route' => "reports.index",
		'icon' => 'fa fa-copy',
		'access' => ['view-reports'],
		'child' => [
			[
				'name' => 'Empty Cell',
				'route' => 'reports.emptyCell',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'New Batch - Stock In Report',
				'route' => 'reports.newBatch_StockInReport',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Stock Level',
				'route' => 'reports.stockLevel',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Warehouse occupancy',
				'route' => 'reports.warehouseOccupancy',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Picking speed report',
				'route' => 'reports.staffPickingReport',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Packing speed report',
				'route' => 'reports.staffPackingReport',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Shipping speed report',
				'route' => 'reports.staffShippingReport',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'divider'
			],
			[
				'name' => 'Best Selling SKU',
				'route' => 'reports.bestSellingSKU',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Daily Average Vs Daily Sales',
				'route' => 'reports.dailyAvgVsDailySales',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Fast Moving SKU',
				'route' => 'reports.fastMovingSKU',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Slow Moving SKU',
				'route' => 'reports.slowMovingSKU',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			],
			[
				'name' => 'Sales By SKU',
				'route' => 'reports.salesBySKU',
				'icon' => 'fa fa-copy',
				'access' => ['view-reports']
			]
		]
	],
	[
		'name' => 'System',
		'route' => "dashboard.system",
		'icon' => 'fa fa-sliders',
		'access' => ['view-companyProfiles','view-products','view-productCategories','view-shop','view-carrier'],
		'child' => [
			[
				'name' => 'User Company',
				'route' => 'companyProfiles.index',
				'icon' => 'fa fa-sitemap',
				'access' => ['view-companyProfiles']
			],
			[
				'name' => 'divider'
			],
			[
				'name' => 'Shop',
				'route' => 'shop.index',
				'icon' => 'fa fa-shopping-bag',
				'access' => ['view-shop'],
			],
			[
				'name' => 'Carrier',
				'route' => 'carrier.index',
				'icon' => 'fa fa-ship',
				'access' => ['view-carrier'],
			],
			[
				'name' => 'Payment method',
				'route' => 'paymentMethod.index',
				'icon' => 'fa fa-credit-card',
				'access' => ['view-paymentMethod'],
			],
			[
				'name' => 'Picking types',
				'route' => 'pickingTypes.index',
				'icon' => 'fa fa-tasks',
				'access' => ['view-pickingTypes'],
			],
			[
				'name' => 'Order type',
				'route' => 'orderType.index',
				'icon' => 'fa fa-envelope-square',
				'access' => ['view-orderType'],
			],
			[
			'name' => 'divider'
			],
			[
				'name' => 'User',
				'route' => 'user.index',
				'icon' => 'fa fa-user',
				'access' => ['view-user'],
			],
			[
				'name' => 'Role',
				'route' => 'role.index',
				'icon' => 'fa fa-lock',
				'access' => ['view-role']
			],
			[
				'name' => 'divider'
			],
			[
				'name' => 'Audit Trails',
				'route' => 'auditTrails.index',
				'icon' => 'fa fa-archive',
				'access' => ['view-auditTrails']
			]
		]
	],
//	[
//		'name' => 'User Managements',
//		'route' => null,
//		'icon' => 'fa fa-users',
//		'access' => ['view-user','view-role','view-auditTrails'],
//		'child' => [
//			[
//				'name' => 'User',
//				'route' => 'user.index',
//				'icon' => 'fa fa-user',
//				'access' => ['view-user'],
//			],
//			[
//				'name' => 'Role',
//				'route' => 'role.index',
//				'icon' => 'fa fa-lock',
//				'access' => ['view-role']
//			],
//			[
//				'name' => 'divider'
//			],
//			[
//				'name' => 'Audit Trails',
//				'route' => 'auditTrails.index',
//				'icon' => 'fa fa-archive',
//				'access' => ['view-auditTrails']
//			]
//		]
//	],
];
