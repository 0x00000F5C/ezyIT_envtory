<?php

/*
|--------------------------------------------------------------------------
| Models Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Models factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(App\Supplier::class, function (Faker\Generator $faker) {
    return [
	    'supplier_name'=>$faker->name,
		'supplier_prefix'=>$faker->name,
		'supplier_address'=>$faker->address,
		'supplier_mode'=>1,
		'supplier_pic'=>$faker->name,
		'supplier_phone'=>$faker->e164PhoneNumber,
		'supplier_email'=>$faker->email,
		'supplier_country'=>$faker->country,
		'supplier_state'=>$faker->state,
		'supplier_city'=>$faker->city,
		'supplier_zipcode'=>234345,
		'supplier_min_po'=>5,
		'supplier_max_po'=>100,
		'data_status'=>0,
    ];
});
