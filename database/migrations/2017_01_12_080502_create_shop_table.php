<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("m_shop", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("platform_id");
            $table->uuid("m_shop_categories_id");
            $table->string("shop_name", 255);
            $table->string("pic_name", 51);
            $table->boolean("mode");
            $table->string("account", 255);
            $table->string("api_key", 255);
            $table->boolean("status");
            $table->string("prefix", 6);
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_shop_categories_id")->references("id")->on("m_shop_categories");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_shop');
    }
}
