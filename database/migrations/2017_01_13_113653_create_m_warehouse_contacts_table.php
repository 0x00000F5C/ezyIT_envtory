<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMWarehouseContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("m_warehouse_contacts", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("pic", 150)->nullable();
            $table->string("phone", 45)->nullable();
            $table->string("mobile", 45)->nullable();
            $table->string("email", 45)->nullable();
            $table->uuid("m_warehouse_id");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_warehouse_id")->references("id")->on("m_warehouses")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_warehouse_contacts');
    }
}
