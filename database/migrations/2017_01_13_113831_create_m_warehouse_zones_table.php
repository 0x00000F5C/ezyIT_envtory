<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMWarehouseZonesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("m_warehouse_zones", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("name", 45);
            $table->string("prefix", 45);
            $table->integer("cycle_count");
			$table->tinyInteger("type")->default(0);
            $table->uuid("m_warehouse_id");
            $table->uuid("m_warehouse_types_id");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_warehouse_id")->references("id")->on("m_warehouses")->onDelete("cascade");
            $table->foreign("m_warehouse_types_id")->references("id")->on("m_warehouse_types")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_warehouse_zones');
    }
}
