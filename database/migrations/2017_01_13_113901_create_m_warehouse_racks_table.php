<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMWarehouseRacksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("m_warehouse_racks", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("name", 45);
            $table->string("prefix", 45);
            $table->integer("lenght");
            $table->integer("widht");
            $table->integer("height");
            $table->string("cell_prefix", 45);
            $table->integer("cell_columns");
            $table->integer("cell_rows");
            $table->uuid("m_warehouse_zones_id");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_warehouse_zones_id")->references("id")->on("m_warehouse_zones")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_warehouse_racks');
    }
}
