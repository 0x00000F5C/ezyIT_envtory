<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMWarehouseCellsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("m_warehouse_cells", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("prefix", 45);
            $table->integer("colomn");
            $table->integer("row");
            $table->string("barcode", 45);
            $table->uuid("m_warehouse_racks_id");
			$table->tinyInteger("status")->default(0)->comment("0 = Empty; 1 = Reserved;");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_warehouse_racks_id")->references("id")->on("m_warehouse_racks")->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_warehouse_cells');
    }
}
