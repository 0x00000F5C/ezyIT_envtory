<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMSuppliersTable extends Migration {
    public function up() {
		Schema::create("m_suppliers", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("supplier_name", 255);
			$table->string("supplier_prefix", 3)->unique();
			$table->string("supplier_address", 1024)->nullable();
			$table->tinyInteger("supplier_mode")->comment("1 = consigment; 2 = direct;");
			$table->string("supplier_country", 255)->nullable();
			$table->string("supplier_state", 255)->nullable();
			$table->string("supplier_city", 255)->nullable();
			$table->string("supplier_zipcode", 6)->nullable();
			$table->integer("supplier_min_po")->default(0)->nullable();
			$table->integer("supplier_max_po")->default(0)->nullable();
			$table->string("supplier_categories", 2048)->default("[]")->nullable()->comment("json format");
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
    }

    public function down() {
        Schema::dropIfExists('m_suppliers');
    }
}
