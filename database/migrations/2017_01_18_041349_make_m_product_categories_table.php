<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMProductCategoriesTable extends Migration {
    public function up() {
        Schema::create("m_product_categories", function (Blueprint $table) {
            $table->increments("id")->index();
            $table->integer("cat_parent")->comment("If parent is 0, then it''s main.");
            $table->string("cat_name", 255);
            $table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
        });
		Schema::create("m_products", function (Blueprint $table) {
			$table->increments("id")->index();
			$table->string("product_prefix", 6)->unique();
			$table->string("product_code", 16)->unique();
			$table->string("product_name", 255);
			$table->integer("product_category")->unsigned()->default(0)->comment("Correlates with `product_categories`.`id`");
			$table->integer("product_subcategory")->unsigned()->default(0)->comment("Correlates with `product_categories`.`id`");
			$table->integer("product_stock")->default(0);
			$table->string("product_brand", 255)->nullable();
			$table->string("product_model", 255)->nullable();
			$table->string("product_submodel", 255)->nullable();
			$table->string("product_size", 255)->nullable();
			$table->string("product_color", 255)->nullable();
			$table->string("product_suppliers", 2048)->default("[]")->comment("json format");
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->foreign("product_category")->references("id")->on("m_product_categories")->onDelete("restrict");
			$table->foreign("product_subcategory")->references("id")->on("m_product_categories")->onDelete("restrict");
		});
		Schema::create("t_products_specs", function (Blueprint $table) {
			$table->increments("id")->index();
			$table->integer("product_id")->unsigned()->default(0);
			$table->string("product_specs_label", 255);
			$table->string("product_specs_value", 255);
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->foreign("product_id")->references("id")->on("m_products")->onDelete("cascade");
		});
    }

    public function down() {
        Schema::dropIfExists('m_product_categories');
		Schema::dropIfExists('m_products');
		Schema::dropIfExists('t_products_specs');
    }
}
