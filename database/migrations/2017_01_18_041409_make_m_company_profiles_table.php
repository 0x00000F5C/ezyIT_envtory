<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeMCompanyProfilesTable extends Migration {
    public function up() {
		Schema::create("m_company_profiles", function (Blueprint $table) {
			$table->uuid("id");
			$table->tinyInteger("profile_type")->comment("1 = company; 2 = accounting; 3 = sales; 4 = purchase;");
			$table->string("profile_name", 255);
			$table->string("profile_billing_address", 255);
			$table->string("profile_city", 255)->nullable();
			$table->string("profile_state", 255)->nullable();
			$table->string("profile_zip", 6)->nullable();
			$table->string("profile_country", 255)->nullable();
			$table->string("profile_phone_number", 16)->nullable();
			$table->string("profile_fax_number", 16)->nullable();
			$table->string("profile_email_address", 48);
			$table->string("profile_website", 64)->nullable();
			$table->string("profile_gst", 16)->nullable();
			$table->string("profile_business_number", 16)->nullable();
			$table->string("profile_currency", 3)->nullable();
			$table->string("profile_timezone", 4)->nullable();
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
    }

    public function down() {
        Schema::dropIfExists('m_company_profiles');
    }
}
