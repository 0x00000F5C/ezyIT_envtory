<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoMasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_po_master", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("prefix",45);
            $table->date("po_date");
            $table->date("exp_date")->nullable();
            $table->uuid("m_supplier_id");
            $table->string("ship_address", 45);
            $table->string("email");
            $table->float("misc_cost")->nullable();
            $table->float("ship_cost")->nullable();
            $table->float("total_det_mount")->nullable();
            $table->float("total_amount")->nullable();
            $table->tinyInteger("status")->nullable();
            $table->string("rate", 45)->nullable();
            $table->string("reason_closed", 255)->nullable();
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_supplier_id")->references("id")->on("m_suppliers");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_po_master');
    }
}
