<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePoDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_po_detail", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("t_po_master_id");
            $table->integer("m_products_id")->unsigned()->default(0);
            $table->string("product_po_id", 17);
            $table->float("qty");
            $table->float("price");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("t_po_master_id")->references("id")->on("t_po_master");
            $table->foreign("m_products_id")->references("id")->on("m_products");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_po_detail');
    }
}
