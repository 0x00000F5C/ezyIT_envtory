<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeCarrierTable extends Migration {
	public function up() {
		Schema::create("m_carriers", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("carrier_name", 255);
			$table->string("carrier_account", 255)->nullable();
			$table->string("carrier_api_address", 255);
			$table->string("carrier_api_username", 255);
			$table->string("carrier_api_password", 255);
			$table->string("carrier_pickup_time", 255)->default("[]")->comment("json format");
			$table->integer("carrier_warehouse")->nullable();
			$table->boolean("carrier_isdefault")->default("0");
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
	}

	public function down() {
		Schema::drop('m_carriers');
	}
}
