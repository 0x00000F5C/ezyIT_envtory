<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_order_master", function (Blueprint $table) {
            $table->uuid("id");
            $table->string("order_no", 45)->nullable();
            $table->date("date");
            $table->date("due_date");
            $table->string("shop_order_no", 45)->nullable();
            $table->string("customer_name", 45)->nullable();
            $table->string("person_in_charge", 45)->nullable();
            $table->string("bill_address", 45)->nullable();
            $table->string("main_phone_cust", 45)->nullable();
            $table->string("sec_phone_cust", 45)->nullable();
            $table->string("email", 45)->nullable();
            $table->string("city", 45)->nullable();
            $table->string("state", 45)->nullable();
            $table->string("country", 45)->nullable();
            $table->text("remark")->nullable();
            $table->unsignedTinyInteger("status")->nullable();
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_order_master');
    }
}
