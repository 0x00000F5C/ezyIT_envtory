<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeAuditTrailsTable extends Migration {
    public function up() {
		Schema::create("m_audit_trails", function (Blueprint $table) {
			$table->uuid("id");
			$table->uuid("audit_actor")->references("id")->on("user");
			$table->string("audit_trigger", 255);
			$table->string("audit_event", 255);
			$table->string("audit_item", 255);
			$table->string("audit_ip", 255);
			$table->string("audit_comment", 1024)->nullable();
			$table->timestamp("audit_timestamp");
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->primary("id");
		});
    }

    public function down() {
        Schema::drop('m_audit_trails');
    }
}
