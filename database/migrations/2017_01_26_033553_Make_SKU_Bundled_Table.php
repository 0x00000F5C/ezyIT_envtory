<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSKUBundledTable extends Migration {
    public function up() {
        Schema::create("m_sku_bundles", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("sku_bundle_prefix", 255)->unique();
			$table->tinyInteger("data_status")->default("0")->comment("0 = deleted; 1 = private; 2 = public;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
    }

	public function down() {
        Schema::drop('m_sku_bundles');
    }
}
