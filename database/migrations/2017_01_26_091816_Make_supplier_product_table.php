<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSupplierProductTable extends Migration {
    public function up() {
		Schema::create("p_supplier_product", function (Blueprint $table) {
			$table->uuid("supplier_id");
			$table->uuid("product_id");
			$table->foreign("supplier_id")->references("id")->on("m_suppliers")->onDelete("cascade");
		});
    }

    public function down() {
        Schema::drop('p_supplier_product');
    }
}
