<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProductPoIdToPoDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* Schema::table('t_po_detail', function (Blueprint $table) {
            $table->string('product_po_id',17)->after('m_products_id');
        }); */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_po_detail', function (Blueprint $table) {
            $table->dropColumn('product_po_id');
        });
    }
}
