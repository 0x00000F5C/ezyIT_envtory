<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeSkuProductPriceTables extends Migration {
    public function up() {
		Schema::create("m_sku_product_price", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("sku_price_sku_prefix", 255)->comment("correlates to sku_products.sku_product_prefix");
			$table->decimal("sku_price_current", 16, 2);
			$table->decimal("sku_price_cost", 16, 2);
			$table->decimal("sku_price_min", 16, 2);
			$table->decimal("sku_price_max", 16, 2);
			$table->tinyInteger("sku_price_discounttype");
			$table->decimal("sku_price_discamount", 16, 2);
			$table->dateTime("sku_price_period_begin");
			$table->dateTime("sku_price_period_end");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
			$table->foreign("sku_price_sku_prefix")->references("sku_product_prefix")->on("m_sku_products")->onDelete("cascade");
		});
		Schema::create("m_sku_product_price_history", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("sku_price_product_prefix", 255)->comment("correlates to sku_products.sku_product_prefix");
			$table->decimal("sku_price_new", 16, 2);
			$table->uuid("sku_price_updator")->comment("correlates to users.id");
			$table->tinyInteger("sku_price_status")->comment("1 = open; 2 = accept; 3 = reject;");
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
			$table->foreign("sku_price_product_prefix")->references("sku_product_prefix")->on("m_sku_products")->onDelete("cascade");
		});
    }

    public function down() {
        Schema::dropIfExists('m_sku_product_price');
		Schema::dropIfExists('m_sku_product_price_history');
    }
}
