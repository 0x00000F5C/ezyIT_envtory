<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteInvoiceColumnFromTInvoiceMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasColumn("t_invoice_master", "invoice")) {
			Schema::table('t_invoice_master', function (Blueprint $table) {
				$table->dropColumn('invoice');
			});
		}
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_invoice_master', function (Blueprint $table) {
            $table->string('invoice',45)->nullable()->after('total_amount');
        });
    }
}
