<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePSkuproductsShopTable extends Migration {
    public function up() {
		Schema::create("p_skuproducts_shops", function (Blueprint $table) {
			$table->uuid("sku_product_id");
			$table->uuid("shop_id");
			$table->foreign("sku_product_id")->references("id")->on("m_sku_products")->onDelete("cascade");
		});
		Schema::create("m_zone_type", function (Blueprint $table) {
			$table->uuid("id");
			$table->string("prefix", 3);
			$table->string("name", 64);
			$table->integer("cycle_count")->default(0);
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
		});
		Schema::create("m_warehouse_zones_categories", function (Blueprint $table) {
			$table->uuid("warehouse_zones_id");
			$table->uuid("category_id");
		});
    }

    public function down() {
        Schema::dropIfExists('p_skuproducts_shops');
        Schema::drop('m_zone_type');
		Schema::dropIfExists('m_warehouse_zones_categories');
    }
}
