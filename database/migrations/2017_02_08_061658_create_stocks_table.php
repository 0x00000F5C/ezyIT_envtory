<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_stocks", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("t_invoice_master_id")->nullable();
            $table->integer("m_products_id")->unsigned()->default(0);
            $table->date("received_date");
            $table->string("batch_id",45);
            $table->string("stock_type",2);
            $table->tinyInteger("status");
            $table->uuid("zone_type");
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("m_products_id")->references("id")->on("m_products");
            $table->foreign("zone_type")->references("id")->on("m_zone_type")->onDelete("RESTRICT")->onUpdate("CASCADE");
            $table->foreign("t_invoice_master_id")->references("id")->on("t_invoice_master")->onDelete("RESTRICT")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_stocks');
    }
}
