<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStockHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("t_stock_histories", function (Blueprint $table) {
            $table->uuid("id");
            $table->uuid("t_stocks_id");
            $table->integer("qty");
            $table->boolean("is_in")->default(1);
			$table->string("note",25)->nullable();
            $table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->primary("id");
            $table->foreign("t_stocks_id")->references("id")->on("t_stocks")->onDelete("CASCADE")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_stock_histories');
    }
}
