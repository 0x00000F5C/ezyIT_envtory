<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeBrandsTable extends Migration {
    public function up() {
        Schema::create("m_brands", function (Blueprint $table) {
			$table->increments("id")->index();
			$table->string("name", 256);
			$table->string("global_identifier", 256)->nullable();
			$table->timestamps();
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
		});
    }

    public function down() {
        Schema::dropIfExists("m_brands");
    }
}
