<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Alter2TableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->dropColumn('product_prefix');
            $table->dropColumn('product_name');
            $table->dropColumn('product_brand');
            $table->dropColumn('product_model');
            $table->dropColumn('data_status');
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->string('product_prefix',50);
            $table->string('product_name',50);
            $table->string('product_brand',50);
            $table->string('product_model',50);
            $table->unsignedTinyInteger('data_status');
            $table->dropColumn('deleted_at');
        });
    }
}
