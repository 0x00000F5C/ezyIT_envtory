<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkuAttributesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_product_sku_attributes', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('m_product_sku_id');
            $table->string('name',255);
            $table->string('value',255);
            $table->timestamps();
            $table->primary("id");
            $table->foreign('m_product_sku_id')->references('id')->on('m_product_sku')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_product_sku_attributes');
    }
}
