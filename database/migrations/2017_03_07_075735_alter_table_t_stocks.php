<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableTStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_stocks', function (Blueprint $table) {
            $table->dropForeign("t_stocks_zone_type_foreign");
            $table->dropColumn("zone_type");
        });
        Schema::table('t_stocks', function (Blueprint $table) {
            $table->uuid("zone_type")->nullable()->after("status");
            $table->foreign("zone_type")->references("id")->on("m_zone_type")->onDelete("RESTRICT")->onUpdate("CASCADE");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_stocks', function (Blueprint $table) {
            // $table->uuid("zone_type")->nullable()->after("status");
            // $table->foreign("zone_type")->references("id")->on("m_zone_type")->onDelete("RESTRICT")->onUpdate("CASCADE");
        });
    }
}
