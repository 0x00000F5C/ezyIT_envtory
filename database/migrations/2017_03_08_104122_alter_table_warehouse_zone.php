<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableWarehouseZone extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_warehouse_zones', function (Blueprint $table) {
            $table->dropForeign('m_warehouse_zones_m_warehouse_types_id_foreign');
            $table->dropColumn('m_warehouse_types_id');
            $table->dropColumn('type');
        });
        Schema::table('m_warehouse_zones', function (Blueprint $table) {
            $table->uuid('type')->after('cycle_count');
            $table->foreign('type')->references('id')->on('m_zone_type')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_warehouse_zones', function (Blueprint $table) {
            $table->dropForeign('m_warehouse_zones_type_foreign');
            $table->dropColumn('type');
        });
        Schema::table('m_warehouse_zones', function (Blueprint $table) {
            $table->tinyInteger("type")->default(0)->after('cycle_count');
            $table->uuid("m_warehouse_types_id")->after('m_warehouse_id');
        });
    }
}
