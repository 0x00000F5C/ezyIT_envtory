<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCellToTableTStocks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_stocks', function (Blueprint $table) {
            $table->uuid('cell_id')->nullable()->after('zone_type');
            $table->foreign('cell_id')->references('id')->on('m_warehouse_cells')->onDelete('restrict')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_stocks', function (Blueprint $table) {
            $table->dropForeign('t_stocks_cell_id_foreign');
            $table->dropColumn('cell_id');
        });
    }
}
