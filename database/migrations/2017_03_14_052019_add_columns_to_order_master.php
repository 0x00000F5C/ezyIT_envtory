<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrderMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
        	$table->uuid('shop_id')->after('order_no');
        	$table->integer('shop_order_id')->nullable()->after('shop_id');
            $table->string('customer_last_name', 45)->nullabel()->after('customer_name');
			$table->string('payment_method', 45)->nullable();
			$table->string('delivery_info', 100)->nullable();
			$table->boolean('gift_option')->default(0);
			$table->string('gift_message', 255)->nullable();
			$table->string('bill_first_name', 45)->nullable()->after('person_in_charge');
			$table->string('bill_last_name', 45)->nullable()->after('bill_first_name');
			$table->string('bill_address2', 45)->nullable()->after('bill_address');
			$table->string('bill_postal_code', 20)->nullable()->after('bill_address2');
			$table->string('nat_registration_no', 45)->nullable();
			$table->integer('items_count')->nullable();
			$table->string('extra_attributes')->nullable();
			$table->dropColumn('billing_cust_lastname');
	        $table->foreign('shop_id')
		        ->references('id')
		        ->on('m_shop')
		        ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
	        $table->dropForeign('t_order_master_shop_id_foreign');
            $table->dropColumn('shop_id');
            $table->dropColumn('shop_order_id');
            $table->dropColumn('customer_last_name');
            $table->dropColumn('payment_method');
            $table->dropColumn('delivery_info');
            $table->dropColumn('gift_option');
            $table->dropColumn('gift_message');
            $table->dropColumn('bill_first_name');
            $table->dropColumn('bill_last_name');
            $table->dropColumn('bill_address2');
            $table->dropColumn('bill_postal_code');
            $table->dropColumn('nat_registration_no');
            $table->dropColumn('items_count');
            $table->dropColumn('extra_attributes');
            $table->string('billing_cust_lastname', 100)->nullable();
        });
    }
}
