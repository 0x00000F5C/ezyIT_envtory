<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class RemoveModeFromShopTable extends Migration {
    public function up() {
		Schema::table("m_shop", function(Blueprint $table) {
			$table->dropColumn("mode");
		});
    }

    public function down() {
		Schema::table("m_shop", function(Blueprint $table) {
			$table->tinyInteger("mode")->default(0)->after("pic_name");
		});
    }
}
