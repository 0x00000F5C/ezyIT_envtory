<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MOrderTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_order_types', function (Blueprint $table) {
            $table->uuid('id');
            $table->string("name", 255);
            $table->integer('min_qty')->nullable();
            $table->integer('max_qty')->nullable();
            $table->uuid('insert_by');
            $table->uuid('update_by');
            $table->timestamps();
            $table->primary("id");
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('m_order_types');
    }
}
