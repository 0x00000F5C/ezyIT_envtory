<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePickingListTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_picking_lists', function (Blueprint $table) {
            $table->uuid('id');
	        $table->uuid("user_id");
	        $table->string("picking_code", 50);
	        $table->date("date");
	        $table->tinyInteger('status')->default(0);
	        $table->uuid("insert_by")->nullable();
	        $table->uuid("update_by")->nullable();
            $table->timestamps();
	        $table->primary("id");
	        $table->foreign("user_id")->references("id")->on("users");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_picking_lists');
    }
}
