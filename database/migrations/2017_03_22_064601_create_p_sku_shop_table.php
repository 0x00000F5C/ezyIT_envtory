<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePSkuShopTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('p_skuproducts_shops');
        Schema::create('p_sku_shop', function (Blueprint $table) {
            $table->uuid('sku_id');
            $table->uuid('shop_id');
            $table->foreign('sku_id')->references('id')->on('m_product_sku')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('m_shop')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_sku_shop');
    }
}
