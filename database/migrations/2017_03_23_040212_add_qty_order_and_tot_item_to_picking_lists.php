<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQtyOrderAndTotItemToPickingLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_picking_lists', function (Blueprint $table) {
            $table->integer('qty_of_order')->nullable();
            $table->integer('total_item')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_picking_lists', function (Blueprint $table) {
            $table->dropColumn(['qty_of_order','total_item']);
        });
    }
}
