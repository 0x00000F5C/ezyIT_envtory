<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropTablePickingDet extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
	    Schema::dropIfExists('t_picking_items');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
	    Schema::create('t_picking_items', function (Blueprint $table) {
		    $table->uuid('id');
		    $table->uuid("picking_list_id");
		    $table->uuid("order_detail_id");
		    $table->uuid("tray_id")->nullable();
		    $table->string("item_id");
		    $table->tinyInteger("status")->default(0);
		    $table->timestamps();
		    $table->primary("id");
		    $table->foreign("picking_list_id")->references("id")->on("t_picking_lists");
		    $table->foreign("order_detail_id")->references("id")->on("t_order_detail");
	    });
    }
}
