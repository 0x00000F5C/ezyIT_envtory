<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkuPricesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('m_sku_product_price');
        Schema::create('m_product_sku_price', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('sku_id');
            $table->uuid('shop_id');
            $table->decimal('price', 16, 2);
            $table->decimal('special_price', 16, 2)->nullable();
            $table->date('special_from_date')->nullable();
            $table->date('special_to_date')->nullable();
            $table->uuid('insert_by');
            $table->uuid('update_by');
            $table->timestamps();

            $table->primary('id');
            $table->foreign('sku_id')->references('id')->on('m_product_sku')->onDelete('cascade');
            $table->foreign('shop_id')->references('id')->on('m_shop')->onDelete('cascade');
            $table->foreign('insert_by')->references('id')->on('users');
            $table->foreign('update_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_product_sku_price');
    }
}
