<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Support\Facades\DB;

class MakeOrderStatusTable extends Migration {
    public function up() {
		Schema::create("m_order_statuses", function(Blueprint $table){
			$table->increments("id");
			$table->string("status_name");
		});
    }

    public function down() {
        Schema::drop("m_order_statuses");
    }
}