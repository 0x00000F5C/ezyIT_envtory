<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakePickingTypeTable extends Migration {
    public function up() {
		Schema::create("m_picking_types", function(Blueprint $table){
			$table->uuid("id");
			$table->string("picking_type_name", 255);
			$table->integer("qty");
			$table->uuid("order_type");
			$table->primary("id");
			$table->foreign("order_type")->references("id")->on("m_order_types")->onDelete("restrict");
		});
    }

    public function down() {
        Schema::drop("m_picking_types");
    }
}
