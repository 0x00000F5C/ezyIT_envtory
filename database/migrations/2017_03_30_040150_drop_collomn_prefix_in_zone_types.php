<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropCollomnPrefixInZoneTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_zone_type', function (Blueprint $table) {
            $table->dropColumn('prefix');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_zone_type', function (Blueprint $table) {
            $table->string('prefix', 3)->nullable();
        });
    }
}
