<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DropColumnInOrderType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_order_types', function (Blueprint $table) {
	        $table->dropColumn(['insert_by','update_by']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_order_types', function (Blueprint $table) {
            $table->string('insert_by');
            $table->string('update_by');
        });
    }
}
