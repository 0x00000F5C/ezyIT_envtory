<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePickingTypeTable extends Migration {
    public function up() {
		Schema::table("m_picking_types", function(Blueprint $table){
			$table->uuid("update_by")->nullable()->after("order_type");
			$table->uuid("insert_by")->nullable()->after("order_type");
			$table->timestamps();
		});
    }

    public function down() {
	    Schema::table("m_picking_types", function(Blueprint $table) {
	        $table->dropColumn(['update_by','insert_by','created_at','updated_at']);
	    });
    }
}
