<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddOrderTypesToTOrderMaster extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->uuid("order_types_id")->nullable();
            $table->foreign("order_types_id")->references("id")->on("m_order_types");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->dropForeign('t_order_master_order_types_id_foreign');
            $table->dropColumn("order_types_id");
        });
    }
}
