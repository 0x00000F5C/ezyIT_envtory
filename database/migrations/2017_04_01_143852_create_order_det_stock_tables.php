<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderDetStockTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_order_detail_t_stocks', function (Blueprint $table) {
            $table->increments('id');
            $table->uuid('t_order_detail_id');
            $table->uuid('t_stocks_id');
            $table->string('batch_id')->nullable();
            $table->string('cell_id')->nullable();
            $table->integer('qty');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_order_detail_t_stocks');
    }
}
