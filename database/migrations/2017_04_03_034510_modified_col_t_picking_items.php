<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifiedColTPickingItems extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_picking_items', function (Blueprint $table) {
            $table->dropForeign('t_picking_items_order_detail_id_foreign');
            $table->dropColumn('order_detail_id');

            $table->integer('det_stock_id')->after('picking_list_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_picking_items', function (Blueprint $table) {
	        $table->uuid("order_detail_id");
	        $table->foreign("order_detail_id")->references("id")->on("t_order_detail")->onDelete('cascade');

	        $table->dropColumn('det_stock_id');
        });
    }
}
