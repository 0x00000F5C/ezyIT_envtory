<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ModifTOrderMastersTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->dropColumn([
            	'bill_address','bill_address2','ship_address_1','ship_address_2','ship_city','extra_attributes',
            ]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->string('bill_address')->nullable();
            $table->string('bill_address2')->nullable();
            $table->string('ship_address_1')->nullable();
            $table->string('ship_address_2')->nullable();
            $table->string('ship_city')->nullable();
            $table->string('extra_attributes')->nullable();
        });
    }
}
