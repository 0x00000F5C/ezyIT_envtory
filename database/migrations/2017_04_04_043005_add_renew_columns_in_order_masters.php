<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRenewColumnsInOrderMasters extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
            $table->text('bill_address')->after('bill_last_name')->nullable();
            $table->text('bill_address2')->after('bill_address')->nullable();
            $table->text('ship_address_1')->after('ship_company_name')->nullable();
            $table->text('ship_address_2')->after('ship_address_1')->nullable();
            $table->string('ship_city',45)->after('ship_country')->nullable();
            $table->text('extra_attributes')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_master', function (Blueprint $table) {
	        $table->dropColumn([
		        'bill_address','bill_address2','ship_address_1','ship_address_2','ship_city','extra_attributes',
	        ]);
        });
    }
}
