<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsToOrderDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_detail', function (Blueprint $table) {
            $table->integer('order_item_id')->after('product_sku_id')->nullable();
            $table->double('paid_price', 15,2)->after('price')->default(0);
            $table->string('currency', 10)->after('paid_price')->nullable();
            $table->double('tax_amount',15,2)->after('currency')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_detail', function (Blueprint $table) {
            $table->dropColumn(['order_item_id','paid_price','currency','tax_amount']);
        });
    }
}
