<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;
use App\Library;

class AddLevelToPermissions extends Migration {
    public function up() {
		/* $config = config("laravel-permission.table_names");
		Schema::table($config["permissions"], function(Blueprint $table){
			$table->tinyInteger("level")->default(3)->after("name");
		}); */
		Schema::create("module_levels", function(Blueprint $table){
			$table->uuid("id");
			$table->string("module", 48);
			$table->tinyInteger("level")->default("3");
			$table->timestamps();
			$table->primary("id");
		});
    }

    public function down() {
		/* $config = config("laravel-permission.table_names");
		Schema::table($config["permissions"], function(Blueprint $table){
			$table->dropColumn("level");
		}); */
		Schema::drop("module_levels");
    }
}
