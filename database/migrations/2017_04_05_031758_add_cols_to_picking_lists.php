<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColsToPickingLists extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_picking_lists', function (Blueprint $table) {
            $table->uuid('picking_type_id')->nullable();
            $table->integer('qty_of_tray')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_picking_lists', function (Blueprint $table) {
            $table->dropColumn(['picking_type_id','qty_of_tray']);
        });
    }
}
