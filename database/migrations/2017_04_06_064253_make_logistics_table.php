<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use Illuminate\Support\Facades\DB;
use Webpatser\Uuid\Uuid;
use App\User;

class MakeLogisticsTable extends Migration {
    public function up() {
		Schema::create("m_packing_logistics", function(Blueprint $table){
			$table->uuid("id");
			$table->string("packing_code", 47)->unique();
			$table->string("packing_name", 255);
			$table->uuid("operator");
			$table->float("width");
			$table->float("height");
			$table->float("length");
			$table->integer("qty");
			$table->uuid("insert_by");
			$table->uuid("update_by");
			$table->timestamps();
			$table->primary("id");
			$table->foreign("operator")->references("id")->on("users")->onDelete("restrict");
		});
    }

    public function down() {
		Schema::drop("m_packing_logistics");
    }
}
