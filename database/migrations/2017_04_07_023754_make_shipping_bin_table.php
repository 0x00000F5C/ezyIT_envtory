<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeShippingBinTable extends Migration {
    public function up() {
		Schema::create("m_shipping_bin", function(Blueprint $table){
			$table->uuid("id");
			$table->string("bin_id", 16);
			$table->tinyInteger("is_used");
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
			$table->timestamps();
			$table->primary("id");
		});
    }

    public function down() {
		Schema::drop("m_shipping_bin");
    }
}
