<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class MakeStockinlogisticTable extends Migration {
    public function up() {
		Schema::create("m_stock_in_logistics", function(Blueprint $table){
			$table->uuid("id");
			$table->string("packing_code", 47)->unique();
			$table->string("packing_name", 255);
			$table->uuid("operator");
			$table->integer("qty");
			$table->uuid("insert_by");
			$table->uuid("update_by");
			$table->timestamps();
			$table->primary("id");
			$table->foreign("operator")->references("id")->on("users")->onDelete("restrict");
		});
    }

    public function down() {
		Schema::drop("m_stock_in_logistics");
    }
}
