<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateLogisticsTable extends Migration {
    public function up() {
		Schema::table("m_packing_logistics", function(Blueprint $table){
			$table->dropColumn("insert_by");
			$table->dropColumn("update_by");
		});
		Schema::table("m_stock_in_logistics", function(Blueprint $table){
			$table->dropColumn("insert_by");
			$table->dropColumn("update_by");
		});
		Schema::table("m_packing_logistics", function(Blueprint $table){
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
		});
		Schema::table("m_stock_in_logistics", function(Blueprint $table){
			$table->uuid("insert_by")->nullable();
			$table->uuid("update_by")->nullable();
		});
    }

    public function down() {
        //
    }
}
