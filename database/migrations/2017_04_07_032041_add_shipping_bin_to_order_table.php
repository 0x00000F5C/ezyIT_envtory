<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddShippingBinToOrderTable extends Migration {
    public function up() {
		Schema::table("t_order_master", function(Blueprint $table) {
			$table->uuid("bin_id")->nullable()->comment("relates with m_shipping_bin.id")->after("status");
		});
    }

    public function down() {
		Schema::table("t_order_master", function(Blueprint $table) {
			$table->dropColumn("bin_id");
		});
    }
}
