<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddForeignKeyInOrderDetStock extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_order_detail_t_stocks', function (Blueprint $table) {
            $table->foreign('t_order_detail_id')->references('id')->on('t_order_detail')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_order_detail_t_stocks', function (Blueprint $table) {
            $table->dropForeign('t_order_detail_t_stocks_t_order_detail_id_foreign');
        });
    }
}
