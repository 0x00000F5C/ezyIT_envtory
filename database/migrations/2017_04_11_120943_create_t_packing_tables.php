<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTPackingTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_packings', function (Blueprint $table) {
            $table->increments('id');
            $table->string('packing_id');
	        $table->uuid('order_id');
            $table->date('packing_date');
            $table->uuid('bin_id');
	        $table->tinyInteger('status')->default(0);
	        $table->uuid("insert_by")->nullable();
	        $table->uuid("update_by")->nullable();
            $table->timestamps();

	        $table->foreign('order_id')->references('id')->on('t_order_master')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_packings');
    }
}
