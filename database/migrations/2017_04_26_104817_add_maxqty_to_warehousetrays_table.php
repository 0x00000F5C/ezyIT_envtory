<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxqtyToWarehousetraysTable extends Migration {
    public function up() {
        Schema::table("m_warehouse_trays", function(Blueprint $table) {
			$table->integer("max_qty")->after("size");
		});
    }

    public function down() {
        Schema::table("m_warehouse_trays", function(Blueprint $table) {
			$table->dropColumn("max_qty");
		});
    }
}
