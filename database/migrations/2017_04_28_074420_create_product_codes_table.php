<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductCodesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_product_code', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('m_product_categories_id');
            $table->string('product_code');
            $table->string('name');
            $table->string('brand');
            $table->string('model');
            $table->string('submodel')->nullable();
            $table->timestamps();

            $table->foreign('m_product_categories_id')->references('id')->on('m_product_categories')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_product_code');
    }
}
