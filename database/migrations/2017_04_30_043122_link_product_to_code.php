<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LinkProductToCode extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->string('product_prefix')->nullable()->change();
            $table->string('product_code',255)->change();
            $table->unsignedInteger('m_product_code_id')->nullable()->after('id');

            $table->foreign('m_product_code_id')->references('id')->on('m_product_code')->onDelete('restrict');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_products', function (Blueprint $table) {
            $table->dropForeign('m_products_m_product_code_id_foreign');
            $table->dropColumn('m_product_code_id');
        });
    }
}
