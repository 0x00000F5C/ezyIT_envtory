<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnToMPickingTypes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_picking_types', function (Blueprint $table) {
            $table->double('troy_needed',20,2);
            $table->string('troy_needed_per')->default('order');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_picking_types', function (Blueprint $table) {
            $table->dropColumn([
                'troy_needed',
                'troy_needed_per',
            ]);
        });
    }
}
