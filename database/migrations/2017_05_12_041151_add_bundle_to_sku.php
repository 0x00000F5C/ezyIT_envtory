<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBundleToSku extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->dropForeign('m_product_sku_bundle_foreign');
        });
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->unsignedInteger('m_products_id')->nullable()->change();
            $table->boolean('bundle')->default(0)->change();
            $table->unsignedInteger('bundle_qty')->default(0)->nullable()->after('bundle');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('m_product_sku', function (Blueprint $table) {
            $table->unsignedInteger('m_products_id')->change();
            $table->dropColumn('bundle_qty');
            $table->uuid('bundle')->nullable()->change();
            $table->foreign('bundle')->references('id')->on('m_product_sku')->onDelete('restrict');
        });
    }
}
