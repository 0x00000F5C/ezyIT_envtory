<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSkuBundlesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('m_product_sku_bundle', function (Blueprint $table) {
            $table->uuid('id');
            $table->uuid('bundle_id');
            $table->uuid('sku_id');
            $table->unsignedInteger('qty');
//            $table->foreign('bundle_id')->references('id')->on('m_product_sku')->onDelete('cascade');
//            $table->foreign('sku_id')->references('id')->on('m_product_sku')->onDelete('restrict');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('m_product_sku_bundle');
    }
}
