<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTShippingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_shippings', function (Blueprint $table) {
            $table->increments('id');
            $table->dateTime('prepare_date');
            $table->integer("weight");
            $table->uuid('packing_logistic_id');
            $table->uuid("courier_id");
            $table->uuid('order_id');
            $table->uuid('shipping_bin_id');
            $table->string('tracking_number', 60);
            $table->string('courier_name', 60)->nullable();
            $table->integer("status")->default(0);

            $table->timestamps();

//            $table->foreign('order_id')->references('id')->on('t_order_master')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_shippings');
    }
}
