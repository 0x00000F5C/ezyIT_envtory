<?php

use Illuminate\Database\Seeder;

use App\User;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::first();
        for ($offset=0; $offset < 60000; $offset = $offset+1000) {
			Artisan::queue('lazada:import-brands', [
		        'user_id' => $user->id,
		        'offset' => $offset
		    ]);
		}
    }
}
