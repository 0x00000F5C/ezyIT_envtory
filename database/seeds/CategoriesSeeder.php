<?php

use Illuminate\Database\Seeder;

use App\User;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$user = User::first();
        Artisan::queue('lazada:import-categories', [
	        'user_id' => $user->id
	    ]);
    }
}
