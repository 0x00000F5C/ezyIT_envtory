<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(UsersTableSeeder::class);
        $this->call(CategoriesSeeder::class);
        $this->call(BrandSeeder::class);
        $this->call(ZoneTypeSeeder::class);
        $this->call(WarehouseSeeder::class);
        $this->call(OrderTypeSeeder::class);
        $this->call(OrderStatusesSeeder::class);
        $this->call(PackingLogisticSeeder::class);
    }
}
