<?php

use Illuminate\Database\Seeder;

class OrderStatusesSeeder extends Seeder {
    public function run() {
		DB::table("m_order_statuses")->insert([
			["status_name" => "HOLD"],
			["status_name" => "NEW"],
			["status_name" => "WFP"],
			["status_name" => "PICKPRO"],
			["status_name" => "PICKCOM"],
            ["status_name" => "PACKPRO"],
            ["status_name" => "PACKCOM"],
			["status_name" => "RTSHP"],
			["status_name" => "SHP"],
			["status_name" => "DELVD"],
			["status_name" => "RETURN"],
			["status_name" => "CANCEL"],
			["status_name" => "CHG"]
		]);
    }
}
