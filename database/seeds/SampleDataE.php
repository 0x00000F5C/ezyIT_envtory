<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;

use App\Supplier;
use App\SupplierContacts;
use App\ProductCategories;
use App\Brands;
use App\User;

class SampleDataE extends Seeder {
    public function run() {
		$suppliers[0] = Supplier::create([
			"supplier_name" => "Ezy Account Pte Ltd",
			"supplier_prefix" => "EAP",
			"supplier_address" => "Desert Road",
			"supplier_mode" => "1",
			"supplier_country" => "SomeCountry",
			"supplier_state" => "SomeState",
			"supplier_city" => "SomeCity",
			"supplier_zipcode" => "32767",
			"supplier_min_po" => "1",
			"supplier_max_po" => "100",
			"supplier_categories" => "[]",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06"
		])->id;
		$suppliers[1] = Supplier::create([
			"supplier_name" => "EzyIT Pte Ltd",
			"supplier_prefix" => "EIT",
			"supplier_address" => "Appetizer road",
			"supplier_mode" => "2",
			"supplier_country" => "SomeCountry2",
			"supplier_state" => "SomeState2",
			"supplier_city" => "SomeCity3",
			"supplier_zipcode" => "40274",
			"supplier_min_po" => "1",
			"supplier_max_po" => "250",
			"supplier_categories" => "[]",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
		])->id;
		SupplierContacts::create([
			"supplier_id" => $suppliers[0],
			"contact_name" => "Jimmy T",
			"phone" => "1234567",
			"email" => "JimBo@gMbo.com"
		]);
		SupplierContacts::create([
			"supplier_id" => $suppliers[0],
			"contact_name" => "Nama 2",
			"phone" => "000000000",
			"email" => "emaildua@gMbo.com"
		]);
		SupplierContacts::create([
			"supplier_id" => $suppliers[0],
			"contact_name" => "Nama 3",
			"phone" => "111111111",
			"email" => "emailtiga@gMbo.com"
		]);
		SupplierContacts::create([
			"supplier_id" => $suppliers[1],
			"contact_name" => "F Richakdi",
			"phone" => "5432311",
			"email" => "fr-nya@prot.com"
		]);
		ProductCategories::create([
			"id" => "1",
			"cat_parent" => "0",
			"cat_name" => "Handphone and Gadget",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "2",
			"cat_parent" => "1",
			"cat_name" => "Smartphone",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "3",
			"cat_parent" => "1",
			"cat_name" => "Smartwatch",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "4",
			"cat_parent" => "1",
			"cat_name" => "Utility phone",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "5",
			"cat_parent" => "0",
			"cat_name" => "Furniture",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "6",
			"cat_parent" => "5",
			"cat_name" => "Sofa",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "7",
			"cat_parent" => "5",
			"cat_name" => "Couch",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "8",
			"cat_parent" => "5",
			"cat_name" => "Chair",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "9",
			"cat_parent" => "0",
			"cat_name" => "Electronics",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "10",
			"cat_parent" => "9",
			"cat_name" => "HDTV",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "11",
			"cat_parent" => "9",
			"cat_name" => "Sound system",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
		ProductCategories::create([
			"id" => "12",
			"cat_parent" => "9",
			"cat_name" => "Notebook",
			"data_status" => "2",
			"created_at" => "2017-01-19 14:17:06",
			"updated_at" => "2017-01-19 14:17:06",
			"insert_by" => User::first()->id,
			"update_by" => User::first()->id
		]);
    }
}
