<?php

use Illuminate\Database\Seeder;

use App\User;
use App\Role;
use App\Permission;
use App\Library;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        $user1 = User::create([
			  'name' => 'Superadmin',
    		  'firstname' => 'Super',
    		  'email' => 'superadmin@envitory.com',
    		  'password' => bcrypt('secret'),
    		]);
        $user2 = User::create([
              'name' => 'Administrator',
              'firstname' => 'Admin',
    		  'email' => 'admin@envitory.com',
    		  'password' => bcrypt('secret'),
    		]);

        $admin = Role::create(['name' => 'admin']);
        $superadmin = Role::create(['name' => 'superadmin']);
        $user1->assignRole('superadmin');
    	$user2->assignRole('admin');

        $menus = ['user','role'];

        foreach ($menus as $menu) {
          foreach (Library::permission() as $permission) {
            Permission::create(['name' => $permission.'-'.$menu]);
            $superadmin->givePermissionTo($permission.'-'.$menu);
            if ($menu != 'role') {
                $admin->givePermissionTo($permission.'-'.$menu);
            }
          }
        }

    }
}
