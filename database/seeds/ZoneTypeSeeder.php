<?php

use Illuminate\Database\Seeder;

class ZoneTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $zone= [
                ['id' => Uuid::generate()->string,
                'prefix' => 'GEN1',
                'name' => 'General Stock',
                'cycle_count' =>'0',
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
                ],
                ['id' => Uuid::generate()->string,
                    'prefix' => 'FAS1',
                    'name' => 'Fast Stock',
                    'cycle_count' =>'0',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
                ['id' => Uuid::generate()->string,
                    'prefix' => 'PRO1',
                    'name' => 'Promotion Stock',
                    'cycle_count' =>'0',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ],
                ['id' => Uuid::generate()->string,
                    'prefix' => 'RET1',
                    'name' => 'Return Stock',
                    'cycle_count' =>'0',
                    'created_at' => \Carbon\Carbon::now(),
                    'updated_at' => \Carbon\Carbon::now(),
                ]
        ];
        DB::table('m_zone_type')->insert($zone);
    }
}
