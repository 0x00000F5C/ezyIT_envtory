	Vue.filter('truncate', function(value) {
	  if(value.length < 30) {
	    return value;
	  }
	  return value.substring(0, 27) + ' ...';
	});

	var app = new Vue({
	
		el: "#categories",
		data: {
			categories: [],
			childCategories1: [],
			childCategories2: [],
			childCategories3: [],
			childCategories4: [],
			childCategories5: [],
			activeItem: [],
			selectedCategory: []
		},
		mounted() {
			const vm = this;
			axios.get('/app/products/categories/0').then(function(response){
				vm.categories.push(response.data)
			});
		},
		methods: {
			isActive(categories,type) {
		      return this.activeItem[type] === categories
		    },
			isSelected(categories) {
		      return this.selectedCategory === categories
		    },
		    getChild(parent,index) {
				const vm = this;
				length = vm.categories.length;
				vm.categories.splice(index+1,length);
				if (parent.children) {
					this.selectedCategory = [];
					axios.get('/app/products/categories/'+parent.id).then(function(response){
						vm.categories.push(response.data)
					});
				}else{
					this.selectedCategory = parent;
				}
				this.activeItem.splice(index,length); this.activeItem.push(parent);
			},
		}
	})
