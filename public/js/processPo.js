$(document).ready(function(){
	var prefix = $('#prefix').val();
	if($.trim(prefix))
		loadPoData(prefix)
});
$.ajaxSetup({
	headers: {
		'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	}
});
$('#purchase_prefix').selectize({
    valueField: 'prefix',
    labelField: 'prefix',
    searchField: 'prefix',
    options: [],
    create: false,
    load: function(query, callback) {
        if (!query.length) return callback();
        dataProvider('/app/po/searchPo',{po : query},callback);
    }
});

function dataProvider(dataUrl,query,callback){
    $.ajax({
      url: dataUrl,
      type: 'POST',
      dataType: 'json',
      data: query,
      error: function() {
          callback();
      },
      success: function(res) {
          callback(res);
      }
    });
  }

var tableProduct = $("#product-table").DataTable({
	data:[],
	columns: [
	            { "data": "no" },
	            { "data": "product_po_id" },
	            { "data": "product_code"  },
	            { "data": "product_name"  },
	            { "data": "qty", "class": "right" },
	            { "data": "received", "class": "right" },
	            { "data": "price", "class": "right" },
	            { "data": "amount", "class": "right" },
	],
	rowCallback: function (row, data) {},
	filter: false,
	info: false,
	paging: false,
	ordering: false,
	processing: true,
	retrieve: true,
    scrollX: true,
    scrollCollapse: true,
	language: {
	  emptyTable: "Add Purchase Order Id"
	}
});

var tableInvoice = $("#invoice-table").DataTable({
	data:[],
	columns: [
	            { "data": "no" },
	            { "data": "product_code"  },
	            { "data": "product_name"  },
	            { "data": "qty", "class": "right" },
	            { "data": "received", "class": "right" },
	            { "data": "invoice_qty" },
	            { "data": "price", "class": "right"},
	            { "data": "invoice_price" },
	            { "data": "invoice_amount", "class": "right" },
	],
	rowCallback: function (row, data) {},
	filter: false,
	info: false,
	paging: false,
	ordering: false,
	processing: true,
	retrieve: true,
	language: {
	  emptyTable: "No Product"
	}
});

var invoiceList = $("#invoice-list").DataTable({
	data:[],
	columns: [
	            { "data": "no" },
	            { "data": "prefix"  },
	            { "data": "date"  },
	            { "data": "total_amount", "class": "right" },
	            { "data": "action", "class": "right" },
	],
	rowCallback: function (row, data) {},
	filter: false,
	info: false,
	paging: false,
	ordering: false,
	processing: true,
	retrieve: true,
	language: {
	  emptyTable: "No Invoice Yet"
	}
});

function loadPoData(id){
    $.ajax({
        url: "/app/po/loadPoData",
        type: "post",
        data: { po: id }
    }).done(function (result) {
    	$('.prefix').text(result.po.prefix);
    	$('.po_prefix').val(result.po.prefix);
    	$('#supplier_name').text(result.po.supplier.supplier_name);
    	$('#po_date').text(result.po.po_date);
    	$('#email').text(result.po.email);
    	$('#invoice_ship_cost').val(result.po.ship_cost);
    	$('#invoice_misc_cost').val(result.po.misc_cost);
    	$('.t_po_master_id').val(result.po.id);
    	$('.processContent').show();
        tableProduct.clear().draw();
        tableProduct.rows.add(result.products).draw();
        tableInvoice.clear().draw();
        tableInvoice.rows.add(result.products).draw();
        invoiceList.clear().draw();
        invoiceList.rows.add(result.invoice).draw();
        calculateInvoice();
        loadFileList(result.file);
        }).fail(function (jqXHR, textStatus, errorThrown) { 
              // needs to implement if it fails
        });
}

$('#invoiceModal').on('shown.bs.modal', function () {
  $('#invoice_no').focus();
});

function calculateAmount(row){
	var qty = $('#invoice_qty_'+row).val();
	var price = $('#invoice_price_'+row).val();
	var amount = qty*price;

	$('#invoice_total_'+row).val(amount);
	$('#text_invoice_total_'+row).text(amount);

	calculateInvoice();
}

function calculateInvoice(){
    var subtotal = 0;
    var misccost = parseFloat($('#invoice_misc_cost').val()) || 0;
    var shipcost = parseFloat($('#invoice_ship_cost').val()) || 0;
    var amount = 0;
    var grandtotal = 0;
    
    $('.invoice_amount').each(function() {
      amount = parseFloat(this.value);
      subtotal += amount;
    });
    if(!subtotal){
      subtotal = 0;
    }

    grandtotal = subtotal + misccost + shipcost;
    if (!grandtotal) {
      grandtotal = 0;
    }
    $('#invoice_total_amount').val(grandtotal);
    $('#text_invoice_total_amount').text(grandtotal);
}

function loadFileList(data){
	var list = $('#fileList');
	list.empty();
    $.each(data, function(){
	  list.append('<li><a href="'+this.url+'" target="_blank">'+this.title+'<span class="pull-right text-red">'+this.extension+'</span></a></li>');
	});
}

$('#invoiceModal').on('shown.bs.modal', function (event) {
  var button = $(event.relatedTarget)
  var id = button.data('id')
  var action = button.data('action')
  var po = $('.po_prefix').val()
  var modal = $(this)
	$.ajax({
	    url: "/app/po/getInvoice",
	    type: "post",
	    data: { invoice: id, action: action, po: po }
	}).done(function(data){
	  modal.find('.modal-title').text(data.title)
	  modal.find('form').append(data.additional_form)
	  modal.find('form').attr('action', data.action_form)
	  modal.find('#invoice_no').val(data.invoice.prefix)
	  modal.find('#invoice_date').val(data.invoice.date)
	  modal.find('#invoice_ship_cost').val(data.invoice.ship_cost);
	  modal.find('#invoice_misc_cost').val(data.invoice.misc_cost);
	  modal.find('#invoice_total_amount').val(data.invoice.total_amount);
	  modal.find('#text_invoice_total_amount').text(data.invoice.total_amount);
	  tableInvoice.clear().draw()
	  tableInvoice.rows.add(data.products).draw()
	  activeDatePicker()
	});

})

function activeDatePicker(){
	$('.datepicker').datepicker({
		autoclose: true,
		format: 'yyyy-mm-dd',
		todayHighlight: true
	});
}
//# sourceMappingURL=processPo.js.map
