$(document).ready(function() {
  // $('#product-form').on('keyup keypress', function(e) {
	//   var keyCode = e.keyCode || e.which;
	//   if (keyCode === 13) {
	//     e.preventDefault();
	//     return false;
	//   }
	// });
});

class Errors {
  constructor() {
    this.errors = {};
  }

  get(field) {
    if (this.errors[field]) {
      return this.errors[field][0];
    }
  }

  record(errors) {
    this.errors = errors;
  }

  clear(field) {
    this.errors[field] = '';
  }
}

Vue.filter('truncate', function(value) {
  if(value.length < 30) {
    return value;
  }
  return value.substring(0, 27) + ' ...';
});

Vue.filter('sanitize', function(value) {
  var slug = "";
  // Change to lower case
  var lower = value.toLowerCase();
  // Letter "e"
  slug = lower.replace(/e|é|è|ẽ|ẻ|ẹ|ê|ế|ề|ễ|ể|ệ/gi, 'e');
  // Letter "a"
  slug = slug.replace(/a|á|à|ã|ả|ạ|ă|ắ|ằ|ẵ|ẳ|ặ|â|ấ|ầ|ẫ|ẩ|ậ/gi, 'a');
  // Letter "o"
  slug = slug.replace(/o|ó|ò|õ|ỏ|ọ|ô|ố|ồ|ỗ|ổ|ộ|ơ|ớ|ờ|ỡ|ở|ợ/gi, 'o');
  // Letter "u"
  slug = slug.replace(/u|ú|ù|ũ|ủ|ụ|ư|ứ|ừ|ữ|ử|ự/gi, 'u');
  // Letter "d"
  slug = slug.replace(/đ/gi, 'd');
  // Trim the last whitespace
  slug = slug.replace(/\s*$/g, '');
  // Change whitespace to "-"
  slug = slug.replace(/\s+/g, '-');

  return slug;
});


Vue.component('v-select', VueSelect.VueSelect);

Vue.component('attribute',{
	props: ['label','type','name','options','mandatory','id','value','error'],
	template: '#attributes-list',
    methods: {
    	update(name,value) {
	      this.$emit('update', {'name' : name, 'value': value})
	    }
    },
	mounted() {
		var vm = this;
	    $("select[name='general[brand]']").select2({
	    	placeholder: 'Enter a Brand Name',
            ajax: {
                dataType: 'json',
                url: '/app/products/brands',
                delay: 500,
                data: function(params) {
                    return {
                        query: params.term
                    }
                },
                processResults: function (data, page) {
                	var select2Data = $.map(data, function (obj) {
	                    obj.id = obj.name;
	                    obj.text = obj.name;
	                    return obj;
	                });
					return {
						results: select2Data,
					};
                },
                cache: true
            },
            width: 'resolve',
            language: {
			    noResults: function (params) {
			      return "Type a Brand Name";
			    }
			  }
	    })
      .val(this.value)
      .trigger('change')
      .on('change', function () {
        vm.$emit('update', {'name' : 'brand', 'value': this.value})
      });
	    $("select[name!='general[brand]']").not('.noselect2').select2({
	    	placeholder: 'Please Select',
	    	width: 'resolve',
	    })
      .val(this.value)
      .trigger('change')
      .on('change', function () {
        vm.$emit('update', {'name' : this.id, 'value': this.value})
      });
	},
})

Vue.component('attributes-variant',{
	props: ['label','type','name','options','mandatory','id','value','error'],
	template: '#attributes-variation',
    methods: {
    	update(name,value) {
	      this.$emit('update', {'name' : name, 'value': value})
	    }
    },
  	mounted() {
  		var vm = this;
      $('.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      });
      $('.iCheck').on('ifChecked', function(event){
        var name = $(this).attr('data-id');
        var value = $(this).val();
        vm.$emit('update', {'name' : name, 'value': value})
      });
      $('.iCheck').on('ifUnchecked', function(event){
        var name = $(this).attr('data-id');
        var value = $(this).val();
        vm.$emit('remove', {'name' : name, 'value': value})
      });
  	},
})

var app = new Vue({
	el: "#root",
	data: {
		categories: [],
		activeItem: [],
		activeSection: 'categories',
		selectedCategory: [],
		attributes: {
      normal: [],
			blocked: ['warranty','warranty_type','product_warranty','product_warranty_en','Hazmat'],
			general: [],
      variation:[]
		},
		formValue: {

		},
		productPrefix: {
			status: 'ok',
			prefix: ''
		},
		suppliers: {
			available: [],
			assigned: []
		},
    variation: {},
    allVariation: [],
    recursiveVariation: [],
    errors: new Errors(),
    prefixCode: '',
    codeList: []
	},
  computed: {
  },
  watch: {
    allVariation: function(variations) {

      var options = variations;
      var recursiveSearch;
      var possibilities = [];

      if (options.length > 0) {


        recursiveSearch = function (text, depth, varKey )
        {

         text = text || "";
         depth = depth || 0;
         varKey = varKey || {};

         var vrnt = options[depth].key;

         for ( var i = 0; i < options[depth].value.length; i++ )
         {
           var option = options[depth].value;

           varKey[vrnt] = option[i];

           if (depth +1 < options.length ){
             recursiveSearch ( text + ((text=="") ? "" : " ") + option[i] , depth +1, varKey );
           }else {
             possibilities.push ({
               'key': varKey,
               'variation': text + option[i]
             });
             varKey = {};
           }
         }
        }

        recursiveSearch ( );
      }
      this.recursiveVariation = possibilities;
    }
  },
	mounted() {
		const vm = this;
    axios.get('/app/products/data/'+id).then(function(response) {
			var data = response.data;
			$.each( data.attributes, function(key,attribute){
				vm.formValue[attribute.name] = attribute.value;
			})
			vm.activeItem = data.categories;
			vm.suppliers.available = data.suppliers;
			vm.suppliers.assigned = data.assigned;
			vm.selectedCategory = data.category;
		});
		this.selectAttr(category);
	},
	methods: {
		getChild(parent,index) {
			const vm = this;
			length = vm.categories.length;
			vm.categories.splice(index+1,length);
			if (parent.children) {
				this.selectedCategory = [];
				axios.get('/app/products/categories/'+parent.id).then(function(response){
					vm.categories.push(response.data)
				});
			}else{
				this.selectedCategory = parent;
			}
			this.activeItem.splice(index,length); this.activeItem.push(parent);
		},
    getCodeList() {
      $('body').waitMe({effect:'rotation'});
      const vm = this;
      axios.get('/app/products/codeList/'+this.selectedCategory.id)
        .then(function(response){
          vm.codeList = response.data;
          vm.activateSection('productCodeList');
          $('body').waitMe("hide");
        })
        .catch(function (error) {
          $('body').waitMe("hide");
          swal('Error','Please Try Again','error');
        });
    },
		isActive(categories,type) {
      return this.activeItem[type] === categories
    },
    activateSection(section) {
    	this.activeSection = section;
    	$('html, body').animate({
			    scrollTop: $(".content-header").offset().top
			}, 1000);
    },
		isSectionActive(section) {
      return this.activeSection === section
    },
		isSelected(categories) {
      return this.selectedCategory === categories
    },
		selectAttr() {
			$('body').waitMe({effect:'rotation'});
			const vm = this;
      vm.activateSection('productInformation');
			vm.attributes.general = [];
			vm.attributes.variation = [];
			axios.get('/app/products/attributes/'+category)
        .then(function(response){
          $.each( response.data, function(key,attribute){
            var value = vm.formValue[attribute.name] ? vm.formValue[attribute.name] : '';
            attribute['value'] = value;
            vm.attributes.normal.push(attribute);
            if (attribute.type == 'general' || attribute.type == 'code') {
              vm.attributes.general.push(attribute);
            }else if (attribute.type == 'variation') {
              vm.attributes.variation.push(attribute);
              vm.variation[attribute.name] = [];
            }
          });

          $('body').waitMe("hide");
          vm.generatePrefix();
          vm.activateSection('productInformation');
        })
				.catch(function (error) {
					$('body').waitMe("hide");
					swal({
            title: 'Failed to Retrive Attributes',
            text: 'Please Try Again',
            type: 'error'
          },
          function(){
            location.reload();
          });
				});
		},
		getSuppliers() {
			axios.get('/app/products/suppliers/'+this.activeItem[1].id)
				.then(response => this.suppliers.available = response.data);
		},
		generatePrefix() {
			var length = 6;
			var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    var result = '';
		    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
	    	this.validateProductPrefix(result);
		    if (this.productPrefix.status === 'used') {
		    	this.generatePrefix();
		    }else{
  				this.productPrefix.prefix = result;
          this.formValue['prefix'] = result;
		    }
		},
		validateProductPrefix(prefix) {
			const vm = this;
			axios.get('/app/products/prefix/'+prefix).then(function(response) {
				vm.productPrefix.status = response.data
			});
		},
		updateValue(data) {
			this.formValue[data.name] = data.value;
		},
		updateVariant(data) {
      var $name = data.name;
			this.formValue[$name].push(data.value);
			this.variation[$name].push(data.value);
      this.errors.clear($name);
      this.generateVariant();
		},
		removeVariant(data) {
      var $name = data.name;
      i = this.formValue[$name].indexOf(data.value);
			this.formValue[$name].splice(i,1);
			this.variation[$name].splice(i,1);
      this.errors.clear($name);
      this.generateVariant();
		},
    generateVariant() {
      var variation = [];
      const vm = this;
      $.each(vm.variation, function(key,variant) {
        if (variant.length > 0) {
          variation.push({
            'key': key,
            'value': variant
          })
        }
      });
      // console.log(variation);
      this.allVariation = variation;
    },

		validateGeneral() {
      const vm = this;
      var error = {};
      var isValid = true;
      $.each(vm.attributes.general, function(key,attribute) {
        attribute.value = vm.formValue[attribute.name];
        if (attribute.isMandatory == 1) {
          if (vm.formValue[attribute.name] == '') {
            error[attribute.name] = ['The '+attribute.label+' is Required'];
            isValid = false;
          }
        }
      })
      vm.errors.record(error);
      if (isValid) {
        vm.activateSection('productVariation');
      }
		},
		validateVariation() {
      const vm = this;
      var error = {};
      var isValid = true;
      var attributes = vm.attributes.variation;
      console.log(attributes);
      $.each(attributes, function(key,attribute) {
        attribute.value = vm.formValue[attribute.name];
        if (attribute.isMandatory == 1) {
          if (vm.formValue[attribute.name] == '') {
            error[attribute.name] = ['The '+attribute.label+' is Required'];
            isValid = false;
          }
        }
      })
      if (vm.formValue['prefix'] == '') {
        error['prefix'] = ['The Prefix is Required'];
        // isValid = false;
      }
      if (vm.formValue['code'] == '') {
        error['code'] = ['The Product Code is Required'];
        // isValid = false;
      }
      vm.errors.record(error);
      if (isValid) {
        vm.activateSection('assignSuppliers');
      }
		},
    permutation(arr) {
      if (arr.length == 1) {
        return arr[0];
      } else {
        var result = [];
        var allCasesOfRest = allPossibleCases(arr.slice(1));  // recur with the rest of array
        for (var i = 0; i < allCasesOfRest.length; i++) {
          for (var j = 0; j < arr[0].length; j++) {
            result.push(arr[0][j] + allCasesOfRest[i]);
          }
        }
        return result;
      }
    },
    submitForm() {
      var check = $('#assigned').children('option').length;
      if (check) {
        $('#assigned option').prop('selected', true);
        $('#product-form').submit();
      }else{
        swal({
          title: "Product not Assigned to Any Supplier",
          text: "Are you sure want to save it?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#f39c12",
          confirmButtonText: "Yes, Save Product",
          closeOnConfirm: false
        },
        function(){
          $('#product-form').submit();
        });
      }
		},
	}
})
