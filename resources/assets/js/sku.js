$(function () {
    $('.checkbox').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
});

Vue.component('v-select', VueSelect.VueSelect);

var price = new Vue({

  el: "#app",

  data: {
    sku: '',
    shops: '',
    price: {
      sku_id: '',
      shop_id: '',
      retail_price: '',
      max_price: '',
      min_price: '',
      price: '',
      special_price: '',
      special_from_date: '',
      special_to_date: '',
    },
    qty: {
      sku_id: '',
      active: '',
      cold: '',
      save_qty: '',
      max_qty: '',
      min_qty: '',
      max_shelved_life: '',
    },
  },

  watch: {
    'price.shop_id': function(selected_shop) {
      if (selected_shop) {
        this.getPrice(this.price.sku_id,selected_shop);
      }
    },
    'price.special_price': function(filled) {
      if (!filled) {
        this.price.special_from_date = '';
        this.price.special_to_date = '';
      }
    },
  },

  mounted() {
    const vm = this;
      $('.special_period').datepicker({
        format: 'yyyy-mm-dd'
      });
      $('#special_to_date').on('changeDate', function() {
        vm.price.special_to_date = $('#special_to_date').val();
    });
      $('#special_from_date').on('changeDate', function() {
        vm.price.special_from_date = $('#special_from_date').val();
    });
  },

  methods: {
    showPriceModal(sku_id,product_name) {
      var vm = this;
      axios.get('/app/sku/get/'+sku_id)
        .then(function(response){
          data = response.data;
          vm.price.sku_id = sku_id;
          vm.price.retail_price = data.retail_price;
          vm.price.max_price = data.max_price;
          vm.price.min_price = data.min_price;
          vm.sku = data.sku+' - '+data.product.attributes[0].value;
          vm.shops = data.shops;
      });
      $('#priceModal').modal({
        show: true,
        backdrop: false,
        keyboard: false
      })
    },
    hidePriceModal() {
      this.price.sku_id = '';
      this.price.shop_id = '';
      this.price.price = '';
      this.price.special_price = '';
      this.price.special_from_date = '';
      this.price.special_to_date = '';
      $('#priceModal').modal('hide')
    },
    showQtyeModal(sku_id) {
      var vm = this;
      axios.get('/app/sku/get/'+sku_id)
        .then(function(response){
          data = response.data;
          vm.qty.sku_id = sku_id;
          vm.sku = data.sku+' - '+data.product.attributes[0].value;
          vm.qty.active = data.active;
          vm.qty.cold = data.cold;
          vm.qty.save_qty = data.save_qty;
          vm.qty.max_qty = data.max_qty;
          vm.qty.min_qty = data.min_qty;
          vm.qty.max_shelved_life = data.max_shelved_life;
      });
      $('#qtyModal').modal({
        show: true,
        backdrop: false,
        keyboard: false
      })
    },
    hideQtyModal() {
      this.qty.sku_id = '';
      this.qty.active = '';
      this.qty.cold_ = '';
      this.qty.save_qty = '';
      this.qty.max_qty = '';
      this.qty.min_qty = '';
      this.qty.max_shelved_life = '';
      $('#qtyModal').modal('hide');
    },
    getPrice(sku_id,shop_id) {
      const vm = this;
      axios.get('/app/sku/price/'+sku_id+'/'+shop_id)
        .then(function(response) {
          var price = response.data;
          vm.price.price = price.price;
          vm.price.special_price = price.special_price;
          vm.price.special_from_date = price.special_from_date;
          vm.price.special_to_date = price.special_to_date;
        });
    },
    setPrice() {
      const vm = this;
      axios.post('/app/sku/price/store', this.$data.price)
        .then(function(response) {
          $('#price-'+vm.price.sku_id).empty();
          $('#special_price-'+vm.price.sku_id).empty();
          var data = response.data;
          vm.hidePriceModal();
        });
    },
    setQtySetting() {
      const vm = this;
      console.log(this.qty);
      axios.post('/app/sku/qtySetting/set', this.$data.qty)
        .then(function(response) {
          vm.hideQtyModal();
        });
    },
  }
})
