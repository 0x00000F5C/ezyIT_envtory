$(document).ready(function() {
  $('#product-form').on('keyup keypress', function(e) {
	  var keyCode = e.keyCode || e.which;
	  if (keyCode === 13) {
	    e.preventDefault();
	    return false;
	  }
	});
});

class Errors {
  constructor() {
    this.errors = {};
  }

  get(field) {
    if (this.errors[field]) {
      return this.errors[field][0];
    }
  }

  record(errors) {
    this.errors = errors;
  }

  clear(field) {
    this.errors[field] = '';
  }
}

Vue.filter('truncate', function(value) {
  if(value.length < 30) {
    return value;
  }
  return value.substring(0, 27) + ' ...';
});

Vue.component('v-select', VueSelect.VueSelect);

Vue.component('attribute',{
	props: ['label','type','name','options','mandatory','id','value','error'],
	template: '#attributes-list',
    methods: {
    	update(name,value) {
	      this.$emit('update', {'name' : name, 'value': value})
	    }
    },
	mounted() {
		var vm = this;
	    $("select[name='attribute[brand]']").select2({
	    	placeholder: 'Enter a Brand Name',
            ajax: {
                dataType: 'json',
                url: '/app/products/brands',
                delay: 500,
                data: function(params) {
                    return {
                        query: params.term
                    }
                },
                processResults: function (data, page) {
                	var select2Data = $.map(data, function (obj) {
	                    obj.id = obj.name;
	                    obj.text = obj.name;
	                    return obj;
	                });
					return {
						results: select2Data,
					};
                },
                cache: true
            },
            width: 'resolve',
            language: {
			    noResults: function (params) {
			      return "Type a Brand Name";
			    }
			  }
	    });
	    $("select[name!='attribute[brand]']").not('.noselect2').select2({
	    	placeholder: 'Please Select',
	    	width: 'resolve',
	    })
	},
})

Vue.component('attributes-variant',{
	props: ['label','type','name','options','mandatory','id','value','error'],
	template: '#attributes-variation',
    methods: {
    	update(name,value) {
	      this.$emit('update', {'name' : name, 'value': value})
	    }
    },
  	mounted() {
  		var vm = this;
      $('.iCheck').iCheck({
        checkboxClass: 'icheckbox_square-blue',
        radioClass: 'iradio_square-blue',
        increaseArea: '20%'
      });
      $('.iCheck').on('ifChecked', function(event){
        var name = $(this).attr('data-id');
        var value = $(this).val();
        vm.$emit('update', {'name' : name, 'value': value})
      });
      $('.iCheck').on('ifUnchecked', function(event){
        var name = $(this).attr('data-id');
        var value = $(this).val();
        vm.$emit('remove', {'name' : name, 'value': value})
      });
  	},
})

var app = new Vue({
	el: "#root",
	data: {
		categories: [],
		activeItem: [],
		activeSection: 'productInformation',
		selectedCategory: [],
		attributes: {
      normal: [],
      blocked: ['warranty','warranty_type','product_warranty','product_warranty_en','Hazmat'],
      general: [],
      variation:[]
		},
		formValue: {

		},
		productPrefix: {
			status: 'ok',
			prefix: ''
		},
		suppliers: {
			available: [],
			assigned: []
		},
    variation: {},
    allVariation: [],
    recursiveVariation: [],
    errors: new Errors(),
    recursiveVariation: [],
    codeList: []
	},
	mounted() {
		const vm = this;
		axios.get('/app/products/data/'+id).then(function(response) {
			var data = response.data;
			$.each( data.attributes, function(key,attribute){
				vm.formValue[attribute.name] = attribute.value;
			})
			vm.activeItem = data.categories;
			vm.suppliers.available = data.suppliers;
			vm.suppliers.assigned = data.assigned;
			vm.selectedCategory = data.category;
		});
		this.selectAttr(category);
	},
	methods: {
    getCodeList() {
      $('body').waitMe({effect:'rotation'});
      const vm = this;
      axios.get('/app/products/codeList/'+this.selectedCategory.id)
        .then(function(response){
          vm.codeList = response.data;
          vm.activateSection('productCodeList');
          $('body').waitMe("hide");
        })
        .catch(function (error) {
          $('body').waitMe("hide");
          swal('Error','Please Try Again','error');
        });
    },
		isActive(categories,type) {
	      return this.activeItem[type] === categories
	    },
	    activateSection(section) {
	    	this.activeSection = section;
	    	$('html, body').animate({
			    scrollTop: $(".content-header").offset().top
			}, 1000);
	    },
		isSectionActive(section) {
	      return this.activeSection === section
	    },
		isSelected(categories) {
	      return this.selectedCategory === categories
	    },
		selectAttr(category) {
			$('body').waitMe({effect:'rotation'});
			const vm = this;
			axios.get('/app/products/attributes/'+category)
				.then(function(response){
          $.each( response.data, function(key,attribute){
            var value = vm.formValue[attribute.name] ? vm.formValue[attribute.name] : '';
            attribute['value'] = value;
            vm.attributes.normal.push(attribute);
            if (attribute.type == 'general' || attribute.type == 'code') {
              vm.attributes.general.push(attribute);
            }else if (attribute.type == 'variation') {
              vm.attributes.variation.push(attribute);
              vm.variation[attribute.name] = [];
            }
          });

					$('body').waitMe("hide");
					vm.generatePrefix();
					vm.activateSection('productInformation');
				})
				.catch(function (error) {
					$('body').waitMe("hide");
					swal('Failed to Retrive Attributes','Please Try Again','error');
				});
		},
		generatePrefix() {
			var length = 6;
			var chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		    var result = '';
		    for (var i = length; i > 0; --i) result += chars[Math.round(Math.random() * (chars.length - 1))];
	    	this.validateProductPrefix(result);
		    if (this.productPrefix.status === 'used') {
		    	this.generatePrefix();
		    }else{
				this.productPrefix.prefix = result;
		    }
		},
		validateProductPrefix(prefix) {
			const vm = this;
			axios.get('/app/products/prefix/'+prefix).then(function(response) {
				vm.productPrefix.status = response.data
			});
		},
		updateValue(data) {
			this.formValue[data.name] = data.value;
		},
    updateVariant(data) {
      var $name = data.name;
			this.formValue[$name].push(data.value);
			this.variation[$name].push(data.value);
      this.errors.clear($name);
      this.generateVariant();
		},
		removeVariant(data) {
      var $name = data.name;
      i = this.formValue[$name].indexOf(data.value);
			this.formValue[$name].splice(i,1);
			this.variation[$name].splice(i,1);
      this.errors.clear($name);
      this.generateVariant();
		},
    validateForm() {
      const vm = this;
      var error = {};
      var isValid = true;
      $.each(vm.attributes.normal.concat(vm.attributes.code), function(key,attribute) {
        attribute.value = vm.formValue[attribute.name];
        if (attribute.isMandatory == 1) {
          if (vm.formValue[attribute.name] == '') {
            error[attribute.name] = ['The '+attribute.label+' is Required'];
            isValid = false;
          }
        }
      })
      if (vm.formValue['prefix'] == '') {
        error['prefix'] = ['The Prefix is Required'];
        isValid = false;
      }
      if (vm.formValue['code'] == '') {
        error['code'] = ['The Product Code is Required'];
        isValid = false;
      }
      vm.errors.record(error);
      if (isValid) {
        vm.activateSection('assignSuppliers');
      }
		},
    validateGeneral() {
      const vm = this;
      var error = {};
      var isValid = true;
      $.each(vm.attributes.general, function(key,attribute) {
        attribute.value = vm.formValue[attribute.name];
        if (attribute.isMandatory == 1) {
          if (vm.formValue[attribute.name] == '') {
            error[attribute.name] = ['The '+attribute.label+' is Required'];
            isValid = false;
          }
        }
      })
      vm.errors.record(error);
      if (isValid) {
        vm.activateSection('productVariation');
      }
		},
		validateVariation() {
      const vm = this;
      var error = {};
      var isValid = true;
      var attributes = vm.attributes.variation;
      console.log(attributes);
      $.each(attributes, function(key,attribute) {
        attribute.value = vm.formValue[attribute.name];
        if (attribute.isMandatory == 1) {
          if (vm.formValue[attribute.name] == '') {
            error[attribute.name] = ['The '+attribute.label+' is Required'];
            isValid = false;
          }
        }
      })
      if (vm.formValue['prefix'] == '') {
        error['prefix'] = ['The Prefix is Required'];
        // isValid = false;
      }
      if (vm.formValue['code'] == '') {
        error['code'] = ['The Product Code is Required'];
        // isValid = false;
      }
      vm.errors.record(error);
      if (isValid) {
        vm.activateSection('assignSuppliers');
      }
		},
		submitForm() {
      var check = $('#assigned').children('option').length;
      if (check) {
        $('#assigned option').prop('selected', true);
        $('#product-form').submit();
      }else{
        swal({
          title: "Are you sure?",
          text: "Product not assigned to any supplier",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#f39c12",
          confirmButtonText: "Yes, Save Product",
          closeOnConfirm: false
        },
        function(){
          $('#product-form').submit();
        });
      }
		}
	}
})
