	Vue.filter('truncate', function(value) {
	  if(value.length < 35) {
	    return value;
	  }
	  return value.substring(0, 23) + ' ...';
	});

	var app = new Vue({
	
		el: "#categories",
		data: {
			categories: [],
			activeItem: [],
			selectedCategory: [],
			categoriesId: [],
			togled: []
		},
		mounted() {
			const vm = this;
			if (rid) {
				axios.get('/app/warehouse/rackCategories/'+rid).then(function(response){
					vm.categoriesId = response.data
				});
			}
		},
		methods: {
			isActive(categories) {
		      return this.activeItem === categories
		    },
			isSelected(categories) {
				if (this.categoriesId.indexOf(categories) > -1) {
					return true;
				}else{
					return false;
				}
		    },
		    getChild(parent) {
				const vm = this;
				this.activeItem = parent;
				axios.get('/app/products/categories/'+parent).then(response => this.categories = response.data);
			},
		}
	})
