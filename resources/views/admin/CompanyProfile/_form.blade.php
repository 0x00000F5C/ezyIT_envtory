<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/companyProfiles/' . $model->id) : url('/app/companyProfiles') }}" enctype="multipart/form-data">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<ul class="nav nav-tabs" role="tablist">
	  <li role="presentation" class="active"><a href="#company-profile" aria-controls="company-profile" role="tab" data-toggle="tab">Company profile</a></li>
	  <li role="presentation"><a href="#report-template" aria-controls="report-template" role="tab" data-toggle="tab">Report template</a></li>
	  <li role="presentation"><a href="#email-template" aria-controls="report-template" role="tab" data-toggle="tab">Email template</a></li>
	</ul>
	<div class="box-body">
		<div class="tab-content form-group has-feedback">
			<div role="tabpanel" class="tab-pane active" id="company-profile">
				<div class="form-group has-feedback">
					<div class="col-md-6">
						<label for="profile-logo" class="col-md-4 control-label text-left">Profile logo</label>
						<div class="col-md-8">
							<div style="display: table;">
								<div id="logo-frame" class="border-1" style="width: 64px; height: 64px; display: table-cell; border: 1px solid #ccc;">
									@if(File::exists("img/cProfiles/" . $model->profile_type . ".jpg"))
										<img src="{{ Asset("img/cProfiles/" . $model->profile_type . ".jpg") }}" height="63" width="63" />
									@endif
								</div>
								<div id="logo-frame" class="border-1" style="display: table-cell; padding-left: 12px; vertical-align: top;">
									<input type="file" id="profile-logo" name="profile-logo" accept="image/*">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="profile-name" class="col-md-4 control-label text-left">Profile name<span class="red"> *</span></label>
						<div class="col-md-8">
							<input type="text" name="profile-name" class="form-control" id="profile-name" placeholder="Profile name" value="{{ old("profile-name") ? old("profile-name") : $model->profile_name }}" required="required">
							@if ($errors->has("profile-name"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-name") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group hidden">
						<label for="profile-type" class="col-md-4 control-label text-left">Profile type<span class="red"> *</span></label>
						<div class="col-md-8">
							<select name="profile-type" class="form-control" id="profile-type" required="required">
								<option value="1"@if($model->exists && $model->profile_type == "Company") selected @endif>Company</option>
								<option value="2"@if($model->exists && $model->profile_type == "Accounting") selected @endif>Accounting</option>
								<option value="3"@if($model->exists && $model->profile_type == "Sales") selected @endif>Sales</option>
								<option value="4"@if($model->exists && $model->profile_type == "Purchase") selected @endif>Purchase</option>
							</select>
							@if ($errors->has("profile-type"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-type") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-billingaddress" class="col-md-4 control-label text-left">Billing address<span class="red"> *</span></label>
						<div class="col-md-8">
							<textarea name="profile-billingaddress" class="form-control" id="profile-billingaddress" placeholder="Billing address" rows="3.5" required="required">{{ old("profile-billingaddress") ? old("profile-billingaddress") : $model->profile_billing_address }}</textarea>
							@if ($errors->has("profile-billingaddress"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-billingaddress") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-city" class="col-md-4 control-label text-left">Profile city</label>
						<div class="col-md-8">
							<input type="text" name="profile-city" class="form-control" id="profile-city" placeholder="City" value="{{ old("profile-city") ? old("profile-city") : $model->profile_city }}">
							@if ($errors->has("profile-city"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-city") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-state" class="col-md-4 control-label text-left">Profile state</label>
						<div class="col-md-8">
							<input type="text" name="profile-state" class="form-control" id="profile-state" placeholder="State" value="{{ old("profile-state") ? old("profile-state") : $model->profile_state }}">
							@if ($errors->has("profile-state"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-state") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-zipcode" class="col-md-4 control-label text-left">ZIP code</label>
						<div class="col-md-8">
							<input type="text" name="profile-zipcode" class="form-control" id="profile-zipcode" placeholder="ZIP code" value="{{ old("profile-zipcode") ? old("profile-zipcode") : $model->profile_zip }}">
							@if ($errors->has("profile-zipcode"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-zipcode") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-country" class="col-md-4 control-label text-left">Country</label>
						<div class="col-md-8">
							<input type="text" name="profile-country" class="form-control" id="profile-country" placeholder="Country" value="{{ old("profile-country") ? old("profile-country") : $model->profile_country }}">
							@if ($errors->has("profile-country"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-country") }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
				<div class="col-md-6">
					<div class="form-group">
						<label for="profile-phonenumber" class="col-md-4 control-label text-left">Phone number</label>
						<div class="col-md-8">
							<input type="text" name="profile-phonenumber" class="form-control" id="profile-phonenumber" placeholder="Phone number" value="{{ old("profile-phonenumber") ? old("profile-phonenumber") : $model->profile_phone_number }}">
							@if ($errors->has("profile-phonenumber"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-phonenumber") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-faxnumber" class="col-md-4 control-label text-left">Fax number</label>
						<div class="col-md-8">
							<input type="text" name="profile-faxnumber" class="form-control" id="profile-faxnumber" placeholder="Fax number" value="{{ old("profile-faxnumber") ? old("profile-faxnumber") : $model->profile_fax_number }}">
							@if ($errors->has("profile-faxnumber"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-faxnumber") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-emailaddress" class="col-md-4 control-label text-left">Email<span class="red"> *</span></label>
						<div class="col-md-8">
							<input type="text" name="profile-emailaddress" class="form-control" id="profile-emailaddress" placeholder="Email" value="{{ old("profile-emailaddress") ? old("profile-emailaddress") : $model->profile_email_address }}" required="required">
							@if ($errors->has("profile-emailaddress"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-emailaddress") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-website" class="col-md-4 control-label text-left">Website</label>
						<div class="col-md-8">
							<input type="text" name="profile-website" class="form-control" id="profile-website" placeholder="Website" value="{{ old("profile-website") ? old("profile-website") : $model->profile_website }}">
							@if ($errors->has("profile-website"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-website") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group hidden">
						<label for="profile-gst" class="col-md-4 control-label text-left">GST reg number</label>
						<div class="col-md-8">
							<input type="text" name="profile-gst" class="form-control" id="profile-gst" placeholder="GST reg number" value="{{ old("profile-gst") ? old("profile-gst") : $model->profile_gst }}">
							@if ($errors->has("profile-gst"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-gst") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group hidden">
						<label for="profile-businessnumber" class="col-md-4 control-label text-left">Business reg number</label>
						<div class="col-md-8">
							<input type="text" name="profile-businessnumber" class="form-control" id="profile-businessnumber" placeholder="Business reg number" value="{{ old("profile-businessnumber") ? old("profile-businessnumber") : $model->profile_business_number }}">
							@if ($errors->has("profile-businessnumber"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-businessnumber") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group">
						<label for="profile-currency" class="col-md-4 control-label text-left">Currency</label>
						<div class="col-md-8">
							<select name="profile-currency" class="form-control" id="profile-currency" required="required">
								@foreach($currency as $i => $o)
									<option value="{{ $i }}"{{ $model->exists && $model->profile_currency == $i ? " selected" : "" }}>{{ $i . " - " . $o->name }}</option>
								@endforeach
							</select>
							@if ($errors->has("profile-currency"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-currency") }}</strong>
							</span>
							@endif
						</div>
					</div>
					<div class="form-group hidden">
						<label for="profile-timezone" class="col-md-4 control-label text-left">Timezone</label>
						<div class="col-md-8">
							<select type="text" name="profile-timezone" class="form-control" id="profile-timezone">
								<!-- NEED MORE OPTIONS -->
								<option value="127"{{ $model->exists && $model->profile_timezone == "127" ? " selected" : "" }}>GMT+7.00</option>
							</select>
							@if ($errors->has("profile-timezone"))
							<span class="help-block">
								<strong>{{ $errors->first("profile-timezone") }}</strong>
							</span>
							@endif
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="report-template">
				<div class="col-md-12">
					<div class="form-group">
						<label for="profile-reporttemplatetitle" class="col-md-2 control-label text-left">Template name</label>
						<div class="col-md-4">
							<input type="text" name="profile-reporttemplatetitle" class="form-control" id="profile-reporttemplatetitle" placeholder="General template" value="{{ old("profile-reporttemplatetitle") ? old("profile-reporttemplatetitle") : @$reportTemplate->name }}" readonly="readonly">
						</div>
						<label for="profile-reporttemplatesize" class="col-md-2 control-label text-left">Paper size</label>
						<div class="col-md-2">
							<select name="profile-reporttemplatesize" class="form-control" id="profile-reporttemplatesize">
								<option selected hidden>Select a size...</option>
								<option value="A3"{{ @$reportTemplate->paper_size == "A3" ? " selected" : "" }}>A3</option>
								<option value="A4"{{ @$reportTemplate->paper_size == "A4" ? " selected" : "" }}>A4</option>
								<option value="A5"{{ @$reportTemplate->paper_size == "A5" ? " selected" : "" }}>A5</option>
								<option value="LEGAL"{{ @$reportTemplate->paper_size == "LEGAL" ? " selected" : "" }}>Legal</option>
								<option value="LETTER"{{ @$reportTemplate->paper_size == "LETTER" ? " selected" : "" }}>Letter</option>
							</select>
						</div>
					</div>
					<div class="form-group">
						<label for="profile-reporttemplateheader" class="col-md-2 control-label text-left">Report header</label>
						<div class="col-md-8">
							<textarea name="profile-reporttemplateheader" class="ckeditor form-control" id="profile-reporttemplateheader" rows="3.5">{{ old("profile-reporttemplateheader") ? old("profile-reporttemplateheader") : @$reportTemplate->header }}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="profile-reporttemplatefooter" class="col-md-2 control-label text-left">Report footer</label>
						<div class="col-md-8">
							<textarea name="profile-reporttemplatefooter" class="ckeditor form-control" id="profile-reporttemplatefooter" rows="3.5">{{ old("profile-reporttemplatefooter") ? old("profile-reporttemplatefooter") : @$reportTemplate->footer }}</textarea>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="email-template">
				<div class="col-md-12">
					<div class="form-group">
						<label for="profile-emailtemplate" class="col-md-2 control-label text-left">Template name</label>
						<div class="col-md-4">
							<input type="text" name="profile-emailtemplate" class="form-control" id="profile-emailtemplate" placeholder="Email template" value="{{ old("profile-emailtemplate") ? old("profile-emailtemplate") : @$mailTemplate->name }}" readonly="readonly">
						</div>
					</div>
					<div class="form-group">
						<label for="profile-emailtemplateheader" class="col-md-2 control-label text-left">Email header</label>
						<div class="col-md-8">
							<textarea name="profile-emailtemplateheader" class="ckeditor form-control" id="profile-emailtemplateheader" rows="3.5">{{ old("profile-emailtemplateheader") ? old("profile-emailtemplateheader") : @$mailTemplate->header }}</textarea>
						</div>
					</div>
					<div class="form-group">
						<label for="profile-emailtemplatefooter" class="col-md-2 control-label text-left">Email footer</label>
						<div class="col-md-8">
							<textarea name="profile-emailtemplatefooter" class="ckeditor form-control" id="profile-emailtemplatefooter" rows="3.5">{{ old("profile-emailtemplatefooter") ? old("profile-emailtemplatefooter") : @$mailTemplate->footer }}</textarea>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/companyProfiles") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>