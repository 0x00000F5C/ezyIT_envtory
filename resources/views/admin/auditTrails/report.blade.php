<html>
	<head>
		<title>{{$title or 'Title Here'}}</title>
		<style>
			table {
				width: 100%;
				border: 0;
			}
			table th {
				background-color: #dfdfdf;
				border: 0;
			}
		</style>
	</head>
	<body>
	<h1>{{$title or 'Title Here'}}</h1>
		<table>
			<thead>
				<tr>
                  <th>#</th>
                  <th>Date</th>
                  <th>Information</th>
                  <th>IP Address</th>
                  <th>Comment</th>
				</tr>
			</thead>
			<tbody>
			@foreach ($datas as $data)
				<tr>
					<td>{{ $data[0] }}</td>
					<td>{{ $data[1] }}</td>
					<td>{{ $data[2] }}</td>
					<td>{{ $data[3] }}</td>
					<td>{{ $data[4] }}</td>
				</tr>
			@endforeach
			</tbody>
		</table>
	</body>
</html>