@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group hidden">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right">
			@if ($lastSync)
				<h3 class="box-title">Synced : {{ $lastSync->created_at }} </h3>
			@else
				@can('create-brands')
				<button type="button" class="btn btn-warning btn-flat" onclick="storeLazadaBrands()"><i class="fa fa-refresh"></i> Import Brands From Lazada</button>
				@endcan
			@endif
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="brands-table" cellspacing>
          <thead>
              <tr>
                  <th width="100">Brand Id</th>
                  <th>Brand name</th>
                  <th>Global identifier</th>
              </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		$('#brands-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('brands.index') !!}',
			columns: [
				{ data: 'id', name: 'id' },
				{ data: 'name', name: 'name' },
				{ data: 'global_identifier', name: 'global_identifier' }
			]
		});
	});

	function storeLazadaBrands()
	{
	swal({
	  title: "Import Brands from Lazada?",
	  // text: "Process may take a long time",
	  type: "warning",
	  showCancelButton: true,
	  closeOnConfirm: false,
	  confirmButtonText: "Yes",
	  showLoaderOnConfirm: true,
	},
	function(){
	  $.ajax({
	    url: "/app/brands/storeLazadaBrands",
	    type: "post",
	    data: { }
	  }).done(function(data){
	    if(data.status == 'success'){
	      swal({
	      	title: "Brands is Importing", 
	      	text: "While this process, you are free to browse",
	      	type: "success"
	      },function(){
	      	location.reload();
	      })
	    }else{
	      swal("Failed", "Import Product Categories Failed", "error")
	    }
	  });
	});
	}
</script>
@endpush