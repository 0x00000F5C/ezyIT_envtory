<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/carrier/' . $model->id) : url('/app/carrier') }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="form-group">
				<label for="name" class="col-md-2 control-label text-left">Courier name<span class="red"> *</span></label>
				<div class="col-md-6">
					<input type="text" name="name" class="form-control" id="name" placeholder="Carrier name" value="{{ old("name") ? old("name") : $model->Name }}" maxlength="255" required="required">
					@if ($errors->has("carrier-name"))
					<span class="help-block">
						<strong>{{ $errors->first("name") }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="form-group">
				<label for="default" class="col-md-2 control-label text-left">Default</label>
				<div class="col-md-1">
					<input type="checkbox" name="default" id="default" value="1" {{ $model->exists ? ($model->Default ? 'checked="checked"' : "") : "" }}>
				</div>
				<label for="api-integration" class="col-md-2 control-label text-left">API Integration</label>
				<div class="col-md-1">
					<input type="checkbox" name="api-integration" id="api-integration" value="1" {{ $model->exists ? ($model->ApiIntegration ? 'checked="checked"' : "") : "" }}>
				</div>
			</div>
			<div id="api-formgroup" class="{{ $model->exists ? ((bool) $model->ApiIntegration ? "" : "hidden") : "hidden" }}">
				<div class="form-group">
					<label for="tracking-code-validation-regex" class="col-md-2 control-label text-left">Tracking code validation regex<span class="red"> *</span></label>
					<div class="col-md-6">
						<input type="text" name="tracking-code-validation-regex" class="form-control" id="tracking-code-validation-regex" placeholder="Tracking code validation regex" value="{{ old("tracking-code-validation-regex") ? old("tracking-code-validation-regex") : $model->TrackingCodeValidationRegex }}" maxlength="255">
						@if ($errors->has("tracking-code-validation-regex"))
						<span class="help-block">
							<strong>{{ $errors->first("tracking-code-validation-regex") }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="form-group">
					<label for="tracking-code-example" class="col-md-2 control-label text-left">Tracking code example<span class="red"> *</span></label>
					<div class="col-md-6">
						<input type="text" name="tracking-code-example" class="form-control" id="tracking-code-example" placeholder="Tracking code example" value="{{ old("tracking-code-example") ? old("tracking-code-example") : $model->TrackingCodeExample }}" maxlength="48">
						@if ($errors->has("tracking-code-example"))
						<span class="help-block">
							<strong>{{ $errors->first("tracking-code-example") }}</strong>
						</span>
						@endif
					</div>
				</div>
				<div class="form-group">
					<label for="tracking-url" class="col-md-2 control-label text-left">Tracking URL<span class="red"> *</span></label>
					<div class="col-md-6">
						<input type="text" name="tracking-url" class="form-control" id="tracking-url" placeholder="Tracking URL" value="{{ old("tracking-url") ? old("tracking-url") : $model->TrackingUrl }}" maxlength="255">
						@if ($errors->has("tracking-url"))
						<span class="help-block">
							<strong>{{ $errors->first("tracking-url") }}</strong>
						</span>
						@endif
					</div>
				</div>
			</div>
			<div class="form-group">
				<label for="delivery-option" class="col-md-2 control-label text-left">Delivery options</label>
				<div id="delivery-options" class="col-md-6">
					@foreach($deliveryOptions as $i => $deliveryOption)
					<input type="text" name="delivery-option[]" class="delivery-options form-control" id="delivery-option" placeholder="E.g. standard, express, economy..." value="{{ $deliveryOption->DeliveryOption }}" maxlength="16" size="16" style="margin-bottom: 9px;">
					@endforeach
					<input type="text" name="delivery-option[]" class="delivery-options form-control" id="delivery-option" placeholder="E.g. standard, express, economy..." value="" maxlength="16" size="16" style="margin-bottom: 9px;">
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/carrier") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
