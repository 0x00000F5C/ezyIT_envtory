@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.carrier._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%'
		});
	});
	
	$("#api-integration").on("ifChanged", function(){
		if($("#api-integration").is(":checked")){
			$("#api-formgroup").removeClass("hidden");
		} else {
			$("#api-formgroup").addClass("hidden");
		}
	});
	
	$("#delivery-options").on("input", ".delivery-options:last-of-type", function() {
		if($(this).val() != "") {
			$(this).clone().val("").appendTo("#delivery-options");
		}
	});
	
	$("#delivery-options").on("input", ".delivery-options:nth-last-of-type(2)", function() {
		if($(this).val() == "") {
			$("#delivery-options > .delivery-options:last-of-type").remove();
		}
	});
</script>
@endpush