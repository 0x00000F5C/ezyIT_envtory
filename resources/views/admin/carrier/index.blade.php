<style>
	td.details-control {
		background: url('{{ asset('img/common/details_open.png') }}') no-repeat center center;
		cursor: pointer;
	}

	tr.shown td.details-control {
		background: url('{{ asset('img/common/details_close.png') }}') no-repeat center center;
	}
</style>
@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group hidden">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right">
          @can('create-carrier')
          <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New carrier</a>
          @endcan
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="carriers-table" cellspacing>
          <thead>
              <tr>
                  <th>Name</th>
                  <th>Tracking code example</th>
                  <th style="width: 128px;">API Integration</th>
                  <th style="width: 64px;">Default</th>
                  <th style="width: 128px;"></th>
              </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
  $(function() {
    $('#carriers-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('carrier.index') !!}',
        columns: [
            { data: 'Name', name: 'Name' },
            { data: 'TrackingCodeExample', name: 'TrackingCodeExample' },
            { data: 'ApiIntegration_actual', name: 'ApiIntegration_actual' },
            { data: 'Default_actual', name: 'Default_actual' },
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ]
    });
});
</script>
@endpush