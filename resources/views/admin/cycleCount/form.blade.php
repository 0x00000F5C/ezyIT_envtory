@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
	  @include('layouts.lte.status')
		<div id="root">
				{{ csrf_field() }}
				<input type="hidden" name="data">
				<div class="stepwizard">
					<div class="stepwizard-row setup-panel">
						<div class="stepwizard-step">
							<button type="button" id="step-one" class="steps btn btn-circle btn-{{ $step == 1 ? "warning" : "default"}}" data-step="1"{{ $step <= 1 ? " disabled" : ""}}>1</button>
							<p>Select Warehouse</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" id="step-two" class="steps btn btn-circle btn-{{ $step == 2 ? "warning" : "default"}}" data-step="2"{{ $step <= 2 ? " disabled" : ""}}>2</button>
							<p>Select Zone</p>
						</div>
						<div class="stepwizard-step">
							<button type="button" id="step-three" class="steps btn btn-circle btn-{{ $step == 3 ? "warning" : "default"}}" data-step="3"{{ $step <= 3 ? " disabled" : ""}}>3</button>
							<p>Set SKU cycle count</p>
						</div>
					</div>
				</div>
				@if($step == 1)
				<section id="warehouse">
					<div class="box box-default">
						<div class="box-header with-border"><h3 class="box-title">Select warehouse</h3></div>
						<div class="box-body">
							@foreach($warehouses as $warehouse)
							<form method="post" class="col-md-3 col-sm-6 col-xs-12">
								{{ csrf_field() }}
								<input type="hidden" name="warehouse-id" value="{{$warehouse->id}}">
								<div class="info-box" style="border: 1px solid gray;">
									<span class="box_title"><small class="label label-default">{{$warehouse->name}}</small></span>
									<span style="background-color: white !important; padding-top: 10px;" class="info-box-icon "><i class="fa fa-home"></i></span>
									<div class="info-box-content ">
										<br>
										<br>
										<br>
										<button type="submit" class="btn btn-flat btn-block btn-primary btn-xs">Select</button>
									</div>
									<!-- /.info-box-content -->
								</div>
								<!-- /.info-box -->
							</form>
							@endforeach
						</div>
						<div class="box-footer"></div>
					</div>
				</section>
				@endif
				@if($step == 2)
				<section id="zone">
					<div class="box box-default">
						<div class="box-header with-border"><h3 class="box-title">Select zone</h3></div>
						<div class="box-body">
							@foreach($zones as $zone)
							<form method="post" class="col-md-3 col-sm-6 col-xs-12">
								{{ csrf_field() }}
								<input type="hidden" name="warehouse-id" value="{{$warehouse->id}}">
								<input type="hidden" name="zone-id" value="{{$zone->id}}">
								<div class="media bordered">
									<div class="media-body">
										<h4 class="warehouse-zone-names media-heading">{{ $zone->name }}</h4>
										<div class="media">
											...
										</div>
										<div class="media">
											<button type="submit" class="btn btn-primary btn-xs btn-flat">Select</button>
										</div>
									</div>
								</div>
							</form>
							@endforeach
						</div>
						<div class="box-footer"></div>
					</div>
				</section>
				@endif
				@if($step == 3)
				<section id="sku">
					<div class="box box-default">
						<div class="box-header with-border"><h3 class="box-title">Select SKU</h3></div>
						<div class="box-body">
							<table class="table table-bordered table-hover">
								<thead>
									<tr>
										<th>Inventory SKU</th>
										<th>Product code</th>
										<th>Product name</th>
										<th>Current qty</th>
										<th>Actual qty</th>
										<th>Action</th>
									</tr>
								</thead>
								<tbody>
									@foreach($stocks as $stock)
									<?php $product = $stock->product()->first(); ?>
									<tr>
										<td>{{ $product->product_prefix }}</td>
										<td>{{ $product->product_code }}</td>
										<td>{{ @$product->attribute("name") }}</td>
										<td>{{ $stock->histories()->sum("qty") }}</td>
										<td>0</td>
										<td>
											<a href="#" class="update-cCount btn btn-warning btn-xs btn-flat" data-id="{{ $product->id }}"><i class="fa fa-pencil"></i> Update</a>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<div class="box-footer"></div>
					</div>
				</section>
				@endif
		</div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
	.stepwizard-step p{margin-top:10px}.stepwizard-row{display:table-row}.stepwizard{display:table;width:100%;position:relative;margin-top:15px;margin-bottom:20px}.stepwizard-step button[disabled]{opacity:1!important;filter:alpha(opacity=100)!important}.stepwizard-row:before{top:14px;bottom:0;position:absolute;content:" ";width:100%;height:1px;background-color:#ccc;z-order:0}.stepwizard-step{display:table-cell;text-align:center;position:relative}.btn-circle{width:30px;height:30px;text-align:center;padding:6px 0;font-size:12px;line-height:1.428571429;border-radius:15px}.prevBtn{margin-right:10px}.btn-warning{color:#fff}.variation{border:1px solid #ddd;height:250px;overflow-x:auto;overflow-y:auto;margin-left:18px;padding-top:10px;padding-bottom:10px}

    .bordered {
        min-height: 106px;
        padding: 8px;
        /*margin-bottom: 20px;*/
        background-color: #f5f5f5;
        border: 1px solid gray;
        /*border-radius: 4px;*/
        /*-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);*/
        /*box-shadow: inset 0 1px 1px rgba(0,0,0,.05);*/
    }
	
	.warehouse-zones, .warehouse-racks, .warehouse-trays {
		margin-bottom: 16px;
	}
	
	.btn-remZone, .btn-remRack, .btn-remTray, #btn-hideRacks, #btn-hideTrays {
		position: absolute;
		top: 0px;
		right: 15px;
	}
</style>
@endpush

@push('scripts')
<div id="cCount-modal" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="cCount-form" class="form-horizontal" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="">
				<div class="modal-header">
					<h5 class="modal-title" id="cCountForm-title" style="display: inline; font-weight: bold; font-size: 14pt;">Manage quantity</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<table id="cCount-table" class="table table-bordered table-hover">
						<thead>
							<tr>
								<th>Batch ID</th>
								<th>Batch date</th>
								<th>Current qty</th>
								<th>Actual qty</th>
							</tr>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	function move(fromList,toList){
		var selectOptions = document.getElementById(fromList);
		for (var i = 0; i < selectOptions.length; i++) {
			var opt = selectOptions[i];
			if (opt.selected) {
				document.getElementById(fromList).removeChild(opt);
				document.getElementById(toList).appendChild(opt);
				i--;
			}
		}
	}
	
	$(".steps").on("click", function() {
		history.back();
	});
	
	$(".update-cCount").on("click", function(e) {
		e.preventDefault;
		var d = $(this).data("id");
		$.ajax({
			type: "post",
			url: "{{ url("app/cycleCount/getStocks") }}",
			data: { "id": d },
			dataType: "json",
			success: function(data) {
				$.each(data, function(i, o){
					$("#cCount-table > tbody").append($('<tr><td>' + o.batch_id + '</td><td>' + o.received_date +  '</td><td>' + o.qty + '</td><td><input type="number" name="actualQty[' + o.batch_id + ']" data-id="' + o.batch_id + '" class="cCount-data form-control" /></td></tr>'));
				});
				$("#cCount-modal").modal("show");
			}
		});
	});
	
	$("#cCount-modal").on("hidden.bs.modal", function() {
		$("#cCount-table > tbody").empty();
	});
	
	$("#cCount-form").on("submit", function() {
		var d = [];
		$(".cCount-data").each(function(i){
			d[$(this).data("id")] = $(this).val();
		};
		$.ajax({
			type: "post",
			url: "{{ url("app/cycleCount/saveCycleCount") }}",
			data: { d },
			dataType: "json",
			success: function(data) {
				$("#cCount-modal").modal("hide");
				swal({
					title: "Success", 
					text: "Data saved",
					type: "success"
				});
			}
		});
	});
</script>
@endpush
