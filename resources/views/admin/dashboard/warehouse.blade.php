@extends('layouts.lte.main')
@section('content')
  <div class="callout callout-info">
      <h4>Welcome, {{ Auth::user()->firstname }}!</h4>
  </div>
  <div class="row">
    <div class="col-md-4">
      <div class="box box-warning box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>10</strong> Order Need to Ship Today</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
          </ul>
        </div>
      </div>
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>2</strong> Picking List Need to Process</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="box box-danger box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>2</strong> Orders Are Late</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
          </ul>
        </div>
      </div>
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>10</strong> Product Below Min Quantity</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">IPHO6S16GBWHITE <span class="pull-right">Iphone 6</span></a></li>
            <li><a href="#">IPHO6S16GBWHITE <span class="pull-right">Iphone 6</span></a></li>
            <li><a href="#">IPHO6S16GBWHITE <span class="pull-right">Iphone 6</span></a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="col-md-4">
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>1</strong> Total Orders Nedd to be Ship</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">LazBob1234 <span class="pull-right badge">10 Oct 2016</span></a></li>
          </ul>
        </div>
      </div>
      <div class="box box-default box-solid">
        <div class="box-header with-border">
          <h3 class="box-title"><strong>2</strong> Overdue PO</h3>
        </div>
        <div class="box-body no-padding">
          <ul class="nav nav-stacked">
            <li><a href="#">PO214523 <span class="pull-right badge">10 Oct 2016</span></a></li>
            <li><a href="#">PO214523 <span class="pull-right badge">10 Oct 2016</span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
@endsection
