@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group hidden">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<div class="pull-right">
					@can('create-user')
					<a id="btn-newLogistic" class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New logistic</a>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="logistic-table" cellspacing>
					<thead>
						<tr>
							<th>Logistic name</th>
							<th style="width: 96px">Operator</th>
							<th style="width: 96px">Dimension</th>
							<th style="width: 96px">Qty</th>
							<th style="width: 180px;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.structure.min.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		$('#logistic-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('logisticInventory.index') !!}',
			columns: [
				{ data: 'packing_name', name: 'packing_name' },
				{ data: 'operator_name', name: 'operator_name' },
				{ data: 'dimension', name: 'dimension' },
				{ data: 'qty', name: 'qty' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id).data("id", data.id).data("code", data.packing_code);
			}
		});

		$("#btn-newLogistic").on("click", function(evt) {
			evt.preventDefault();
			$("#logistic-modal #logisticform-title").text("Create new logistic");
			$("#logistic-modal input[name=_method]").val("POST");
			$("#logistic-modal input[name=id]").val("");
			$("#logistic-modal #logistic-code").val("");
			$("#logistic-modal #logistic-name").val("");
			$("#logistic-modal #logistic-operator > option").prop("selected", false);
			$("#logistic-modal #logistic-qty").val("");
			$("#logistic-modal #logistic-width").val("");
			$("#logistic-modal #logistic-height").val("");
			$("#logistic-modal #logistic-length").val("");
			$("#logistic-modal").modal("show");
		});

		$("#logistic-table").on("click", ".btn-editLogistic", function(evt) {
			var button = $(this);
			evt.preventDefault();
			$("#logistic-modal #logisticform-title").text("Edit logistic");
			$("#logistic-modal input[name=_method]").val("POST");
			$("#logistic-modal input[name=id]").val($(this).data("value"));
			$("#logistic-modal #logistic-code").val($(this).parents("tr").data("code"));
			$("#logistic-modal #logistic-name").val($(this).parents("tr").children("td:nth-child(1)").text());
			$("#logistic-modal #logistic-operator > option").each(function(){ if($(this).data("name") == button.parents("tr").children("td:nth-child(2)").text()){ $(this).attr("selected", "selected"); } });
			$("#logistic-modal #logistic-qty").val($(this).parents("tr").children("td:nth-child(4)").text());
			$("#logistic-modal #logistic-width").val($(this).parents("tr").children("td:nth-child(3)").text().split(" x ")[0]);
			$("#logistic-modal #logistic-height").val($(this).parents("tr").children("td:nth-child(3)").text().split(" x ")[1]);
			$("#logistic-modal #logistic-length").val($(this).parents("tr").children("td:nth-child(3)").text().split(" x ")[2]);
			$("#logistic-modal").modal("show");
		});

		$("#logistic-table").on("click", ".btn-stockInLogistic", function(evt) {
			var button = $(this);
			evt.preventDefault();
			$("#stockIn-modal #packing-code").val($(this).parents("tr").data("code"));
			$("#stockIn-modal #packing-name").val($(this).parents("tr").children("td:nth-child(1)").text());
			$("#stockIn-modal #packing-operator > option").each(function(){ if($(this).data("name") == button.parents("tr").children("td:nth-child(2)").text()){ $(this).attr("selected", "selected"); } });
			$("#stockIn-modal #latest-qty").val($(this).parents("tr").children("td:nth-child(4)").text());
			$("#stockIn-modal #stockin-qty").val(0).attr("max", $(this).parents("tr").children("td:nth-child(4)").text());
			$("#stockIn-modal").modal("show");
		});
	});
</script>

<div id="logistic-modal" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-newLogistic" class="form-horizontal" method="POST" action="{{ url("app/logisticInventory") }}">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="">
				<div class="modal-header">
					<h5 class="modal-title" id="logisticform-title" style="display: inline; font-weight: bold; font-size: 14pt;">Manage logistic</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<fieldset>
						<table style="border-collapse: separate; border-spacing: 8px;">
							<tr>
								<td style="width: 128px;"><label for="logistic-code">Logistic code</label></td>
								<td colspan="5"><input type="text" id="logistic-code" class="text ui-widget-content ui-corner-all form-control" name="logistic-code" placeholder="Logistic code" value="" style="margin-bottom: 8px;" maxlength="64" required="required"></td>
							</tr>
							<tr>
								<td style="width: 128px;"><label for="logistic-name">Logistic name</label></td>
								<td colspan="5"><input type="text" id="logistic-name" class="text ui-widget-content ui-corner-all form-control" name="logistic-name" placeholder="Logistic name" value="" style="margin-bottom: 8px;" maxlength="64" required="required"></td>
							</tr>
							<tr>
								<td><label for="logistic-operator">Operator</label></td>
								<td colspan="5">
									<select id="logistic-operator" class="ui-widget-content ui-corner-all form-control" name="logistic-operator" value="" style="margin-bottom: 8px;" maxlength="64" required="required">
										@foreach($staffs as $staff)
										<option value="{{ $staff->id }}" data-name="{{ $staff->name }}">{{ $staff->name }}</value>
										@endforeach
									</select>
								</td>
							</tr>
							<tr>
								<td><label type="text" for="logistic-qty">Qty</label></td>
								<td colspan="5"><input type="number" name="logistic-qty" id="logistic-qty" value="1" class="text ui-widget-content ui-corner-all form-control" style="margin-bottom: 8px;"></td>
							</tr>
							<tr>
								<td class="text-right"><label type="text" for="logistic-width">W</label></td>
								<td><input type="number" name="logistic-width" id="logistic-width" value="1" class="text ui-widget-content ui-corner-all form-control" style="margin-bottom: 8px;"></td>
								<td><label type="text" for="logistic-height">H</label></td>
								<td><input type="number" name="logistic-height" id="logistic-height" value="1" class="text ui-widget-content ui-corner-all form-control" style="margin-bottom: 8px;"></td>
								<td><label type="text" for="logistic-length">L</label></td>
								<td><input type="number" name="logistic-length" id="logistic-length" value="1" class="text ui-widget-content ui-corner-all form-control" style="margin-bottom: 8px;"></td>
							</tr>
						</table>
						<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
					</fieldset>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="stockIn-modal" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="form-stockInLogistic" class="" method="POST" action="{{ url("app/stockInLogistic") }}">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="">
				<div class="modal-header">
					<h5 class="modal-title" id="stockinform-title" style="display: inline; font-weight: bold; font-size: 14pt;">Manage stock in logistic</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-6">
							<div class="form-group">
								<label for="packing-code">Packing code</label>
								<input type="text" id="packing-code" class="form-control" name="packing-code" placeholder="Packing code" value="">
							</div>
							<div class="form-group">
								<label for="packing-name">Packing name</label>
								<input type="text" id="packing-name" class="form-control" name="packing-name" placeholder="Packing name" value="">
							</div>
							<div class="form-group">
								<label for="packing-operator">Operator</label>
								<select id="packing-operator" class="form-control" name="packing-operator">
									@foreach($staffs as $staff)
									<option value="{{ $staff->id }}" data-name="{{ $staff->name }}">{{ $staff->name }}</value>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="latest-qty">Latest qty</label>
								<input type="number" id="latest-qty" class="form-control" name="latest-qty" value="" disabled="disabled">
							</div>
							<div class="form-group">
								<label for="stockin-qty">Stock in qty</label>
								<input type="number" id="stockin-qty" class="form-control" name="stockin-qty" placeholder="Stock in qty" value="">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endpush