<form role="form" class="form-horizontal" method="POST" action="{{ !empty($model["payment"]) ? url('/app/makePayment/' . $model["order"]->id) : url('/app/makePayment') }}">
	{{ csrf_field() }}
	@if(!empty($model["payment"]))
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model["order"]->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="form-group">
				<label for="order-id" class="col-md-1 control-label text-left">Order ID</label>
				<div class="col-md-4">
					<input type="hidden" name="order-id-actual" value="{{ old("order-id") ? old("order-id") : ($model["status"] ? $model["order"]->id : "") }}">
					<input type="text" name="order-id" class="form-control" id="order-id" placeholder="Order ID" value="{{ old("order-id") ? old("order-id") : ($model["status"] ? $model["order"]->prefix : "") }}" required="required" {{ !empty($model["payment"]) ? 'readonly="readonly"' : ""}}>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<input type="text" name="payment-id" class="form-control" id="payment-id" placeholder="Payment ID" value="{{ old("payment-id") ? old("payment-id") : (!empty($model["payment"]) ? $model["payment"]->payment_id : $model["paymentID"]) }}" required="required" {{ !empty($model["payment"]) ? 'readonly="readonly"' : ""}}>
					@if ($errors->has("payment-id"))
					<span class="help-block">
						<strong>{{ $errors->first("payment-id") }}</strong>
					</span>
					@endif
				</div>
				<label for="payment-date" class="col-md-1 control-label text-left">Date</label>
				<div class="col-md-3">
					<div class="input-group date">
						<input type="text" name="payment-date" class="form-control datepicker" id="payment-date" value="{{ old('payment-date') ? old('payment-date') : (!empty($model["payment"]) ? $model["payment"]->payment_date : date("Y-m-d")) }}" {{ !empty($model["payment"]) ? 'readonly="readonly"' : ""}}>
						<div class="input-group-addon">
							<i class="fa fa-calendar"></i>
						</div>
					</div>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-1"></div>
				<div class="col-md-4">
					<select name="supplier-id" class="select2 form-control" id="supplier-id" required="required" {{ !empty($model["payment"]) ? 'disabled="disabled"' : ""}}>
						@foreach($suppliers as $supplier)
							<option value="{{ $supplier->id }}" data-email="{{ $supplier->email }}" {{ !empty($model["payment"]) && $model["supplier"]->id == $supplier->id ? 'selected="selected"' : "" }}>{{ $supplier->supplier_name }}</option>
						@endforeach
					</select>
					@if ($errors->has("supplier-id"))
					<span class="help-block">
						<strong>{{ $errors->first("supplier-id") }}</strong>
					</span>
					@endif
				</div>
				<div class="col-md-4">
					<input type="email" name="supplier-contact" class="form-control" id="supplier-contact" placeholder="Supplier contact" value="{{ old("supplier-contact") ? old("supplier-contact") : "" }}" {{ !empty($model["payment"]) ? 'readonly="readonly"' : ""}}>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-1"></div>
				<div class="col-md-3">
					<select name="payment-method" class="select2 form-control" id="payment-method" required="required" {{ !empty($model["payment"]) ? 'disabled="disabled"' : ""}}>
						@foreach($pMethods as $pMethod)
							<option value="{{ $pMethod->id }}" {{ !empty($model["payment"]) && $model["payment"]->payment_method == $pMethod->id ? 'selected="selected"' : "" }}>{{ $pMethod->name }}</option>
						@endforeach
					</select>
					@if ($errors->has("supplier-id"))
					<span class="help-block">
						<strong>{{ $errors->first("payment-method") }}</strong>
					</span>
					@endif
				</div>
				<div class="col-md-1 text-center">
					<button type="button" id="btn-newPaymentMethod" class="btn btn-default btn-flat"><i class="fa fa-plus"></i></button>
				</div>
			</div>
			<div class="form-group">
				<div class="col-md-1"></div>
				<div class=" col-md-8">
					<table class="table">
						<thead>
							<tr>
								<th>#</th>
								<th>Invoice number</th>
								<th>Invoice date</th>
								<th>Amount</th>
								<th>Amount due</th>
								<th>Due date</th>
								<th>Payment</th>
							</tr>
						</thead>
						<tbody>
							@if($model["status"])
							@foreach($model["invoices"] as $i => $invoice)
								<tr>
									<td>{{ ++$i }}</td>
									<td>{{ $invoice->prefix }}</td>
									<td>{{ date_format(date_create_from_format("Y-m-d", $invoice->date), "d F Y") }}</td>
									<td>{{ $invoice->total_amount_actual }}</td>
									<td>{{ $invoice->amount_due_actual }}</td>
									<td>{{ @date_format(date_create_from_format("Y-m-d", $invoice->due_date), "d F Y") }}</td>
									<td>
										<input type="hidden" name="payment-invoices[]" class="form-control" value="{{ $invoice->id }}" />
										<input type="number" name="payments[]" class="form-control" value="{{ $invoice->amount === null ? 0 : $invoice->amount }}" max="{{ $invoice->total_amount }}" />
									</td>
								</tr>
							@endforeach
							@endif
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/po/process?po=" . $model["order"]->prefix) }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model["status"] && $model["order"]->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
