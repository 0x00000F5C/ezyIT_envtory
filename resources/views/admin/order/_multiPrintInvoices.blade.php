<table style="width: 100%;">
	@foreach($orders as $order)
	<?php $details = $order->detail; ?>
	<tr>
		<td>
			<div class="row">
				<div class="col-xs-3">
					<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
					<div align="center">
					<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />
					{{ $order->order_no }}
					</div>
				</div>
				<div class="col-xs-7">
					<div align="right">
					  <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
					  <p>{{ $company->profile_billing_address }}</p>
					</div>
				</div>
				<div class="col-xs-2">
					<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
				</div>
			</div>
			<hr>
			<div class="row">
				<div class="col-md-12">
					<h2>ORDER</h2>
				</div>
			</div>
			<div class="row invoice-info">
				<div class="col-sm-4 invoice-col">
				<address>
					<strong>{{ $order->shop->shop_name }}</strong><br>
				  </address>
				</div>
				<div class="col-sm-4 invoice-col">
					<strong class="pull-right">Shipping To</strong>
				</div>
				<div class="col-sm-4 invoice-col">
				  <address>
					<strong>{{ $order->bill_first_name . " " . $order->bill_last_name }}</strong><br>
					{{ $order->bill_address }}<br>
					{{ $order->bill_address2 }}<br>
					Email: {{ $order->email }}
				  </address>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-12 table-responsive">
				  <table class="table table-bordered">
					<thead>
					<tr>
					  <th></th>
					  <th>Product Code</th>
					  <th>Product Name</th>
					  <th>Qty</th>
					  <th>Price</th>
					  <th>Amount</th>
					</tr>
					</thead>
					<tbody>
					@foreach ($details as $key => $detail)
						<tr>
						  <td>{{++$key}}</td>
						  <td>{{$detail->sku->sku}}</td>
						  <td>{{$detail->sku->product->attributes()->where("name", "name")->first()->name}}</td>
						  <td>{{$detail->qty}}</td>
						  <td align="right">{{$currency->code . " " . number_format($detail->price, 2)}}</td>
						  <td align="right">{{$currency->code . " " . number_format($detail->qty * $detail->price, 2)}}</td>
						</tr>
					@endforeach
					</tbody>
					<tfoot>
						<tr>
							<td colspan="5" align="right"><strong>Sub Total</strong></td>
							<td align="right">{{ $currency->code . " " . number_format($order->subtotal, 2) }}</td>
						</tr>
						<tr>
							<td colspan="5" align="right"><strong>Shipping Cost</strong></td>
							<td align="right">{{ $currency->code . " " . number_format($order->shipping_cost, 2) }}</td>
						</tr>
						<tr>
							<td colspan="5" align="right"><strong>Tax</strong></td>
							<td align="right">{{ $currency->code . " " . number_format($order->tax, 2) }}</td>
						</tr>
						<tr>
							<td colspan="5" align="right"><strong>Total Amount</strong></td>
							<td align="right">{{ $currency->code . " " . number_format($order->total, 2) }}</td>
						</tr>
					</tfoot>
				  </table>
				</div>
			</div>

			<div class="row">
				<div class="col-xs-6">
				  <p class="lead">Term and Condition</p>
				  <p class="text-muted well well-sm no-shadow" style="margin-top: 10px;">{{ $order->remark }}</p>
				</div>
			</div>
		</td>
	</tr>
	@endforeach
</table>