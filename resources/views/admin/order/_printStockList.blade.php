	<div class="row">
		<div class="col-xs-3">
			<?php $barcode = Barcode::getBarcodePNG($order->order_no, "C128", 3, 100); ?>
			<div align="center">
			<img src="data:image/png;base64,{{ $barcode }}" alt="{{ $order->order_no }}" width="100%" />
			{{ $order->order_no }}
			</div>
		</div>
	    <div class="col-xs-7">
	        <div align="right">
	          <h2 style="margin-top: 0">{{ $company->profile_name }}</h2>
	          <p>{{ $company->profile_billing_address }}</p>
	        </div>
	    </div>
	    <div class="col-xs-2">
	    	<img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="100%">
	    </div>
	</div>
	<hr>
