@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    @include('layouts.lte.status')
    <div class="box box-primary">
      @include('admin.order._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datepicker/datepicker3.css') }}">
<style>
  .red {
    color: #dd4b39;
  }

  .stepwizard-step p {
      margin-top: 10px;
  }

  .stepwizard-row {
      display: table-row;
  }

  .stepwizard {
      display: table;
      width: 100%;
      position: relative;
  }

  .stepwizard-step button[disabled] {
      opacity: 1 !important;
      filter: alpha(opacity=100) !important;
  }

  .stepwizard-row:before {
      top: 14px;
      bottom: 0;
      position: absolute;
      content: " ";
      width: 100%;
      height: 1px;
      background-color: #ccc;
      z-order: 0;

  }

  .stepwizard-step {
      display: table-cell;
      text-align: center;
      position: relative;
  }

  .btn-circle {
    width: 30px;
    height: 30px;
    text-align: center;
    padding: 6px 0;
    font-size: 12px;
    line-height: 1.428571429;
    border-radius: 15px;
  }

  .prevBtn {
    margin-right: 10px;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  var data_sku = <?= $sku_autocomplete ?>;

  $('.datepicker').datepicker({
    autoclose: true,
    format: 'yyyy-mm-dd',
    todayHighlight: true,
  });

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  $(document).ready(function () {
      formWizard();
      if($('#subtotal').val()==''){
        addProductRow();
      }
      $('.search_sku').focusin(function () {
          $('select.search_sku').select2({
              data:data_sku,
              placeholder: "Select SKU Product",
          })
      })
  });

  var item = [];
  $(document).on('change', '.search_sku', function () {
      var val=$(this).val()
      if(val!=null) {
          if (item.indexOf(val) < 0) {
              var data = $(this).select2('data')[0];
              $(this).parent('td').next().find('input').val(data.product)
              $(this).parents('tr').find('td:eq(3) input').prop('max',data.stock)
          } else {
              sweetAlert("Duplicate...", "Product already choose!", "error");
              $(this).select2('val', '""')
          }
      }
  })

  function formWizard(){
    var navListItems = $('div.setup-panel div a'),
            allWells = $('.setup-content'),
            allNextBtn = $('.nextBtn'),
            allPrevBtn = $('.prevBtn');

    allWells.hide();

    navListItems.click(function (e) {
        e.preventDefault();
        var $target = $($(this).attr('href')),
                $item = $(this);

        if (!$item.hasClass('disabled')) {
            navListItems.removeClass('btn-primary').addClass('btn-default');
            $item.addClass('btn-primary');
            allWells.hide();
            $target.show();
            $target.find('input:eq(0)').focus();
            $target.find('select:eq(0)').focus();
        }
    });

    allNextBtn.click(function(){
        var curStep = $(this).closest(".setup-content"),
            curStepBtn = curStep.attr("id"),
            nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().next().children("a"),
            curInputs = curStep.find("input[type='text'],input[type='url']"),
            isValid = true;

        $(".form-group").removeClass("has-error");
        for(var i=0; i<curInputs.length; i++){
            if (!curInputs[i].validity.valid){
                isValid = false;
                $(curInputs[i]).closest(".form-group").addClass("has-error");
            }
        }

        if (isValid)
            nextStepWizard.removeAttr('disabled').trigger('click');
      });

      allPrevBtn.click(function(){
          var curStep = $(this).closest(".setup-content"),
              curStepBtn = curStep.attr("id"),
              nextStepWizard = $('div.setup-panel div a[href="#' + curStepBtn + '"]').parent().prev().children("a"),
              curInputs = curStep.find("input[type='text'],input[type='url']"),
              isValid = true;

          if (isValid)
              nextStepWizard.removeAttr('disabled').trigger('click');
        });

      $('div.setup-panel div a.btn-primary').trigger('click');
  }

  function addProductRow(){
      var rowCount = $('#productTable tbody tr').length;
      var row = rowCount + 1;
      $html = '<tr id="row-'+row+'">'+
          '<td width="300"><select name="products['+row+'][product_sku_id]" class="form-control search_sku" placeholder="SKU Product"></select></td>'+
          '<td width="300"><input type="text" class="form-control" placeholder="Product Name" readonly=""></td>'+
          '<td width="200"><input type="number" name="products['+row+'][price]" id="price'+row+'" class="form-control" placeholder="Price" min="0" onchange="calculateAmount('+row+')"></td>'+
          '<td width="100"><input type="number" name="products['+row+'][qty]" id="qty'+row+'" class="form-control" placeholder="Qty" min="1" value="1" onchange="calculateAmount('+row+')"></td>'+
          '<td width="200"><input type="text" class="form-control tableAmount" placeholder="Amount" readonly="" id="amount'+row+'"></td>'+
          '<td><button type="button" class="btn btn-xs btn-danger btn-flat" onclick="removeProductRow('+row+')"><i class="fa fa-times"></i></button></td>'+
          '</tr>';
      $('#productTable tbody').append($html);
      $('select.search_sku').select2({
          data:data_sku
      })
      $("#productTable tbody select.search_sku").each(function(){
          item.push($(this).val());
      })
  }

  $('#btn-add').click(function(){
    addProductRow();
  });

  function removeProductRow(row){
    swal({
      title: "Remove Product?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Remove!",
      closeOnConfirm: true
    },
    function(){
      $('#row-'+row).remove();
      // calculatePo();
    });
  }

  function calculateAmount(row){
    var max = $('#qty'+row).attr('max');
    var qty = $('#qty'+row).val();
    if(parseInt(qty) > parseInt(max)){
        sweetAlert("Out Of Stock...", "Can't more than "+max+" amount!", "error");
        $('#qty'+row).val(max);
        return false
    }
    var price = $('#price'+row).val();
    var amount = qty*price;
    $('#amount'+row).val(amount);
      calcSubtotal();
  }

  function getSON(obj){
    var shop = obj.value
    $.ajax({
        type: 'post',
        url: '{!! url('app/order/getSON') !!}',
        data: {shop : shop},
        dataType: 'json',
        success: function(data){
          $('#shop_order_no').val(data);
        },
        error: function(data){
          var errors = data.responseJSON;
          console.log(errors);
        }
      });
  }

  function calcSubtotal(){
      var subtotal= 0;
      $('.tableAmount').each(function(i, obj) {
          console.log(obj);
          subtotal += parseInt($(obj).val());
      });
      $("#subtotal").val(subtotal);
      calcTotal();
  }

  function calcTotal(){
      if($("#subtotal").val()==""){
          $("#subtotal").val(0);
      }
      if($("#shipping_cost").val()==""){
          $("#shipping_cost").val(0);
      }
      if($("#tax").val()==""){
          $("#tax").val(0);
      }
      if($("#discount").val()==""){
          $("#discount").val(0);
      }
      var total = parseInt($("#subtotal").val()) + parseInt($("#shipping_cost").val()) + parseInt($("#tax").val()) - parseInt($("#discount").val())
      $("#total").val(total);
  }

  $('input').on('ifChecked', function(event){
      $("#ship_first_name").val($('#bill_first_name').val())
      $("#ship_last_name").val($('#bill_last_name').val())
      $("#ship_address_1").val($('#bill_address').val())
      $("#ship_address_2").val($('#bill_address2').val())
      $("#ship_country").val($('#country').val())
      $("#ship_state").val($('#state').val())
      $("#ship_city").val($('#city').val())
      $("#ship_postal_code").val($('#bill_postal_code').val())
      $("#ship_email").val($('#email').val())
      $("#ship_phone").val($('#main_phone_cust').val())
  });
  $('input').on('ifUnchecked', function(event){
      $('.shipping-info').val('')
  });

  function validateFormOrder() {
      var valid = true
      if($('#shop').val()=='')valid = false
      if($('#customer_name').val()=='')valid = false
      if (valid == false) {
          sweetAlert("Error...", "Shop and customer name can't empty!", "error");
          return false;
      }
  }

</script>
@endpush