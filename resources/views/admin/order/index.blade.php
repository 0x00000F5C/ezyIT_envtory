@extends('layouts.lte.main')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <div class="box-header">
                    <div class="btn-group">
                        <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown"
                                aria-expanded="false">
                            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
                            <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu" role="menu">
                            <li><a href="{{ URL::current().'/export/excel'}}" target="_blank"><i
                                            class="fa fa-file-excel-o"></i> Excel</a></li>
                            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i
                                            class="fa fa-file-pdf-o"></i> PDF</a></li>
                            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i
                                            class="fa fa-file-o"></i> HTML</a></li>
                        </ul>
                    </div>
                    <div class="box-tools">
                        @can('create-user')
                            <button type="button" onclick="syncOrders()" id="btn-sync-orders" class="btn btn-primary btn-flat"><i
                                        class="fa fa-refresh"></i> Sync Data</button>
                            <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i
                                        class="fa fa-plus"></i> Create Order</a>
                        @endcan
                    </div>
                    <form method="POST" id="filter-order" class="form-inline" role="form" style="text-align: right;">
                        <div class="form-group">
                            <label>Filter by status :</label>
                            <select class="form-control" id="filter-status">
                                <option value="0">All</option>
                                @foreach($statuses as $status)
                                    <option value="{{$status->id}}">{{$status->status_name}}</option>
                                @endforeach
                            </select>
                        </div>
                    </form>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-hover dataTable dt-responsive nowrap" id="order-table" cellspacing>
                        <thead>
                        <tr>
                            <th>Order Number</th>
                            <th>Shop Order Number</th>
                            <th>Date</th>
                            <th>Platform</th>
                            <th>Shop</th>
                            <th>Customer</th>
                            <th>Type</th>
                            <th>Status</th>
                            <th width="220">Action</th>
                            <th style="width: 20px;"></th>
                        </tr>
                        </thead>
						<tfoot>
							<tr>
								<td class="text-right" colspan="10">
									<div class="btn-group">
										<button type="button" class="btn btn-flat btn-xs btn-default dropdown-toggle" data-toggle="dropdown"><i class="fa fa-file-text-o"></i> Print for selected</button>
										<button type="button" class="btn btn-default btn-flat btn-xs dropdown-toggle" data-toggle="dropdown">
											<span class="caret"></span>
											<span class="sr-only">Toggle Dropdown</span>
										</button>
										<ul class="dropdown-menu pull-right" role="menu">
											<li><a href="{{ url('app/order/multiPrintOrder') }}" id="btn-multiPrintOrders"><i class="fa fa-print"></i> Print orders</a></li>
											<li><a href="{{ url('app/order/multiPrintInvoice') }}" id="btn-multiPrintInvoices"><i class="fa fa-print"></i> Print invoices</a></li>
											<li><a href="{{ url('app/order/multiPrintShippingLabel') }}" id="btn-multiPrintShippingLabels"><i class="fa fa-print"></i> Print shipping labels</a></li>
											<li><a href="{{ url('app/order/multiPrintStockList') }}" id="btn-multiPrintStockLists"><i class="fa fa-print"></i> Print stock lists</a></li>
											<li><a href="{{ url('app/order/multiPrintCarrierManifest') }}" id="btn-multiPrintCarrierManifests"><i class="fa fa-print"></i> Print carrier manifests</a></li>
										</ul>
									</div>
								</td>
							</tr>
						</tfoot>
                    </table>
                </div>
            </div>
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">Order Detail</h3>
                </div>
                <div class="box-body">
                    <table class="table" id="order-detail-table">
                        <thead>
                        <tr>
                            <th>Shop SKU</th>
                            <th>SKU</th>
                            <th>Name</th>
                            <th>Item Price</th>
                            <th>Qty</th>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
	<form id="form-multiPrintOrders" action="{{ url("/app/order/multiPrintOrders") }}" method="post" role="form">{{ csrf_field() }}</form>
	<form id="form-multiPrintInvoices" action="{{ url("/app/order/multiPrintInvoices") }}" method="post" role="form">{{ csrf_field() }}</form>
	<form id="form-multiPrintShippingLabels" action="{{ url("/app/order/multiPrintShippingLabels") }}" method="post" role="form">{{ csrf_field() }}</form>
	<form id="form-multiPrintStockLists" action="{{ url("/app/order/multiPrintStockLists") }}" method="post" role="form">{{ csrf_field() }}</form>
	<form id="form-multiPrintCarrierManifests" action="{{ url("/app/order/multiPrintCarrierManifests") }}" method="post" role="form">{{ csrf_field() }}</form>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<style>
    .click {
        cursor: pointer;
    }

    .clicked {
        background-color: #f5f5f5;
    }
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var table = $('#order-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: {
            url : '{!! route('order.index') !!}',
            data: function (d) {
                d.status = $('#filter-status').val();
            }
        },
        columns: [
			{data: 'order_no', name: 'order_no', class: 'click'},
            {data: 'shop_order_no', name: 'shop_order_no', class: 'click'},
            {data: 'date', name: 'date', class: 'click'},
            {data: 'platform', name: 'platform', class: 'click', searchable: false},
            {data: 'shop', name: 'shop', class: 'click', searchable: false},
            {data: 'customer_name', name: 'customer_name', class: 'click'},
            {data: 'type_order', name: 'type_order', class: 'click', orderable: false, searchable: false},
            {data: 'status_order', name: 'status_order', class: 'click', orderable: false, searchable: false},
            {data: 'actions', name: 'actions', orderable: false, searchable: false},
			{data: function(row, type, val, meta){ return '<input type="checkbox" class="multiprint-selection" name="selection[]" />'; }, orderable: false, searchable: false }
        ]
    });

    $('#order-table tbody').on('click', 'td.click', function () {
        var data = table.row(this.closest('tr')).data();
        $('#order-table tbody tr').removeClass("clicked");
        $(this.closest('tr')).toggleClass("clicked");
        loadDetail(data.id);
    });

    $('#filter-status').on('change', function (e) {
        table.draw();
        e.preventDefault();
    })
	
	$("#btn-multiPrintOrders").on("click", function(e) {
		e.preventDefault();
		if($(".multiprint-selection:checked").length > 0) {
			$("form#form-multiPrintOrders").children(":not(input[name=_token])").remove();
			$(".multiprint-selection:checked").each(function(i) {
				$("form#form-multiPrintOrders").append('<input type="hidden" name="selection[]" value="' + $(this).parents("tr").find("td:nth-child(1)").text() + '" />');
			});
			$("form#form-multiPrintOrders").submit();
		}
	});
	
	$("#btn-multiPrintInvoices").on("click", function(e) {
		e.preventDefault();
		if($(".multiprint-selection:checked").length > 0) {
			$("form#form-multiPrintInvoices").children(":not(input[name=_token])").remove();
			$(".multiprint-selection:checked").each(function(i) {
				$("form#form-multiPrintInvoices").append('<input type="hidden" name="selection[]" value="' + $(this).parents("tr").find("td:nth-child(1)").text() + '" />');
			});
			$("form#form-multiPrintInvoices").submit();
		}
	});
	
	$("#btn-multiPrintShippingLabels").on("click", function(e) {
		e.preventDefault();
		if($(".multiprint-selection:checked").length > 0) {
			$("form#form-multiPrintShippingLabels").children(":not(input[name=_token])").remove();
			$(".multiprint-selection:checked").each(function(i) {
				$("form#form-multiPrintShippingLabels").append('<input type="hidden" name="selection[]" value="' + $(this).parents("tr").find("td:nth-child(1)").text() + '" />');
			});
			$("form#form-multiPrintShippingLabels").submit();
		}
	});
	
	$("#btn-multiPrintStockLists").on("click", function(e) {
		e.preventDefault();
		if($(".multiprint-selection:checked").length > 0) {
			$("form#form-multiPrintStockLists").children(":not(input[name=_token])").remove();
			$(".multiprint-selection:checked").each(function(i) {
				$("form#form-multiPrintStockLists").append('<input type="hidden" name="selection[]" value="' + $(this).parents("tr").find("td:nth-child(1)").text() + '" />');
			});
			$("form#form-multiPrintStockLists").submit();
		}
	});
	
	$("#btn-multiPrintCarrierManifests").on("click", function(e) {
		e.preventDefault();
		if($(".multiprint-selection:checked").length > 0) {
			$("form#form-multiPrintCarrierManifests").children(":not(input[name=_token])").remove();
			$(".multiprint-selection:checked").each(function(i) {
				$("form#form-multiPrintCarrierManifests").append('<input type="hidden" name="selection[]" value="' + $(this).parents("tr").find("td:nth-child(1)").text() + '" />');
			});
			$("form#form-multiPrintCarrierManifests").submit();
		}
	});

    var tableDetail = $("#order-detail-table").DataTable({
        data: [],
        columns: [
            {"data": "shop_order_no"},
            {"data": "product_sku_id"},
            {"data": "product_name"},
            {"data": "price"},
            {"data": "qty"},
        ],
        rowCallback: function (row, data) {
        },
        filter: false,
        info: false,
        paging: false,
        ordering: true,
        processing: true,
        retrieve: true,
        language: {
            emptyTable: "Click a Row on Order Table Above to Show Order Details"
        }
    });

    function loadDetail(id) {
        $.ajax({
            url: "{{ url('app/order/getDetail') }}",
            type: "post",
            data: {order: id}
        }).done(function (result) {
            tableDetail.clear().draw();
            tableDetail.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            tableDetail.clear().draw();
        });
    }

    function delOrder(el) {
        id = $(el).data('id')
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, cancel the order!",
            closeOnConfirm: false,
            html: false
        }, function () {
            url = "{{ route('order.destroy', ':id') }}";
            url = url.replace(':id', id);
            data = [{'name': '_token', 'value': '{{ csrf_token() }}'},
                {'name': '_method', 'value': 'DELETE'}
            ];
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (resp) {
                    swal(resp.text, resp.msg, resp.status);
                    $('#order-table').DataTable().draw()
                }
            })
        });

    }

    var syncProgress = false;
    function syncOrders() {
        $('#btn-sync-orders i').addClass('fa-spin');
        $('#btn-sync-orders').prop('disabled',true);
        syncProgress = true;
        $.ajax({
            url: "{{ url('app/order/syncOrders') }}",
            type: 'post',
            dataType: 'JSON',
            success: function (resp) {
                $('#btn-sync-orders i').removeClass('fa-spin');
                $('#btn-sync-orders').prop('disabled',false);
                if (resp.data == '') {
                    swal({title: "No orders yet!",text: resp.msg, type: 'info'});
                } else {
                    if(resp.status=='success'){
                        swal({title: "New Orders received!", text: resp.msg, type: 'success'});
                    }else{
                        swal({title: "Synchronization Failed!", text: resp.msg, type: 'error'});
                    }
                    table.draw();
                }
                syncProgress = false
            },
            error: function (resp) {
                $('#btn-sync-orders i').removeClass('fa-spin');
                $('#btn-sync-orders').prop('disabled',false);
                swal({title: "Synchronization Failed!",text: "Server platform cannot be reach",type: 'error'});
                syncProgress = false
            }
        })
    }

    window.onbeforeunload = confirmExit;
    function confirmExit() {
        if(syncProgress)
            return "You have attempted to leave this page.  If you have made any changes to the fields without clicking the Save button, your changes will be lost.  Are you sure you want to exit this page?";
    }

</script>
@endpush