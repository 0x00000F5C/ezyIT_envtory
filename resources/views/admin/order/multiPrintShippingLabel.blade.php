<!DOCTYPE html>
<html>
<head>
	<title>Multi shipping label print</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="{!! asset('assets/lte/bootstrap/css/bootstrap.css') !!}">
    {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css"> --}}
    <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/AdminLTE.min.css') !!}">

    <style>
    	body {
    		/*padding-top: 30px;*/
    	}
		
		table#shipping-labels {
			border-collapse: collapse;
			width: 100%;
		}

		table#shipping-labels td {
			border: 2px solid #000;
		}

		table#shipping-labels td:first-child {
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			width: 1.5em;
		}

		table#shipping-labels td:nth-child(2) {
			padding: 1em;
		}

		table#shipping-labels td:first-child div {
			margin: 0px -15em;
			text-transform: uppercase;
			transform: rotateZ(-90deg);
		}
    </style>
</head>
<body onload="{{ $print ? 'window.print();' : ''}}">
<div class="wrapper">
	<section class="invoice">
		@include('admin.order._multiPrintShippingLabels')
	</section>
</div>
</body>
</html>