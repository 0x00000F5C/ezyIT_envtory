@extends('layouts.lte.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('layouts.lte.status')
            <div class="row">
                <div class="col-md-4">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Order Information</h3>
                                </div>
                                <div class="box-body">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Shop Name</b> <span class="pull-right">{{@$model->shop->shop_name}}</span>
                                        </li>
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Order Date</b> <span class="pull-right">{{date('d-m-Y', strtotime(@$model->date))}}</span>
                                        </li>
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Proses Due Date</b> <span class="pull-right">{{date('d-m-Y', strtotime(@$model->due_date))}}</span>
                                        </li>
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Shop Order Number</b> <span class="pull-right">{{@$model->order_no}}</span>
                                        </li>
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Created By</b> <span class="pull-right">{{@$model->staff->firstname}}</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Customer Information</h3>
                                </div>
                                <div class="box-body">
                                    <address>
                                        <strong>{{@$model->customer_name}}, {{@$model->billing_cust_lastname}}.</strong><br>
                                        {{@$model->bill_address}}<br>
                                        {{@$model->city}}, {{@$model->state}} {{@$model->remark}},{{@$model->county}}<br>
                                        Phone 1: {{@$model->main_phone_cust}}<br>
                                        Phone 2: {{@$model->sec_phone_cust}}<br>
                                        Email: {{@$model->email}}
                                    </address>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Shipping Information</h3>
                                </div>
                                <div class="box-body">
                                    <address>
                                        <strong>{{@$model->ship_first_name}}, {{@$model->ship_last_name}}.</strong><br>
                                        {{@$model->ship_address_1}}<br>
                                        {{@$model->ship_address_2}}<br>
                                        {{@$model->ship_city}}, {{@$model->ship_state}} {{@$model->ship_country}}<br>
                                        Phone: {{@$model->ship_phone}}<br>
                                        Email: {{@$model->ship_email}}
                                    </address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Products Information</h3>
                                </div>
                                <div class="box-body">
                                <div class="col-xs-12">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th width="110">Shop SKU</th>
                                            <th>SKU</th>
                                            <th>Product Name</th>
                                            <th width="100">Price</th>
                                            <th width="50">Qty</th>
                                            <th width="100">Amount</th>
                                        </tr>
                                        @foreach ($details as $k=>$detail)
                                            @php($cur = \App\Library::getSymbolCurrency($detail->currency))
                                            <tr id="row-{{$k+1}}">
                                                <td>{{$detail->order->shop_order_no}}</td>
                                                <td>{{$detail->sku->sku}}</td>
                                                <td>{{$detail->sku->product->attribute('name')}}</td>
                                                <td class="text-right">{{$cur}} {{number_format($detail->price,2)}}</td>
                                                <td class="text-right">{{number_format($detail->qty)}}</td>
                                                <td class="text-right">{{$cur}} {{number_format($detail->price*$detail->qty,2)}}</td>
                                            </tr>
                                        @endforeach
                                        </table>
                                </div>
                                    <div class="col-xs-6"></div>
                                    <div class="col-xs-6">
                                            <div class="col-xs-12 table-responsive">
                                                <table class="table tabel-bordered">
                                                    <thead>
                                                    <tr>
                                                        <th width="60%">Subtotal</th>
                                                        <th>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{$cur}}</span>
                                                                <input id="subtotal" name="subtotal" readonly="" type="text" value="{{number_format(@$model->subtotal,2)}}" class="form-control input-sm text-right">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Shipping</th>
                                                        <th>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{$cur}}</span>
                                                                <input  readonly="" id="shipping_cost" name="shipping_cost"  type="text" class="text-right form-control input-sm" value="{{number_format(@$model->shipping_cost,2)}}">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Discount</th>
                                                        <th>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{$cur}}</span>
                                                                <input  readonly="" id="discount" name="discount"  type="text" class="form-control input-sm text-right" value="{{number_format(@$model->discount,2)}}">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Tax</th>
                                                        <th>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{$cur}}</span>
                                                                <input  readonly="" id="tax" name="tax"  type="text" class="form-control input-sm text-right" value="{{number_format(@$model->tax,2)}}">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th>Total</th>
                                                        <th>
                                                            <div class="input-group">
                                                                <span class="input-group-addon">{{$cur}}</span>
                                                                <input id="total" name="total" readonly="" type="text" class="form-control input-sm text-right" value="{{number_format(@$model->total,2)}}">
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    </thead>
                                                </table>
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <h3 class="box-title">Payment Information</h3>
                                </div>
                                <div class="box-body">
                                    <ul class="list-group list-group-unbordered">
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Payment type</b> <span class="pull-right">Transfer Bank</span>
                                        </li>
                                        <li class="list-group-item list-group-unbordered">
                                            <b>Bank Name</b> <span class="pull-right">BANK XYZ</span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="box box-default">
                    <div class="box-body">
                        <a href="{{route('order.index')}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Back</a>
                        <a href="{{url('app/order/' . @$model->order_no . '/pdfOrder')}}" class="btn btn-primary btn-flat pull-right"><i class="fa fa-download"></i> Download PDF</a>
                        <a href="{{url('app/order/' . @$model->order_no . '/printOrder')}}" class="btn btn-default btn-flat pull-right" style="margin-right: 8px;"><i class="fa fa-print"></i> Print</a>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop