<table class="table table-bordered table-hover dt-responsive nowrap" id="PackingListTABLE" cellspacing>
    <thead>
    <tr>
        <th>SKU</th>
        <th>BATCH ID</th>
        <th>PRODUCT NAME</th>
    </tr>
    </thead>
    <tbody>
    @foreach($order->detail as $key => $detail)
        <tr id="{{$detail->sku->sku}}">
            <td>{{$detail->sku->sku}}</td>
            <td>{{$detail->detailStock->batch_id}}</td>
            <td>{{$detail->sku->product->attribute('name')}}</td>
        </tr>
    @endforeach
    </tbody>
</table>
<input type="hidden" id="order_id_pack" value="{{$order->id}}" />
