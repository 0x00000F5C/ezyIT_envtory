@extends('layouts.lte.main')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <form role="form" class="form-horizontal" method="POST" action="{{ url('/app/packingList') }}" onsubmit="return validatePacking()">
                    {{ csrf_field() }}
                    <input type="hidden" name="packing[order_id]" id="order_id">
                    <div class="box-header">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-sm-2">Tray ID :</label>
                                <div class="col-sm-4">
                                    <input type="text" id="tray_id" name="tray_id" class="form-control" required autofocus>
                                </div>
                            </div>
                            <div class="form-group" style="display:none;">
                                <label class="col-sm-2">Picking ID :</label>
                                <div class="col-sm-4">
                                    <input type="text" class="form-control" id="picking_code" disabled>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group pull-right">
                                <label>Picking Date :</label>
                                <span id="picking_date">{{date('d F Y')}}</span>
                            </div>
                        </div>
                        <div class="clear" style="clear: both;"></div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <div class="row">
                            <div class="col-md-6">
                                <table class="table table-bordered table-hover dt-responsive nowrap" id="orderList-table" cellspacing>
                                    <thead>
                                    <tr>
                                        <th>Order No</th>
                                        <th style="width: 100px;">Action</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                            <div class="col-md-6">
                                <table class="table table-bordered table-hover dt-responsive nowrap" id="tbl-detailOrder" cellspacing>
                                    <thead>
                                    <tr>
                                        <th>SKU</th>
                                        <th>BATCH ID</th>
                                        <th>PRODUCT NAME</th>
                                    </tr>
                                    </thead>
                                    <tbody></tbody>
                                </table>
                            </div>
                        </div>

                        <div class="clear" style="clear: both; padding-bottom: 15px"></div>

                    </div>
                    <div class="box-footer">
                        <button class="btn btn-success btn-flat pull-right" type="submit"><i
                                    class="fa fa-save"></i> Finish
                        </button>
                        <button class="btn btn-default prevBtn btn-flat pull-left" type="button"><i
                                    class="fa fa-arrow-left"></i> Previous
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="packDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{--<h4 class="modal-title" id="myModalLabel">Packing</h4>--}}
                    <p>Move your device so that the QR code is in the viewer and hold still</p>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-6">

                        </div>
                        <div class="col-sm-6"></div>
                        <div class="col-sm-8">
                            <div id="dialogContainer"></div>

                        </div>
                        <div class="col-sm-4">
                            <input type="text"  placeholder="Barcode" class="form-control" id="productBarcode" />
                            <br>
                        </div>


                    </div>
                </div>
                <div class="modal-footer">
                    {{--<button type="button" class="btn btn-sm btn-default" data-dismiss="modal">x</button>--}}
                    {{--<button id="save_detail" onclick="save(0);" type="button" class="btn  btn-sm btn-danger">Tolak Konfirmasi</button>--}}
                    <button id="save_detail" onclick="savePACK();" type="button" disabled class="btn btn-flat  btn-xs btn-success"> <i class="fa fa-fw fa-print"></i>Print Invoice</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <style>
        .click {
            cursor: pointer;
        }

        .clicked {
            background-color: #f5f5f5;
        }

        .packed{
            background-color: #76f576;
        }
    </style>
@endpush

@push('scripts')
<script>
    $.ajaxSetup({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
    });
    $('#tray_id').keypress(function (e) {
        if (e.which == 13) {
        e.preventDefault();
            $('#msg_status_container').html('');
            var tray_id = $(this).val();
            $('#orderList-table tbody').html('');
            $('#picking_code').val('');
            $('#picking_date').text('');
            $.ajax({
                type: 'post',
                url: '{!! url('app/packingList/orderList') !!}',
                data: {'tray_id':tray_id},
                dataType: 'json',
                success: function (resp) {
                    if(resp.status){
                        var data = resp.data
                        $('#picking_code').val(data.pickings.picking_code);
                        $('#picking_date').text(data.pickings.date);
                        $('#orderList-table tbody').html(resp.html.orders);
                        $('#tbl-detailOrder tbody').html(resp.html.details)
                    }else{
                        push_msg('error', resp.msg);
                    }
                }
            });
        }
    });

    jQuery(document).ready(function () {

    });

    $('#orderList-table tbody').on('click', 'tr.click', function () {
        $('#orderList-table tbody tr').removeClass("clicked");
        $('#tbl-detailOrder tbody tr').hide();
        $(this).toggleClass("clicked");
        var order_id = $(this).data('order');
        $('#order_id').val(order_id)
        $('#tbl-detailOrder tbody tr.'+order_id).show()
    });

    function validatePacking() {
        if($('#order_id').val()==''){
            sweetAlert("Warning...", "Please select order!", "warning");
            return false
        }
    }

    function loadPack(order_id){
        var formData = {
            order_id :order_id,
            _token : "{{ csrf_token() }}"
        };

        jQuery.ajax({
            type: "POST",
            url: '{!! url('app/packingList/loadPacking') !!}',
            data: formData,
            success: function (resp) {
                $('#dialogContainer').html(resp);
                $('#packDialog').modal('show');
                $('#productBarcode').focus();
            }
        });
    }

    $('#packDialog').on('shown.bs.modal', function () {
        $('#productBarcode').focus();
    });

    $('#productBarcode').keypress(function (e) {
        if (e.which == 13) {
            var dstring = "#" + $('#productBarcode').val();
            $(dstring).addClass("packed");
            $('#productBarcode').val('');
            var lengthPacket = $('.packed').length;
            var lengthProduct = $('#PackingListTABLE tbody tr').length;
            if(lengthPacket==lengthProduct){
                $("#save_detail").removeAttr('disabled');
            }else{
                $("#save_detail").attr('disabled', 'disabled');

            }
        }
    });

    function savePACK(){
        var formData = {
            order_id : $('#order_id_pack').val(),
            type_pack: 'create',
            _token : "{{ csrf_token() }}"
        };

        jQuery.ajax({
            type: "POST",
            url: '{!! url('app/packingList/savePACK') !!}',
            data: formData,
            success: function (resp) {
                if(resp.status){
                    window.location = '{!! url('app/packingList') !!}'+'/'+resp.order_id+'/printInvoice'
                }

            }
        });
    }

</script>
@endpush