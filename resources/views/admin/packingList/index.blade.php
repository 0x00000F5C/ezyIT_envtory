@extends('layouts.lte.main')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <div class="box-header">
                    <div class="pull-right">
                        @can('create-packingList')
                            <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> Create Packing</a>
                        @endcan
                    </div>
                    <div class="clear" style="clear: both;"></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="packingList-table" cellspacing>
                        <thead>
                        <tr>
                            <th>Packing ID</th>
                            <th style="width: 144px">Packing date</th>
                            <th style="width: 144px">Staff</th>
                            <th style="width: 144px">Status</th>
                            <th style="width: 140px;"></th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('styles')
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
    });

    var table = $('#packingList-table').DataTable({
        processing: true,
        serverSide: true,
        ajax: '{!! route('packingList.index') !!}',
        columns: [
            { data: 'packing_id'},
            { data: 'packing_date'},
            { data: 'staff'},
            { data: 'packing_status', class : 'click'},
            { data: 'action', name: 'action', orderable: false, searchable: false }
        ],
    });
</script>
@endpush