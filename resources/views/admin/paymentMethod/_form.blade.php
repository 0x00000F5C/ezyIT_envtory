<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/paymentMethod/' . $model->id) : url('/app/paymentMethod') }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="method-name" class="col-md-5 control-label text-left">Payment method name<span class="red"> *</span></label>
				<div class="col-md-7">
					<input name="method-name" class="form-control" id="method-name" placeholder="Payment method name" value="{{ $model->exists ? $model->name : "" }}" required="required">
					@if ($errors->has("method-name"))
					<span class="help-block">
						<strong>{{ $errors->first("method-name") }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="method-isbank" value="1"{{ $model->exists ? ($model->is_bank ? ' checked="checked"' : '') : ' checked="checked"' }}>
						Payment method is bank
					</label>
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/paymentMethod") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
