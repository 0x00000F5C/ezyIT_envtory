<form role="form" class="form-horizontal" method="POST"
      action="{{ $model->exists ? url('/app/pickingTypes/' . $model->id) : url('/app/pickingTypes') }}">
    {{ csrf_field() }}
    @if($model->exists)
        {{ method_field('PUT') }}
        <input type="hidden" name="id" value="{{ $model->id }}">
    @endif
    <div class="box-body">
        <div class="form-group has-feedback">
            <div class="form-group">
                <label for="pickingtype-name" class="col-md-2 control-label text-left">Picking type name<span
                            class="red"> *</span></label>
                <div class="col-md-6">
                    <input type="text" name="pickingtype-name" class="form-control" id="pickingtype-name"
                           placeholder="Picking type name"
                           value="{{ old("pickingtype-name") ? old("pickingtype-name") : $model->picking_type_name }}"
                           required="required">
                    @if ($errors->has("pickingtype-name"))
                        <span class="help-block">
						<strong>{{ $errors->first("pickingtype-name") }}</strong>
					</span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="pickingtype-qty" class="col-md-2 control-label text-left">Quantity</label>
                <div class="col-md-6">
                    <input type="number" name="pickingtype-qty" class="form-control" id="pickingtype-qty"
                           placeholder="Quantity"
                           value="{{ old("pickingtype-qty") ? old("pickingtype-qty") : ($model->exists ? $model->qty : 0) }}">
                    @if ($errors->has("pickingtype-qty"))
                        <span class="help-block">
						<strong>{{ $errors->first("pickingtype-qty") }}</strong>
					</span>
                    @endif
                </div>
            </div>
            <div class="form-group">
                <label for="pickingtype-ordertype" class="col-md-2 control-label text-left">Order type</label>
                <div class="col-md-6">
                    <select name="pickingtype-ordertype" class="form-control" id="pickingtype-ordertype">
                        @foreach($orderTypes as $orderType)
                            <option value="{{ $orderType->id }}" {{ $orderType->id == $model->order_type ? 'selected="selected"' : "" }}>{{ $orderType->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group">
                <label for="pickingtype-ordertype" class="col-md-2 control-label text-left">Tray Needed</label>
                <div class="col-md-3">
                    <input type="number" name="pickingtype-troy-needed" class="form-control" id="pickingtype-troy-needed"
                           placeholder="1"
                           value="{{ old("pickingtype-troy-needed") ? old("pickingtype-troy-needed") : ($model->exists ? $model->troy_needed : 1) }}">
                    @if ($errors->has("pickingtype-troy-needed"))
                        <span class="help-block">
						<strong>{{ $errors->first("pickingtype-troy-needed") }}</strong>
					</span>
                    @endif
                </div>
                <div class="col-md-3">
                    <label for="pickingtype-troy-needed-per" class="col-md-2 control-label text-left">/</label>
                    <div class="col-md-6">
                        <select name="pickingtype-troy-needed-per" class="form-control" id="pickingtype-troy-needed-per">
                            @foreach($troyNeededPer as $per)
                                <option value="{{ $per['value'] }}" {{ $per['value'] == $model->troy_needed_per ? 'selected="selected"' : "" }}>{{ $per['label'] }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="box-footer">
        <div class="form-group">
            <label class="col-md-2 control-label"></label>
            <div class="col-md-4">
                <a href="{{ url("app/pickingTypes") }}" class="btn btn-default btn-flat"><i
                            class="fa fa-arrow-left"></i> Cancel</a>
                <button type="submit" class="btn btn-primary btn-flat"><i
                            class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
            </div>
        </div>
    </div>
</form>
