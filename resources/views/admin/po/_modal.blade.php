	<div class="modal fade" id="viewPoModal">
      <form action="/app/po/close" class="form-horizontal" method="POST" id="closeForm">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
            {{ csrf_field() }}{{ method_field('PUT') }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Purchase Order</h4>
          </div>
          <div class="modal-body">
          <form role="form" class="form-horizontal" id="process-form">
            <div class="row">
              <div class="col-md-6">
                <div class="form-group has-feedback">
                    <label for="purchase_prefix" class="col-sm-4 control-label">Purchase Order Id</label>
                    <input type="hidden" name="purchase_prefix" id="prefix" value="">
                    <label class="col-sm-8 control-label label2">: <span class="prefix"></span></label>
                  </div>
                <div class="form-group">
                    <label class="col-sm-4 control-label">Supplier</label>
                    <label class="col-sm-8 control-label label2">: <span id="supplier_name"></span></label>
                  </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                    <label class="col-sm-4 control-label">Purchase Date</label>
                    <label class="col-sm-8 control-label label2">: <span id="po_date"></span></label>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-4 control-label">Supplier Email</label>
                    <label class="col-sm-8 control-label label2">: <span id="email"></span></label>
                  </div>
              </div>
            </div>
            <div class="row" style="margin-top: 15px">
              <div class="col-md-12">
                <table class="table" id="product-table" cellspacing="0" width="100%">
                  <thead>
                    <tr>
                      <th></th>
                      <th width="150">Product PO Id</th>
                      <th width="100">Product Code</th>
                      <th width="100">Product Name</th>
                      <th align="center">Ordered</th>
                      <th align="center">Received</th>
                      <th align="right">Price</th>
                      <th align="right">Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                  </tbody>
                </table>
              </div>
            </div>
            <hr>
            <div class="row">
              <div class="col-md-8">
                <table class="table table-bordered" id="invoice-list">
                  <thead>
                    <tr>
                      <th></th>
                      <th>Invoice Number</th>
                      <th>Date</th>
                      <th>Qty Received</th>
                      <th>Amount</th>
                    </tr>
                  </thead>
                </table>
              </div>
              <div class="col-md-4">
              <h4>File</h4>
              <div style="height: 100px; overflow-x: auto">
                <table class="table table-bordered table-hover" id="fileList">
                  <tbody></tbody>
                </table>
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Cancel</button>
          </div>
        </form>
    </div>
  </div>
</div>