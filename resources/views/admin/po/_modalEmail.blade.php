<div class="modal fade" id="emailModal">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	              <span aria-hidden="true">×</span></button>
	            <h4 class="modal-title">Send Purchase Order to Supplier</h4>
			</div>
			<div class="modal-body">
				<div class="form-group">
		          <label for="email_email" class="col-sm-4 control-label">Supplier Email</label>
		          <div class="col-sm-7">
		            <input type="text" name="" class="form-control" id="email_email" value="" readonly="">
		          </div>
		        </div>
				<div class="form-group">
		          <label for="email_subject" class="col-sm-4 control-label">Subject</label>
		          <div class="col-sm-7">
		            <input type="text" name="email_subject" class="form-control" id="email_subject">
		          </div>
		        </div>
				<div class="form-group">
		          <label for="email_content" class="col-sm-4 control-label">Content</label>
		          <div class="col-sm-7">
		            <textarea class="form-control" rows="3" id="email_content" name="email_content">{{ @$mailTemplate->header . "\r\n\r\n\r\n" . @$mailTemplate->footer }}</textarea>
		            <a href="#" id="email_attach"></a>
					@if($mailTemplate->missingTemplate)
					<div class="form-group alert alert-warning alert-dismissible">
					  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
					  Email template is missing.
					  Set up this <a href="{{ url("app/companyProfiles#email-template") }}">here</a>.
					</div>
					@endif
		          </div>
		        </div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-primary btn-flat" id="emailSubmit">Send Email</button>
			</div>
		</div>
	</div>
</div>