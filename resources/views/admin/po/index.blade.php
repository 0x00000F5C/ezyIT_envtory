@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        @include('layouts.lte._export')
        <div class="box-tools">
          @can('create-po')
          <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> Add Puchase Order</a>
          @endcan
        </div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        {{-- <div class="row">
          <form action="" class="form-horizontal" method="GET" id="searchForm">
          <div class="col-md-2">
              <select name="status" id="statusPo" class="form-control">
                <option value="">PO Status</option>
                @foreach ($status as $id => $status)
                  <option value="{{$id}}" {{ $activeStatus == $id ? 'selected' : ''}}>{{$status['name']}}</option>
                @endforeach
              </select>
            </div>
          </form>
        </div> --}}
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="po-table" cellspacing>
          <thead>
              <tr>
                  <th>Date</th>
                  <th>PO Number</th>
                  <th>Supplier</th>
                  <th>Exp Date</th>
                  <th>Total Amount</th>
                  <th>Status</th>
                  <th></th>
              </tr>
          </thead>
          <tbody></tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@include('admin.po._modal')
@include('admin.processPo._upload')
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
{{-- <link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}"> --}}
<style>
    .right {
      text-align: right;
    }
    .center {
      text-align: center;
    }
    .action {
      min-width: 120px;
    }
    table.dataTable tbody th,
    table.dataTable tbody td {
        white-space: nowrap;
    }
    .form-horizontal .label1 {
        text-align: left;
    }
    .form-horizontal .label2 {
        text-align: left;
        font-weight: inherit;
    }
    .form-inline .qty {
      width: 80px
    }
    .form-inline .price {
      width: 100px
    }
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
{{-- <script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script> --}}
<script src="{{ elixir('js/poIndex.js') }}"></script>
@endpush