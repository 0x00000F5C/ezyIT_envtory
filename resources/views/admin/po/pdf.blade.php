@extends('layouts.report.main')
@section('content')
<h3>PURCHASE ORDER</h3>
<table class="no-border">
    <tr>
        <td width="40%">
            <strong>{{ $po->supplier->supplier_name }}</strong><br>
        </td>
        <td align="right" width="20%">Shipping To</td>
        <td width="40%">
            <strong>Company Name</strong><br>
        </td>
    </tr>
    <tr>
        <td>
            {{ $po->supplier->supplier_address }}<br>
            Email: {{ $po->email }}
        </td>
        <td></td>
        <td>
            Company Address<br>
            Email: {{ Auth::user()->email }}
        </td>
    </tr>
</table>
<table>
    <thead>
    <tr>
      <th></th>
      <th>Product Code</th>
      <th>Product Name</th>
      <th>Qty</th>
      <th>Price</th>
      <th>Amount</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($po->detail as $key => $detail)
        <tr>
          <td align="center">{{++$key}}</td>
          <td>{{$detail->product->product_code}}</td>
          <td>{{$detail->product->product_name}}</td>
          <td align="center">{{$detail->qty}}</td>
          <td align="right">{{$detail->price}}</td>
          <td align="right">{{$detail->qty * $detail->price}}</td>
        </tr>
    @endforeach
    </tbody>
    <tfoot>
        <tr>
            <td colspan="5" align="right" class="name"><strong>Sub Total</strong></td>
            <td align="right">{{ $po->total_det_mount }}</td>
        </tr>
        <tr>
            <td colspan="5" align="right" class="name"><strong>Shipping Cost</strong></td>
            <td align="right">{{ $po->ship_cost }}</td>
        </tr>
        <tr>
            <td colspan="5" align="right" class="name"><strong>Misc Cost</strong></td>
            <td align="right">{{ $po->misc_cost }}</td>
        </tr>
        <tr>
            <td colspan="5" align="right" class="name"><strong>Total Amount</strong></td>
            <td align="right">{{ $po->total_amount }}</td>
        </tr>
    </tfoot>
</table>
<br><br>
<table class="no-border">
    <tr>
        <td width="50%">
            Term and Condition
        </td>
        <td width="20%"></td>
        <td width="30%" align="right"></td>
    </tr>
    <tr>
        <td style="background-color: #f4f4f4;padding: 10px">
            Lorem ipsum Eiusmod cillum proident aliqua ut officia qui aliqua nisi labore pariatur quis sint exercitation.
        </td>
        <td></td>
        <td align="right">
            <div align="center">
                <?php $barcode = Barcode::getBarcodePNG($po->prefix, "C128", 3, 100); ?>
                <img src="data:image/png;base64,{{ $barcode }}" alt="{{ $po->prefix }}" width="200px" /><br>
                {{ $po->prefix }}
            </div>
        </td>
    </tr>
</table>
@stop