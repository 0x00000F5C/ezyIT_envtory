	<div class="box box-default">
	  <div class="box-body">
		<form role="form" class="form-horizontal" id="process-form">
		{{ csrf_field() }}
	  	<div class="row">
	  		<div class="col-md-6">
	  			<div class="form-group has-feedback">
		          <label for="purchase_prefix" class="col-sm-4 control-label">Purchase Order Id</label>
		          @if ($po->prefix)
		              <input type="hidden" name="purchase_prefix" id="prefix" value="{{ $po->prefix }}">
		              <label class="col-sm-8 control-label label2">: <span class="prefix">{{ $po->prefix }}</span></label>
	              @else
		          <div class="col-sm-8">
		              <select class="form-control" id="purchase_prefix" onchange="loadPoData(this.value)">
			              <option value="">Search PO</option>
			            </select>
		          </div>
		          @endif
		        </div>
	  			<div class="form-group">
		          <label class="col-sm-4 control-label">Supplier</label>
		          <label class="col-sm-8 control-label label2">: <span id="supplier_name">{{ $po->supplier->supplier_name or null }}</span></label>
		        </div>
	  		</div>
	  		<div class="col-md-6">
	  			<div class="form-group">
		          <label class="col-sm-4 control-label">Purchase Date</label>
		          <label class="col-sm-8 control-label label2">: <span id="po_date">{{ $po->po_date }}</span></label>
		        </div>
		        <div class="form-group">
		          <label class="col-sm-4 control-label">Supplier Email</label>
		          <label class="col-sm-8 control-label label2">: <span id="email">{{ $po->email }}</span></label>
		        </div>
	  		</div>
	  	</div>
	  	</form>
	  	<div class="row" style="margin-top: 15px">
	  		<div class="col-md-12">
	  			<table class="table" id="product-table" cellspacing="0" width="100%">
	  				<thead>
	  					<tr>
	  						<th></th>
	  						<th>Product PO Id</th>
	  						<th>Product Code</th>
	  						<th>Product Name</th>
	  						<th>Ordered</th>
	  						<th>Received</th>
	  						<th>Price</th>
	  						<th>Amount</th>
	  					</tr>
	  				</thead>
	  				<tbody>
	  				</tbody>
	  			</table>
	  		</div>
	  	</div>
	  </div>
	</div>

<div class="row processContent">
	<div class="col-md-9">
		<div class="box box-primary">
			@can('create-processPo')
			<div class="box-header">
				<h3 class="box-title">Invoice</h3>
				<div class="box-tools pull-right">
					<form method="GET" action="{{ url("app/makePayment") }}" style="display: inline-block; margin-right: 8px;">
						<input type="hidden" id="poid" name="poid" value="{{ @$po->prefix }}">
						<button type="submit" class="btn btn-primary btn-sm btn-flat pull-right"><i class="fa fa-credit-card"></i> Make payment</button>
					</form>
					<button type="button" class="btn btn-primary btn-sm btn-flat pull-right" data-toggle="modal" data-target="#invoiceModal" data-action="add"><i class="fa fa-plus"></i> Add Invoice</button>
				</div>
			</div>
			@endcan
			<div class="box-body">
				<table class="table" id="invoice-list">
					<thead>
						<tr>
							<th></th>
							<th>Invoice No</th>
							<th>Date</th>
							<th>Amount</th>
							<th></th>
						</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
	<div class="col-md-3">
		<div class="box box-success">
			<div class="box-header with-border">
				<h3 class="box-title">Files</h3>
				<div class="box-tools pull-right">
					<button class="btn btn-default btn-sm btn-flat" data-toggle="modal" data-target="#uploadModal"><i class="fa fa-upload"></i> Upload File</button>
				</div>
			</div>
			<div class="box-body no-padding">
				<ul class="nav nav-pills nav-stacked" id="fileList">
					<li><a href="#">No File Uploaded</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>
	@can('create-processPo')
		@include('admin.processPo._modal')
		@include('admin.processPo._upload')
	@endcan