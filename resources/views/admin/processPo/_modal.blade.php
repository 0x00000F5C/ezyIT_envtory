	<div class="modal fade" id="invoiceModal">
      <div class="modal-dialog modal-lg">
        <div class="modal-content">
        <form action="" class="form-horizontal" method="POST">
            {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Add Invoice</h4>
          </div>
          <div class="modal-body">
              <div class="row">
              	<div class="col-md-5 col-md-offset-1">
		  	  			<div class="form-group">
  			          <label class="col-sm-5 control-label">Purchase Order Id</label>
  			          <label class="col-sm-7 control-label label2"><span class="prefix">{{ $po->prefix }}</span></label>
                  <input type="hidden" name="t_po_master_id" class="t_po_master_id" value="">
  			          <input type="hidden" name="po_prefix" class="po_prefix" value="">
  			        </div>
              	</div>
              </div>
              <div class="row">
              	<div class="col-md-5 col-md-offset-1">
  		  	  			<div class="form-group">
    			          <label class="col-sm-5 control-label">Invoice Number</label>
    			          <div class="col-md-7">
    			          	<input type="text" class="form-control" name="prefix" id="invoice_no" autofocus="" required="">
    			          </div>
    			        </div>
              	</div>
              	<div class="col-md-5">
  		  	  			<div class="form-group">
    			          <label class="col-sm-5 control-label">Invoice Date</label>
    			          <div class="col-md-7">
            					<div class="input-group date">
            					  <input type="text" name="date" class="form-control datepicker" id="invoice_date" required="" value="">
            					  <div class="input-group-addon">
            					    <i class="fa fa-calendar"></i>
            					  </div>
            					</div>
    			          </div>
    			        </div>
              	</div>
              </div>
              <div class="row">
              	<div class="col-md-12">
              		<table class="table table-hover table-bordered" id="invoice-table" cellspacing="0" width="100%">
              			<thead>
              				<tr>
	              				<th></th>
	              				<th>Product Code</th>
	              				<th>Product Name</th>
	              				<th>Ordered</th>
	              				<th>Received</th>
	              				<th>Invoice Qty</th>
	              				<th>Price</th>
	              				<th>Invoice Price</th>
	              				<th>Amount</th>
              				</tr>
              			</thead>
              			<tbody>
              			</tbody>
              			<tfoot>
              				<tr>
              					<td colspan="8" align="right">Shipping Cost</td>
              					<td><input type="number" name="ship_cost" class="form-control price" id="invoice_ship_cost" value="{{ $po->ship_cost }}" onchange="calculateInvoice()" required></td>
              				</tr>
              				<tr>
              					<td colspan="8" align="right">Misc Cost</td>
              					<td><input type="number" name="misc_cost" class="form-control price" id="invoice_misc_cost" value="{{ $po->misc_cost }}" onchange="calculateInvoice()" required></td>
              				</tr>
              				<tr>
              					<td colspan="8" align="right">Total Amount</td>
              					<td align="right"><input type="hidden" name="total_amount" id="invoice_total_amount"><span id="text_invoice_total_amount"></span></td>
              				</tr>
              			</tfoot>
              		</table>
              	</div>
              </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Cancel</button>
            <button type="button" id="btn-printInvoice" class="btn btn-default btn-flat"><i class="fa fa-print"></i> Print</button>
            <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-check"></i> Save Invoice</button>
          </div>
        </form>
    </div>
	<form id="form-printInvoice" action="{{ url("app/po/printInvoice") }}" method="get">
		<input type="hidden" name="invoice" id="printIV-invoice">
		<input type="hidden" name="po" id="printIV-po">
	</form>
  </div>
</div>