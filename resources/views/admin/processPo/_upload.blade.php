<div class="modal fade" id="uploadModal">
  <div class="modal-dialog">
      <div class="modal-content">
        <form action="/app/po/upload" class="form-horizontal" method="POST" enctype="multipart/form-data">
          {{ csrf_field() }}
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span></button>
            <h4 class="modal-title">Upload Invoice</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-md-12">
                <div class="form-group">
                  <label class="col-sm-3 control-label">File Name</label>
                  <div class="col-md-8">
                    <input type="text" class="form-control" name="filename" autofocus="">
                   <input type="hidden" name="t_po_master_id" class="t_po_master_id" value="">
                    <input type="hidden" name="po_prefix" class="po_prefix" value="">
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-3 control-label">File</label>
                  <div class="col-md-8">
                    <input type="file" class="form-control" name="file" required="">
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default pull-left btn-flat" data-dismiss="modal">Cancel</button>
            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-upload"></i> Upload</button>
          </div>
        </form>
      </div>
  </div>
</div>