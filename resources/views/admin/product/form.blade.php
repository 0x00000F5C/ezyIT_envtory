@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
	  @include('layouts.lte.status')
	  @include('admin.product._form')
  </div>
</div>
@stop

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/waitme/waitMe.min.css') }}">
<link rel="stylesheet" href="{{ elixir('css/createProduct.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/select2/select2.min.js') }}"></script>
<script src="{{ asset('assets/plugins/waitme/waitMe.min.js') }}"></script>
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="{{ asset('assets/lte/plugins/ckeditor/ckeditor.js') }}"></script>
@if ($model->exists)
<script>
	var id = {{ $model->id }};
	var category = {{ $model->m_product_categories_id }};
</script>
<script src="{{ elixir('js/updateProduct.js') }}"></script>
@elseif ($clone)
<script>
	var id = {{ $code->id }};
	var category = {{ $code->m_product_categories_id }};
</script>
<script src="{{ elixir('js/cloneProduct.js') }}"></script>
@else
<script src="{{ elixir('js/addProduct.js') }}"></script>
@endif

<script type="text/x-template" id="attributes-list">
	<div class="form-group" :class="{'has-error': error}">
		<label class="col-sm-3 control-label">@{{ label }}  <span class="pull-right red" v-show="mandatory"> *</span></label>
		<div class="col-sm-5">
			<input v-if="type === 'text'" {{ $clone ? 'readonly' : '' }} type="text" :id="id" :name="name" class="form-control" :value="value" @input="update(id,$event.target.value)">
			<input v-else-if="type === 'numeric'" {{ $clone ? 'readonly' : '' }} :id="id" type="number" :name="name" class="form-control" :value="value" @input="update(id,$event.target.value)">
			<div v-else-if="type === 'date'" :id="id" class="input-group date">
				<div class="input-group-addon"><i class="fa fa-calendar"></i></div>
				<input type="text" {{ $clone ? 'readonly' : '' }} :name="name" class="form-control pull-right datepicker" :value="value" @input="update(id,$event.target.value)">
			</div>
			<textarea v-else-if="type === 'richText'" {{ $clone ? 'readonly' : '' }} :id="id" :name="name" rows="3" class="form-control" :value="value" @input="update(id,$event.target.value)"></textarea>
      @if ($clone)
      <input v-else-if="type === 'singleSelect' || type === 'multiSelect'" readonly type="text" :id="id" :name="name" class="form-control" :value="value">
      @else
      <select v-else-if="type === 'singleSelect' || type === 'multiSelect'" :name="name" class="form-control" :id="id" style="width:100%">
        <option value="">Please Select</option>
        <option if="id === 'brand'" :value="value" selected="">@{{ value }}</option>
        <option v-for="option in options" :selected="option.name === value">@{{ option.name }}</option>
      </select>
      @endif
      <span class="help-block">@{{ error }}</span>
		</div>
	</div>
</script>

<script type="text/x-template" id="attributes-variation">
	<div class="form-group" :class="{'has-error': error}">
		<label class="col-sm-3 control-label">@{{ label }}  <span class="pull-right red" v-show="mandatory"> *</span></label>
		<div class="col-sm-5">
      <div class="row variation">
        <div class="col-md-4" v-for="option in options">
          <label>
            <input :type="type === 'singleSelect' ? 'radio' : 'checkbox'" class="iCheck" :name="name" :value="option.name" :data-id="id"> @{{ option.name }}
          </label>
        </div>
      </div>
      <span class="help-block">@{{ error }}</span>
		</div>
	</div>
</script>

<script>
	function move(fromList,toList){
		var selectOptions = document.getElementById(fromList);
		for (var i = 0; i < selectOptions.length; i++) {
			var opt = selectOptions[i];
			if (opt.selected) {
				document.getElementById(fromList).removeChild(opt);
				document.getElementById(toList).appendChild(opt);
				i--;
			}
		}
	}
</script>
@endpush
