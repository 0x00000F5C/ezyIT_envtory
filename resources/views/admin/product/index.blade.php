@extends('layouts.lte.main')
@section('content')
<div class="row" id="app">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group hidden">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <form id="filter" action="" method="GET">
          <div class="col-md-1">
            <select name="limit" class="form-control" onchange="this.form.submit()">
              <option value="10" {{ $filter['limit'] == 10 ? 'selected' : ''}}>10</option>
              <option value="25" {{ $filter['limit'] == 25 ? 'selected' : ''}}>25</option>
              <option value="50" {{ $filter['limit'] == 50 ? 'selected' : ''}}>50</option>
              <option value="100" {{ $filter['limit'] == 100 ? 'selected' : ''}}>100</option>
            </select>
          </div>
          <div class="col-md-3">
            <div class="input-group">
                <input type="text" name="search" class="form-control" placeholder="Search Product Code or Name" value="{{ $filter['search'] }}">
                <span class="input-group-btn">
                  <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                </span>
              </div>
          </div>
        </form>
        <div class="pull-right">
          @can('create-products')
          @if ($sync['enable'])
            <button type="button" class="btn btn-warning btn-flat" onclick="syncLazada()"><i class="fa fa-refresh"></i> Sync with Lazada</button>
          @else
            <button type="button" class="btn btn-default btn-flat" disabled="">{!! $sync['reason'] !!}</button>
          @endif
          <a class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New Product</a>
          @endcan
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="products-table" cellspacing>
          <thead>
              <tr>
                <th>Name</th>
                <th>iSKU</th>
                <th>Brand</th>
                <th>Model</th>
                <th>Category</th>
                <th style="white-space: nowrap;">Active Qty</th>
                <th style="white-space: nowrap;">Cold Qty</th>
                <th>Created</th>
                <th></th>
              </tr>
          </thead>
          <tbody>
            @foreach ($products as $product)
            <?php
              $product_name = $product->attribute('name');
              $productName = strlen($product_name) > 35 ? substr($product_name, 0, 32).' . .' :  $product_name;
              $class = '';
              if(empty($product->stock_active)){
                $class = 'empty';
              }
            ?>
              <tr class="{{ $class }}">
                <td id="product-name">{{ $productName }}</td>
                <td>{{ $product->product_code }}</td>
                <td>{{ $product->attribute('brand') }}</td>
                <td>{{ $product->attribute('model') }}</td>
                <td>{{ $product->category->cat_name }}</td>
                <td align="center">{{ $product->stock_active }}</td>
                <td align="center">{{ $product->stock_cold }}</td>
                <td style="white-space: nowrap;">{{ date('Y-m-d', strtotime($product->created_at)) }}</td>
                <td align="right" style="white-space: nowrap;">
      					<?php
      						$disabled = !empty($product->stock_active_pending) ? 'disabled' : '';
      						$button = !empty($product->stock_active_pending) ? 'Quantity Added' : '<i class="fa fa-cart-plus"></i> Add Quantity';
                  $btn = '<button type="button" @click="showQtyModal('.$product->id.')" class="btn btn-flat btn-xs btn-default" '.$disabled.'>'.$button.'</button> ';
                  if (isset($product->code->id) && Auth::user()->can('create-products')) {
                    $btn .= '<a href="' . url('app/products/'.$product->id.'/clone') . '" class="btn btn-flat btn-xs btn-default"><i class="fa fa-copy"></i> Clone</a> ';
                  }
                  if (isset($product->code->id) && Auth::user()->can('update-products')) {
                    $btn .= '<a href="'.url('app/products/'.$product->code->id.'/update').'" class="disabled btn btn-warning btn-xs btn-flat"><i class="fa fa-pencil"></i> Update</a> ';
                  }
                  if (Auth::user()->can('delete-products')) {
                    $btn .= '<button type="button" class="btn btn-danger btn-xs btn-flat" onclick="confirmDelete(\''.$product->id.'\',\''.ucwords('Product').'\')"><i class="fa fa-trash"></i> Delete</button>';
                  }
      					?>
                <form action="{{url('app/products/'.$product->id)}}" method="POST" id="delete-{{$product->id}}">
                  {!! csrf_field().method_field("DELETE").$btn !!}
        				</form>
                </td>
              </tr>
            @endforeach
          </tbody>
        </table>
      </div>
      <div class="box-footer">
        <i class="fa fa-square" style="color:#ffc3c3"></i> Quantity is Empty
      </div>
    </div>
    <div class="pull-right">
      {{ $products->appends($filter)->links() }}
    </div>
  </div>
  @include('admin.product._setQtyModal')
</div>
@stop
@push('styles')
<style>
  .low-qty {
    background-color: #fff8a8;
  }
  .empty {
    background-color: #fde4e4;
  }
</style>
@endpush
@push('scripts')
<script src="https://unpkg.com/vue-select@latest"></script>
<script src="{{ elixir('js/product.js') }}"></script>
<script>
  function syncLazada() {
    axios.post('/app/products/syncLazada')
    .then(function(response) {
      swal({
          title: "Product is Synchronizing",
          text: "While this process, you are free to browse",
          type: "success"
        },function(){
          location.reload();
        });
    })
    .catch(function(error) {
        swal("Sync Product Failed", "Please try again", "error");
    });
  }
  CKEDITOR.replaceAll();
</script>
@endpush
