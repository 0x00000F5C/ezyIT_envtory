<form role="form" class="form-horizontal" id="receive-form" method="POST">
{{ csrf_field() }}
<div class="box box-primary">
  <div class="box-body">
  	<div class="row">
  		<div class="col-md-6">
  			<div class="form-group has-feedback">
	          <label for="purchase_prefix" class="col-sm-4 control-label">Purchase Order Id</label>
	          @if ($po->prefix)
	              <input type="hidden" name="purchase_prefix" id="prefix" value="{{ $po->prefix }}">
	              <label class="col-sm-8 control-label label2">: <span class="prefix">{{ $po->prefix }}</span></label>
              @else
	          <div class="col-sm-8">
	              <select class="form-control" name="purchase_prefix" id="purchase_prefix" onchange="loadProductReceived(this.value)">
                  <option value="">Search PO</option>
                  @foreach ($pos as $po)
  		              <option value="{{ $po->prefix }}">{{ $po->prefix }}</option>
                  @endforeach
		            </select>
	          </div>
	          @endif
	        </div>
  			<div class="form-group">
	          <label class="col-sm-4 control-label">Supplier</label>
	          <label class="col-sm-8 control-label label2">: <span id="supplier_name">{{ $po->supplier->supplier_name or null }}</span></label>
	        </div>
  		</div>
  		<div class="col-md-6">
  			<div class="form-group">
	          <label class="col-sm-4 control-label">Purchase Date</label>
	          <label class="col-sm-8 control-label label2">: <span id="po_date">{{ $po->po_date }}</span></label>
	        </div>
	        <div class="form-group">
	          <label class="col-sm-4 control-label">Supplier Email</label>
	          <label class="col-sm-8 control-label label2">: <span id="email">{{ $po->email }}</span></label>
	        </div>
  		</div>
  	</div>
  	<div class="row" style="margin-top: 15px">
  		<div class="col-md-12">
  			<table class="table dataTable" id="receive-table" cellspacing="0" width="100%">
  				<thead>
  					<tr>
  						<th>Receive</th>
              <th>Cold Batch ID</th>
              <th>Invoice</th>
              <th>Product</th>
              <th>Qty</th>
              <th>Reject</th>
              <th>Batch Type</th>
              <th>Warehouse</th>
              <th>Location</th>
  					</tr>
  				</thead>
  				<tbody>
  				</tbody>
  			</table>
  		</div>
  	</div>
  </div>
</div>
<div class="row">
  <div class="col-md-12">
    <button type="submit" class="btn btn-primary btn-flat pull-right">Proceed</button>
  </div>
</div>
</form>