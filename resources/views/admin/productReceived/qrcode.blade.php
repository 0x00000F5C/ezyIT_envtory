@extends('layouts.report.main')
@section('content')
  <h3 align="center">Product Received QR Code</h3>
  <table class="no-border">
    <thead>
      <tr>
        <th>Product</th>
        <th>QR Code</th>
      </tr>
    </thead>
    @foreach ($received as $key => $stock)
      <tr>
        <td width="200">
          <?php $supplier = isset($stock->invoice->po->supplier->supplier_name) ? $stock->invoice->po->supplier->supplier_name : ''; ?>
          Batch ID: {{ $stock->batch_id }}<br />
          Supplier: {{ $supplier }}<br />
          Product Code: {{ $stock->product->product_code }}<br />
          Receive Date: {{ $stock->received_date }}<br />
        </td>
        <td align="center">
          <?php
          $code =	"Batch ID : ".$stock->batch_id.
                  "\nSupplier : ".$supplier.
                  "\nProduct Code : ".$stock->product->product_code.
                  "\nReceive Date: ".$stock->received_date;
          ?>
          <img src="data:image/png;base64, {!! base64_encode(QrCode::format('png')->size(200)->generate($code)) !!} ">
        </td>
      </tr>
    @endforeach
  </table>
@stop
