{{--<form role="form" class="form-horizontal" id="receive-form" method="POST">--}}
{{ csrf_field() }}
<div class="box box-primary">
    <div class="box-body">
        <div class="row">
            <div class="col-sm-2">
                <label for="form-field-9">Date:</label>
                <div class="input-group date">
                    <input type="text" name="po_date" class="form-control datepicker" id="received_date"
                           value="{{$todayDate}}" required="">
                    <div class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </div>
                </div>

            </div>
            <div class="col-sm-1"></div>


            <div class="col-sm-2">
                <label for="form-field-9">Batch type:</label>
                <select class="form-control" name="batch_type" id="batch_type">
                    {{--<option value="">Search Batch</option>--}}
                    @foreach ($zones as $zone)
                        <option value="{{ $zone->prefix }}">{{ $zone->name }}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-sm-1"></div>


            <div class="col-sm-2">
                <label for="form-field-9">Active Batch ID:</label>
                <input type="text" placeholder="" class="form-control" name="active_batch_id" id="active_batch_id"
                       value="">
            </div>
            <div class="col-sm-1"></div>

            <div class="col-sm-3">
                <label for="form-field-9">&nbsp;</label>
                <input type="text" placeholder="search by PO, Invoice, Cold" autofocus class="form-control"
                       name="po_invoice_cold_id" id="po_invoice_cold_id" value="">
            </div>

        </div>
        <div class="row" style="margin-top: 15px">
            <div class="col-md-12">
                <table class="table dataTable" id="receive-table" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>INVID</th>
                        <th>PO number:</th>
                        <th>Invoice Number</th>
                        <th>Date</th>
                        <th>Cold Batch Id</th>
                        <th>Stock Type</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($pos as $po)
                        <tr>
                            <td>{{$po->invoice_id}}</td>
                            <td>{{$po->po_number}}</td>
                            <td>{{$po->invoice_number}}</td>
                            <td>{{$po->po_date}}</td>
                            <td>{{$po->cold_batch_id}}</td>
                            <td>{{$po->stock_type}}</td>
                            <td>{!!$po->action!!}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>

        <hr>
        <h4>Received Product List</h4>
        <div class="row">
            <div class="col-md-12">
                <table class="table dataTable" id="receive-table-detail" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th>INVID</th>
                        <th>Prod Code</th>
                        <th>Prod Name</th>
                        <th>Categories</th>
                        <th>Sub Categories</th>
                        <th>Cold Qty</th>
                        <th>Received</th>
                        <th>Reject</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <button type="submit" class="btn btn-primary btn-flat pull-right">Proceed</button>
    </div>
</div>
{{--</form>--}}