@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-md-12">
    @include('layouts.lte.status')
    @include('admin.productReceived2._index')
  </div>
</div>
@stop
@push('styles')
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<link rel="stylesheet" href="{{ asset('assets/plugins/selectize/css/selectize.bootstrap3.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
	<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
	<style>
		.red {
		    color: #dd4b39;
		  }
		.processContent {
		 	display: none;
		 }
		.form-horizontal .label1 {
		    text-align: left;
		}
		.form-horizontal .label2 {
		    text-align: left;
		    font-weight: inherit;
		}
		.form-inline .qty {
			width: 70px
		}
		.form-inline .price {
			width: 100px
		}
		.right {
			text-align: right;
		}
		.center {
			text-align: center;
		}
		table.dataTable tbody th {
		    white-space: nowrap;
		}
	</style>
@endpush
@push('scripts')
	<script src="{{ asset('assets/plugins/selectize/js/standalone/selectize.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/select2/select2.min.js') }}"></script>
	<script src="{{ elixir('js/productReceived2.js') }}"></script>
@endpush