@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box" id="categories">
      <div class="box-header">
        <div class="btn-group">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right">
          @if ($lastSync)
            <ol class="breadcrumb" v-if="activeItem.length">
              <li v-for="(active, index) in activeItem">@{{ active.cat_name }}</li>
            </ol>
            {{-- <h3 class="box-title">Synced : {{ $lastSync->created_at }} </h3> --}}
          @else
            @can('create-productCategories')
              <button type="button" class="btn btn-warning btn-flat" onclick="storeLazadaCategories()"><i class="fa fa-refresh"></i> Import Product Categories From Lazada</button>
            @endcan
          @endif
        </div>
			</div>
			<div class="box-body">
				<div class="box-body">
					<div class="row">
						<div class="col-md-12">
              <div class="categoryContainer">
                <div class="col-md-3 selectCategories" v-for="(category, index) in categories">
                  <ul class="nav nav-pills nav-stacked" :id="'category-'+index">
                    <li v-for="category in category" :data-id="category.id" @click="getChild(category,index);" :class="{ active: isActive(category,index), selected: isSelected(category) }"><a>@{{ category.cat_name | truncate }} <span class="pull-right" v-show="category.children"><i class="fa fa-angle-right"></i></span></a></li>
                  </ul>
                </div>
              </div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
.categoryContainer {
  width: 100%;
  background-color: #f4f4f4;
  border: 1px solid #ddd;
  height: 302px;
  overflow-x:auto;
  overflow-y:hidden;
  white-space:nowrap;
}
.categoryContainer [class*="col-lg"], 
.categoryContainer [class*="col-md"], 
.categoryContainer [class*="col-sm"] {
    float:none;
    display:inline-block;
}
.selectCategories {
  height: 300px;
  overflow: auto;
  background-color: #fff;
  padding-top: 15px;
  padding-bottom: 15px;
}  .selectCategories {
  	height: 300px;
  	overflow: auto;
  	background-color: #fff;
  	padding-top: 15px;
  	padding-bottom: 15px;
  }
  li {
  	cursor: pointer;
  }
  .child {
  	display: none
  }
  .nav-stacked>li.active>a,
  .nav-stacked>li.active>a:hover{
  	background-color: #f7f7f7;
  	border-left-color: transparent;
  }
  .nav-stacked>li.selected>a,
  .nav-stacked>li.selected>a:hover{
  	background-color: #f7f7f7;
  	border-left-color: #3c8dbc;
  }
</style>
@endpush
@push('scripts')
<script src="https://unpkg.com/vue/dist/vue.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="{{ asset('/js/categories.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: {
			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
	});

  function storeLazadaCategories()
  {
    swal({
      title: "Import Product Categories from Lazada?",
      text: "Process may take a while",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      confirmButtonText: "Yes",
      showLoaderOnConfirm: true,
    },
    function(){
      $.ajax({
        url: "/app/productCategories/storeLazadaCategories",
        type: "post",
        data: { }
      }).done(function(data){
        if(data.status == 'success'){
          swal({
            title: "Product Categories is Importing", 
            text: "While this process, you are free to browse",
            type: "success"
          },function(){
            location.reload();
          })
        }else{
          swal("Failed", "Import Product Categories Failed", "error")
        }
      });
    });
  }
</script>
@endpush