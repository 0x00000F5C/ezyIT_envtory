<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url("/app/profiles/" . $model->id) : url("/app/profiles") }}">
  {{ csrf_field() }}
  @if($model->exists)
	{{ method_field("PUT") }}
	<input type="hidden" name="id" value="{{ $model->id }}">
  @endif
  <div class="box-body">
    <div class="form-group has-feedback">
      <label for="profile-username" class="col-sm-2 control-label">Username <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="text" name="profile-username" class="form-control" id="profile-username" placeholder="Username" value="{{ $model->exists ? $model->name : "" }}"{{ $model->exists ? " readonly" : "" }}>
        @if ($errors->has("profile-username"))
            <span class="help-block">
                <strong>{{ $errors->first("profile-username") }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback">
      <label for="profile-firstname" class="col-sm-2 control-label">First Name <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="text" name="profile-firstname" class="form-control" id="profile-firstname" placeholder="First Name" value="{{ $model->exists ? $model->firstname : "" }}">
        @if ($errors->has("profile-firstname"))
            <span class="help-block">
                <strong>{{ $errors->first("profile-firstname") }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback">
      <label for="profile-lastname" class="col-sm-2 control-label">Last Name</label>
      <div class="col-sm-4">
        <input type="text" name="profile-lastname" class="form-control" id="profile-lastname" placeholder="Last Name" value="{{ $model->exists ? $model->lastname : "" }}">
        @if ($errors->has("profile-lastname"))
            <span class="help-block">
                <strong>{{ $errors->first("profile-lastname") }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback">
      <label for="profile-email" class="col-sm-2 control-label">Email <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="email" name="profile-email" class="form-control" id="profile-email" placeholder="Email" value="{{ $model->exists ? $model->email : "" }}">
        @if ($errors->has("profile-email"))
            <span class="help-block">
                <strong>{{ $errors->first("profile-email") }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback">
      <label for="profile-password" class="col-sm-2 control-label">
        @if($model->exists)Change Password
        @else Password <span class="red">*</span>
        @endif
      </label>
      <div class="col-sm-4">
        <input type="password" name="profile-password" class="form-control" id="profile-password" placeholder="{{ $model->exists ? "New" : ""}} Password" value="{{ old("password") }}">
        @if ($errors->has("profile-password"))
            <span class="help-block">
                <strong>{{ $errors->first("profile-password") }}</strong>
            </span>
        @endif
      </div>
      <div class="col-sm-4">
        <input type="password" name="profile-password_confirmation" class="form-control" id="profile-password_confirmation" placeholder="Retype Password">
        @if ($errors->has("password_confirmation"))
            <span class="help-block">
                <strong>{{ $errors->first("profile-password_confirmation") }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has("active") ? " has-error" : "" }}">
      <div class="col-sm-4 col-md-offset-2 hidden">
        <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="active" value="1">
              Active
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-footer">
    <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-4">
        <a href="{{url("app")}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Create profile" }}</button>
      </div>
    </div>
  </div>
</form>
