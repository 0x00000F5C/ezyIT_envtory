@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current() . '/export/xls' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current() . '/export/pdf' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current() . '/export/html' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<form class="form-inline pull-right">
					<input name="date" class="datepicker form-control" placeholder="Select starting date range" value="{{ $filter->dateRange }}">
					<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-print"></i> Submit</button>
				</form>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="report">
					<h2>Empty cell report</h2>
					<p><b>As for: </b>{{ date("d M Y", strtotime($filter->dateRange)) }}</p>
					<table class="table table-bordered table-hover">
						<thead><tr><th>No.</th><th>Zone category</th><th>Rack ID</th><th>Cell ID</th></tr></thead>
						<tbody>
							@for($i = 0; $i < count($emptyCells); $i++)
								<tr>
									<td>{{ $i + 1 }}</td>
									<td>{{ @$emptyCells[$i]->rack()->first()->zone()->first()->name }}</td>
									<td>{{ @$emptyCells[$i]->rack()->first()->prefix }}</td>
									<td>{{ @$emptyCells[$i]->prefix }}</td>
								</tr>
							@endfor
						</tbody>
					</table>
					<h2>Empty cell zone</h2>
					<p><b>As for: </b>{{ date("d M Y", strtotime($filter->dateRange)) }}</p>
					<table class="table table-bordered table-hover">
						<thead><tr><th>No.</th><th>Zone category</th></tr></thead>
						<tbody>
							@for($i = 0; $i < count($emptyCategories); $i++)
								<tr>
									<td>{{ $i + 1 }}</td>
									<td>{{ $emptyCategories[$i]->cat_name }}</td>
								</tr>
							@endfor
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.js') }}"></script>
<script>
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});
	$(".datetimepicker").datetimepicker({
		showSecond: true,
		dateFormat: "yy-mm-dd",
		timeFormat: "HH:mm:ss"
	});
</script>
@endpush