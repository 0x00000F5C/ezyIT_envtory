@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-body">
				<div class="col-md-4">
					<form class="form-horizontal" action="{{ URL::current().'/bestSellingSKU' }}">
						<fieldset>
							<legend>Best selling SKU</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
					<hr />
					<form class="form-horizontal" action="{{ URL::current().'/dailyAvgVsDailySales' }}">
						<fieldset>
							<legend>Daily price vs daily sales</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
					<hr />
					<form class="form-horizontal" action="{{ URL::current().'/emptyCell' }}">
						<fieldset>
							<legend>Empty cells</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="date" class="datepicker form-control" placeholder="Select date">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
					<hr />
					<form class="form-horizontal" action="{{ URL::current().'/fastMovingSKU' }}">
						<fieldset>
							<legend>Fast moving SKU</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
				</div>
				<div class="col-md-4">
					<form class="form-horizontal" action="{{ URL::current().'/slowMovingSKU' }}">
						<fieldset>
							<legend>Slow moving SKU</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
					<hr />
					<form class="form-horizontal" action="{{ URL::current().'/newBatch_StockInReport' }}">
						<fieldset>
							<legend>New batch - stock in report</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
					<hr />
					<form class="form-horizontal" action="{{ URL::current().'/salesBySKU' }}">
						<fieldset>
							<legend>Sales by SKU</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
					<hr />
					<form class="form-horizontal" action="{{ URL::current().'/stockLevel' }}">
						<fieldset>
							<legend>Stock level</legend>
							<div class="form-group">
								<div class="col-md-12">
									<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range">
								</div>
							</div>
							<div class="form-group">
								<div class="col-md-12">
									<input name="ceil" class="datetimepicker form-control col-md-12" placeholder="Select end date range">
								</div>
							</div>
							<button type="submit" class="btn btn-primary btn-flat pull-right"><i class="fa fa-print"></i> Submit</button>
						</fieldset>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.js') }}"></script>
<script>
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});
	$(".datetimepicker").datetimepicker({
		showSecond: true,
		dateFormat: "yy-mm-dd",
		timeFormat: "HH:mm:ss"
	});
</script>
@endpush