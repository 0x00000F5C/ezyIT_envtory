@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current() . '/export/xls' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current() . '/export/pdf' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current() . '/export/html' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<form class="form-inline pull-right">
					<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range" value="{{ $filter->dateFloor }}">
					<input name="ceil" class="datetimepicker form-control" placeholder="Select end date range" value="{{ $filter->dateCeil }}">
					<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-print"></i> Submit</button>
				</form>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<div class="report">
					<h4>Total Active batch id Qty</h4>
					<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
					<p><b>Active batch ID: </b></p>
					<table class="table table-bordered table-hover">
						<thead><tr><th>No.</th><th>SKU</th><th>Description</th><th>Category</th><th>Quantity</th><th>Batch ID</th></tr></thead>
						<tbody>
							@foreach($stock as $i => $o)
							<?php $product = $o->product()->first(); ?>
							<tr>
								<td>{{ $i + 1 }}</td>
								<td>{{ @$product->sku()->first()->sku }}</td>
								<td>{!! @$product->attribute("description") ? $product->attribute("description") : $product->attribute("short_description") !!}</td>
								<td>{{ @$product->category()->first()->cat_name }}</td>
								<td>{{ App\Stock::active($product) }}</td>
								<td>{{ $o->batch_id }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
					<h4>Total Cold Batch id Qty</h4>
					<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
					<p><b>Cold batch ID: </b></p>
					<table class="table table-bordered table-hover">
						<thead><tr><th>No.</th><th>SKU</th><th>Description</th><th>Category</th><th>Quantity</th><th>Batch ID</th></tr></thead>
						<tbody>
							@foreach($stock as $i => $o)
							<?php $product = $o->product()->first(); ?>
							<tr>
								<td>{{ $i + 1 }}</td>
								<td>{{ $product->sku()->first()->sku }}</td>
								<td>{!! $product->attribute("description") ? $product->attribute("description") : $product->attribute("short_description") !!}</td>
								<td>{{ $product->category()->first()->cat_name }}</td>
								<td>{{ App\Stock::cold($product) }}</td>
								<td>{{ $o->batch_id }}</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.js') }}"></script>
<script>
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});
	$(".datetimepicker").datetimepicker({
		showSecond: true,
		dateFormat: "yy-mm-dd",
		timeFormat: "HH:mm:ss"
	});
</script>
@endpush