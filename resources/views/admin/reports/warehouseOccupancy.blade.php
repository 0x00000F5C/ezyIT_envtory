@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current() . '/export/xls' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank" class="submit-btn"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current() . '/export/pdf' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank" class="submit-btn"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current() . '/export/html' . (!empty($_SERVER["QUERY_STRING"]) ? "?" . $_SERVER["QUERY_STRING"] : "") }}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<form class="form-inline pull-right">
					<input name="floor" class="datetimepicker form-control" placeholder="Select starting date range" value="{{ $filter->dateFloor }}">
					<input name="ceil" class="datetimepicker form-control" placeholder="Select end date range" value="{{ $filter->dateCeil }}">
					<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-print"></i> Submit</button>
				</form>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<form method="post" id="exportForm">{{ csrf_field() }}<input type="hidden" id="exportImage" name="chartImage"></form>
				<script>
					function readyCallback(event, chart) {
						$("#exportImage").val(chart.getImageURI());
					}
				</script>
				<h2 class="text-center">{{ $title }}</h2>
				<p class="text-center">
					<div id="warehouseOccupancy-chart"></div>
					@linechart("warehouseOccupancyChart", "warehouseOccupancy-chart")
				</p>
				<h2>By SKU/Amount sold</h2>
				<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
				<table class="table table-bordered table-hover">
					<thead><tr><th>Date</th><th>Warehouse</th><th>Total cells</th><th>Empty cells</th><th>Occupancy percentage</th></tr></thead>
					<tbody>
						@foreach($warehouse as $i => $o)
						<tr>
							<td>{{ date_format(date_create_from_format("Y-m-d", $o->date), "d F Y") }}</td>
							<td>{{ $o->prefix . " - " . $o->name }}</td>
							<td>{{ $o->total_cells }}</td>
							<td>{{ $o->empty_cells }}</td>
							<td>{{ $o->percentage }} %</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui-timepicker-addon.js') }}"></script>
<script>
	$(".datepicker").datepicker({
		dateFormat: "yy-mm-dd"
	});

	$(".datetimepicker").datetimepicker({
		showSecond: true,
		dateFormat: "yy-mm-dd",
		timeFormat: "HH:mm:ss"
	});
	
	$(".submit-btn").on("click", function(ev) {
		ev.preventDefault();
		$("#exportForm").attr("action", $(this).attr("href")).submit();
	});
</script>
@endpush