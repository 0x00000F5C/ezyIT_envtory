@extends('layouts.lte.main')
@section('content')
@include('layouts.lte.status')
<div class="row">
  <div class="col-md-8 col-sm-8 col-xs-12">
    <div class="box box-default">
      <div class="box-header">
        <h2 class="box-title">Role</h2>
      </div>
      <div class="box-body">
        <div class="responsive-table">
          <table class="table table-hover">
            <tbody>
              @foreach($roles as $role)
              <tr>
                <td>{{$role->name}}</td>
                @can('update-role')
                <td><a class="btn btn-xs btn-warning btn-flat pull-right" href="{{URL::current().'/'.$role->id.'/edit'}}"><i class="fa fa-lock"></i> Set Permission</a></td>
                @endcan
              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-4 col-sm-4 col-xs-12">
  @can('create-role')
    <div class="box box-default">
      <div class="box-header">
        <h2 class="box-title">New Role</h2>
      </div>
      <form method="post" action="{{URL::current()}}">
        {{ csrf_field() }}
        <div class="box-body">
          <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
            <input type="text" name="name" class="form-control" id="name" placeholder="Role Name" value="{{old('name')}}">
            @if ($errors->has('name'))
                <span class="help-block">
                    <strong>{{ $errors->first('name') }}</strong>
                </span>
            @endif
          </div>
        </div>
        <div class="box-footer">
          <button class="btn btn-sm btn-primary btn-flat pull-right" type="submit">Save Role</button>
        </div>
      </form>
    </div>
    @endcan
  </div>
</div>
@stop
