<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url("/app/warehouse/" . $wid . "/trays/" . $model->id) : url("/app/warehouse/" . $wid . "/trays") }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="tray-size" class="col-md-4 control-label text-left">Size</label>
				<div class="col-md-8">
					<select id="tray-size" class="form-control" name="tray-size">
						<option value="0"{{ $model->exists && $model->size == 0 ? ' selected' : "" }}>Small</option>
						<option value="1"{{ $model->exists && $model->size == 1 ? ' selected' : "" }}>Medium</option>
						<option value="2"{{ $model->exists && $model->size == 2 ? ' selected' : "" }}>Large</option>
					</select>
					@if ($errors->has("tray-size"))
					<span class="help-block">
						<strong>{{ $errors->first("tray-size") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="tray-code" class="col-md-4 control-label text-left">Tray code<span class="red"> *</span></label>
				<div class="col-md-8">
					<input name="tray-code" class="form-control" id="tray-code" placeholder="Tray code" value="{{ $model->exists ? $model->prefix : "" }}" maxlength="10" required="required">
					@if ($errors->has("tray-code"))
					<span class="help-block">
						<strong>{{ $errors->first("tray-code") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-6">
				<label for="tray-status" class="col-md-4 control-label text-left">Tray is in use</label>
				<div class="col-md-8">
					<input type="checkbox" name="tray-status" class="form-control" id="tray-status" value="1"{{ $model->exists && $model->status == 1 ? ' checked="checked"' : "" }}>
					@if ($errors->has("tray-status"))
					<span class="help-block">
						<strong>{{ $errors->first("tray-status") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/warehouse/" . $wid . "/trays") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
