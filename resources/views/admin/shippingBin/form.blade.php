@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.warehouse.trays._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});
	
	$("#tray-size").on("change", function(){
		$.ajax({
			type: "post",
			url: "{!! url("app/warehouse/" . $wid . "/trays/genPrefix") !!}",
			data: { q: $("#tray-size > option:selected").text() },
			dataType: "json",
			success: function(data) {
				$("#tray-code").val(data.data);
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
</script>
@endpush