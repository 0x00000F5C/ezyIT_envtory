@extends('layouts.lte.main')
@section('content')
<div class="row">
  <div class="col-xs-12">
    @include('layouts.lte.status')
    <div class="box">
      <div class="box-header">
        <div class="btn-group hidden">
          <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
            <i class="fa fa-external-link"></i> Export <span class="caret"></span>
            <span class="sr-only">Toggle Dropdown</span>
          </button>
          <ul class="dropdown-menu" role="menu">
            <li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
            <li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
            <li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
          </ul>
        </div>
        <div class="pull-right">
          @can('create-warehouseTrays')
          <a id="btn-newShippingBin" class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New Shipping Bin</a>
          @endcan
        </div>
		<div class="clear" style="clear: both;"></div>
      </div>
      <!-- /.box-header -->
      <div class="box-body">
        <table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="shippingBin-table" cellspacing>
          <thead>
              <tr>
                  <th style="width: 16px">#</th>
                  <th>Bin ID</th>
                  <th style="width: 80px">status</th>
                  <th style="width: 128px">Action</th>
              </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<div id="shippingBin-modal" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<form id="shippingBin-form" class="form-horizontal" method="POST">
				{{ csrf_field() }}
				<input type="hidden" name="id" value="">
				<div class="modal-header">
					<h5 class="modal-title" id="shippingBinForm-title" style="display: inline; font-weight: bold; font-size: 14pt;">Manage shipping bin</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group has-feedback">
						<div class="col-md-8">
							<label for="bin-id" class="col-md-4 control-label text-left">Bin ID</label>
							<div class="col-md-8">
								<input name="bin-id" class="form-control" id="bin-id" placeholder="Bin ID" value="" maxlength="16" required="required">
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Save changes</button>
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
	$(function() {
		$('#shippingBin-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('shippingBin.index') !!}',
			columns: [
				{ data: function(row, type, val, meta){ return meta.row + 1; }, name: 'id' },
				{ data: 'bin_id', name: 'bin_id' },
				{ data: 'status', name: 'status' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id).data("id", data.id).data("code", data.packing_code);
			}
		});
	});

	$("#btn-newShippingBin").on("click", function(e){
		e.preventDefault();
		$("#shippingBin-modal input:not([name=_token])").val("");
		$("#shippingBin-modal #shippingBinForm-title").text("Add new shipping bin");
		$("#shippingBin-modal").modal("show");
	});

	$("#shippingBin-table").on("click", ".btn-editShippingBin", function(e){
		e.preventDefault();
		$("#shippingBin-modal #shippingBinForm-title").text("Edit shipping bin");
		$("#shippingBin-form input[name=id]").val($(this).parents("tr").data("id"));
		$("#shippingBin-form #bin-id").val($(this).parents("tr").children("td:nth-child(2)").text());
		$("#shippingBin-modal").modal("show");
	});
</script>
@endpush