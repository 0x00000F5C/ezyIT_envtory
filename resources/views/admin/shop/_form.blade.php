<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/shop/'.$model->id) : url('/app/shop') }}">
  {{ csrf_field() }}
  @if($model->exists){{ method_field('PUT') }}@endif
  <div class="box-body" id="form">
    <div class="form-group has-feedback{{ $errors->has('platform_id') ? ' has-error' : '' }}">
      <label for="platform_id" class="col-sm-2 control-label">Platform <span class="red">*</span></label>
      <div class="col-sm-4">
        <select class="form-control" name="platform_id" id="platform" {{ $model->exists ? 'disabled' : '' }}>
          @foreach($platforms as $key => $platform)
            <option value="{{$key}}" {{$model->platform_id == $key || old('platform_id') == $key ? 'selected' : ''}} data-prefix="{{ strtoupper(substr($platform, 0, 3)) }}">{{$platform}}</option>
          @endforeach
        </select>
        @if ($errors->has('platform_id'))
            <span class="help-block">
                <strong>{{ $errors->first('platform_id') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback {{ $errors->has('shop_name') || $errors->has('prefix')  ? ' has-error' : '' }}">
      <label for="shop_name" class="col-sm-2 control-label">Shop Name <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="text" name="shop_name" class="form-control" id="shop_name" placeholder="Shop Name" value="{{old('shop_name') ? old('shop_name') : $model->shop_name}}" onkeyup="{{ !$model->exists ? 'generatePrefix(this)' : ''}}">
        @if ($errors->has('shop_name'))
            <span class="help-block">
                <strong>{{ $errors->first('shop_name') }}</strong>
            </span>
        @endif
      </div>
      <div class="col-sm-2">
        <input type="text" name="prefix" class="form-control" id="prefix" placeholder="Shop Prefix" value="{{old('prefix') ? old('prefix') : $model->prefix}}" {{ $model->exists ? 'disabled' : ''}}>
        @if ($errors->has('prefix'))
            <span class="help-block">
                <strong>{{ $errors->first('prefix') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('pic_name') ? ' has-error' : '' }}">
      <label for="pic_name" class="col-sm-2 control-label">PIC Name <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="text" name="pic_name" class="form-control" id="nama" placeholder="PIC Name" value="{{old('pic_name') ? old('pic_name') : $model->pic_name}}">
        @if ($errors->has('pic_name'))
            <span class="help-block">
                <strong>{{ $errors->first('pic_name') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <hr>
    <div class="form-group has-feedback{{ $errors->has('account')  ? ' has-error' : '' }}">
      <label for="account" class="col-sm-2 control-label">Platform Account</label>
      <div class="col-sm-4">
        <input type="email" name="account" class="form-control" id="account" onkeydown="refreshButton()" placeholder="Shop Name" value="{{old('account') ? old('account') : $model->account}}">
        @if ($errors->has('account'))
            <span class="help-block">
                <strong>{{ $errors->first('account') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('api_key')  ? ' has-error' : '' }}">
      <label for="api_key" class="col-sm-2 control-label">API Key</label>
      <div class="col-sm-4">
        <input type="text" name="api_key" class="form-control" id="api_key" onkeydown="refreshButton()" placeholder="Shop Name" value="{{old('api_key') ? old('api_key') : $model->api_key}}">
        @if ($errors->has('api_key'))
            <span class="help-block">
                <strong>{{ $errors->first('api_key') }}</strong>
            </span>
        @endif
      </div>
      <div class="c0l-sm-2">
        <button type="button" class="btn btn-warning btn-flat" id="test" onclick="testApi()"><i class="fa fa-rocket"></i> Test Connection</button>
        <button type="button" class="btn btn-success btn-flat" id="connected" style="display: none"><i class="fa fa-check"></i> Connected</button>
        <button type="button" class="btn btn-danger btn-flat" id="fail" style="display: none"><i class="fa fa-times"></i> Connection Fail</button>
      </div>
    </div>
  </div>
  <div class="box-footer">
    <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-4">
        <a href="{{url('app/shop')}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? 'Update Shop' : 'Add Shop'}}</button>
      </div>
    </div>
  </div>
</form>
