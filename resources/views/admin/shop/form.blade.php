@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    @include('layouts.lte.status')
    <div class="box box-primary" id="app">
      @include('admin.shop._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/waitme/waitMe.min.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/plugins/waitme/waitMe.min.js') }}"></script>
<script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });

  $(function () {
    $('input').iCheck({
      checkboxClass: 'icheckbox_square-blue',
      radioClass: 'iradio_square-blue',
      increaseArea: '20%' // optional
    });
  });

  function generatePrefix(shop){
    var platformPrefix = $('#platform option:selected ').data('prefix');
    var shopNamePrefix = shop.value.substring(0,3);
    var shopPrefix = (platformPrefix+shopNamePrefix).toUpperCase();
    $('#prefix').val(shopPrefix);
  }

  function testApi(){
    $('#test').waitMe({effect:'bounce'});
    var account = $('#account').val();
    var api_key = $('#api_key').val();
    axios.get('/app/shop/testApi/'+account+'/'+api_key)
      .then(function(response) {
        console.log(response);
        $('#test').waitMe("hide");
        $('#test').hide();
        var data = response.data;
        if(data.message == 'ok'){
          $('#connected').show();
        }else{
          $('#fail').show();
        }
      })
      .catch(function(error) {
        console.log(error);
        $('#test').waitMe("hide");
      });
  }

  function refreshButton(){
    $('#test').show();
    $('#connected').hide();
    $('#fail').hide();
  }

</script>
@endpush