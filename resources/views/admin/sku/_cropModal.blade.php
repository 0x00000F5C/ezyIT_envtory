<div class="modal fade" id="cropModal" tabindex="-1" role="dialog" aria-labelledby="cropModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form class="form-horizontal" method="POST">
      <div class="modal-header">
        <button type="button" class="close" aria-label="Close" data-dismiss="modal"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Crop Image</h4>
      </div>
      <div class="modal-body">
        <div id="main-cropper"></div>
        <input type="hidden" name="sku" id="sku" value="">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
        <button type="button" class="btn btn-primary btn-flat" id="btn-result" data-sku="">Crop</button>
      </div>
    </form>
    </div>
  </div>
</div>
