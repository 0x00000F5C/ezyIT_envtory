<form id="filter" action="" method="GET">
	<div class="row">
		<div class="col-md-12">
			@can('create-bundle')
			<a class="btn btn-default btn-flat pull-right" href="{{URL::current().'/bundle/create'}}">Create a SKU Bundle</a>
			@endcan
      @can('create-sku')
      <a class="btn btn-primary btn-flat pull-right" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> Register SKU</a>
      @endcan
			<input type="hidden" name="status" value="{{ $filter['status'] }}">
    	<ul class="nav nav-tabs" role="tablist">
	    <li class="{{ $filter['status'] == '' ? 'active' : '' }}"><a href="{{ url('app/sku') }}">All</a></li>
			<li class="{{ $filter['status'] == 'bundle' ? 'active' : '' }}"><a href="{{ url('app/sku?status=bundle') }}">Bundle</a></li>
	    <li class="{{ $filter['status'] == 'active' ? 'active' : '' }}"><a href="{{ url('app/sku?status=active') }}">Active</a></li>
	    <li class="{{ $filter['status'] == 'inactive' ? 'active' : '' }}"><a href="{{ url('app/sku?status=inactive') }}">Inactive</a></li>
	  </ul>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-md-1">
			<select name="limit" class="form-control" onchange="this.form.submit()">
				<option value="10" {{ $filter['limit'] == 10 ? 'selected' : ''}}>10</option>
				<option value="25" {{ $filter['limit'] == 25 ? 'selected' : ''}}>25</option>
				<option value="50" {{ $filter['limit'] == 50 ? 'selected' : ''}}>50</option>
				<option value="100" {{ $filter['limit'] == 100 ? 'selected' : ''}}>100</option>
			</select>
		</div>
		{{-- <div class="col-md-2 col-md-offset-4">
			<select name="qty" class="form-control" onchange="this.form.submit()">
				<option value="">All Quantity</option>
				<option value="save" {{ $filter['qty'] == 'save' ? 'selected' : ''}}>Save</option>
				<option value="below-save" {{ $filter['qty'] == 'below-save' ? 'selected' : ''}}>Below Save</option>
				<option value="empty" {{ $filter['qty'] == 'empty' ? 'selected' : ''}}>Empty</option>
			</select>
		</div> --}}
		<div class="col-md-2">
			<select name="shop" class="form-control" onchange="this.form.submit()">
				<option value="">All Shop</option>
				@foreach (\App\Shop::all() as $shop)
					<option value="{{ $shop->id }}" {{ $shop->id == $filter['shop'] ? 'selected' : ''}}>{{ $shop->shop_name }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-3">
			<div class="input-group">
		      <input type="text" name="search" class="form-control" placeholder="Search SKU, Product Code or Name" value="{{ $filter['search'] }}">
		      <span class="input-group-btn">
		        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
		      </span>
		    </div>
		</div>
	</div>
</form>
