<div class="modal fade" id="priceModal" tabindex="-1" role="dialog" aria-labelledby="priceModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form class="form-horizontal" method="POST" @submit.prevent="setPrice">
      <div class="modal-header">
        <button type="button" class="close" @click="hidePriceModal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Set SKU Price</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" name="sku_id" v-model="price.sku_id">
          <div class="form-group">
            <label class="control-label col-sm-3">SKU Product</label>
            <div class="col-sm-9">
	            <input type="text" class="form-control" readonly="" v-model="sku">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Retail Price</label>
            <div class="col-sm-9">
	            <input type="number" class="form-control" id="price" v-model="price.retail_price">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Range Price</label>
            <div class="col-sm-4">
	            <input type="number" class="form-control" id="price" placeholder="Min" v-model="price.min_price">
            </div>
            <div class="col-sm-4 col-sm-offset-1">
	            <input type="number" class="form-control" id="price" placeholder="Max" v-model="price.max_price">
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label class="control-label col-sm-3">Shop</label>
            <div class="col-sm-9">
	            <select name="shop_id" class="form-control" v-model="price.shop_id">
		            <option disabled value="">Please Select a Shop</option>
	            	<option v-for="shop in shops" :value="shop.id">@{{ shop.prefix+' - '+shop.shop_name}}</option>
	            </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Sale Price</label>
            <div class="col-sm-9">
	            <input type="number" class="form-control" id="price" v-model="price.price" :disabled="!price.shop_id">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Special Price</label>
            <div class="col-sm-9">
	            <input type="number" class="form-control" v-model="price.special_price" :disabled="!price.shop_id">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Special Price Period</label>
            <div class="col-sm-4">
            	<input type="text" class="form-control special_period" id="special_from_date" placeholder="From" :value="price.special_from_date" :disabled="!price.special_price">
            </div>
            <div class="col-sm-1">
            </div>
            <div class="col-sm-4">
            	<input type="text" class="form-control special_period" id="special_to_date" placeholder="To" :value="price.special_to_date" :disabled="!price.special_price">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" @click="hidePriceModal">Close</button>
        <button type="submit" class="btn btn-primary btn-flat" :disabled="!price.price">Set Price</button>
      </div>
    </form>
    </div>
  </div>
</div>
