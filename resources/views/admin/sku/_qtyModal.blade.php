<div class="modal fade" id="qtyModal" tabindex="-1" role="dialog" aria-labelledby="qtyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form class="form-horizontal" method="POST" @submit.prevent="setQtySetting">
      <div class="modal-header">
        <button type="button" class="close" @click="hideQtyModal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="exampleModalLabel">Quantity Setting</h4>
      </div>
      <div class="modal-body">
        <input type="hidden" class="form-control" name="sku_id" v-model="price.sku_id">
          <div class="form-group">
            <label class="control-label col-sm-3">SKU Product</label>
            <div class="col-sm-9">
	            <input type="text" class="form-control" readonly="" v-model="sku">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Active Qty</label>
            <div class="col-sm-3">
              <input type="number" class="form-control" disabled="" v-model="qty.active">
            </div>
            <label class="control-label col-sm-3">Cold Qty</label>
            <div class="col-sm-3">
              <input type="number" class="form-control" disabled="" v-model="qty.cold">
            </div>
          </div>
          <hr>
          <div class="form-group">
            <label class="control-label col-sm-3">Save Qty</label>
            <div class="col-sm-3">
              <input type="number" class="form-control" v-model="qty.save_qty">
            </div>
            <label class="control-label col-sm-3">Max shelved life</label>
            <div class="col-sm-3">
              <input type="number" class="form-control"  v-model="qty.max_shelved_life">
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-sm-3">Min Qty</label>
            <div class="col-sm-3">
              <input type="number" class="form-control"  v-model="qty.min_qty">
            </div>
            <label class="control-label col-sm-3">Max Qty</label>
            <div class="col-sm-3">
	            <input type="number" class="form-control"  v-model="qty.max_qty">
            </div>
          </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default btn-flat" @click="hideQtyModal">Close</button>
        <button type="submit" class="btn btn-primary btn-flat">Set Quantity Setting</button>
      </div>
    </form>
    </div>
  </div>
</div>
