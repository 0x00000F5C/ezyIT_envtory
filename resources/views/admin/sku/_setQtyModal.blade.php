<div class="modal fade" id="setQtyModal" tabindex="-1" role="dialog" aria-labelledby="setQtyModal">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <form class="form-horizontal" method="POST" @submit.prevent="">
        <div class="modal-header">
          <button type="button" class="close" @click="hideSetQtyModal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="exampleModalLabel">Set Quantity</h4>
        </div>
        <div class="modal-body">
          <input type="hidden" class="form-control" name="sku_id" v-model="price.sku_id">
          <div class="form-group">
            <label class="control-label col-sm-3">SKU Product</label>
            <div class="col-sm-9">
              <input type="text" class="form-control" readonly="" v-model="sku">
            </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3"></label>
              <div class="col-sm-4">
                <select class="form-control" v-model="setQty.zone_type">
                  <option value="" disabled="">Select Batch Type</option>
                  <option v-for="zone in zone_type" :value="zone.id" v-text="zone.name"></option>
                </select>
              </div>
              <div class="col-sm-4">
                <select class="form-control" v-model="setQty.Warehouse">
                  <option value="" disabled="">Select Warehouse</option>
                  <option v-for="zone in zone_type" :value="zone.id" v-text="zone.name"></option>
                </select>
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3"></label>
              <div class="col-sm-8">
                <v-select v-model="selectedCell" label="name" :debounce="250" :on-search="searchCell" :options="cells" placeholder="Search Cell"></v-select>
              <input type="hidden" v-model="setQty.cell_id">
              </div>
          </div>
          <div class="form-group">
              <label class="control-label col-sm-3">Quantity</label>
              <div class="col-sm-4">
                <input type="number" class="form-control" v-model="setQty.qty">
              </div>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>