@extends('layouts.lte.main')

@push('styles')
<style media="screen">
  .browseImage {
    border: 1px dashed #dfdfdf;
    width: 100px;
    height: 100px;
    margin-right: 10px;
    text-align: center;
    padding-top: 25px;
  }
  .image-container {
    border: 1px solid #dfdfdf;
    width: 100px;
    height: 100px;
    margin-right: 10px;
    text-align: left;
    padding-top: 0;
    padding-left: 0;
  }
  .btn-trans {
    background-color: transparent;
    color: #3c8dbc;
  }
  .txt-browse {
    white-space: nowrap;
  }
</style>
<style media="screen">
  .img-container {
    position: relative;
    width: 100px;
    height: 100px;
    margin-right: 10px;
    padding-top: 0;
    padding-left: 0;
    text-align: left;
    background-color: black;
  }

  .image {
  opacity: 1;
  display: block;
  /*width: 100%;*/
  height: auto;
  transition: .5s ease;
  backface-visibility: hidden;
  }

  .middle {
  transition: .5s ease;
  opacity: 0;
  position: absolute;
  top: 42%;
  /*left: 30%;*/
  transform: translate(-50%, -50%);
  -ms-transform: translate(-50%, -50%);
  }

  .img-container:hover .image {
  opacity: 0.6;
  }

  .img-container:hover .middle {
  opacity: 1;
  }

  .img-control {
    color: white;
    font-size: 16px;
    width: 100px;
    position: absolute;
    text-align: center;
  }

  .img-control a {
    padding-left: 8px;
    padding-right: 8px;
    color: #fff;
  }
</style>
<link rel="stylesheet" href="{{ asset('assets/plugins/croppie/croppie.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/plugins/waitme/waitMe.min.css') }}">
@endpush

@section('content')

  <div class="row" id="app">
    <div class="col-md-12">
      @include('layouts.lte.status')
      <div class="box">
        <div class="box-header">
          <form class="" action="" method="get">
            <input type="hidden" name="status" value="{{ $filter['status'] }}">
            <ul class="nav nav-tabs" role="tablist">
              <li class="{{ $filter['status'] == '' ? 'active' : '' }}"><a href="{{ url('app/sku/image') }}">All ({{ $total['all'] }})</a></li>
              <li class="{{ $filter['status'] == 'missing' ? 'active' : '' }}"><a href="{{ url('app/sku/image?status=missing') }}">Image Missing ({{ $total['missing'] }})</a></li>
            </ul>
            <br>
            <div class="row">
              <div class="col-md-3 pull-right">
                <div class="input-group">
                  <input type="text" name="search" class="form-control" placeholder="Search SKU or Product" value="{{ $filter['search'] }}">
                  <span class="input-group-btn">
                    <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                  </span>
                </div>
              </div>
            </div>
          </form>
        </div>
        <div class="box-body table-responsive">
          <table class="table">
            <thead>
              <tr>
                <th>Product</th>
                <th>SKU</th>
                <th>All Images</th>
              </tr>
            </thead>
            <tbody>
              @forelse ($sku as $key => $s)
                <tr>
                  <td width="13%">{{ $s->product->attribute('name') }}</td>
                  <td width="13%">{{ $s->sku }}</td>
                  <td width="74%">
                    <div class="row">
                      <?php $images = $s->images('local')->get() ?>
                      @for ($i=0; $i < 8 ; $i++)
                        @if (!empty($s->images[$i]))
                          <div class="col-md-1 col-xs-1 img-container" data-sku="{{ $s->id }}">
                            <img width="100px" class="image" src="{{ asset(Storage::url('sku/'.$images[$i]->filename)) }}">
                            <div class="middle">
                              <div class="img-control" data-img="{{ asset(Storage::url('sku/'.$images[$i]->filename)) }}" data-code="{{ $images[$i]->id }}">
                                <a class="viewImage" href="javascript:void(0)"><i class="ion-search"></i></a>
                                <a class="removeImage" href="javascript:void(0)"><i class="ion-trash-b"></i></a>
                              </div>
                            </div>
                          </div>
                        @else
                        <div class="browseImage col-md-1" data-sku="{{ $s->id }}">
                          <input type="file" name="image[{{ $s->id }}][{{ $i }}]" class="fileImage" accept="image/*" style="display: none">
                          <button type="button" name="button" class="btn btn-lg btn-trans btn-browse">
                            <i class="ion-plus"></i>
                          </button>
                          <p class="txt-browse">Select Image</p>
                        </div>
                        @endif
                      @endfor
                    </div>
                  </td>
                </tr>

              @empty
                <tr>
                  <td colspan="3">No Sku</td>
                </tr>
              @endforelse
            </tbody>
          </table>

        </div>
      </div>
      <div class="pull-right">
        {{ $sku->appends($filter)->links() }}
      </div>
    </div>
  </div>
  @include('admin.sku._cropModal')

  <div class="modal fade" id="viewImage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <img src="" class="imagePreview" style="width: 100%;" >
      </div>
    </div>
  </div>

  <div class="modal fade" id="removeImage" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-sm">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title">Remove this Image?</h4>
        </div>
        <div class="modal-body">
          <img src="" class="imagePreview" style="width: 100%;" >
          <input type="hidden" id="removeImg" value="">
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-danger btn-flat" onclick="removeImage()">Yes</button>
        </div>
      </div>
    </div>
  </div>

@endsection

@push('scripts')
<script src="{{ asset('/assets/plugins/croppie/croppie.min.js') }}"></script>
<script src="{{ asset('assets/plugins/waitme/waitMe.min.js') }}"></script>
<script type="text/javascript">

  $(".viewImage").on("click", function() {
     $('.imagePreview').attr('src', $(this).parent('.img-control').attr('data-img'));
     $('#viewImage').modal('show');
  });

  $('#viewImage').click(function() {
     $('#viewImage').modal('hide');
  });

  $(".removeImage").on("click", function() {
    $('.imagePreview').attr('src', $(this).parent('.img-control').attr('data-img'));
    $('#removeImg').val($(this).parent('.img-control').attr('data-code'));
    $('#removeImage').modal('show');
  });

  function removeImage()
  {
    var image = $('#removeImg').val();
    console.log(image);
    axios.delete('/app/sku/image/'+image)
    .then(function() {
      location.reload();
      // $('#removeImage').modal('hide');
    });
  }

  $('.btn-browse').click(function() {
    var input = $(this).closest('.browseImage').find('.fileImage');
    var sku = $(this).closest('.browseImage').attr('data-sku');
    $('#sku').val(sku);
    input.click();
  });

  $('.fileImage').change(function() {
    readFile(this);
    var target = $(this).closest('.browseImage');
    var fileImage = $(this).val();
    if (fileImage) {
      target.addClass('target image-container');
      $('#cropModal').modal({
        show: true,
        backdrop: false,
        keyboard: false
      });
    }
  });

  var basic = $('#main-cropper').croppie({
    viewport: { width: 500, height: 500 },
    boundary: { width: 530, height: 530 },
    showZoomer: true,
    // url: 'http://lorempixel.com/500/400/'
  });

  function readFile(input) {
    if (input.files && input.files[0]) {
      var reader = new FileReader();

      reader.onload = function (e) {
        basic.croppie('bind', {
          url: e.target.result
        });
      }

      reader.readAsDataURL(input.files[0]);
    }
  }

  $('#btn-result').on('click', function (ev) {
			basic.croppie('result', {
				type: 'canvas',
				size: 'viewport'
			}).then(function (resp) {
				processResult({
					src: resp
				});
			});

      $('#cropModal').modal('hide');
		});

    function processResult(result) {
      $('body').waitMe({effect:'rotation'});
      // console.log(result);
      var html;
  		if (result.html) {
  			html = result.html;
  		}
  		if (result.src) {
  			html = '<a href="#"><img class="img" src="' + result.src + '" width="100px" /></a>';
  		}
      var sku = $('#sku').val();
      axios.post('/app/sku/image', {
        sku: sku,
        image: result.src
      })
        .then(function(response) {
          $('.target').html(html);
          $('.target').removeClass('target');
          $('body').waitMe("hide");
        })
    }

</script>
@endpush
