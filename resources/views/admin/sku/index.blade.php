@extends('layouts.lte.main')

@section('content')
<div id="app">
<div class="row">
	<div class="col-md-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				@include('admin.sku._filter')
			</div>
			<div class="box-body table-responsive">
				<table class="table table-hover">
					<thead>
						<tr>
							<th>SKU</th>
							@if($filter['status'] != 'bundle')
							<th>Product Code</th>
							@endif
							<th>Product Name</th>
							<th>Active Qty</th>
							@if($filter['status'] != 'bundle')
							<th>Cold Qty</th>
							@endif
							<th>Retail Price</th>
							<th>Status</th>
							<th></th>
						</tr>
					</thead>
					<tbody>
					@forelse ($sku as $s)
						<?php
						if ($filter['status'] == 'bundle') {
							$product_name = $s->attribute('name');
						}else {
							$product_name = $s->product->attribute('name');
						}
							$class = '';
							if(empty($s->product->stock_active)){
								$class = 'empty';
							}elseif($s->product->stock_active <= $s->min_qty){
								$class = 'low-qty';
							}
						?>
						<tr class="{{ $class }}">
							<td>{{ $s->sku }}</td>
							@if($filter['status'] != 'bundle')
							<td>{{ $s->product->product_code }}</td>
							@endif
							<td>{{ strlen($product_name) > 40 ? substr($product_name, 0, 38).' . .' :  $product_name }}</td>
							<td>{{ $filter['status'] == 'bundle' ? $s->bundle_qty : $s->product->stock_active }}</td>
							@if($filter['status'] != 'bundle')
							<td>{{ $s->product->stock_cold }}</td>
							@endif
							<td><a style="cursor: pointer;" @click="showPriceModal('{{ $s->id }}')">{{ $s->retail_price or 'Not Set' }}</a></td>
							<td>
								<input data-sku="{{ $s->id }}" class="status" type="checkbox" data-toggle="toggle" data-style="isToggle" data-on="Active" data-off="Inactive" data-onstyle="success" {{ $s->attribute('Status') == 'active' ? 'checked' : ''}}>
							</td>
							<td align="right">
							@can('update-sku')
							<form action="{{ url('app/sku/'.$s->id) }}" method="POST" id="delete-{{ $s->id }}">
								{!! csrf_field().method_field("DELETE") !!}
								<div class="btn-group">
                  <button type="button" class="btn btn-flat btn-default" @click="showPriceModal('{{ $s->id }}')"><i class="fa fa-usd"></i> Set Price</button>
                  <button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                  </button>
                  <ul class="dropdown-menu pull-right" role="menu">
                    <li><a href="#" @click="showQtyeModal('{{ $s->id }}')"> <i class="fa fa-sort-numeric-asc"></i> Quantity Setting</a></li>
                    <li><a href="{{ url('app/sku/'.$s->id.'/edit') }}"> <i class="fa fa-edit"></i> Edit</a></li>
					<!--<li><a href="{{ url('app/sku/'.$s->id.'/clone') }}"> <i class="fa fa-copy"></i> Clone</a></li>-->
                    <li class="divider"></li>
                    <li><a href="#" onclick="confirmDelete('{{ $s->id }}','SKU')"> <i class="fa fa-trash"></i> Delete</a></li>
                </div>
							</form>
							@endcan
							</td>
						</tr>
					@empty
						<tr>
							<td colspan="8" align="center"><h1>SKU Not Found</h1></td>
						</tr>
					@endforelse
					</tbody>
				</table>
			</div>
		</div>
		<div class="pull-right">
			{{ $sku->appends($filter)->links() }}
		</div>
	</div>
</div>
@include('admin.sku._priceModal')
@include('admin.sku._qtyModal')
</div>
@stop

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datepicker/datepicker3.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/bootstrap-toggle/css/bootstrap-toggle.min.css') }}">
<style>
	.low-qty {
		background-color: #fff8a8;
	}
	.empty {
		background-color: #fde4e4;
	}
	.isToggle .toggle-group { transition: left 0.7s; -webkit-transition: left 0.7s; }
	.toggle.isToggle { border-radius: 0px;}
  .toggle.isToggle .toggle-handle { border-radius: 0px; }
</style>
@endpush

@push('scripts')
	<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
	<script src="{{ asset('assets/lte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
	<script src="{{ asset('assets/plugins/bootstrap-toggle/js/bootstrap-toggle.min.js') }}"></script>
	<script src="https://unpkg.com/vue-select@latest"></script>
	<script src="{{ elixir('js/sku.js') }}"></script>
	<script type="text/javascript">
	$('.status').change(function() {
		var status;
		var checked;
		if ($(this).prop('checked')) {
			status = 'active';
			checked = true;
		}else{
			status = 'inactive';
			checked = false;
		}
		var sku = $(this).data('sku');

		axios.post('/app/sku/status', {sku:sku,status:status})
		.then(function(response) {
			var resp = response.data;
			if (resp.message === 'error') {
				$(this).prop('checked', !checked).change();
			}
		})
		.catch(function(error) {
			$(this).prop('checked', !checked).change();
		});
	})
	</script>
@endpush
