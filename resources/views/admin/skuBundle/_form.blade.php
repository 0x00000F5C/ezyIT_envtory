<form role="form" class="form-horizontal" id="product-form" method="POST" action="{{ $model->exists && @$model->product_code != "" ? url('/app/products/' . $model->id) : url('/app/products') }}">
	{{ csrf_field() }}
	@if($model->exists && @$model->product_code != "")
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="product-code" class="col-md-3 control-label text-left">Product code<span class="red"> *</span></label>
				<div class="col-md-9">
					<input type="text" name="product-code" class="form-control" id="product-code" placeholder="Product code" value="{{ $model->exists || @$model->product_code != "" ? $model->product_code : "" }}" maxlength="16" required="required">
					@if ($errors->has("product-code"))
					<span class="help-block">
						<strong>{{ $errors->first("product-code") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="product-name" class="col-md-3 control-label text-left">Product name<span class="red"> *</span></label>
				<div class="col-md-9">
					<input type="text" name="product-name" class="form-control" id="product-name" placeholder="Product name" value="{{ $model->exists || @$model->product_code != "" ? $model->product_name : "" }}" maxlength="255" required="required">
					@if ($errors->has("product-name"))
					<span class="help-block">
						<strong>{{ $errors->first("product-name") }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<label for="product-brand" class="col-md-2 control-label text-left">Brand</label>
				<div class="col-md-10">
					<input type="text" name="product-brand" class="form-control" id="product-brand" placeholder="Product brand" value="{{ $model->exists ? $model->product_brand : "" }}" maxlength="255" required="required">
					@if ($errors->has("product-brand"))
					<span class="help-block">
						<strong>{{ $errors->first("product-brand") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="product-model" class="col-md-3 control-label text-left">Model</label>
				<div class="col-md-9">
					<input type="text" name="product-model" class="form-control" id="product-model" placeholder="Product model" value="{{ $model->exists ? $model->product_model : "" }}" maxlength="255" required="required">
					@if ($errors->has("product-model"))
					<span class="help-block">
						<strong>{{ $errors->first("product-model") }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<label for="product-submodel" class="col-md-2 control-label text-left">Sub-model</label>
				<div class="col-md-10">
					<input type="text" name="product-submodel" class="form-control" id="product-submodel" placeholder="Product sub-model" value="{{ $model->exists ? $model->product_submodel : "" }}" maxlength="255" required="required">
					@if ($errors->has("product-submodel"))
					<span class="help-block">
						<strong>{{ $errors->first("product-submodel") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="product-size" class="col-md-3 control-label text-left">Size</label>
				<div class="col-md-9">
					<input type="text" name="product-size" class="form-control" id="product-size" placeholder="Product size" value="{{ $model->exists ? $model->product_size : "" }}" maxlength="255" required="required">
					@if ($errors->has("product-size"))
					<span class="help-block">
						<strong>{{ $errors->first("product-size") }}</strong>
					</span>
					@endif
				</div>
			</div>
			<div class="col-md-6">
				<label for="product-color" class="col-md-2 control-label text-left">Color</label>
				<div class="col-md-10">
					<input type="text" name="product-color" class="form-control" id="product-color" placeholder="Product color" value="{{ $model->exists ? $model->product_color : "" }}" maxlength="255" required="required">
					@if ($errors->has("product-color"))
					<span class="help-block">
						<strong>{{ $errors->first("product-color") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<!-- <div class="form-group has-feedback">
			<label for="product-category" class="col-md-4 control-label text-left">Product category</label>
			<div class="col-md-7">
				<select name="product-category" class="form-control" id="product-category">
					<option value="" selected hidden disabled>Please select a category</option>
					@foreach($categories as $category)
						@if($category->cat_parent == "0")
							<optgroup label="{{ $category->cat_name }}" value="{{ $category->id }}">
								@foreach($categories as $child)
									@if($child->cat_parent != "0" && $child->cat_parent == $category->id)
										<option value="{{ $child->id }}"@if($model->exists && $child->id == $model->cat_id) selected @endif>{{ $child->cat_name }}</option>
									@endif
								@endforeach
							</optgroup>
						@endif
					@endforeach
				</select>
			</div>
			<div class="col-sm-1">
				<button class="btn btn-default" onclick="addNewCategory()" type="button"><i class="fa fa-plus"></i></button>
			</div>
		</div> -->
		<div class="form-group has-feedback">
			<div class="col-md-6">
				<label for="product-category" class="col-md-3 control-label text-left">Product category<span class="red"> *</span></label>
				<div class="col-md-9">
					<select name="product-category" class="form-control" id="product-category">
						<option value="" selected hidden disabled>Please select a category</option>
						@foreach($categories as $category)
							@if($category->cat_parent == "0")
								<option value="{{ $category->id }}"{{ $model->exists && $model->product_category == $category->id ? " selected" : "" }}>{{ $category->cat_name }}</option>
							@endif
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-6">
				<label for="product-subcategory" class="col-md-2 control-label text-left">Product sub-category<span class="red"> *</span></label>
				<div class="col-md-8">
					<select name="product-subcategory" class="form-control" id="product-subcategory">
						<option value=""{{ !$model->exists ? " selected" : "" }} hidden disabled>Please select a category</option>
						@if($model->exists)
							@foreach($categories as $category)
								@if($category->cat_parent != "0" && $category->cat_parent == $model->product_category)
									<option value="{{ $category->id }}"{{ $model->exists && $model->product_category == $category->id ? " selected" : "" }}>{{ $category->cat_name }}</option>
								@endif
							@endforeach
						@endif
					</select>
				</div>
				<div class="col-sm-1">
					<button class="btn btn-default" onclick="addNewCategory()" type="button"><i class="fa fa-plus"></i></button>
				</div>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-6" style="margin-left: auto; margin-right: auto; float: none; display: table;">
				<div class="" style="display: table-cell; width: 45%;">
					<label for="supplier-list">Available supplier</label>
					<select name="supplier-list" id="supplier-list" style="display: block; width: 100%; min-height: 256px;" multiple="multiple">
						<option value="" disabled>Please select a subcategory</option>
						@if($model->exists)
							@foreach($suppliers as $supplier)
								@if($model->exists && in_array($supplier->id, $model->product_suppliers))
									<option value="{{ $supplier->id }}"{{ in_array($supplier->id, $model->product_suppliers) ? " hidden disabled" : "" }}>{{ $supplier->supplier_name }}</option>
								@endif
							@endforeach
						@endif
					</select>
				</div>
				<div class="" style="display: table-cell; width: 10%; vertical-align: middle; text-align: center;">
					<input type="button" id="btn_addCategories" value=" > " /><br /><br />
					<input type="button" id="btn_remCategories" value=" < " />
				</div>
				<div class="" style="display: table-cell; width: 45%;">
					<label for="product-suppliers">Product suppliers<span class="red"> *</span></label>
					<select name="product-suppliers[]" id="product-suppliers" style="display: block; width: 100%; min-height: 256px;" multiple="multiple">
						@if($model->exists)
							@foreach($suppliers as $supplier)
								@if($model->exists && in_array($supplier->id, $model->product_suppliers))
									<option value="{{ $supplier->id }}">{{ $supplier->supplier_name }}</option>
								@endif
							@endforeach
						@endif
					</select>
					@if ($errors->has("product-suppliers"))
					<span class="help-block">
						<strong>{{ $errors->first("product-suppliers") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/products") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> </button>
			</div>
		</div>
	</div>
</form>