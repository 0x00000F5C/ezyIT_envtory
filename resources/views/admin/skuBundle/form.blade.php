@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.product._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function () {
		$('input').iCheck({
			checkboxClass: 'icheckbox_square-blue',
			radioClass: 'iradio_square-blue',
			increaseArea: '20%' // optional
		});
	});

	$("#product-category").on("change", function() {
		$.ajax({
			type: "post",
			url: "{!! route("productCategories.getCategories") !!}",
			data: { "id": $(this).val() },
			dataType: "json",
			success: function(data) {
				$("#product-subcategory").empty();
				$("#product-subcategory").append($("<option></option>").attr("value", "").attr("hidden", "hidden").attr("disabled", "disabled").attr("selected", "selected").text("Please select a " + (Object.keys(data.data).length > 0 ? "sub" : "") + "category"));
				$.each(data.data, function(keyA, category) {
					$("#product-subcategory").append($("<option></option>").attr("value", category.id).text(category.cat_name));
				});
			},
			error: function(data) {
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});

	$("#product-subcategory").on("change", function() {
		$.ajax({
			type: "post",
			url: "{!! route("supplier.getSuppliers") !!}",
			data: { "val": $(this).val() },
			dataType: "json",
			success: function(data) {
				$("#supplier-list").empty();
				$("#product-suppliers").empty();
				if(Object.keys(data.data).length != 0) {
					$.each(data.data, function(keyA, supplier) {
						$("#supplier-list").append($("<option></option>").attr("value", supplier.id).text(supplier.supplier_name));
						$("#product-suppliers").append($("<option></option>").attr("value", supplier.id).text(supplier.supplier_name).prop("hidden", true).prop("disabled", true));
					});
				} else {
					$("#supplier-list").append($("<option></option>").attr("value", "").attr("disabled", "disabled").text("There are currently no suppliers for"));
					$("#supplier-list").append($("<option></option>").attr("value", "").attr("disabled", "disabled").text("this category."));
					$("#supplier-list").append($("<option></option>").attr("value", "").attr("disabled", "disabled").text("Please select another subcategory."));
				}
			},
			error: function(data) {
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});

	function addNewCategory(){
		var d = {};
		var opts = { "0": "_MAIN" };
		/* $("#product-category > optgroup").each(function(i) {
			opts[$(this).attr("value")] = $(this).attr("label");
		}); */
		$("#product-category > option").each(function(i) {
			if ($(this).attr("value") != "") {
				opts[$(this).attr("value")] = $(this).text();
			}
		});

		swal({
			title: "Select parent category",
			input: "select",
			inputOptions: opts,
			showCancelButton: true
		}).then(function (value) {
			d.parent = value;
			swal({
				title: "Type new category name",
				input: "text",
				inputPlaceholder: "New category name",
				showCancelButton: true,
				inputValidator: function (value) {
					return new Promise(function (resolve, reject) {
						if (value != "") {
							resolve();
						} else {
							reject("Please type the new category name!")
						}
					});
				}
			}).then(function (value) {
				d.category = value;

				$.ajax({
					type: "post",
					url: "{!! route("productCategories.storeCategory") !!}",
					data: d,
					dataType: "json",
					success: function(data) {
						swal("Nice!", data.message, "success");
						$("#product-category").empty();
						$("#product-category").append($("<option></option>").attr("value", "").attr("hidden", "hidden").attr("disabled", "disabled").prop("selected", true).text("Please select a category"));
						$("#product-subcategory").empty();
						$("#product-subcategory").append($("<option></option>").attr("value", "").attr("hidden", "hidden").attr("disabled", "disabled").prop("selected", true).text("Please select a category"));
						$.each(data.data, function(keyA, category) {
							if(category.cat_parent == "0") {
								$("#product-category").append($("<option></option>").attr("value", category.id).text(category.cat_name));
							}
						});
					},
					error: function(data){
						var errors = data.responseJSON;
						swal.showInputError(errors.name);
					}
				});
			});
		});
	}

	var addCatCallback = function() {
		var source = $("#supplier-list");
		var target = $("#product-suppliers");
		$.each(source.find("option:selected").map(function(){ return this.value; }).get(), function(key, val) {
			source.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			target.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", false).prop("disabled", false);
		});
	};
	
	var remCatCallback = function() {
		var source = $("#product-suppliers");
		var target = $("#supplier-list");
		$.each(source.find("option:selected").map(function(){ return this.value; }).get(), function(key, val) {
			source.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			target.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", false).prop("disabled", false);
		});
	};
	
	$("#btn_addCategories").on("click", addCatCallback);
  
	$("#btn_remCategories").on("click", remCatCallback);
	
	$("#supplier-list > option").on("dblclick", addCatCallback);
  
	$("#product-suppliers > option").on("dblclick", remCatCallback); // event is not delegated
	
	$("#product-form").submit(function(event){
		$("#product-suppliers option").prop("selected", true);
		return true;
	});
</script>
@endpush