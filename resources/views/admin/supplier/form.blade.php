@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.supplier._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function () {
		$('input').iCheck({
		  checkboxClass: 'icheckbox_square-blue',
		  radioClass: 'iradio_square-blue',
		  increaseArea: '20%' // optional
		});
		$.ajax({
			type: "get",
			url: "http://api.geonames.org/countryInfoJSON?username=anssen",
			dataType: "jsonp",
			success: function(data) {
				$("#company-country").empty();
				var editCountry = $("#placeholder-country").val();
				$.each(data.geonames, function(key, val){
					$("#company-country").append($('<option value="' + val.countryName + '" data-id="' + val.geonameId + '" ' + (val.countryName == editCountry ? "selected" : "") + '>' + val.countryName + '</option>'));
				});
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});

	var addCatCallback = function() {
		var source = $("#category-list");
		var target = $("#supplier-categories");
		$.each(source.find("option:selected").map(function(){ return this.value; }).get(), function(key, val) {
			source.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			target.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", false).prop("disabled", false);
		});
		source.find("optgroup").each(function() {
			if($(this).children("option:disabled").length == $(this).children("option").length) {
				$(this).prop("hidden", true).prop("disabled", true);
			} else {
				$(this).prop("hidden", false).prop("disabled", false);
			}
		});
		target.find("optgroup").each(function() {
			if($(this).children("option:disabled").length == $(this).children("option").length) {
				$(this).prop("hidden", true).prop("disabled", true);
			} else {
				$(this).prop("hidden", false).prop("disabled", false);
			}
		});
	};
	
	var remCatCallback = function() {
		var source = $("#supplier-categories");
		var target = $("#category-list");
		$.each(source.find("option:selected").map(function(){ return this.value; }).get(), function(key, val) {
			source.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			target.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", false).prop("disabled", false);
		});
		source.find("optgroup").each(function() {
			if($(this).children("option:disabled").length == $(this).children("option").length) {
				$(this).prop("hidden", true).prop("disabled", true);
			} else {
				$(this).prop("hidden", false).prop("disabled", false);
			}
		});
		target.find("optgroup").each(function() {
			if($(this).children("option:disabled").length == $(this).children("option").length) {
				$(this).prop("hidden", true).prop("disabled", true);
			} else {
				$(this).prop("hidden", false).prop("disabled", false);
			}
		});
	};
	
	$("#btn_addCategories").on("click", addCatCallback);
  
	$("#btn_remCategories").on("click", remCatCallback);
	
	$("#category-list option").on("dblclick", addCatCallback);
  
	$("#supplier-categories option").on("dblclick", remCatCallback);
	
	$("#supplier-form").submit(function(event){
		$("#supplier-categories option").prop("selected", true);
		return true;
	});

	$("#supplier-categories").find("optgroup").each(function() {
		if($(this).children("option:disabled").length == $(this).children("option").length) {
			$(this).prop("hidden", true).prop("disabled", true);
		} else {
			$(this).prop("hidden", false).prop("disabled", false);
		}
	});
	
	$("#btn-addContact").on("click", function(){
		$("tr.supplier-contacts:last").clone().attr("id", "sc-row-" + ($("table#supplier-contacts tbody tr").length + 1)).appendTo("table#supplier-contacts tbody");
		$("table#supplier-contacts tbody tr.supplier-contacts:last input").val("");
		if($("table#supplier-contacts tbody tr").length === 2) {
			$("table#supplier-contacts tbody tr.supplier-contacts:last td:last").append('<button type="button" class="btn btn-xs btn-danger btn-flat" onclick="removeContactRow(' + $("table#supplier-contacts tbody tr").length + ')"><i class="fa fa-times"></i></button>');
		}
	});

	$("#categorylist-search").on("input", function() {
		var q = $(this).val();
		var me = $("#category-list");
		var it = $("#supplier-categories");
		if(q !== "") {
			me.find("option").prop("hidden", true).prop("disabled", true);
			var pat = new RegExp(q, "i");
			me.find("option").filter(function() {
				return $(this).text().match(pat);
			}).prop("hidden", false).prop("disabled", false);
			$.each(it.find(":not(option:disabled)").map(function(){ return this.value; }).get(), function(key, val) {
				me.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			});
			me.find("optgroup").each(function() {
				if($(this).children("option:disabled").length === $(this).children("option").length) {
					$(this).prop("hidden", true).prop("disabled", true);
				} else {
					$(this).prop("hidden", false).prop("disabled", false);
				}
			});
		} else {
			me.find("option").prop("hidden", false).prop("disabled", false).parent().prop("hidden", false).prop("disabled", false);
			$.each(it.find(":not(option:disabled)").map(function(){ return this.value; }).get(), function(key, val) {
				me.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			});
			me.find("optgroup").each(function() {
				if($(this).children("option:disabled").length === $(this).children("option").length) {
					$(this).prop("hidden", true).prop("disabled", true);
				} else {
					$(this).prop("hidden", false).prop("disabled", false);
				}
			});
		}
	});

	$("#suppliercat-search").on("input", function() {
		var q = $(this).val();
		var me = $("#supplier-categories");
		var it = $("#category-list");
		if(q !== "") {
			me.find("option").prop("hidden", true).prop("disabled", true);
			var pat = new RegExp(q, "i");
			me.find("option").filter(function() {
				return $(this).text().match(pat);
			}).prop("hidden", false).prop("disabled", false);
			$.each(it.find(":not(option:disabled)").map(function(){ return this.value; }).get(), function(key, val) {
				me.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			});
			me.find("optgroup").each(function() {
				if($(this).children("option:disabled").length === $(this).children("option").length) {
					$(this).prop("hidden", true).prop("disabled", true);
				} else {
					$(this).prop("hidden", false).prop("disabled", false);
				}
			});
		} else {
			me.find("option").prop("hidden", false).prop("disabled", false).parent().prop("hidden", false).prop("disabled", false);
			$.each(it.find(":not(option:disabled)").map(function(){ return this.value; }).get(), function(key, val) {
				me.find("option").filter(function() { return $(this).val() == val;}).prop("hidden", true).prop("disabled", true);
			});
			me.find("optgroup").each(function() {
				if($(this).children("option:disabled").length === $(this).children("option").length) {
					$(this).prop("hidden", true).prop("disabled", true);
				} else {
					$(this).prop("hidden", false).prop("disabled", false);
				}
			});
		}
	});
	
	$("#company-name").on("change", function() {
		if($(this).val() !== "") {
			$.ajax({
				type: "post",
				url: "{!! route("supplier.genPrefix") !!}",
				data: { q: $(this).val() },
				dataType: "json",
				success: function(data) {
					$("#company-prefix").val(data.data);
				},
				error: function(data){
					var errors = data.responseJSON;
					swal.showInputError(errors.name);
				}
			});
		}
	});
	
	$("#company-name").on("blur", function() {
		$("#company-address").focus();
	});
	
	$("#company-country").on("change", function(){
		var q = $("#company-country > option:selected").data("id");
		$.ajax({
			type: "get",
			url: "http://api.geonames.org/childrenJSON?username=anssen",
			dataType: "jsonp",
			data: {"geonameId": q},
			success: function(data) {
				$("#company-state").empty();
				$("#company-city").empty();
				$.each(data.geonames, function(key, val){
					$("#company-state").append($('<option value="' + val.name + '" data-id="' + val.geonameId + '">' + val.name + '</option>'));
				});
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
	
	$("#company-state").on("change", function(){
		var q = $("#company-state > option:selected").data("id");
		$.ajax({
			type: "get",
			url: "http://api.geonames.org/childrenJSON?username=anssen",
			dataType: "jsonp",
			data: {"geonameId": q},
			success: function(data) {
				$("#company-city").empty();
				$.each(data.geonames, function(key, val){
					$("#company-city").append($('<option value="' + val.name + '" data-id="' + val.geonameId + '">' + val.name + '</option>'));
				});
			},
			error: function(data){
				var errors = data.responseJSON;
				swal.showInputError(errors.name);
			}
		});
	});
	
	$("#company-zipcode").on("input", function() {
		$(this).val($(this).val().replace(/^0*/i, ""));
	});
	
	$("#company-minpo").on("input", function() {
		$(this).val($(this).val().replace(/^0*/i, ""));
		$(this).attr("max", parseInt($("#company-maxpo").val()) - 1);
		$("#company-maxpo").attr("min", parseInt($(this).val()) + 1);
	});
	
	$("#company-maxpo").on("input", function() {
		$(this).val($(this).val().replace(/^0*/i, ""));
		$(this).attr("min", parseInt($("#company-minpo").val()) + 1);
		$("#company-minpo").attr("max", parseInt($(this).val()) - 1);
	});

	function removeContactRow(row) {
		swal({
			title: "Remove contact?",
			type: "warning",
			showCancelButton: true,
			confirmButtonColor: "#DD6B55",
			confirmButtonText: "Remove!",
			closeOnConfirm: true
		},
		function(){
			$("#sc-row-" + row).remove();
		});
	}
</script>
@endpush