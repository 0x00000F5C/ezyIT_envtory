<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/user/'.$model->id) : url('/app/user') }}">
  {{ csrf_field() }}
  @if($model->exists){{ method_field('PUT') }}@endif
  <div class="box-body">
    <div class="form-group has-feedback{{ $errors->has('name') ? ' has-error' : '' }}">
      <label for="name" class="col-sm-2 control-label">Username <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="text" name="name" class="form-control" id="nama" placeholder="Username" value="{{old('name') ? old('name') : $model->name}}"{{ $model->exists ? " readonly" : "" }}>
        @if ($errors->has('name'))
            <span class="help-block">
                <strong>{{ $errors->first('name') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('firstname') ? ' has-error' : '' }}">
      <label for="firstname" class="col-sm-2 control-label">First Name <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="text" name="firstname" class="form-control" id="nama" placeholder="First Name" value="{{old('firstname') ? old('firstname') : $model->firstname}}">
        @if ($errors->has('firstname'))
            <span class="help-block">
                <strong>{{ $errors->first('firstname') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('lastname') ? ' has-error' : '' }}">
      <label for="lastname" class="col-sm-2 control-label">Last Name</label>
      <div class="col-sm-4">
        <input type="text" name="lastname" class="form-control" id="nama" placeholder="Last Name" value="{{old('lastname') ? old('lastname') : $model->lastname}}">
        @if ($errors->has('lastname'))
            <span class="help-block">
                <strong>{{ $errors->first('lastname') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
      <label for="email" class="col-sm-2 control-label">Email <span class="red">*</span></label>
      <div class="col-sm-4">
        <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{old('email') ? old('email') : $model->email}}">
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('role') ? ' has-error' : '' }}">
      <label for="role" class="col-sm-2 control-label">Role <span class="red">*</span></label>
      <div class="col-sm-4">
        <select class="form-control" name="role">
          <option value="">Select Role</option>
          @foreach($roles as $role)
          <?php $showRole = false;
          if ($role->hasPermissionTo('create-role') && Auth::user()->hasPermissionTo('create-role')){
            $showRole = true;
          }elseif(!$role->hasPermissionTo('create-role')){
            $showRole = true;
          } ?>
          @if ($showRole)
            <option value="{{$role->name}}" {{$model->hasRole($role->name) || old('role') == $role->name ? 'selected' : ''}}>{{$role->name}}</option>
          @endif
          @endforeach
        </select>
        @if ($errors->has('role'))
            <span class="help-block">
                <strong>{{ $errors->first('role') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
      <label for="password" class="col-sm-2 control-label">
        @if($model->exists)Change Password
        @else Password <span class="red">*</span>
        @endif
      </label>
      <div class="col-sm-4">
        <input type="password" name="password" class="form-control" id="password" placeholder="{{$model->exists ? 'New' : ''}} Password" value="{{old('password')}}">
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
      <div class="col-sm-4">
        <input type="password" name="password_confirmation" class="form-control" id="password_confirmation" placeholder="Retype Password">
        @if ($errors->has('password_confirmation'))
            <span class="help-block">
                <strong>{{ $errors->first('password_confirmation') }}</strong>
            </span>
        @endif
      </div>
    </div>
    <div class="form-group has-feedback{{ $errors->has('active') ? ' has-error' : '' }}">
      <div class="col-sm-4 col-md-offset-2">
        <div class="checkbox icheck">
            <label>
              <input type="checkbox" name="active" {{ $model->active == 1 ? 'checked' : ''}} value="1">
              Active
            </label>
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="box-footer">
    <div class="form-group">
      <label class="col-sm-2 control-label"></label>
      <div class="col-sm-4">
        <a href="{{url('app/user')}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
        <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? 'Update' : 'Create User'}}</button>
      </div>
    </div>
  </div>
</form>
