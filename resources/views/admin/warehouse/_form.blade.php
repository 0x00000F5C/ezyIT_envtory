{{ csrf_field() }}
<div class="box-body">
    <div class="row">

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-8">
                    <div class="form-group {{ $errors->has('name')   ? ' has-error' : '' }}">
                        <label >Warehouse Name</label>
						<input type="text" id="warehouse-name" class="form-control" name="name" placeholder="Warehouse name" value="{{ old("name") ? old("name") : $warehouse->name }}" required="required" {{ $warehouse->exists ? 'readonly="readonly"' : ""}}>
                        @include('layouts.lte._help_block', ['helpParameter' => 'name'])
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('prefix')   ? ' has-error' : '' }}">
                        <label >Prefix</label>
						<input type="text" id="warehouse-prefix" class="form-control" name="prefix" placeholder="Prefix" value="{{ old("prefix") ? old("prefix") : $warehouse->prefix }}" required="required" {{ $warehouse->exists ? 'readonly="readonly"' : ""}}>
                        @include('layouts.lte._help_block', ['helpParameter' => 'prefix'])
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('address')  ? ' has-error' : '' }}">
                        <label >Address</label>
						<textarea class="form-control" name="address" placeholder="Warehouse address" required="required" rows="9">{{ old("address") ? old("address") : $warehouse->address }}</textarea>
                        @include('layouts.lte._help_block', ['helpParameter' => 'address'])
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group {{ $errors->has('contact.0.pic')  ? ' has-error' : '' }}">
                        <label >Contact Person</label>
						<input type="text" class="warehouse-contacts form-control" name="contact[0][pic]" placeholder="Contact person" value="{{ old("contact.0.pic") ? old("contact.0.pic") : @$contacts[0]->pic }}" list="contact-options">
						<datalist id="contact-options"></datalist>
                        @include('layouts.lte._help_block', ['helpParameter' => 'contact.0.pic'])
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('contact.0.phone')   ? ' has-error' : '' }}">
                        <label >Phone</label>
						<input type="text" class="warehouse-phone form-control" name="contact[0][phone]" placeholder="Phone number" value="{{ old("contact.0.phone") ? old("contact.0.phone") : @$contacts[0]->phone }}">
                        @include('layouts.lte._help_block', ['helpParameter' => 'contact.0.phone'])
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('contact.0.mobile')  ? ' has-error' : '' }}">
                        <label >Mobile</label>
						<input type="text" class="warehouse-mobile form-control" name="contact[0][mobile]" placeholder="Mobile number" value="{{ old("contact.0.mobile") ? old("contact.0.mobile") : @$contacts[0]->mobile }}">
                        @include('layouts.lte._help_block', ['helpParameter' => 'contact.0.mobile'])
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('contact.0.email') ? ' has-error' : '' }}">
                        <label >Email</label>
						<input type="email" class="warehouse-email form-control" name="contact[0][email]" placeholder="Email address" value="{{ old("contact.0.email") ? old("contact.0.email") : @$contacts[0]->email }}">
                        @include('layouts.lte._help_block', ['helpParameter' => 'contact.0.email'])
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="form-group">
                        <label >Contact Person / PIC</label>
						<input type="text" class="warehouse-contacts form-control" name="contact[1][pic]" placeholder="Phone number" value="{{ old("contact.1.phone") ? old("contact.1.phone") : @$contacts[1]->phone }}" list="contact-options">
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label >Phone</label>
						<input type="text" class="warehouse-phone form-control" name="contact[1][phone]" placeholder="Phone number" value="{{ old("contact.1.phone") ? old("contact.1.phone") : @$contacts[1]->phone }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label >Mobile</label>
						<input type="text" class="warehouse-mobile form-control" name="contact[1][mobile]" placeholder="Mobile number" value="{{ old("contact.1.mobile") ? old("contact.1.mobile") : @$contacts[1]->mobile }}">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label >Email</label>
						<input type="email" class="warehouse-email form-control" name="contact[1][email]" placeholder="Email address" value="{{ old("contact.1.email") ? old("contact.1.email") : @$contacts[1]->email }}">
                    </div>
                </div>
            </div>
        </div>

    </div>

</div>
<!-- /.box-body -->

<div class="box-footer">
    {{--<button type="submit" class="btn btn-primary">Submit</button>--}}
    <div class="form-group">
        <label class="col-md-2 control-label"></label>
        <div class="col-md-4">
            <a href="{{route('warehouse.index')}}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
            <button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> Save</button>
        </div>
    </div>
</div>
@push('scripts')
<script>
	$("input#warehouse-name").on("change", function() {
		var dis = $(this);
		$.ajax({
			method: "post",
			url: "{{ route("warehouse.genPrefix") }}",
			dataType: "json",
			headers: { "X-CSRF-TOKEN": $('input[name="_token"]').val() },
			data: { q: dis.val() },
			success: function(data) {
				$("input#warehouse-prefix").val(data.prefix);
			}
		});
	});

	$("input.warehouse-contacts").on("input", function() {
		var dis = $(this);
		var idx = $(".warehouse-contacts").index(this);
		$.ajax({
			method: "post",
			url: "{{ route("warehouse.getContact") }}",
			dataType: "json",
			headers: { "X-CSRF-TOKEN": $('input[name="_token"]').val() },
			data: { n: dis.val() },
			success: function(data) {
				$("datalist#contact-options").empty();
				var test = [];
				$.each(data.users, function(key, val) {
					$("datalist#contact-options").append($('<option value="' + (val.firstname ? val.firstname : "") + " " + (val.lastname ? val.lastname : "") + '">'));
					test.push((val.firstname ? val.firstname : "") + " " + (val.lastname ? val.lastname : ""));
				});
				if(data.users.length === 1 && dis.val() == test[0]) {
					$(".warehouse-contacts").eq(idx).val((data.users[0].firstname ? data.users[0].firstname : "") + " " + (data.users[0].lastname ? data.users[0].lastname : ""));
					$(".warehouse-phone").eq(idx).val(data.users[0].phone ? data.users[0].phone : "");
					$(".warehouse-mobile").eq(idx).val(data.users[0].mobile ? data.users[0].mobile : "");
					$(".warehouse-email").eq(idx).val(data.users[0].email ? data.users[0].email : "");
				}
			}
		});
	});
</script>
@endpush