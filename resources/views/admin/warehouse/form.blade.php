@extends('layouts.lte.main')
@section('content')
	<div class="row">
		<!-- left column -->
		<div class="col-md-12">
			<!-- general form elements -->
			<div class="box box-primary">
				<div class="box-header with-border">
					<h3 class="box-title">Warehouse Information</h3>
				</div>
				<!-- /.box-header -->
				<!-- form start -->
				@if($warehouse->exists)
					{!! Form::model($warehouse, array('method'=>'PUT', 'route' => ['warehouse.update', $warehouse->id], 'role' =>'form')) !!}
				@else
					{!! Form::open(array('route' => 'warehouse.store', 'method'=>'POST', 'role' =>'form' )) !!}
				@endif
					@include('admin.warehouse._form');
				{!! Form::close() !!}
			</div>
			<!-- /.box -->
		</div>
	</div>
@stop