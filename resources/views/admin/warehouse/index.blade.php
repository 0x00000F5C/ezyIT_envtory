@extends('layouts.lte.main')
@section('content')
    <div class="row" id="warehouse-container">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Warehouses List</h3>
                </div>
                <div class="box-body">
                    @foreach($warehouses as $warehouse)
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="info-box" style="border: 1px solid gray;">
                            <span class="box_title"><small class="label label-default">{{$warehouse->name}}</small></span>
                            <span style="background-color: white !important; padding-top: 10px;" class="info-box-icon "><i class="fa fa-home"></i></span>

                            <div class="info-box-content ">
                                <br>
                                <a href="{{route("warehouse.edit", $warehouse->id)}}" class="btn btn-flat btn-block btn-primary btn-xs">Information</a>
                                <a href="{{route("warehouse.mapping.index", $warehouse->id)}}" class="btn btn-flat btn-block btn-primary btn-xs">Mapping</a>
                                <a href="{{route("warehouse.trays.index", $warehouse->id)}}" class="btn btn-flat btn-block btn-primary btn-xs">Tray</a>
                            </div>
                            <!-- /.info-box-content -->
                        </div>
                        <!-- /.info-box -->
                    </div>
                    @endforeach
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a class="btn btn-primary btn-lg btn-app" href="{{route('warehouse.create')}}">
                            <i class="fa fa-plus"></i> Add
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop