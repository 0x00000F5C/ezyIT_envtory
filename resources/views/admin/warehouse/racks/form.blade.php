@extends('layouts.lte.main')

@section('content')
<div class="row">
  <div class="col-md-12">
    <div class="box box-primary">
      @include('admin.warehouse.racks._form')
    </div>
  </div>
</div>
@stop

@push('styles')
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/iCheck/square/blue.css') }}">
<style>
  .red {
    color: #dd4b39;
  }
  .categoryContainer {
	width: 100%;
	background-color: #f4f4f4;
	border: 1px solid #ddd;
	height: 302px;
	overflow-x:auto;
	overflow-y:hidden;
	white-space:nowrap;
	}
	.categoryContainer [class*="col-lg"], 
	.categoryContainer [class*="col-md"], 
	.categoryContainer [class*="col-sm"] {
	    float:none;
	    display:inline-block;
	}
	.selectCategories {
		height: 300px;
		overflow: auto;
		background-color: #fff;
		padding-top: 15px;
		padding-bottom: 15px;
	}
	li {
		cursor: pointer;
	}
	.child {
		display: none
	}
	.nav-stacked>li.active>a,
	.nav-stacked>li.active>a:hover {
		background-color: #f7f7f7;
		border-left-color: transparent;
	}
	.nav-stacked>li.selected>a {
		background-color: #fff;
		border-left-color: #d2d6de;	
	}
	.nav-stacked>li.selected>a:hover {
		background-color: #f7f7f7;
		border-left-color: #d2d6de;
	}
</style>
@endpush

@push('scripts')
<script src="{{ asset('assets/lte/plugins/iCheck/icheck.min.js') }}"></script>
<script>	
	$("#btn-generateLocation").on("click", function() {
		if($("#rack-cellcolumns").val() != 0 && $("#rack-cellrows").val() != 0) {
			$("#generated-locations").empty();
			/* $.ajax({
				type: "post",
				url: "{!! url('app/warehouse/' . $warehouse->id . "/mapping/" . $zone->id . "racks/genLocations") !!}/",
				dataType: "json",
				data: { "rack": {!! $model->exists ? $model->id : '""' !!}, "prefix": $("#rack-id").val(), "row": $("#rack-cellrows").val(), "col": $("#rack-cellcolumns").val() },
				success: function(data) {
					//
				}
			}); */
			var iii = 1;
			for(var i = 0; i < $("#rack-cellrows").val(); i++) {
				for(var ii = 0; ii < $("#rack-cellcolumns").val(); ii++) {
					$("#generated-locations").append($('<tr><td>' + (iii++) + '</td><td><input name="cells[]" value="' + $("#rack-id").val() + ("00".substring(0, "00".length - i.toString().length) + (i + 1)) + ("00".substring(0, "00".length - ii.toString().length) + (ii + 1)) + '" style="border: none; width: 100%; background-color: transparent;" readonly="readonly"></td><td style="text-align: center;"><select class="form-control" name="cellStatus[]"><option value="0">Empty</option><option value="1">Reserved</option></select></td><td style="text-align: right;"><input type="checkbox" id="cell-' + iii + '" class="checkCells" name="checkCells[]" /></td></tr>'));
				}
			}
			$("#locs").removeClass("hidden");
		}
	});
	
	$("#checkCellHeader").on("change", function() {
		if($(this).is(":checked")) {
			$(".checkCells").prop("checked", true);
		} else {
			$(".checkCells").prop("checked", false);
		}
	});
	
	$("#btn-printBarcodes").on("click", function() {
		if($(".checkCells:checked").length > 0) {
			$("form#getPDF").children(":not(input[name=_token])").remove();
			$(".checkCells:checked").each(function(i) {
				$("form#getPDF").append('<input type="hidden" name="cells[]" value="' + $(this).parents("tr").find("td:nth-child(2) > input").val() + '" />');
			});
			$("form#getPDF").submit();
		}
	});
</script>
<script>
	var rid = "{{ $model->id }}";
</script>
<script src="{{ elixir('js/warehouseRack.js') }}"></script>
@endpush