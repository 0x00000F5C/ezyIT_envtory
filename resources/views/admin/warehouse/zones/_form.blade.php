<form role="form" method="POST" action="{{ $model->exists ? url('/app/warehouse/' . $warehouse->id . '/mapping/' . $model->id) : url('/app/warehouse/' . $warehouse->id . '/mapping') }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="row">
			<div class="col-md-8">
				<div class="row">
					<div class="col-md-8">
						<div class="form-group {{ $errors->has('name')   ? ' has-error' : '' }}">
							<label>Zone Name<span class="red"> *</span></label>
							<div>
								<input type="text" id="zone-name" class="form-control" name="name" placeholder="Name" value="{{ $model->exists ? $model->name : "" }}">
								@include('layouts.lte._help_block', ['helpParameter' => 'name'])
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group {{ $errors->has('prefix')   ? ' has-error' : '' }}">
							<label>Zone Prefix<span class="red"> *</span></label>
							<div>
								<input type="text" id="zone-prefix" class="form-control" name="prefix" placeholder="Prefix" value="{{ $model->exists ? $model->prefix : "" }}">
								@include('layouts.lte._help_block', ['helpParameter' => 'prefix'])
							</div>
						</div>
					</div>
					<div class="col-md-8">
						<div class="form-group {{ $errors->has('type')  ? ' has-error' : '' }}">
							<label >Zone Type<span class="red"> *</span></label>
							<div>
								<select id="zone-type" class="form-control select2" name="type" data-placeholder="Type" style="width: 91%;">
									@foreach ($zones as $type)
										<option value="{{ $type->id }}" {{ $type->id == $model->type ? 'selected' : ''}}>{{ $type->name }}</option>
									@endforeach
								</select>
								<button id ="btn-addZone" class="btn btn-default" type="button"><i class="fa fa-plus"></i></button>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-group {{ $errors->has('prefix')   ? ' has-error' : '' }}">
							<label>Cycle Count </label>
							<div>
								<input type="number" id="zone-cyclecount" class="form-control" name="cycle_count" placeholder="Cycle count" value="{{ $model->exists ? $model->cycle_count : "0" }}">
								@include('layouts.lte._help_block', ['helpParameter' => 'prefix'])
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="row" id="categories">
			<div class="col-md-5">
					<label >Zone Categories<span class="red"> *</span></label>
					<div class="categoryContainer">
						<div class="selectCategories">
							<ul class="nav nav-pills nav-stacked">
							@foreach ($categories as $category)
								<li>
									<a>
										<input type="checkbox" name="zone-categories[]" value="{{ $category->id }}" {{ in_array($category->id, $model->categories) ? 'checked' : '' }}>
										{{ $category->cat_name }}
									</a>
								</li>
							@endforeach
							</ul>
						</div>
					</div>
				</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ route("warehouse.mapping.index", $warehouse->id) }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>