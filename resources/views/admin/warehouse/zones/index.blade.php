@extends('layouts.lte.main')
@section('content')
    <div class="row" id="zones">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Warehouse Zones</h3>
					<input type="search" id="searchZones" class="pull-right form-control" placeholder="Filter zones..." style="display: inline-block; width: 256px;">
                </div>
                <div class="box-body">
					@foreach($zones as $zone)
                    <div id="warehouse-zone-{{ $zone->id }}" class="warehouse-zones col-md-3 col-sm-6 col-xs-12">
                        <div class="media bordered" >
                            <div class="media-body">
                                <h4 class="warehouse-zone-names media-heading">{{ $zone->name }}</h4>
                                <div class="media">
                                    ...
                                </div>
                                <div class="media">
                                    <a href="{{ route('warehouse.mapping.edit', [$warehouse->id, $zone->id]) }}" class="btn btn-warning btn-xs btn-flat"><i class="fa fa-pencil"></i> Update info</a> <button type="button" class="btn-showRacks btn btn-primary btn-xs btn-flat" data-value="{{ $zone->id }}"><i class="fa fa-cubes"></i> Show racks</button>
                                </div>
                            </div>
							<button type="button" class="btn-remZone btn btn-xs btn-danger btn-flat" data-value="{{ $zone->id }}" data-value="{{ $zone->id }}"><i class="fa fa-times"></i></button>
                        </div>
                    </div>
					@endforeach
                    <div class="col-md-3 col-sm-6 col-xs-12">
                        <a class="btn btn-primary btn-lg btn-app" href="{{ route('warehouse.mapping.create', $warehouse->id) }}">
                            <i class="fa fa-plus"></i> Zone
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row hidden" id="racks">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Rack Layout</h3>
					<input type="search" id="searchRacks" class="pull-right form-control" placeholder="Filter racks..." style="display: inline-block; width: 256px; margin-right: 16px;">
                </div>
                <div id="warehouse-racks" class="box-body">
                </div>
            </div>
			<button type="button" id="btn-hideRacks" class="btn btn-xs btn-danger btn-flat"><i class="fa fa-times"></i></button>
        </div>
    </div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.css') }}">
<style>
    .bordered {
        min-height: 106px;
        padding: 8px;
        /*margin-bottom: 20px;*/
        background-color: #f5f5f5;
        border: 1px solid gray;
        /*border-radius: 4px;*/
        /*-webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.05);*/
        /*box-shadow: inset 0 1px 1px rgba(0,0,0,.05);*/
    }
	
	.warehouse-zones, .warehouse-racks, .warehouse-trays {
		margin-bottom: 16px;
	}
	
	.btn-remZone, .btn-remRack, .btn-remTray, #btn-hideRacks, #btn-hideTrays {
		position: absolute;
		top: 0px;
		right: 15px;
	}
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/plugins/sweetalert2/es6-promise.auto.min.js') }}"></script>
<script src="{{ asset('assets/plugins/sweetalert2/sweetalert2.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});
	
	$("#btn-hideRacks").on("click", function() {
		if(!$("#racks").hasClass("hidden")) {
			$("html, body").animate({ scrollTop: $("#zones").offset().top }, "slow");
			$("#racks").addClass("hidden");
			if(!$("#trays").hasClass("hidden")) {
				$("#trays").addClass("hidden");
			}
		}
	});
	
    $(".btn-showRacks").on("click", function() {
		$.ajax({
			type: "post",
			url: "{{ route("warehouse.mapping.racks.getRacks", $warehouse->id) }}",
			data: { "q": $(this).data("value") },
			dataType: "json",
			success: function(data) {
				$("#warehouse-racks").children().remove();
				if(data.st) {
					$.each(data.data, function(key, val) {
						$("#warehouse-racks").append($('<div data-value="' + val.id + '" class="warehouse-racks col-md-3 col-sm-6 col-xs-12"><div class="media bordered" ><button type="button" class="btn-remRack btn btn-xs btn-danger btn-flat" data-zone="' + val.m_warehouse_zones_id + '" data-value="' + val.id + '"><i class="fa fa-times"></i></button><div class="media-body"><h4 class="warehouse-rack-names media-heading">' + val.name + '</h4><div class="media">...</div><div class="media"><a href="' + val.editAddress + '" class="btn btn-warning btn-xs btn-flat"><i class="fa fa-pencil"></i> Update info</a></div></div></div></div>'));
					});
				} else {
					$("#warehouse-racks").append($('<div class="warehouse-racks col-md-3 col-sm-6 col-xs-12"><div class="media bordered" ><div class="media-body"><h4 class="media-heading">No racks for this zone</h4><div class="media">You can add racks to this zone.</div></div></div></div>'));
				}
				$("#warehouse-racks").append($('<div id= "btn-addRack" class="col-md-3 col-sm-6 col-xs-12"><a class="btn btn-primary btn-lg btn-app" href="' + data.addAddress + '"><i class="fa fa-plus"></i> Rack</a></div>'));
				if($("#racks").hasClass("hidden")) {
					$("#racks").removeClass("hidden");
				}
				$("html, body").animate({ scrollTop: $("#racks").offset().top }, "slow");
			}
		});
	});
	
	$(".btn-remZone").on("click", function() {
		var zone = $(this).data("value");
		swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false
		}).then(function () {
			$.ajax({
				type: "post",
				url: "{{ url("app/warehouse/" . $warehouse->id . "/mapping") }}/" + zone,
				data: { "_method": "DELETE", "zid": zone },
				dataType: "json",
				success: function(data) {
					if(data.st) {
						swal("Deleted!", data.msg, "success");
						setTimeout(function() {
							location.reload();
						}, 1000);
					} else {
						swal("Error", data.msg, "error");
					}
				}
			});
		}, function (dismiss) {
		  if (dismiss === "cancel") {
			swal("Cancelled", "Operation cancelled", "error");
		  }
		});
	});
	
	$("#warehouse-racks").on("click", ".btn-remRack", function() {
		var zone = $(this).data("zone");
		var rack = $(this).data("value");
		swal({
			title: 'Are you sure?',
			text: "You won't be able to revert this!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#3085d6',
			cancelButtonColor: '#d33',
			confirmButtonText: 'Yes, delete!',
			cancelButtonText: 'No, cancel!',
			confirmButtonClass: 'btn btn-success',
			cancelButtonClass: 'btn btn-danger',
			buttonsStyling: false
		}).then(function () {
			$.ajax({
				type: "post",
				url: "{{ url("app/warehouse/" . $warehouse->id . "/mapping") }}/" + zone + "/racks/" + rack,
				data: { "_method": "DELETE", "zid": zone, "rid": rack },
				dataType: "json",
				success: function(data) {
					if(data.st) {
						swal("Deleted!", data.msg, "success");
						setTimeout(function() {
							location.reload();
						}, 1000);
					} else {
						swal("Error", data.msg, "error");
					}
				}
			});
		}, function (dismiss) {
		  if (dismiss === "cancel") {
			swal("Cancelled", "Operation cancelled", "error");
		  }
		});
	});
	
	$("#searchZones").on("input", function() {
		if($(this).val() != "") {
			var q = new RegExp("^" + $(this).val(), "ig");
			$(".warehouse-zone-names").each(function(i, obj) {
				if(!q.test($(this).text())) {
					$(this).parents(".warehouse-zones").addClass("hidden");
				}
			});
		} else {
			$(".warehouse-zones").removeClass("hidden");
		}
	});
	
	$("#searchRacks").on("input", function() {
		if($(this).val() != "") {
			var q = new RegExp("^" + $(this).val(), "ig");
			$(".warehouse-rack-names").each(function(i, obj) {
				if(!q.test($(this).text())) {
					$(this).parents(".warehouse-racks").addClass("hidden");
				}
			});
		} else {
			$(".warehouse-racks").removeClass("hidden");
		}
	});
</script>
@endpush