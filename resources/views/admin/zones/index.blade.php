@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group hidden">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<div class="pull-right">
					@can('create-zones')
					<a id="btn-newZone" class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New zone</a>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="zones-table" cellspacing>
					<thead>
							<tr>
									<th>Zone name</th>
									<th style="width: 80px">Prefix</th>
									<th style="width: 96px">Cycle count</th>
									<th style="width: 128px;"></th>
							</tr>
					</thead>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.structure.min.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		$('#zones-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('zones.index') !!}',
			columns: [
				{ data: 'name', name: 'name' },
				{ data: 'prefix', name: 'prefix' },
				{ data: 'cycle_count', name: 'cycle_count' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id);
			}
		});

		var newZone, nz;

		newZone = $("#dialog-newZone").dialog({
			autoOpen: false,
			height: 400,
			width: 350,
			modal: true,
			show: { effect: "fade", duration: 333 },
			hide: { effect: "fade", duration: 333 },
			buttons: {
				Cancel: function() {
					newZone.dialog("close");
				}, 
				Submit: function() {
					if(newZone.find("input[name=_method]").val() == "POST"){
						var adr = "{!! route('zones.store') !!}";
						var dta = { "zone-name": newZone.find("input#zone-name").val(), "zone-prefix": newZone.find("input#zone-prefix").val(), "cycle-count": newZone.find("input#zone-cyclecount").val() };
					} else if(newZone.find("input[name=_method]").val() == "PUT") {
						var adr = "{!! url('app/zones') !!}/" + newZone.find("input[name=id]").val();
						var dta =	{ "_method": "PUT", "id": newZone.find("input[name=id]").val(), "zone-name": newZone.find("input#zone-name").val(), "zone-prefix": newZone.find("input#zone-prefix").val(), "cycle-count": newZone.find("input#zone-cyclecount").val() };
					}
					$.ajax({
						type: "post",
						url: adr,
						dataType: "json",
						data: dta,
						success: function(data) {
							if(data.st) {
								swal("Nice!", "Data saved.", "success");
								setTimeout(function() {
									location.reload();
								}, 1000);
							} else {
								swal("Oops...", "Failed to save data.", "error");
							}
						}
					});
				}
			},
			close: function() {
				nz[ 0 ].reset();
				//allFields.removeClass("ui-state-error");
			}
		});

		nz = newZone.find("form").on("submit", function(event) {
			event.preventDefault();
		});
	
		$("#zone-name").on("change", function() {
			if($(this).val() !== "") {
				$.ajax({
					type: "post",
					url: "{!! route("zones.genPrefix") !!}",
					data: { q: $(this).val() },
					dataType: "json",
					success: function(data) {
						$("#zone-prefix").val(data.data);
					},
					error: function(data){
						var errors = data.responseJSON;
						swal.showInputError(errors.name);
					}
				});
			}
		});
		
		$("#zone-prefix").on("input", function() {
			var dis = $(this);
			$.ajax({
				type: "post",
				url: "{!! route('zones.checkPrefix') !!}",
				dataType: "json",
				data: { q: dis.val() },
				success: function(data) {
					if(!data.st) {
						dis.parent().append($('<span></span>').attr("id", "product-prefix-warning").addClass("help-block").append($("<strong></strong>").text("Warning: This product prefix has already existed")));
					} else {
						dis.siblings(":not(input)").remove();
					}
				}
			});
		});

		$("#btn-newZone").on("click", function(evt) {
			evt.preventDefault();
			newZone.find("input[name=_method]").val("POST");
			newZone.find("input[name=id]").val("");
			newZone.dialog({title: "Create new zone"}).dialog("open");
		});

		$("#zones-table").on("click", ".btn-editZone", function(evt) {
			evt.preventDefault();
			newZone.find("input[name=_method]").val("PUT");
			newZone.find("input[name=id]").val($(this).data("value"));
			newZone.find("#zone-name").val($(this).parents("tr").children("td:nth-child(2)").text());
			newZone.find("#zone-prefix").val($(this).parents("tr").children("td:nth-child(3)").text());
			newZone.find("#zone-cyclecount").val($(this).parents("tr").children("td:nth-child(4)").text());
			newZone.dialog({title: "Update zone"}).dialog("open");
		});
	});

	/* $("#btn-newZone").on("click", function(evt) {
		evt.preventDefault();
		var q = {};
		swal({
			title: "New Zone",
			text: "Enter new zone name",
			type: "input",
			showCancelButton: true,
			closeOnConfirm: true,
			closeOnCancel: true,
			animation: "slide-from-top",
			inputPlaceholder: "Zone name"
		}, function(val){
			if (val === "") {
				swal.showInputError("This field is required");
				return false;
			}
			q.zone_name = val;
			var pref = $.ajax({
				type: "post",
				url: "{!! route('zones.genPrefix') !!}",
				dataType: "json",
				data: { q: val },
				success: function(data) {
					swal({
						title: "New Zone",
						text: "Enter new zone prefix",
						type: "input",
						showCancelButton: true,
						closeOnConfirm: false,
						closeOnCancel: true,
						animation: "slide-from-top",
						inputPlaceholder: "Zone prefix",
						inputValue: data.data
					}, function(val){
						if (val === "") {
							swal.showInputError("This field is required");
							return false;
						}
						if (val.length > 4) {
							swal.showInputError("Prefixes shouldn't be longer than 3 characters");
							return false;
						}
						$.ajax({
							type: "post",
							url: "{!! route('zones.checkPrefix') !!}",
							dataType: "json",
							data: { q: val },
							success: function(data) {
								if(!data.st) {
									swal.showInputError("A zone prefix with this name has already exists");
									return false;
								}
							}
						});
						q.zone_prefix = val;
						swal({
							title: "New Zone",
							text: "Enter new cycle count",
							type: "input",
							showCancelButton: true,
							closeOnConfirm: true,
							animation: "slide-from-top",
							inputPlaceholder: "Cycle count",
							inputValue: 0
						}, function(val){
							if (val === "") {
								swal.showInputError("This field is required");
								return false;
							}
							if(!$.isNumeric(val)) {
								swal.showInputError("Inputted value is not number");
								return false;
							}
							$.ajax({
								type: "post",
								url: "{!! route('zones.store') !!}",
								dataType: "json",
								data: { "zone-name": q.zone_name, "zone-prefix": q.zone_prefix, "cycle-count": val },
								success: function(data) {
									if(data.st) {
										swal("Nice!", "Data saved.", "success");
										setTimeout(function() {
											location.reload();
										}, 1000);
									} else {
										swal("Oops...", "Failed to save data.", "error");
									}
								}
							});
						});
					});
				}
			});
		});
	});

	$("#zones-table").on("click", ".btn-editZone", function(evt) {
		evt.preventDefault();
		var dis = $(this).data("value");
		var q = {};
		swal({
			title: "Update Zone",
			text: "Enter new zone name",
			type: "input",
			showCancelButton: true,
			closeOnConfirm: false,
			animation: "slide-from-top",
			inputPlaceholder: "Zone name",
			inputValue: $("tr#row-" + dis + " > td:nth-child(2)").text()
		}, function(val){
			if (val === "") {
				swal.showInputError("This field is required");
				return false;
			}
			q.zone_name = val;
				swal({
					title: "New Zone",
					text: "Enter new zone prefix",
					type: "input",
					showCancelButton: true,
					closeOnConfirm: false,
					animation: "slide-from-top",
					inputPlaceholder: "Zone prefix",
					inputValue: $("#row-" + dis + " > td:nth-child(3)").text()
				}, function(val){
					if (val === "") {
						swal.showInputError("This field is required");
						return false;
					}
					if (val.length > 4) {
						swal.showInputError("Prefixes shouldn't be longer than 3 characters");
						return false;
					}
					if(val != $("tr#row-" + dis + " > td:nth-child(3)").text()) {
						$.ajax({
							type: "post",
							url: "{!! route('zones.checkPrefix') !!}",
							dataType: "json",
							data: { q: val },
							success: function(data) {
								if(!data.st) {
									swal.showInputError("A zone prefix with this name has already exists");
									return false;
								}
							}
						});
					}
					q.zone_prefix = val;
					swal({
						title: "Update Zone",
						text: "Enter new cycle count",
						type: "input",
						showCancelButton: true,
						closeOnConfirm: false,
						animation: "slide-from-top",
						inputPlaceholder: "Cycle count",
						inputValue: $("tr#row-" + dis + " > td:nth-child(4)").text()
					}, function(val){
						if (val === "") {
							swal.showInputError("This field is required");
							return false;
						}
						if(!$.isNumeric(val)) {
							swal.showInputError("Inputted value is not number");
							return false;
						}
						$.ajax({
							type: "post",
							url: "{!! url('app/zones') !!}/" + dis,
							dataType: "json",
							data: { "_method": "PUT", "id": dis, "zone-name": q.zone_name, "zone-prefix": q.zone_prefix, "cycle-count": val },
							success: function(data) {
								if(data.st) {
									swal("Nice!", "Data saved.", "success");
									setTimeout(function() {
										location.reload();
									}, 1000);
								} else {
									swal("Oops...", "Failed to save data.", "error");
								}
							}
						});
					});
				});
		});
	}); */
</script>

<div id="dialog-newZone">
	<form id="form-newZone">
		<input type="hidden" name="_method" value="">
		<input type="hidden" name="id" value="">
		<fieldset style="display: table;">
			<div class="row" style="display: table-row;">
				<label for="zone-name" style="display: table-cell; width: 96px;">Zone name<span class="red"> *</span></label>
				<div style="display: table-cell;"><input type="text" id="zone-name" class="text ui-widget-content ui-corner-all form-control" name="zone-name" placeholder="Zone name" value="" style="margin-bottom: 8px;" maxlength="64" required="required"></div>
			</div>
			<div class="row" style="display: table-row;">
				<label for="zone-prefix" style="display: table-cell;">Prefix<span class="red"> *</span></label>
				<div style="display: table-cell;"><input type="text" id="zone-prefix" class="text ui-widget-content ui-corner-all form-control" name="zone-prefix" placeholder="Prefix" value="" style="margin-bottom: 8px;" maxlength="3" required="required"></div>
			</div>
			<div class="row" style="display: table-row;">
				<label type="text" for="zone-cyclecount" style="display: table-cell;">Cycle count</label>
				<div style="display: table-cell;"><input type="number" name="zone-cyclecount" id="zone-cyclecount" value="1" class="text ui-widget-content ui-corner-all form-control"></div>
			</div>
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>
@endpush