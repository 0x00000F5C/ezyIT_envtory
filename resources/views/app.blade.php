<html>
<script type="text/javascript">
    $(document).on('click', '#btn-newOrdertype', function(){
        $('#addOrderType').modal('show');
    });
    // Edit Data (Modal and function edit data)
/*    $(document).on('click', '.btn-editOrderType', function() {
        $('#footer_action_button').text(" Update");
        $('#footer_action_button').addClass('glyphicon-check');
        $('#footer_action_button').removeClass('glyphicon-trash');
        $('.actionBtn').addClass('btn-success');
        $('.actionBtn').removeClass('btn-danger');
        $('.actionBtn').addClass('edit');
        $('.modal-title').text('Edit Order Type');
        //$('.deleteContent').hide();
        $('.form-horizontal').show();
        $('#name_e').val($(this).data('name'));
        $('#min_qty_e').val($(this).data('min_qty'));
        $('#max_qty_e').val($(this).data('max_qty'));
        $('#editOrderType').modal('show');
    });*/

    $('.modal-footer').on('click', '#editOrderType', function() {
        $.ajax({
            type: 'post',
            url: 'orderType/edit',
            data: {
                //'_token': $('input[name=_token]').val(),
                'id': $("#id_e").val(),
                'name': $("#name_e").val(),
                'min_qty': $('#min_qty_e').val(),
                'max_qty': $('#max_qty_e').val()
            },
            success: function(data) {
                window.location.reload();
                $('#editOrderType').modal('hide');
                if ((data.errors)) {
                    $('.error').removeClass('hidden');
                    $('.error').text(data.errors.name);
                    $('.error').text(data.errors.min_qty);
                    $('.error').text(data.errors.max_qty);
                } else {
                    $('.error').remove();
                }
            }
        });
    });
    // add function
    $("#add").click(function() {
        $.ajax({
            type: 'post',
            url: 'orderType/add',
            data: {
                //'_token': $('input[name=_token]').val(),
                'name': $('input[name=name]').val(),
                'min_qty': $('input[name=min_qty]').val(),
                'max_qty': $('input[name=max_qty]').val()
            },
            success: function(data) {
                window.location.reload();
                $('#addOrderType').modal('hide');
                if ((data.errors)) {
                    $('.error').removeClass('hidden');
                    $('.error').text(data.errors.name);
                    $('.error').text(data.errors.min_qty);
                    $('.error').text(data.errors.max_qty);
                } else {
                    $('.error').remove();
                }
            },
        });
        $('#name').val('');
        $('#min_qty').val('');
        $('#max_qty').val('');

    });

    //delete order type
    $('#orderType-table').on('click', '.deleteOrderType', function (e) {
        var order_id = $(this).attr('data-id');
        swal({
                title: "Are you sure?",
                text: "You will not be able to recover this imaginary file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Yes, delete it!",
                closeOnConfirm: false
            },
            function(){
                $.ajax({
                    type: 'DELETE',
                    url: 'orderType/' + order_id,
                    success: function( msg ) {
                        if ( msg.status === 'success' ) {
                            swal("Deleted!", msg.msg);
                            setInterval(function() {
                                window.location.reload();
                            }, 1000);
                        }else{
                            swal("Warning!", msg.msg);
                        }
                    },
                    error: function( data ) {
                        if ( data.status === 422 ) {
                            sweetAlert('Cannot delete this data');
                        }
                    }
                });

            });


//        return false;
    });

</script>
</html>