@extends('layouts.lte.auth')
@section('content')
<div class="login-box">
  <div id="login-logo" class="login-box-body text-center">
  <img src="{{ Asset("img/common/logo.png") }}" width="75%"/>
  </div>
  <div class="login-box-body">
    <form method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="form-group has-feedback{{ $errors->has('password') ? ' has-error' : '' }}">
        <input id="password" type="password" class="form-control" name="password" required placeholder="Password">
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
        @if ($errors->has('password'))
            <span class="help-block">
                <strong>{{ $errors->first('password') }}</strong>
            </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12">
          <a href="{{ url("password/reset") }}">Forgot password</a>
          <button type="submit" class="btn btn-danger btn-flat pull-right">Login</button>
        </div>
      </div>
    </form>

  </div>
</div>
@stop