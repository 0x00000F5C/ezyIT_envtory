@extends('layouts.lte.auth')
@section('content')
<div class="login-box">
  <div id="login-logo" class="login-box-body text-center">
  <a href="{{url('/')}}"><img src="{{ Asset("img/common/logo.png") }}" width="75%"/></a>
  </div>
  <div class="login-box-body">
  <p class="login-box-msg" style="text-align: left;">Please enter the email address associated with your account. A verification code will be sent to you. One you have received the verification code, you will be able to choose a new password for your account.</p>
    <form method="POST" action="{{ url('/password/email') }}">
      {{ csrf_field() }}
      <div class="form-group has-feedback{{ $errors->has('email') ? ' has-error' : '' }}">
        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus placeholder="Email">
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
        @if ($errors->has('email'))
            <span class="help-block">
                <strong>{{ $errors->first('email') }}</strong>
            </span>
        @endif
      </div>
      <div class="row">
        <div class="col-xs-12 text-right">
          <button type="submit" class="btn btn-primary btn-flat">Send Password Reset Link</button>
        </div>
      </div>
    </form>

  </div>
</div>
@stop