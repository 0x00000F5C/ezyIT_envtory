@extends('app')
@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group hidden">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>

				<div class="pull-right">
					@can('create-user')
					<button type="button" class="btn btn-primary btn-flat btn-sm pull-right"  id="btn-newOrdertype">Add New Type</button>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="orderType-table" cellspacing>
					<thead>
						<tr>
							<th>Order type name</th>
							<th style="width: 96px">Minimum qty</th>
							<th style="width: 96px">Maximum qty</th>
							<th style="width: 128px;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
{{--<link rel="stylesheet" href="{{ asset('assets/crop_image/demo.css') }}">--}}
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
{{--<script src="{{ asset('assets/crop_image/demo.js') }}"></script>--}}
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		var table = $('#orderType-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('orderType.index') !!}',
			columns: [
				{ data: 'name', name: 'name' },
				{ data: 'min_qty', name: 'min_qty' },
				{ data: 'max_qty', name: 'max_qty' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id);
			}
		});

		//Edit
        $('#orderType-table').on('click', '.btn-editOrderType', function () {
            $('#editOrderType').modal('show');
            var data = table.row( $(this).parents('tr') ).data();
            var field = ['id','name','min_qty','max_qty'];

            for (i in field) {
                $( '#editOrderType #' + field[i] + '_e' ).val(data[field[i]])
            }
        } );
	});
</script>
@endpush

<div class="modal fade" id="addOrderType" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Add Order Type</h4>
			</div>
			<div class="modal-body">
				<form action="{{ url('orderType') }}" method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<input type="hidden" name="_method" value="">
						<input type="hidden" name="id" value="">
                            <div class="form-group">
                                <label for="name">Order Type Name:</label>
                                <input type="text" class="form-control" id="name" name="name">
                            </div>
                            <div class="form-group">
                                <label for="min_qty">Min QTY:</label>
                                <input type="number" class="form-control" id="min_qty" name="min_qty">
                            </div>
                            <label for="max_qty">Max Qty:</label>
                            <input type="number" class="form-control" id="max_qty" name="max_qty">
                        </div>
                    </form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-flat" id="add">Submit</button>
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>

<div class="modal fade" id="editOrderType" role="dialog">
	<div class="modal-dialog">

		<!-- Modal content-->
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title">Edit Order Type</h4>
			</div>
			<div class="modal-body">
				<form action="{{ url('orderType') }}" method="post">
					{{ csrf_field() }}
					<div class="form-group">
						<input type="hidden" name="_method" value="">
						<input type="hidden" id="id_e" name="id" value="">
						<div class="form-group">
							<label for="name">Order Type Name:</label>
							<input type="text" class="form-control" id="name_e" name="name_e">
						</div>
						<div class="form-group">
							<label for="min_qty">Min QTY:</label>
							<input type="number" class="form-control" id="min_qty_e" name="min_qty_e">
						</div>
						<label for="max_qty">Max Qty:</label>
						<input type="number" class="form-control" id="max_qty_e" name="max_qty_e">
					</div>
				</form>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn btn-primary btn-flat" id="editOrderType">Submit</button>
				<button type="button" class="btn btn-default btn-flat" data-dismiss="modal">Close</button>
			</div>
		</div>

	</div>
</div>
