<form role="form" class="form-horizontal" method="POST" action="{{ $model->exists ? url('/app/zones/' . $model->id) : url('/app/zones') }}">
	{{ csrf_field() }}
	@if($model->exists)
	{{ method_field('PUT') }}
	<input type="hidden" name="id" value="{{ $model->id }}">
	@endif
	<div class="box-body">
		<div class="form-group has-feedback">
			<div class="col-md-8">
				<label for="zone-name" class="col-md-3 control-label text-left">Zone name<span class="red"> *</span></label>
				<div class="col-md-6">
					<input name="zone-name" class="form-control" id="zone-name" placeholder="Zone name" value="{{ $model->exists ? $model->name : "" }}" maxlength="64" required="required">
					@if ($errors->has("zone-name"))
					<span class="help-block">
						<strong>{{ $errors->first("zone-name") }}</strong>
					</span>
					@endif
				</div>
				<div class="col-md-3">
					<input name="zone-prefix" class="form-control" id="zone-prefix" placeholder="Prefix" value="{{ $model->exists ? $model->prefix : "" }}" maxlength="3" required="required">
					@if ($errors->has("zone-prefix"))
					<span class="help-block">
						<strong>{{ $errors->first("zone-prefix") }}</strong>
					</span>
					@endif
				</div>
			</div>
		</div>
		<div class="form-group has-feedback hidden">
			<div class="col-md-4 col-md-offset-2">
				<div class="checkbox icheck">
					<label>
						<input type="checkbox" name="active" value="1">
						Active
					</label>
				</div>
			</div>
		</div>
	</div>
	<div class="box-footer">
		<div class="form-group">
			<label class="col-md-2 control-label"></label>
			<div class="col-md-4">
				<a href="{{ url("app/zones") }}" class="btn btn-default btn-flat"><i class="fa fa-arrow-left"></i> Cancel</a>
				<button type="submit" class="btn btn-primary btn-flat"><i class="fa fa-save"></i> {{ $model->exists ? "Update" : "Save" }}</button>
			</div>
		</div>
	</div>
</form>
