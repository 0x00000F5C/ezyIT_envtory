@extends('layouts.lte.main')
@section('content')
    <div class="row">
        <div class="col-xs-12">
            @include('layouts.lte.status')
            <div class="box">
                <form role="form" class="form-horizontal" method="POST" id="formAssignTray"
                      action="{{ url('/app/pickingList/setTray') }}" onsubmit="return validateForm()">
                    {{ csrf_field() }}
                    <div class="box-header">
                        <div class="col-md-8">
                            <input type="hidden" id="qtyOfTray" value="{{ $pickingList->qty_of_tray }}">
                            @for($x=1;$x<=$pickingList->qty_of_tray;$x++)
                                <div class="form-group col-md-4">
                                    <label for="prefix" class="col-sm-4 control-label">Tray :</label>
                                    <div class="col-sm-8">
                                        <select class="form-control sel-tray" onchange="selectedTray(this)">
                                            <option value="">Select Tray</option>
                                            @foreach($trays as $tray)
                                                <option value="{{$tray->prefix}}" data-id="{{$tray->id}}">{{$tray->prefix}}</option>
                                            @endforeach
                                        </select>

                                    </div>
                                </div>
                            @endfor
                        </div>
                        <div class="col-md-4">
                            <button type="button" class="btn btn-primary scanTray">Scan Tray</button>
                        </div>
                        <div class="clear" style="clear: both;"></div>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        @foreach($data as $k=>$d)
                            <div class="box">
                                <div class="box-header">
                                    <strong>{{$k}}</strong>
                                </div>
                                <div class="box-body">
                                    <table class="table table-striped">
                                        <thead>
                                        <tr>
                                            <td>Tray ID</td>
                                            <td>Batch ID</td>
                                            <td>SKU</td>
                                            <td>Product Name</td>
                                            <td>QTY</td>
                                            <td>Cell ID</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($d as $i)
                                            <tr>
                                                <td id="{{ $i['detail_id'] }}">
                                                    <button type="button" class="btn btn-default btn-mapping hidden"
                                                            onclick="mapping('{{ $i['detail_id'] }}')">Mapping Tray
                                                    </button>
                                                    <span class="tray_of_item"> {{ $i['tray_id'] }}</span>
                                                    <input type="hidden" name="items[picking_id][]"
                                                           value="{{ $i['detail_id'] }}">
                                                    <input type="hidden" name="items[tray_id][]" class="tray_id">
                                                </td>
                                                <td>{{$i['batch_id']}}</td>
                                                <td>{{$i['product_sku']}}</td>
                                                <td>{{$i['product_name']}}</td>
                                                <td>{{$i['qty']}}</td>
                                                <td>{{$i['cell_id']}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <div class="box-footer ">
                        <div class="pull-right">
                            <button type="submit" class="btn btn-success btn-flat"><i class="fa fa-save"></i> Save
                            </button>
                            <a href="{{url('app/pickingList')}}" class="btn btn-warning btn-flat"><i
                                        class="fa fa-arrow-left"></i> Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div id="modalAssignTray" class="modal fade" role="dialog">
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <form role="form" class="form-horizontal" id="formAssingTray" action="">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <h4 class="modal-title">Mapped Tray to Picking</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" id="pickItemId" value="">
                        <div class="form-group">
                            <label for="tray" class="col-sm-4">Tray ID :</label>
                            <div class=" col-sm-8">
                                <select class="form-control" id="select-tray">
                                    <option value="">Select Tray</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary" type="button" onclick="doneAssign(this)">Done</button>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    @include('front.pickingList.scann')
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet"
      href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<style>
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script>
    $.ajaxSetup({
        headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content')}
    });

    var selectedTrays = [];
    function mapping(e) {
        $('#pickItemId').val(e)
        $('#select-tray option').prop('selected', false)
        $('#select-tray').select2({
            placeholder: "Select Tray",
        })
        $('#modalAssignTray').modal('show')
    }

    function selectedTray(e) {
        var id = $(e).find('option:selected').data('id'),
            text = $(e).find('option:selected').text()

        if (id != '') {
            selectedTrays.splice(1, 0, {id: id, text: text});
        }

        if ($('#qtyOfTray').val() == 1) {
            $('.tray_of_item').text(text)
            $('.tray_id').val(id)
            return false
        }

        $('#select-tray').select2({
            data: selectedTrays,
            placeholder: "Select Tray",
        })
        if (selectedTrays.length > 0) {
            $('.btn-mapping').removeClass('hidden')
        } else {
            $('.btn-mapping').addClass('hidden')
        }
        $('#modalScannTray').modal('hide')
    }

    function doneAssign(el) {
        if ($('#select-tray').val() == '') {
            sweetAlert("Failed...", "Please select tray! ", "info");
            return false
        }

        var elId = $('#pickItemId').val(),
            val = $('#select-tray option:selected').val(),
            text = $('#select-tray option:selected').text()

        $('td#' + elId).find('span.tray_of_item').text(text)
        $('td#' + elId).find('input.tray_id').val(val)
        $('td#' + elId).find('.btn-mapping').addClass('hidden').removeClass('btn-mapping')
        $("#modalAssignTray").modal('hide')
    }

    function validateForm() {
        var valid = true;
        $('.tray_id').each(function (i,e) {
            if($(e).val()=='')valid=false;
        })
        if (valid == false) {
            sweetAlert("Error...", "Please mapping all tray to all items!", "error");
            return false;
        }
    }

    $('.scanTray').on('click', function () {
        $('#modalScannTray span').text('Scan Tray')
        $('#modalScannTray').modal('show')
    })

</script>
@endpush