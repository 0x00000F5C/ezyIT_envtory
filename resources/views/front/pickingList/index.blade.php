@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="pull-right">
					@can('create-picking')
					<a id="btn-newPickingList" class="btn btn-primary btn-flat" href="#"><i class="fa fa-plus"></i> Generate list</a>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="pickingList-table" cellspacing>
					<thead>
						<tr>
							<th>Picking ID</th>
							<th style="width: 144px">Picking date</th>
							<th style="width: 144px">Staff</th>
							<th style="width: 144px">Status</th>
							<th style="width: 140px;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
		<div id="pickList" class="box">
		  <div class="box-body">
			<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="pickItem-table" cellspacing>
			  <thead>
				  <tr>
                    <th>Tray ID</th>
                    <th>Order</th>
                    <th>Product No.</th>
                    <th>Batch ID</th>
                    <th>Cell ID</th>
                    <th>Product SKU</th>
                    <th>Qty</th>
                    {{--<th></th>--}}
				  </tr>
			  </thead>
			</table>
		  </div>
		</div>
	</div>
</div>

<div class="modal fade" id="picking_form" tabindex="-1" role="dialog" aria-labelledby="pickingModal">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">Generate Picking List</h4>
			</div>
			<form role="form" class="form-horizontal" method="POST" id="formGeneratePickingList" action="{{ url('/app/pickingList') }}">
				{{ csrf_field() }}
				<div class="modal-body">
					<table>
						<tr>
							<td style="width: 20%;">
								<select name="user_id" class="text ui-widget-content ui-corner-all form-control" id="user_id">
									@foreach($users as $user)
										<option value="{{$user->id}}">{{$user->firstname}}</option>
									@endforeach
								</select>
							</td>
							<td style="width: 28%;">
								<select class="text ui-widget-content ui-corner-all form-control" name="picking_type" id="picking_type" onchange="pickType()">
									<option value="">All</option>
									@foreach($type as $t)
										<option value="{{$t->id}}" data-order_type="{{$t->order_type}}" data-qty="{{$t->qty}}">{{$t->picking_type_name}} </option>
									@endforeach
								</select>
							</td>
							<td style="width: 20%; text-align: right;">Tray needed:</td>
							<td style="width: 40%;">
								<input class="text ui-widget-content ui-corner-all form-control" placeholder="Tray needed" name="qty_of_tray" value="1" id="tray_needed"/>
							</td>
						</tr>
					</table>
					<fieldset>
						<h3>Wait for picking item</h3>
						<div style=" overflow:auto; max-height: 155px;">
							<table class="table table-bordered table-hover" id="tbl-list-order">
								<thead>
								<tr>
									<th></th>
									<th>Order no.</th>
									<th>Shop order no.</th>
									<th>Date</th>
									<th>Platform</th>
									<th>Shop</th>
									<th>Customer</th>
									<th>Category</th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</fieldset>
					<fieldset>
						<h3>Picking item</h3>
						<div style=" overflow:auto; max-height: 155px;">
							<table class="table table-bordered table-hover" id="tbl-detItemForPick">
								<thead>
								<tr>
									<th>Inv. SKU</th>
									<th>Item ID</th>
									<th>Order No</th>
									<th>Cell ID</th>
									<th>Batch ID</th>
									<th>Qty</th>
								</tr>
								</thead>
								<tbody></tbody>
							</table>
						</div>
					</fieldset>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" onclick="validateFormPickList(this)" target="#formGeneratePickingList">Save</button>
				</div>
			</form>
		</div>
	</div>
</div>

<div id="modalAssignTray" class="modal fade" role="dialog">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <form role="form" class="form-horizontal" method="POST" id="formAssingTray" action="{{ url('/app/picking') }}">
                {{ csrf_field() }}
                {{ method_field('PUT') }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Mapped Tray to Picking</h4>
                </div>
                <div class="modal-body">
                    <input type="hidden" id="pickItemId" value="" name="picking_item_id">
                    <table>
                        <tr>
                            <td>
                                <label for="tray">Tray ID :</label>
                            </td>
                            <td>
                                <select class="form-control" name="tray_id" id="select-tray">
                                    <option value="">Select Tray</option>
                                    @foreach($trays as $tray)
                                        <option value="{{$tray->id}}">{{$tray->prefix}}</option>
                                    @endforeach
                                </select>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="modal-footer">
                    <button class="btn btn-primary" type="button" onclick="doneAssign(this)" target="#formAssingTray">Done</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
    </div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<style>
	.click{
		cursor: pointer;
	}
	.clicked{
		background-color: #f5f5f5;
	}
    .scanner-laser{
        position: absolute;
        margin: 40px;
        height: 30px;
        width: 30px;
    }
    .laser-leftTop{
        top: 0;
        left: 0;
        border-top: solid red 5px;
        border-left: solid red 5px;
    }
    .laser-leftBottom{
        bottom: 0;
        left: 0;
        border-bottom: solid red 5px;
        border-left: solid red 5px;
    }
    .laser-rightTop{
        top: 0;
        right: 0;
        border-top: solid red 5px;
        border-right: solid red 5px;
    }
    .laser-rightBottom{
        bottom: 0;
        right: 0;
        border-bottom: solid red 5px;
        border-right: solid red 5px;
    }
</style>
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	var id_selected = '';
	var table = $('#pickingList-table').DataTable({
		processing: true,
		serverSide: true,
		ajax: '{!! route('pickingList.index') !!}',
		columns: [
			{ data: 'picking_code', class : 'click'},
			{ data: 'date', class : 'click'},
			{ data: 'picking_staff', class : 'click'},
			{ data: 'picking_status', class : 'click'},
			{ data: 'action', name: 'action', orderable: false, searchable: false }
		],
		createdRow: function(row, data, dataIndex) {
			$(row).attr("id", "row-" + data.id);
		}
	});

    $('#pickingList-table tbody').on('click', 'tr', function () {
        if ( $(this).hasClass('clicked') ) {
            $(this).removeClass('clicked');
        }
        else {
            table.$('tr.clicked').removeClass('clicked');
            $(this).addClass('clicked');
        }
        var data = table.row( this ).data();
        id_selected = data.id
        loadDetail(data.id);
    } );

    function loadDetail(id){
        $.ajax({
            url: "{!!url('app/pickingList/getDetail')!!}",
            type: "post",
            data: { picking_id: id }
        }).done(function (result) {
            tableDetail.clear().draw();
            tableDetail.rows.add(result).draw();
        }).fail(function (jqXHR, textStatus, errorThrown) {
            tableDetail.clear().draw();
        });
    }

    var tableDetail = $("#pickItem-table").DataTable({
        data:[],
        columns: [
            { "data": "tray_id" },
            { "data": "order_no"  },
            { "data": "product_no"  },
            { "data": "batch_id" },
            { "data": "cell_id" },
            { "data": "product_sku" },
            { "data": "qty" },
//            { "data": "action", orderable: false}
        ],
        rowCallback: function (row, data) {},
        filter: false,
        info: false,
        paging: false,
        ordering: true,
        processing: true,
        retrieve: true,
        language: {
            emptyTable: "Click a Row on Picking List Table Above to Show Item list"
        }
    });

    function pickType() {
        var orderType=$('#picking_type option:selected').data('order_type');
        getListOrders(orderType)
        $('#tray_needed').val($('#picking_type option:selected').data('qty'))
    }

	$("#btn-newPickingList").on("click", function() {
	    getListOrders();
        $('#picking_type option').prop('selected',false)
		$('#picking_form').modal('show')
	});

    function getListOrders(type) {
        var orderType = '';
        if(typeof type!='undefined'){
			orderType = type
        }

        $.ajax({
            type: 'post',
            url: '{!! url('app/pickingList/orderList') !!}',
            data: {'type':orderType},
            dataType: 'json',
            success: function (resp) {
                var html = "";
                $.each(resp, function (i,e) {
                    html += '<tr>'+
                        '<td><input type="checkbox" class="checkOrderForPicking" value="'+e.id+'" name="orders[order_id][]"></td>'+
                        '<td>'+e.order_no+'</td>'+
                        '<td>'+e.shop_order_no+'</td>'+
                        '<td>'+e.date+'</td>'+
                        '<td>'+e.shopPlatform+'</td>'+
                        '<td>'+e.shop_name+'</td>'+
                        '<td>'+e.customer_name+'</td>'+
                        '<td>'+e.type+'</td>'+
                        '</tr>'
                });
                $('#tbl-list-order tbody').html(html);
                $('#tbl-detItemForPick tbody').html('');
            }
        });
    }

	var arr_item = [];
	$(document).on('change','.checkOrderForPicking', function () {
    	var allowed_to_pick = true;
        var num_of_check = 0, max_pick = parseInt($('#picking_type option:selected').data('qty'));
        $('.checkOrderForPicking').each(function(i,e){
            if($(e).is(':checked')){
                num_of_check+=1;
            }
        });
        if(num_of_check > max_pick){
            allowed_to_pick = false
        }

		var order_id = $(this).val()
	    if($(this).is(':checked')){
            if(allowed_to_pick){
                $.ajax({
                    type: 'post',
                    url: '{!! url('app/pickingList/getDetailInStock') !!}',
                    data: {order : order_id},
                    dataType: 'json',
                    success: function(response){
                        var html = '', num = '000';
                        $.each(response, function (i,e) {
                            arr_item.push(e.product_sku_id)
                            var count = 0;
                            for(var i = 0; i < arr_item.length; ++i){
                                if(arr_item[i] == e.product_sku_id)
                                    count++;
                            }
                            n = count.toString();
                            var inc = num.substr(n.length)+n;
                            html += '<tr data-id="'+order_id+'" data-item="'+e.product_sku_id+'">'+
                                '<td>' +
                                '<input type="hidden" name="details[det_stock_id][]" value="'+e.det_stock_id+'">'+e.product_sku_id+
                                '</td>'+
                                '<td>' +
                                '<input type="hidden" name="details[item_id][]" value="'+e.product_sku_id+inc+'">'+e.product_sku_id+inc+
                                '</td>'+
                                '<td>'+e.order_no+'</td>'+
                                '<td>'+e.cell_id+'</td>'+
                                '<td>'+e.batch_id+'</td>'+
                                '<td><input type="hidden" name="details[qty][]" value="'+e.qty+'">'+e.qty+'</td>'+
                                '</tr>';
                        });
                        $('#tbl-detItemForPick tbody').append(html)
                    }
                })
			}else{
                sweetAlert("Max Order...", "Order can't more than "+max_pick+"!", "warning");
                $(this).prop('checked',false)
                return false;
			}
		}else{
            $('#tbl-detItemForPick tbody').find("[data-id='"+order_id+"']").each(function (i,e) {
				idx = arr_item.indexOf($(e).data('item'));
				arr_item.splice(idx,1);
            });
            $('#tbl-detItemForPick tbody').find("[data-id='"+order_id+"']").remove()
		}
    })

	function validateFormPickList(el) {
	    if($('#tbl-detItemForPick tbody tr').length == 0){
            sweetAlert("Failed...", "No item ready to pick!", "info");
	        return false;
        }
        if(parseInt($('#tray_needed').val())==0 || isNaN(parseInt($('#tray_needed').val()))){
            sweetAlert("Failed...", "Please key tray needed!", "info");
            return false;
        }
        var t = $(el).attr('target'),
        	data = $(t).serializeArray(),
        	url = $(t).attr('action')
        $.ajax({
            type	: "POST",
            url		: url,
            data 	: data,
            dataType: "json",
            success : function (resp) {
                $("#picking_form").modal('hide')
                sweetAlert("Saved...", "Successfully created picking list!", "success");
                table.draw();
                tableDetail.clear()
                tableDetail.draw();
                return false;
            }
        })
    }

    function delPickList(el) {
        id = $(el).data('id')
        swal({
            title: "Are you sure?",
            text: "You will not be able to recover this data!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes, delete it!",
            closeOnConfirm: false,
            html: false
        }, function () {
            url = "{{ route('pickingList.destroy', ':id') }}";
            url = url.replace(':id', id);
            data = [{'name': '_token', 'value': '{{ csrf_token() }}'},
                {'name': '_method', 'value': 'DELETE'}
            ];
            $.ajax({
                type: "POST",
                url: url,
                data: data,
                dataType: "json",
                success: function (resp) {
                    swal(resp.text, resp.msg, resp.status);
                    table.draw();
                    tableDetail.clear().draw();
                }
            })
        });
    }

    function assignTray(e) {
	    $('#pickItemId').val($(e).data('id'))
        $('#select-tray option').prop('selected',false)
        $('#select-tray').select2({
            placeholder: "Select Tray",
        })
        $('#modalAssignTray').modal('show')
    }

    function doneAssign(el) {
        if($('#select-tray').val()==''){
            sweetAlert("Failed...", "Please select tray!", "info");
            return false
        }
        var t = $(el).attr('target'),
            data = $(t).serializeArray(),
            url = $(t).attr('action')+'/'+$('#pickItemId').val()
        $.ajax({
            type	: "POST",
            url		: url,
            data 	: data,
            dataType: "json",
            success : function (resp) {
                $("#modalAssignTray").modal('hide')
                sweetAlert({
                    title : "Saved...",
                    text  : "Successfully assign tray!",
                    type  : "success",
                    timer: 2000,
                    showConfirmButton: false
                });
                table.draw();
                loadDetail(id_selected)
                return false;
            }
        })
    }

    function showInfo() {
        sweetAlert({
            title   : "Warning",
            text    : "Please assign tray to all item first!",
            type    : "info",
            timer   : 3000,
            showConfirmButton   : false
        });
    }
</script>
@endpush