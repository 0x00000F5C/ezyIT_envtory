<div id="modalScannTray" class="modal fade" role="dialog">
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">
            <div id="QR-Code" class="container" style="width:100%">
                <div class="panel panel-primary">
                    <div class="panel-heading" style="display: inline-block;width: 100%;">
                        <h4 style="width:50%;float:left;"><span id="title_modal">Scan Tray</span></h4>
                        <input type="hidden" id="target_tray" value="">
                        <input type="hidden" id="target_index" value="">
                        <div style="width:50%;float:right;margin-top: 5px;margin-bottom: 5px;text-align: right;">
                            <select id="cameraId" class="form-control hidden" style="display: inline-block;width: auto;"></select>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="col-md-12" style="text-align: center;">
                            <div class="well" style="position: relative;display: inline-block;">
                                <canvas id="qr-canvas" width="320" height="240"></canvas>
                                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <input type="hidden" id="target-row" value="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
<script src="{{ asset('assets/plugins/webcodecam/qrcodelib.js') }}"></script>
<script src="{{ asset('assets/plugins/webcodecam/webcodecamjquery.js') }}"></script>
<script>
    var txt = "innerText" in HTMLElement.prototype ? "innerText" : "textContent";
    var args = {
        resultFunction: function(result) {
            /*
             result.format: code format,
             result.code: decoded string,
             result.imgData: decoded image data
             */
//            console.log(result)
            var code = result.code, target_tray = $('#target_tray').val(), target_idx = $('#target_index').val();

            if($('.data-stockin').length==0){
                $('.sel-tray').each(function (i,e) {
                    if($(e).val()==''){
                        $(e).val(code).change()
                        $('#modalScannTray').modal('hide')
                        return false
                    }
                })
            }else{
                if(target_tray == ''){
                    checkScann(code)
                }else{
                    if(code.toUpperCase() == target_tray.toUpperCase()){
                        confirmTray(target_idx)
                    }
                }
            }

        }
    };

    var decoder = $("#qr-canvas").WebCodeCamJQuery(args).data().plugin_WebCodeCamJQuery;
    decoder.buildSelectMenu('#cameraId');
    decoder.init();

    $("#modalScannTray").on('shown.bs.modal', function () {
        decoder.play();
    });

    $("#modalScannTray").on('hidden.bs.modal', function () {
        decoder.stop();
    });


</script>
@endpush
