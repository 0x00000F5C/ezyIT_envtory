@extends('layouts.lte.main')

@section('content')
    <div class="row">
        <div class="col-md-12">
            @include('layouts.lte.status')
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="box box-default">
                                <div class="box-header with-border">
                                    <br/>
                                    <div class="row">
                                        <div class="col-xs-6">
                                            <div class="form-group">
                                                <label class="col-sm-3 control-label no-padding-right" for="form-field-1">Shipping Bin </label>

                                                <div class="col-sm-7">
                                                    {!! Form::select('shipping_bin_id', $shippingBins,[], array('id' =>'shipping_bin_id', 'class' => 'form-control chosen-select', 'onchange' => 'changeShippingBin();')) !!}
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-4">
                                            <div class="form-group">
                                                <div class="col-sm-4">
                                                    {!! Form::text('barcode', null, array('id' => 'barcoder_tracking_number','class' => 'form-control', 'placeholder'=>'Barcode', 'autofocus'=>'autofocus')) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    {{--<h3 class="box-title">Products Information</h3>--}}
                                </div>
                                <div class="box-body">
                                    <div class="col-xs-12">
                                        <table id="datatable" class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>id</th>
                                                <th>Tracking Number</th>
                                                <th>Status</th>
                                                <th>status_id</th>
                                                <th>order_id</th>
                                                <th>courier_name</th>
                                            </tr>
                                            </thead>
                                            <tbody>

                                            </tbody>

                                        </table>
                                    </div>
                                    <div class="col-sm-8">
                                        <button type="button" onclick="saveCheckout();" class="btn btn-md btn-success btn-flat"><i class="ace-icon fa fa-check bigger-110"></i>Save</button>
                                    </div>

                                </div>
                            </div>
                        </div>


                    </div>
                </div>
            </div>

        </div>
    </div>

    <div id="shipDialog" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-md">
            <div class="modal-content">
                {{--<div class="modal-header">--}}
                    {{--<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>--}}
                    {{--<h4 class="modal-title" id="myModalLabel">Packing</h4>--}}
                    {{--<p>Move your device so that the QR code is in the viewer and hold still</p>--}}
                {{--</div>--}}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-sm-1">

                        </div>
                        <div class="col-sm-10">
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Tracking Number </label>

                                <div class="col-sm-7">
                                    {!! Form::text('dialog_tracking_number', null, array('id' => 'dialog_tracking_number','class' => 'form-control', 'readonly'=>'readonly')) !!}
                                    {!! Form::hidden('dialog_tracking_id', null, array('id' => 'dialog_tracking_id','class' => 'form-control', 'readonly'=>'readonly')) !!}
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Courier </label>

                                <div class="col-sm-7">
                                    {!! Form::text('dialog_courier_lbl', null, array('id' => 'dialog_courier_lbl','class' => 'form-control', 'readonly'=>'readonly')) !!}
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Date </label>

                                <div class="col-sm-7">
                                    {!! Form::text('dialog_date', null, array('id' => 'dialog_date','class' => 'form-control', 'readonly'=>'readonly')) !!}
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Time </label>

                                <div class="col-sm-7">
                                    {!! Form::text('dialog_time', null, array('id' => 'dialog_time','class' => 'form-control', 'readonly'=>'readonly')) !!}
                                </div>
                            </div>
                            <br/>
                            <br/>
                            <div class="form-group">
                                <label class="col-sm-4 control-label no-padding-right" for="form-field-1">Courier Name </label>

                                <div class="col-sm-7">
                                    {!! Form::text('dialog_courier_name', null, array('id' => 'dialog_courier_name','class' => 'form-control')) !!}
                                </div>
                            </div>

                        </div>
                        <div class="col-sm-1">

                        </div>

                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-flat btn-sm btn-danger" data-dismiss="modal">Close</button>
                    {{--<button id="save_detail" onclick="save(0);" type="button" class="btn  btn-sm btn-danger">Tolak Konfirmasi</button>--}}
                    <button id="save_detail" onclick="saveShip();" type="button"  class="btn btn-flat  btn-sm btn-success"> <i class="fa fa-fw fa-disk"></i>Save</button>
                </div>
            </div>
        </div>
    </div>

@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
    var td = $('#datatable').DataTable({
        processing: true,
        oLanguage: {
            sEmptyTable: 'Empty Data'
        },
        columns: [
            {data: 'id', name: 'id', width: '50%', visible: false},
            {data: 'tracking_number', name: 'tracking_number', width: '50%'},
            {data: 'status_lbl', name: 'status_lbl', width: '10%'},
            {data: 'status', name: 'status', width: '50%', visible: false},
            {data: 'order_id', name: 'order_id', width: '10%', visible: false},
            {data: 'courier_name', name: 'courier_name', width: '10%', visible: false},
        ],
        bPaginate: false,
        bFilter: false,
        bSort: false,
        bInfo: false,
        bLengthChange: false
    });


    function changeShippingBin(){
        td.clear().draw();
        var bin_id = $('#shipping_bin_id').val();
        $('#msg_status_container').html('');

        jQuery.ajax({
            type: "POST",
            url: ' {!! url('app/shippingList') !!}' + '/detail',
            dataType: 'json',
            data:{
                bin_id: bin_id,
                _token: "{{ csrf_token() }}"
            },
            success: function (res) {
                if (res.status) {
                    var det=res.data
                    for (i = 0; i < det.length; i++) {
                        td.row.add(det[i]).draw();
                    }
                    $('#barcoder_tracking_number').focus();
                }else{
                    push_msg('error', res.msg);
                }

            }
        });
    }

    $(document).ready(function () {
        changeShippingBin();
    });

    $('#barcoder_tracking_number').keypress(function (e) {
        if (e.which == 13) {
            e.preventDefault();
            $('#msg_status_container').html('');
            var barcoder_tracking_number = $(this).val();
            var bin_id = $('#shipping_bin_id').val();
            $('#barcoder_tracking_number').val('');
            $.ajax({
                type: 'post',
                url: '{!! url('app/shippingList/checkBarcode') !!}',
                data: {
                    bin_id:bin_id,
                    barcoder_tracking_number:barcoder_tracking_number,
                    _token: "{{ csrf_token() }}"
                },
                dataType: 'json',
                success: function (resp) {
                    if(resp.status){
                        $('#dialog_tracking_number').val(resp.data.tracking_number);
                        $('#dialog_tracking_id').val(resp.data.id);
                        $('#dialog_courier_lbl').val(resp.data.courier_lbl);
                        $('#dialog_date').val(resp.data.Date);
                        $('#dialog_time').val(resp.data.Time);
                        $('#shipDialog').modal('show');
                        $('#dialog_courier_name').focus();

                    }else{
                        push_msg('error', resp.msg);
                    }
                }
            });
        }
    });

    function saveShip(){
        var dets = td.rows().data();
        for (var i = 0; i < dets.length; i++) {
            if($('#dialog_tracking_id').val()==dets[i].id){
                dets[i].status = 1;
                dets[i].status_lbl ='Shipped';
                dets[i].courier_name =$('#dialog_courier_name').val();
                td.row(i).data(dets[i]);

            }
        }
        $('#shipDialog').modal('hide');

    }

    function saveCheckout() {
        var bin_id = $('#shipping_bin_id').val();
        var dets = td.rows().data();
        var det = {};
        for (var i = 0; i < dets.length; i++) {
            var dt = {};
            dt.id = dets[i].id;
            dt.status = dets[i].status;
            dt.courier_name =dets[i].courier_name;
            dt.order_id=dets[i].order_id;
            det[i] = dt;
        }
        $.ajax({
            type: 'post',
            url: '{!! url('app/shippingList/saveCheckout') !!}',
            data: {
                bin_id:bin_id,
                det:det,
                _token: "{{ csrf_token() }}"
            },
            dataType: 'json',
            success: function (resp) {
                if(resp.status){
                    window.location ='{!! url('app/shippingList') !!}';
                }else{
                    push_msg('error', resp.msg);
                }
            }
        });
    }
</script>
@endpush