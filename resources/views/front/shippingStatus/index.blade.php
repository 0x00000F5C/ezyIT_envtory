@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group hidden">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<div class="pull-right hidden">
					@can('create-user')
					<a id="btn-showShippingStatus" class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New type</a>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="shippingList-table" cellspacing>
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Order date</th>
							<th>Customer</th>
							<th>Tracking number</th>
							<th>Total items</th>
							<th style="width: 180px"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		var table = $('#shippingList-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('shippingStatus.index') !!}',
			columns: [
				{ data: 'shipping_order_id', name: 'shipping_order_id' },
				{ data: 'shipping_order_date', name: 'shipping_order_date' },
				{ data: 'shipping_customer', name: 'shipping_customer' },
				{ data: 'shipping_tracking', name: 'shipping_tracking' },
				{ data: 'shipping_items', name: 'shipping_items' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id);
			}
		});

		/* var showShippingStatus, nz;

		showShippingStatus = $("#dialog-showShippingStatus").dialog({
			autoOpen: false,
			height: 400,
			width: 640,
			modal: true,
			show: { effect: "fade", duration: 333 },
			hide: { effect: "fade", duration: 333 },
			buttons: {
				print: function() {
					window.location.href = window.location.hostname + "/app/shippingStatus/" +  + "/print";
				},
				Cancel: function() {
					showShippingStatus.dialog("close");
				}, 
				Submit: function() {
					if(showShippingStatus.find("input[name=_method]").val() == "POST"){
						var adr = "{!! route('shippingList.store') !!}";
						var dta = { "shippingList-name": showShippingStatus.find("input#shippingList-name").val(), "shippingList-min": showShippingStatus.find("input#shippingList-min").val(), "shippingList-max": showShippingStatus.find("input#shippingList-max").val() };
					} else if(showShippingStatus.find("input[name=_method]").val() == "PUT") {
						var adr = "{!! url('shippingList') !!}/" + showShippingStatus.find("input[name=id]").val();
						var dta =	{ "_method": "PUT", "id": showShippingStatus.find("input[name=id]").val(), "shippingList-name": showShippingStatus.find("input#shippingList-name").val(), "shippingList-min": showShippingStatus.find("input#shippingList-min").val(), "shippingList-max": showShippingStatus.find("input#shippingList-max").val() };
					}
					$.ajax({
						type: "post",
						url: adr,
						dataType: "json",
						data: dta,
						success: function(data) {
							if(data.st) {
								swal("Nice!", "Data saved.", "success");
								setTimeout(function() {
									location.reload();
								}, 1000);
							} else {
								swal("Oops...", "Failed to save data.", "error");
							}
						}
					});

					showShippingStatus.dialog("close")
					swal("Nice!", "Data saved.", "success");
				}
			},
			close: function() {
				nz[ 0 ].reset();
				//allFields.removeClass("ui-state-error");
			}
		});

		nz = showShippingStatus.find("form").on("submit", function(event) {
			event.preventDefault();
		}); */

		$("#shippingList-table").on("click", ".btn-showShipping", function(evt) {
			evt.preventDefault();
			var dat = table.row($(this).parents("tr")).data(),
			showShippingStatus = $("#dialog-showShippingStatus");
			showShippingStatus.modal("show");
			showShippingStatus.find("td#modal-orderId").text(dat.shipping_order_id);
			showShippingStatus.find("td#modal-tracking").text(dat.shipping_tracking);
			showShippingStatus.find("td#modal-courier").text(dat.shipping_courier);
			showShippingStatus.find("span#modal-weight").text(dat.shipping_items);
			/* showShippingStatus.dialog({title: "Shipment status"}).dialog("open"); */
		});
	});
</script>

<div id="dialog-showShippingStatus" class="modal fade">
	<div class="modal-dialog" role="document">
		<div class="modal-content" style="width:640px;">
			<div class="modal-header">
				<h5 class="modal-title" id="trayform-title" style="display: inline; font-weight: bold; font-size: 14pt;">Add tray</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form id="form-showShippingStatus">
					<fieldset>
						<table style="border-collapse: separate; border-spacing: 8px; width: 100%;">
							<tr>
								<th style="width: 128px;"></th>
								<th style="width: 6px;"></th>
								<th></th>
								<th style="width: 128px;"></th>
								<th style="width: 6px;"></th>
								<th></th>
							</tr>
							<tr>
								<th>Order ID</th>
								<th>:</th>
								<td id="modal-orderId"></td>
								<th>Tracking number</th>
								<th>:</th>
								<td id="modal-tracking"></td>
							</tr>
							<tr>
								<th>Courier</th>
								<th>:</th>
								<td id="modal-courier"></td>
								<th>Status</th>
								<th>:</th>
								<td id="modal-status">Delivered</td>
							</tr>
							<tr>
								<th>Shipment weight</th>
								<th>:</th>
								<td><span id="modal-weight"></span> kg</td>
								<th></th>
								<th></th>
								<td></td>
							</tr>
						</table>
						<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
					</fieldset>
				</form>
			</div>
			<div class="modal-footer">
				<a href=""><button type="submit" class="btn btn-default pull-left">Print</button></a>
				<button type="submit" class="btn btn-primary">Save changes</button>
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
@endpush