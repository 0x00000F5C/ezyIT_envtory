@extends('layouts.lte.main')
@section('content')
<div class="row">
	<div class="col-xs-12">
		@include('layouts.lte.status')
		<div class="box">
			<div class="box-header">
				<div class="btn-group hidden">
					<button type="button" class="btn btn-default btn-flat dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
						<i class="fa fa-external-link"></i> Export <span class="caret"></span>
						<span class="sr-only">Toggle Dropdown</span>
					</button>
					<ul class="dropdown-menu" role="menu">
						<li><a href="{{ URL::current().'/export/xls'}}" target="_blank"><i class="fa fa-file-excel-o"></i> Excel</a></li>
						<li><a href="{{ URL::current().'/export/pdf'}}" target="_blank"><i class="fa fa-file-pdf-o"></i> PDF</a></li>
						<li><a href="{{ URL::current().'/export/html'}}" target="_blank"><i class="fa fa-file-o"></i> HTML</a></li>
					</ul>
				</div>
				<div class="pull-right hidden">
					@can('create-user')
					<a id="btn-newSKUStock" class="btn btn-primary btn-flat" href="{{URL::current().'/create'}}"><i class="fa fa-plus"></i> New stock</a>
					@endcan
				</div>
				<div class="clear" style="clear: both;"></div>
			</div>
			<!-- /.box-header -->
			<div class="box-body">
				<table class="table table-bordered table-hover dataTable dt-responsive nowrap" id="skuStock-table" cellspacing>
					<thead>
						<tr>
							<th>Product SKU</th>
							<th>Product code</th>
							<th>Product name</th>
							<th style="width: 80px">Current qty</th>
							<th style="width: 128px;"></th>
						</tr>
					</thead>
					<tbody></tbody>
				</table>
			</div>
		</div>
	</div>
</div>
@stop
@push('styles')
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.css') }}">
<link rel="stylesheet" href="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/css/dataTables.responsive.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.theme.min.css') }}">
<link rel="stylesheet" href="{{ asset('assets/plugins/jquery-ui/jquery-ui.structure.min.css') }}">
@endpush
@push('scripts')
<script src="{{ asset('assets/lte/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script src="{{ asset('assets/lte/plugins/datatables/extensions/Responsive/js/dataTables.responsive.min.js') }}"></script>
<script src="{{ asset('assets/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
<script>
	$.ajaxSetup({
		headers: { "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr('content') }
	});

	$(function() {
		var table = $('#skuStock-table').DataTable({
			processing: true,
			serverSide: true,
			ajax: '{!! route('skuStockManagement.index') !!}',
			columns: [
				{ data: 'sku', name: 'sku' },
				{ data: 'code', name: 'code' },
				{ data: 'name', name: 'name' },
				{ data: 'current_qty', name: 'current_qty' },
				{ data: 'action', name: 'action', orderable: false, searchable: false }
			],
			createdRow: function(row, data, dataIndex) {
				$(row).attr("id", "row-" + data.id);
			}
		});

		var newSKUStock, nz;

		newSKUStock = $("#dialog-newSKUStock").dialog({
			autoOpen: false,
			height: 400,
			width: 480,
			modal: true,
			show: { effect: "fade", duration: 333 },
			hide: { effect: "fade", duration: 333 },
			buttons: {
				Cancel: function() {
					newSKUStock.dialog("close");
				}, 
				Submit: function() {
					/* if(newSKUStock.find("input[name=_method]").val() == "POST"){
						var adr = "{!! route('orderType.store') !!}";
						var dta = { "orderType-name": newSKUStock.find("input#orderType-name").val(), "orderType-min": newSKUStock.find("input#orderType-min").val(), "orderType-max": newSKUStock.find("input#orderType-max").val() };
					} else if(newSKUStock.find("input[name=_method]").val() == "PUT") {
						var adr = "{!! url('orderType') !!}/" + newSKUStock.find("input[name=id]").val();
						var dta =	{ "_method": "PUT", "id": newSKUStock.find("input[name=id]").val(), "orderType-name": newSKUStock.find("input#orderType-name").val(), "orderType-min": newSKUStock.find("input#orderType-min").val(), "orderType-max": newSKUStock.find("input#orderType-max").val() };
					}
					$.ajax({
						type: "post",
						url: adr,
						dataType: "json",
						data: dta,
						success: function(data) {
							if(data.st) {
								swal("Nice!", "Data saved.", "success");
								setTimeout(function() {
									location.reload();
								}, 1000);
							} else {
								swal("Oops...", "Failed to save data.", "error");
							}
						}
					}); */

					
					$('#skuStock-table a.btn-editSKUStock[data-value="' + newSKUStock.find("input[name=id]").val() + '"]').parents("tr").children("td:nth-child(1)").text(newSKUStock.find("input#orderType-name").val()).parent().children("td:nth-child(2)").text(newSKUStock.find("input#orderType-min").val()).parent().children("td:nth-child(3)").text(newSKUStock.find("input#orderType-max").val());
					
					newSKUStock.dialog("close")
					swal("Nice!", "Data saved.", "success");
					if(newSKUStock.find("input[name=_method]").val() == "POST") {
						location.reload();
					}
				}
			},
			close: function() {
				nz[ 0 ].reset();
				//allFields.removeClass("ui-state-error");
			}
		});

		nz = newSKUStock.find("form").on("submit", function(event) {
			event.preventDefault();
		});

		$("#skuStock-table").on("click", ".btn-setupSkuStock", function(evt) {
			evt.preventDefault();
			var dat = table.row($(this).parents("tr")).data();
			newSKUStock.find("input[name=_method]").val("PUT");
			newSKUStock.find("input[name=id]").val($(this).data("value"));
			newSKUStock.find("#product-sku").val(dat.sku);
			newSKUStock.find("#product-uom").val(dat.uom);
			newSKUStock.find("#product-current-qty").val(dat.current_qty);
			newSKUStock.find("#product-cold-qty").val(dat.cold_qty);
			newSKUStock.find("#product-save-qty").val(dat.save_qty);
			newSKUStock.find("#product-shelf-life").val(dat.save_qty);
			newSKUStock.find("#product-min-qty").val(dat.min_qty);
			newSKUStock.find("#product-max-qty").val(dat.max_qty);
			newSKUStock.dialog({title: "Update order type"}).dialog("open");
		});
	});
</script>

<div id="dialog-newSKUStock">
	<form id="form-newSKUStock">
		<input type="hidden" name="_method" value="">
		<input type="hidden" name="id" value="">
		<fieldset>
			<table style="border-collapse: separate; border-spacing: 8px;">
				<tr>
					<td style="width: 128px;"><label for="product-sku">Product SKU</label></td>
					<td colspan="3"><input type="text" id="product-sku" class="text ui-widget-content ui-corner-all form-control" name="product-sku" placeholder="Product SKU" value="" style="margin-bottom: 8px;" maxlength="64" required="required" readonly="readonly"></td>
				</tr>
				<tr>
					<td style="width: 128px;"><label for="product-uom">UOM</label></td>
					<td colspan="3"><input type="text" id="product-uom" class="text ui-widget-content ui-corner-all form-control" name="product-uom" placeholder="Product UOM" value="" style="margin-bottom: 8px;" maxlength="64" required="required"></td>
				</tr>
				<tr>
					<td style="width: 128px;"><label for="product-current-qty">Current qty</label></td>
					<td><input type="number" id="product-current-qty" class="text ui-widget-content ui-corner-all form-control" name="product-current-qty" placeholder="Current qty" value="" style="margin-bottom: 8px;" readonly="readonly"></td>
					<td style="width: 128px;"><label for="product-cold-qty">Cold qty</label></td>
					<td><input type="number" id="product-cold-qty" class="text ui-widget-content ui-corner-all form-control" name="product-cold-qty" placeholder="Current qty" value="" style="margin-bottom: 8px;" readonly="readonly"></td>
				</tr>
				<tr>
					<td style="width: 128px;"><label for="product-save-qty">Save qty</label></td>
					<td><input type="number" id="product-save-qty" class="text ui-widget-content ui-corner-all form-control" name="product-save-qty" placeholder="Save qty" value="" style="margin-bottom: 8px;"></td>
					<td style="width: 128px;"><label for="product-uom">Max shelved life</label></td>
					<td><input type="number" id="product-shelf-life" class="text ui-widget-content ui-corner-all form-control" name="product-shelf-life" placeholder="Shelf life" value="" style="margin-bottom: 8px;"></td>
				</tr>
				<tr>
					<td style="width: 128px;"><label for="product-min-qty">Minimum qty</label></td>
					<td><input type="number" id="product-min-qty" class="text ui-widget-content ui-corner-all form-control" name="product-min-qty" placeholder="Minimum qty" value="" style="margin-bottom: 8px;"></td>
					<td style="width: 128px;"><label for="product-max-qty">Maximum qty</label></td>
					<td><input type="number" id="product-max-qty" class="text ui-widget-content ui-corner-all form-control" name="product-max-qty" placeholder="Maximum qty" value="" style="margin-bottom: 8px;"></td>
				</tr>
			</table>
			<input type="submit" tabindex="-1" style="position:absolute; top:-1000px">
		</fieldset>
	</form>
</div>
@endpush