<footer class="main-footer">
    <div class="pull-right hidden-xs">
        <b>Version</b> 1
    </div>
    <strong>Copyright &copy; {{ date('Y') }} <a href="http://almsaeedstudio.com">{{ config('app.name') }}</a>.</strong> All rights
    reserved.
</footer>