<header class="main-header">
    <a href="{{ url('app') }}" class="logo">
        <?php $appName = config('app.name', 'Laravel'); ?>
        <span class="logo-mini"><img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" width="40px" /></span>
        <span class="logo-lg"><img src="{{ File::exists("img/cProfiles/1.jpg") ? Asset("img/cProfiles/1.jpg") : Asset("img/logo-here.png") }}" height="50px" /></span>
    </a>
    <nav class="navbar navbar-static-top">
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </a>
{{--         <form class="navbar-form navbar-left hidden-xs hidden-sm" role="search">
            <div class="form-group">
                <div class="input-group">
                    <div class="input-group-addon navbar-search">
                        <i class="fa fa-search"></i>
                    </div>
                  <input type="text" class="form-control input-lg" id="navbar-search-input" placeholder="Search">
                </div>
            </div>
        </form> --}}
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @foreach (config('topmenu') as $menu)
                    <?php $hasAccess = false;  ?>
                    @foreach ($menu['access'] as $access)
                        <?php $hasAccess .= Gate::check($access);?>
                    @endforeach
                    @if ($hasAccess)
                    @php
                        $class = '';
                        if(!empty($menu['child'])){
                            $class = 'dropdown';
                            foreach ($menu['child'] as $key => $value) {
                                $route = !empty($value['route']) ? route($value['route']) : '';
                                $action = substr(strrchr(Request::url(), "/"), 1);
                                if (in_array($action, ['create','edit'])) {
                                    $route .= '/'.$action;
                                }
                                if ($route == Request::url()) {
                                    $class .= ' active';
                                }
                            }
                        }
                    @endphp
                    <li class="{{$class}}">
                        <a href="{{ !empty($menu['route']) ? route($menu['route']) : '#' }}" {{ !empty($menu['child']) ? 'class= data-toggle=dropdown aria-expanded=false' : ''}}>
                            <i class="{{ $menu['icon'] }}"></i>
                            <span class="hidden-xs hidden-sm">{{ $menu['name'] }}</span>
                            @if (!empty($menu['child']))
                                <span class="caret hidden-xs hidden-sm"></span>
                            @endif
                        </a>
                        @if (!empty($menu['child']))
                            <ul class="dropdown-menu" role="menu">
                                @foreach ($menu['child'] as $child)
                                    @if ($child['name'] == 'divider')
                                        <li class="divider"></li>
                                    @else
                                        <?php $hasChildAccess = false;  ?>
                                        @foreach ($child['access'] as $childAccess)
                                            <?php $hasChildAccess = Gate::check($childAccess);?>
                                        @endforeach
                                        @if ($hasChildAccess)
                                            @php
                                                $childClass = '';
                                                $route = !empty($child['route']) ? route($child['route']) : '';
                                                $action = substr(strrchr(Request::url(), "/"), 1);
                                                if (in_array($action, ['create','update'])) {
                                                    $route .= '/'.$action;
                                                }
                                                if(Request::url() == $route){
                                                    $childClass = 'active';
                                                }
                                            @endphp
                                            @if($child['route']!='picking.index')
                                                <li class="{{$childClass}}"><a href="{{ !empty($child['route']) ? route($child['route']) : '#' }}"><i class="{{$child['icon']}}"></i> {{$child['name']}}</a></li>
                                            @endif
                                        @endif
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                    @endif
                @endforeach
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle user" data-toggle="dropdown">
                        <i class="fa fa-user"></i>
                        <span class="hidden-xs">{{ Auth::user()->firstname }}</span>
                    </a>
                    <ul class="dropdown-menu sub-menu" role="menu">
                        <li>
                            <a href="{{ url('app/profiles/' . Auth::user()->id . '/edit') }}"><i class="fa fa-user"></i> Profile</a>
                        </li>
                        <li>
                              <a href="{{ url('/logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"> <i class="fa fa-sign-out"></i> Logout </a>
                              <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                                  {{ csrf_field() }}
                              </form>
                        </li>
                    </ul>
                </li>
                <li class="hidden-xs hidden-sm">
                    <img src="{{ Asset("img/logo.png") }}" height="50px" style="padding-top: 20px;padding-right: 10px; padding-left: 10px; cursor: pointer;" data-toggle="control-sidebar"/>
                </li>
            </ul>
        </div>
    </nav>
</header>

<style>
    .dropdown-menu .sub-menu {
        left: 100%;
        position: absolute;
        top: 0;
        visibility: hidden;
        margin-top: -1px;
    }

    .dropdown-menu li:hover .sub-menu {
        visibility: visible;
    }

    .dropdown:hover .dropdown-menu {
        display: block;
    }

</style>