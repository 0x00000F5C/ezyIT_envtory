<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{ isset($pageTitle) ? $pageTitle.' |' : ''}} {{ config('app.name', 'Laravel') }}</title>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <link rel="stylesheet" href="{!! asset('assets/lte/bootstrap/css/bootstrap.css') !!}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <link rel="icon" href="/img/favicon32.png" sizes="32x32">
    <link rel="icon" href="/img/favicon48.png" sizes="48x48">

    <link rel="stylesheet" href="{!! asset('assets/lte/plugins/select2/select2.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/lte/plugins/jQueryUI/jquery-ui.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/AdminLTE.min.css') !!}">
    {{-- <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/skins/skin-purple-light.min.css') !!}"> --}}
    <link rel="stylesheet" href="{!! asset('assets/lte/dist/css/skins/skin-black-light.min.css') !!}">
    <link rel="stylesheet" href="{!! asset('assets/plugins/sweetalert/sweetalert.css') !!}">
    <link rel="stylesheet" href="{!! elixir('css/dashboard.css') !!}">
    @stack('styles')

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="hold-transition skin-black-light sidebar-mini fixed sidebar-collapse">
<div class="wrapper">

    @include('layouts.lte._header')
    <aside class="main-sidebar">
        @include('layouts.lte._sidebar')
    </aside>

    <div class="content-wrapper">
        <section class="content-header">
            <h1>
                {{ $pageTitle or ''}}
            </h1>
        </section>

        <section class="content">
        @yield('content')

        </section>
    </div>
    @include('layouts.lte._footer')
    @include('layouts.lte._rightbar')

</div>
<script src="{!! asset('assets/lte/plugins/jQuery/jquery-2.2.3.min.js') !!}"></script>
<script src="{!! asset('assets/lte/plugins/jQueryUI/jquery-ui.js') !!}"></script>
<script src="{!! asset('assets/lte/bootstrap/js/bootstrap.min.js') !!}"></script>
<script src="{!! asset('assets/lte/plugins/slimScroll/jquery.slimscroll.min.js') !!}"></script>
<script src="{!! asset('assets/lte/plugins/select2/select2.js') !!}"></script>
<script src="{!! asset('assets/lte/plugins/fastclick/fastclick.js') !!}"></script>
<script src="{!! asset('assets/lte/dist/js/app.min.js') !!}"></script>
<script src="{{ elixir('js/dashboard.js') }}"></script>
<script src="{!! asset('assets/plugins/sweetalert/sweetalert.min.js') !!}"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.2.0/vue.js"></script>
@stack('scripts')
<script>
    function push_msg(status, msg){
        var html = '<div class="alert alert-'+status+' alert-dismissible">'+
            '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>'+
            msg+
        '</div>';
        $('#msg_status_container').html(html);
    }
</script>
</body>
</html>
