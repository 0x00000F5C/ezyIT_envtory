@if (session('status'))
<div class="alert alert-{{ session('status') }} alert-dismissible">
  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
  {!! session('message') !!}
</div>
@endif
<div id="msg_status_container">

</div>