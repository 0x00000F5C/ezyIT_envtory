<html>
	<head>
		<title>{{$title or ''}}</title>
		<style>
			html * {
				font-family: 'Source Sans Pro',sans-serif;
			}
			.header {
				border-bottom: 2px solid black;
				padding-bottom: -10px;
				margin-bottom: 40px;
			}
			.text-header {
				padding-bottom: -15px;
			}
			h3 {
				margin-bottom: 10px
			}
			table {
				width: 100%;
			    border-collapse: collapse;
			    font-size: 10pt;
			}
			table th {
				background-color: #f4f4f4;
			}
			table, th, td {
			    border: 1px solid #dfdfdf;
			    padding: 5px;
			}
			tfoot .name {
				background-color: #f4f4f4;
			}
			.no-border td{
				border: 0 solid #fff;
			}
		</style>
	</head>
	<body>
	@include('layouts.report._header')
	@yield('content')
	</body>
</html>