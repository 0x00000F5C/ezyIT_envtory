<html>
	<head>
		<title>{{$title or ''}}</title>
		@include('layouts.report.style')
	</head>
	<body>
	{!! @$template->header !!}
	<div class="report">
		<h3>{{ $title }}</h3>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<p><b>Staff: </b>{{ $staff->firstname . " " . $staff->lastname }}</p>
		<table>
			<thead><tr><th>No.</th><th>Date</th><th>Active batch ID</th><th>Zone ID</th><th>Rack ID</th><th>Cell ID</th><th>SKU</th><th>Description</th><th>Qty</th></tr></thead>
			<tbody>
				@foreach($stockIn as $i => $o)
				<tr>
					<td>{{ $i + 1 }}</td>
					<td>{{ $o->received_date }}</td>
					<td>{{ $o->batch_id }}</td>
					<td>{{ $o->zone_name }}</td>
					<td>{{ $o->rack_name }}</td>
					<td>{{ $o->cell_name }}</td>
					<td>{{ $o->product_id }}</td>
					<td>{!! $o->product_sku_description !!}</td>
					<td>{{ $o->product_qty }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! @$template->footer !!}
	<script type="text/javascript">window.print();</script>
	</body>
</html>