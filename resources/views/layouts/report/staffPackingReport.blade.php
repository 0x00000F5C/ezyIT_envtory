<html>
	<head>
		<title>{{$title or ''}}</title>
		@include('layouts.report.style')
	</head>
	<body>
	{!! @$template->header !!}
	<div class="report">
		<h3>{{ $title }}</h3>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<h2 class="text-center">{{ $title }}</h2>
		<p class="text-center">
			<div id="staffPicking-chart">{!! !empty($chart) ? '<img src="' . $chart . '" width="100%">' : "" !!}</div>
			@linechart("staffPickingChart", "staffPicking-chart")
		</p>
		<h2>By warehouse</h2>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<table>
			<thead><tr><th>Date</th><th>Staff name</th><th>Average time per day</th></tr></thead>
			<tbody>
				@foreach($pickingReport as $i => $o)
				<tr>
					<td>{{ date_format(date_create_from_format("Y-m-d", $o->date), "d F Y") }}</td>
					<td>{{ $o->staff_name }}</td>
					<td>{{ $o->average_time . " (" . ($o->average_time_in_sec + 0) . " seconds)" }}</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! @$template->footer !!}
	<script type="text/javascript">window.print();</script>
	</body>
</html>