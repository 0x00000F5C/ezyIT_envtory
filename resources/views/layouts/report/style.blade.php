		<style>
			html * {
				font-family: 'Source Sans Pro',sans-serif;
				font-size: {{ $fontSize }}pt;
			}
			.header {
				border-bottom: 2px solid black;
				padding-bottom: -0.75em;
				margin-bottom: 2.5em;
			}
			.report {
				margin: 1.75em;
			}
			.text-header {
				padding-bottom: -0.9375em;
			}
			h1, h2, h3, h4, h5 {
				font-size: initial;
				margin-bottom: 0.75em;
			}
			table {
				width: 100%;
			    border-collapse: collapse;
			    font-size: 0.75em;
			}
			table th {
				background-color: #f4f4f4;
			}
			table, th, td {
			    border: 1px solid #dfdfdf;
			    padding: 5px;
			}
			tfoot .name {
				background-color: #f4f4f4;
			}
			.no-border td{
				border: 0 solid #fff;
			}
			.text-center {
				text-align: center;
			}
			.text-right {
				text-align: right;
			}
			td {
				max-height: 320px;
				vertical-align: top;
			}
			img {
				max-height: 100%;
			}
		</style>