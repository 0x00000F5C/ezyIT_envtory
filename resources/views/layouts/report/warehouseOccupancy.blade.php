<html>
	<head>
		<title>{{$title or ''}}</title>
		@include('layouts.report.style')
	</head>
	<body>
	{!! @$template->header !!}
	<div class="report">
		<h3>{{ $title }}</h3>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<h2 class="text-center">{{ $title }}</h2>
		<p class="text-center">
			<div id="warehouseOccupancy-chart">{!! !empty($chart) ? '<img src="' . $chart . '" width="100%">' : "" !!}</div>
			@linechart("warehouseOccupancyChart", "warehouseOccupancy-chart")
		</p>
		<h2>By warehouse</h2>
		<p><b>Date: </b>{{ date("d M Y", strtotime($filter->dateFloor)) }} to {{ date("d M Y", strtotime($filter->dateCeil)) }}</p>
		<table>
			<thead><tr><th>Date</th><th>Warehouse</th><th>Total cells</th><th>Empty cells</th><th>Occupancy percentage</th></tr></thead>
			<tbody>
				@foreach($warehouse as $i => $o)
				<tr>
					<td>{{ date_format(date_create_from_format("Y-m-d", $o->date), "d F Y") }}</td>
					<td>{{ $o->prefix . " - " . $o->name }}</td>
					<td>{{ $o->total_cells }}</td>
					<td>{{ $o->empty_cells }}</td>
					<td>{{ $o->percentage }} %</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
	{!! @$template->footer !!}
	<script type="text/javascript">window.print();</script>
	</body>
</html>