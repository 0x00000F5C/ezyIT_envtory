<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Supplierform</title>
		<link rel="{{ asset("css/app.css") }}" type="text/css">
		<script type="text/javascript" src="{{ asset("js/jquery-3.1.1.min.js") }}"></script>
    </head>
    <body>
		<form id="store" action="{{ URL::to('/supplier/save') }}" method="post">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<input type="text" name="id" placeholder="Supplier id" value="3">
			<input type="text" name="input_name" placeholder="Supplier name" value="Not-something corp">
			<input type="text" name="input_address" placeholder="Supplier address" value="Someplace">
			<input type="text" name="input_mode" placeholder="Supplier mode" value="Consigment">
			<input type="text" name="input_contactname" placeholder="Supplier contact name" value="This is name">
			<input type="text" name="input_phonenumber" placeholder="Supplier phone number" value="12345678">
			<input type="text" name="input_emailaddress" placeholder="Supplier email address" value="mail@mail.com">
			<input type="submit" value="Submit">
		</form>
    </body>
</html>
